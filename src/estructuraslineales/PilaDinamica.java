package estructurasLineales;

/**
 * Este clase representa un TDA de una pila con memoria dinamica.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class PilaDinamica implements LoteAlmacenamiento {

    private ListaDinamica pila;

    public PilaDinamica(){
        pila = new ListaDinamica();
    }

    @Override
    public boolean vacio() {
        return pila.vacia();
    }

    @Override
    public boolean lleno() {
        return false;
    }

    @Override
    public boolean poner(Object valor) {
        int valorRetorno = pila.agregar(valor);
        if(valorRetorno == 0){ //Si se pudo agregar
            return true;
        } else { // no se pudo agregar
            return false;
        }
    }

    @Override
    public Object quitar() {
        return pila.eliminar();
    }

    @Override
    public void imprimir() {
        pila.imprimir();
    }

    @Override
    public Object verUltimo() {
        return pila.verUltimo();
    }
}