package registros.escuelas;

import estructurasLineales.ListaEstatica;

/**
 * Clase que representa un alumno de una escuela.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class Alumno {

    protected String matricula;
    protected String nombre;
    protected String apellido;
    protected int edad;
    protected ListaEstatica calificaciones;

    public Alumno(String matricula, String nombre, String apellido, int edad, ListaEstatica calificaciones) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.calificaciones = calificaciones;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public ListaEstatica getCalificaciones() {
        return calificaciones;
    }

    public void setCalificaciones(ListaEstatica calificaciones) {
        this.calificaciones = calificaciones;
    }

    /**
     * Metodo que calcula el promedio de las calificaciones del un alumno.
     * @return promedio de las calificaciones del alumno.
     */
    public Double obtenerPromedio() {
        double sumaCalificaciones = 0.0;
        double promedioCalificaciones = 0.0;

        if (calificaciones.vacia() == false) { // si hay calificaciones
            for (int posicion = 0; posicion < calificaciones.numeroElementos(); posicion++) {
                   sumaCalificaciones = sumaCalificaciones + (double) calificaciones.obtener(posicion);
            }
            promedioCalificaciones = sumaCalificaciones / calificaciones.numeroElementos();
            return promedioCalificaciones;

        } else { // no hay calificaciones
            return null;
        }

    }

    @Override
    public String toString() {
        return matricula;
    }
}