package registros.encriptarmensajespilas;

import entradaSalida.ArchivoTexto;
import estructurasLineales.ListaEstatica;
import estructurasLineales.PilaEstatica;

/**
 * Clase encargada de encriptar y desencriptar un archivo de texto.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class EncriptarArchivo {

    private static PilaEstatica pila;
    private static ListaEstatica archivoDeTexto;
    private static ListaEstatica archivoEncriptado;

    /**
     * Este método encripta el contenido de un archivo de texto con agrupaciones con el caracter Æ.
     * @param archivoOriginal Es la ruta del archivo que se desea encriptar.
     * @param rutaArchivoNuevo Es la ruta en donde se guardará el archivo encriptado.
     * @return Regresa una lista estática con el contenido del archivo encriptado.
     */
    public static ListaEstatica encriptar(String archivoOriginal, String rutaArchivoNuevo) {
        // ASCII del caracter Æ.
        char caracterAE = 198;
        // se inicializa la pila con el dooble de tamaño del archivo original.
        pila = new PilaEstatica(archivoOriginal.length() * 10);
        // generamos un nuemero aleatorio para el ancho de la agrupación.
        int ancho = (int) (Math.random() * 10);
        // variable auxiliar
        int aux = 1;
        // variable que alamacenara el contenido encriptado del archivo.
        String mensaje = "";
        String cadenaTemp = "";
        // se inicializa la lista estática que almacenara el contenido del archivo.
        archivoDeTexto = ArchivoTexto.leer(archivoOriginal);
        // se recorre la lista estática que contiene el contenido del archivo original.
        for (int pos = 0; pos <= archivoDeTexto.numeroElementos() ; pos++){
            // se obtienen los elementos de la lista estática y los concatenamos en la cadena temporal.
            cadenaTemp += archivoDeTexto.obtener(pos);
        }
        // se recorre la cadena temporal.
        for (int pos = cadenaTemp.length() - 1; pos > 0 ; pos--){
            // se obtiene el caracter de la cadena temporal.
            char caracter = cadenaTemp.charAt(pos);
            // se genera un numero aleatorio para saber la posicion de inicio de la agrupación.
            int inicio = (int) (Math.random() * 4);
            // si inicio es igual a 3 y aux es igual a 1, se agrega el caracter Æ a la pila.
            if (inicio == 3 && aux == 1){
                // se agrega el caracter Æ a la pila.
                pila.poner(caracterAE);
                // se concatena el caracter Æ a la variable mensaje.
                mensaje += caracterAE;
                // se inzializa la variable auxiliar en 0.
                aux = 0;
            }
            // si la variable auxiliar es igual a 0, se disminuye en 1 el ancho de la agrupación.
             if (aux == 0){
                ancho = ancho - 1;
            }
             // si el ancho de la agrupación es igual a -1, se coloca el caracter Æ en la pila.
            if (ancho == -1){
                pila.poner(caracterAE);
                // se concatena el caracter Æ a la variable mensaje.
                mensaje += caracterAE;
                // se vuelve a inicializar el ancho de la agrupación en un numero aleatorio.
                ancho = (int) (Math.random() * 5);
                // se inicializa la variable auxiliar en 1.
                aux = 1;
            }
            // se agrega el caracter a la pila.
            pila.poner(caracter);
            // se concatena el caracter a la variable mensaje.
            mensaje += caracter;
        }
        // colocar el mensaje encriptado en la pila
        pila.poner(mensaje);
        // colocar el mensaje encriptado en la lista estática archivoEncriptado
        archivoEncriptado = new ListaEstatica(pila.numeroElementos());
        archivoEncriptado.agregar(pila.quitar());
        // escribir el archivo encriptado en la nueva ruta
        ArchivoTexto.escribir(archivoEncriptado, rutaArchivoNuevo + "archivoEncriptado.txt");
        // regresar la lista estática archivoEncriptado
        return archivoEncriptado;
    }

    /**
     * Este método desencripta el contenido del archivo encriptado con agrupaciones de el caracter Æ.
     * @param archivoADecifrar Ruta del archivo que se desea desencriptar.
     * @return Regresa una lista con el contenido del archivo desencriptado.
     */
    public static ListaEstatica desencriptar(String archivoADecifrar) {
       return null;
    }

    /**
     * Este método imprime el contenido de un archivo que se encuentra en una lista estática.
     * @param archivo Lista estática que contiene el contenido del archivo a imprimir.
     */
    public static void imprimirArchivo(ListaEstatica archivo) {
        archivo.imprimirOI();
    }
}