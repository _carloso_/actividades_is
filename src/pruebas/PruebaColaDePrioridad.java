package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaEstaticaDePrioridad;
import herramientas.comunes.TipoPrioridad;

public class PruebaColaDePrioridad {
    public static void main(String[] args) {
        ColaEstaticaDePrioridad cola = new ColaEstaticaDePrioridad(6, TipoPrioridad.MAYOR_A_MENOR);
        cola.insertar("T", 3);
        cola.insertar("B", 10);
        cola.insertar("C", 2);
        cola.insertar("A", 1);
        cola.insertar("D", 4);

        SalidaPorDefecto.consola("--- Cola de prioridad, ordenada de mayor a menor ----" + "\n");
        cola.imprimir();
        // salida esperada: B, D, T, C, A

        SalidaPorDefecto.consola("->Sacando a el elemento con prioridad mayor: " + cola.quitar() + "\n");

        ColaEstaticaDePrioridad cola2 = new ColaEstaticaDePrioridad(6, TipoPrioridad.MENOR_A_MAYOR);
        cola2.insertar("M", 2);
        cola2.insertar("S", 1);
        cola2.insertar("N", 1);
        cola2.insertar("K", 3);
        cola2.insertar("L", 4);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Cola de prioridad, ordenada de menor a mayor ----" + "\n");
        cola2.imprimir();
        // salida esperada: S, N, M, K, L

        SalidaPorDefecto.consola("->Sacando a el elemento con prioridad menor: " + cola2.quitar() + "\n");
    }
}