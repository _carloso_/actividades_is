package registros.escuelas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;

public class Alumno2 {

    protected String matricula;
    protected String nombre;
    protected String apellido;
    protected int edad;
    protected ListaDinamica calificaciones;

    public Alumno2(String matricula, String nombre, String apellido, int edad, ListaDinamica calificaciones) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.calificaciones = calificaciones;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public ListaDinamica getCalificaciones() {
        return calificaciones;
    }

    public void setCalificaciones(ListaDinamica calificaciones) {
        this.calificaciones = calificaciones;
    }

    public void imprimirCalificaciones(){
        int contadorParcial = 0;
       calificaciones.iniciarIterador();
       SalidaPorDefecto.consola("Calificaciones:" + "\n");
       while(calificaciones.hayNodos() == true) {
           double calificacionParcial = (Double) calificaciones.obtenerNodo();
           ++contadorParcial;
           SalidaPorDefecto.consola("(" + contadorParcial + ")" + calificacionParcial + "\n");
       }
    }

    /**
     * Metodo que calcula el promedio de las calificaciones del un alumno.
     * @return promedio de las calificaciones del alumno.
     */
    public Double obtenerPromedio() {
        double sumaCalificaciones = 0.0;
        double promedioCalificaciones = 0.0;
        int numeroCalificaciones = 0;

        if (calificaciones.vacia() == false) { // si hay calificaciones
            calificaciones.iniciarIterador();
            while (calificaciones.hayNodos() == true) { // mientras iterador esté en una posición donde hay datos
                sumaCalificaciones += (Double) calificaciones.obtenerNodo(); // extrae el contenido de iterador y se mueve al siguiente nodo
                numeroCalificaciones++;
            }
            promedioCalificaciones = sumaCalificaciones / numeroCalificaciones;
            return promedioCalificaciones;
        } else { // no hay calificaciones
            return null;
        }

    }

    @Override
    public String toString() {
        return matricula;
    }
}
