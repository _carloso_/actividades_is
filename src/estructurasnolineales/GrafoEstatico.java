package estructurasnolineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaEstatica;
import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaEstatica;
import estructurasLineales.PilaEstatica;
import estructurasnolineales.auxiliares.Vertice;
import herramientas.generales.EtiquetaGrafo;
import herramientas.generales.TipoOrdenGrafo;

/**
 * Esta clase representa un TDA de grafo estatico.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class GrafoEstatico {

    //Dónde guardar los vértices
    protected ListaEstatica vertices;
    //Dónde guardar las aristas
    protected Matriz2 aristas;
    //Definimos el orden para trabjar con grafos ponderados
    protected TipoOrdenGrafo orden;
    // Donde se guardarán los vértices visitados o marcados en el recorrido en profundidad
    private ListaEstatica marcadosRecorridoProfundidad;

    public GrafoEstatico(int cantidadVertices){
        vertices=new ListaEstatica(cantidadVertices);
        aristas=new Matriz2(cantidadVertices,cantidadVertices,0.0);
    }

    public GrafoEstatico(int cantidadVertices, TipoOrdenGrafo orden){
        vertices=new ListaEstatica(cantidadVertices);
        this.orden=orden;
        //asignar por defecto los valores infinitos
        if(this.orden==TipoOrdenGrafo.DEC){ //decremental
            aristas=new Matriz2(cantidadVertices,cantidadVertices,Double.MAX_VALUE); //+inifnito
        }else{  //incremental
            aristas=new Matriz2(cantidadVertices,cantidadVertices,Double.MIN_VALUE); //-infinito
        }
        aristas.matrizDiagonal(0.0);//diagonal con ceros
    }

    /**
     * Este método agrega un vértice al grafo.
     * @param contenido Es el contenido del vértice.
     * @return Regresa <b>true</b> si se agregó el vértice correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregarVertice(Object contenido){
        //Al momento de agregar vértices es oportuno checar que no se registren contenidos duplicados.
        //Buscar la existencia del vértice
        Integer indiceVertice=(Integer)vertices.buscar(contenido);

        if(indiceVertice==null){ //no existe
            //Crear un vértice
            Vertice nuevoVertice=new Vertice();

            //Le colocamos su atributos
            nuevoVertice.setContenido(contenido);
            nuevoVertice.setNumVertice(vertices.numeroElementos()); // el número de vértice lo obtenemos de la cantidad de vértices

            int retornoOperacion=vertices.agregar(nuevoVertice);
            if(retornoOperacion>=0){ //si se pudo agregar
                return true;
            }else{ //no se pudo agregar
                return false;
            }
        }else{ //si existe
            return false;
        }
    }

    /**
     * Este método agrega una arista al grafo.
     * @param origen Es el vértice origen de la arista.
     * @param destino Es el vértice destino de la arista.
     * @return Regresa <b>true</b> si se agregó la arista correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregarArista(Object origen, Object destino){ // V1 -> V2
        return agregarArista(origen,destino,1.0);
    }

    /**
     * Este método agrega una arista al grafo.
     * @param origen Es el vértice origen de la arista.
     * @param destino Es el vértice destino de la arista.
     * @param peso Es el peso de la arista.
     * @return Regresa <b>true</b> si se agregó la arista correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregarArista(Object origen, Object destino, double peso){ // V1 -> V2
        //Verificar que existan
        Integer indiceOrigen=(Integer)vertices.buscar(origen);
        Integer indiceDestino=(Integer)vertices.buscar(destino);

        if(indiceOrigen!=null && indiceDestino!=null){ //si existen ambos
            //creamos la arista
            return aristas.cambiar(indiceOrigen,indiceDestino,peso);
        }else{ //uno u otro no existe
            return false;
        }
    }

    /**
     * Este método imprime el grafo.
     */
    public void imprimir(){
        SalidaPorDefecto.consola("Vértices:\n");
        vertices.imprimir();

        SalidaPorDefecto.consola("Aristas:\n");
        aristas.imprimirXReglones();
    }

    //método axuliar de paso 1 de OT
    private int gradoDeEntradaXVertice(int cadaDestino){
        int gradodeEntradaVertice=0;
        //recorrer todos los renglones (origenes) hacia el vertice destino
        for(int cadaOrigen=0; cadaOrigen< aristas.obtenerRenglones(); cadaOrigen++){
            //usando la matriz obtenemos esa flecha
            Double flecha=(Double)aristas.obtenerValor(cadaOrigen,cadaDestino);
            if(flecha!=null && flecha>0){ //hay una flecha del orgien a ese destino
                gradodeEntradaVertice++;
            }
        }
        return gradodeEntradaVertice;
    }

    //paso 1 de OT
    private void inicializarGradosEntrada(ListaEstatica gradosEntrada){
        //recorrer todos los posibles vèrtices o procesos (destinos en la matriz), para calcular en cada uno de ellos
        //la cantidad de flechas o aristas que les llega (grados de entrada).
        for(int cadaDestino=0; cadaDestino< aristas.obtenerColumnas(); cadaDestino++){
            //para cada uno de estos destinos posibles, calculemos los grados de entrada o flechas que le llegan a èl.
            //es decir los renglones (origenes) que llegan a este destino
            int gradosEntradaXVerticeDestino=gradoDeEntradaXVertice(cadaDestino);
            gradosEntrada.agregar(gradosEntradaXVerticeDestino); // el grado de entrada se guarda en  la misma posición
            //que el vertice destino
        }
    }

    //paso 2 y 5 de OT
    private void encolarYMarcarVerticesGrado0(ListaEstatica gradosEntrada, ListaEstatica marcados, ColaEstatica colaProcesamiento){
        //recorrer todos los vèrtices para determinar lo que se requiere
        for(int cadaVertice=0;cadaVertice<gradosEntrada.numeroElementos();cadaVertice++){
            //si no esta marcado y tiene grado de E 0, encolamos y marcamos
            if((int)gradosEntrada.obtener(cadaVertice)==0 && (boolean)marcados.obtener(cadaVertice)==false){
                colaProcesamiento.poner(cadaVertice); //encolamos
                marcados.cambiar(cadaVertice,true); //marcamos
            }
        }
    }

    //paso 4 de OT
    private void recalcularGradosEntradaVertices(ListaEstatica gradosEntrada, ListaEstatica marcados, int indiceVerticeActual){
        for(int cadaDestino=0; cadaDestino< aristas.obtenerColumnas(); cadaDestino++){
            //recorremos los destinos posibles provinientes del verticeActual y si tiene flecha, le afectaba
            //y que no estuviera marcado.

            //saber si desde origen a ese destino hay flecha
            Double flecha=(Double)aristas.obtenerValor(indiceVerticeActual,cadaDestino);
            if(flecha!=null && flecha>0 && (boolean)marcados.obtener(cadaDestino)==false){ //hay flecha hacia ese destino
                //actualizamos la incidencia o grado de entrada, es decir, se resta en 1
                int gradoEntradaVerticeDestino=(int)gradosEntrada.obtener(cadaDestino);
                gradosEntrada.cambiar(cadaDestino,gradoEntradaVerticeDestino - 1);
            }
        }
    }

    //mètodo principal de OT
    public ListaDinamica ordenacionTopologica(){
        ListaDinamica ordenProcesos=new ListaDinamica(); // es el resultado de la ordenación topológica
        ColaEstatica colaProcesamiento=new ColaEstatica(vertices.numeroElementos());
        ListaEstatica gradosEntrada=new ListaEstatica(vertices.numeroElementos());
        ListaEstatica marcados=new ListaEstatica(vertices.numeroElementos());

        //0.-En otro módulo o función deberá llevarse a cabo una verificación de no existencia de ciclos.

        //1.- Inicializar las incidencias (grados de entrada de los vértices).
        inicializarGradosEntrada(gradosEntrada);

        //2.- Los procesos (vértices) con grados de entrada en 0 (no marcados)
        // se colocan en una cola de procesamiento y se marcan como ya analizados.

        //inicializar los mrcados como false
        marcados.rellenar(false,vertices.numeroElementos());
        //invocar al mètodo que determina el paso 2 como tal
        encolarYMarcarVerticesGrado0(gradosEntrada, marcados, colaProcesamiento);

        while(colaProcesamiento.vacio()==false) { //mientras no esté vacía
            //3.- Sacar un proceso (vértice) de la cola de procesamiento y
            // lo ejecutamos (mientras haya datos en la cola).
            int indiceVerticeActual=(int)colaProcesamiento.quitar();
            Vertice verticeActual=(Vertice)vertices.obtener(indiceVerticeActual);
            ordenProcesos.agregar(verticeActual.getContenido());

            //4.- Recalcular grados de entrada dado el paso 3.
            recalcularGradosEntradaVertices(gradosEntrada, marcados, indiceVerticeActual);

            //5.- Los procesos (vértices) con grado de entrada 0 (no marcados) se colocan
            // en la cola de procesamiento y se marcan como ya analizados.
            encolarYMarcarVerticesGrado0(gradosEntrada, marcados, colaProcesamiento);
        }
        return ordenProcesos;
    }

    // paso 3 de recorrido en profundidad
    private void enpilarYMarcarVerticesAdyacentes(int indiceVerticeActual, PilaEstatica pila, ListaEstatica marcados){
        for(int cadaDestino=0;cadaDestino<aristas.obtenerColumnas(); cadaDestino++){
            //recorremos a todos los destinos posibles a partir del vértice actual (origen)
            Double flecha=(Double)aristas.obtenerValor(indiceVerticeActual,cadaDestino);
            //hay flecha si hay adyacencia y no están marcados
            if(flecha!=null && flecha>0 && (boolean)marcados.obtener(cadaDestino)==false){
                //enpilamos
                pila.poner(cadaDestino);
                //marcamos
                marcados.cambiar(cadaDestino,true);
            }
        }
    }

    //Recorrido en profundidad
    public ListaDinamica recorridoProfunidad(Object origen){
        ListaDinamica recorridoP=new ListaDinamica();
        PilaEstatica pila=new PilaEstatica(vertices.numeroElementos());
        //ListaEstatica marcados=new ListaEstatica(vertices.numeroElementos());
        marcadosRecorridoProfundidad=new ListaEstatica(vertices.numeroElementos());

        //Pasos:

        //0. Validar la existencia del origen.
        Integer indiceOrigen=(Integer)vertices.buscar(origen);
        if(indiceOrigen!=null){ //existe
            //1.- Partir de un vértice origen. Este vértice se marca y se mete en una pila.
            //Llenar el arreglo de marcados con falsos.
            marcadosRecorridoProfundidad.rellenar(false,vertices.numeroElementos());
            //marcamos este vértice origen
            marcadosRecorridoProfundidad.cambiar(indiceOrigen,true);
            //meter el origen en la pila
            pila.poner(indiceOrigen);

            while(pila.vacio()==false) {
                //2.- Mientras existan vértices en la pila, se van a extraer (de uno por uno) y se procesarán (imprimir).
                int indiceVerticeActual=(int)pila.quitar(); //sacamos de pila
                Vertice verticeActual=(Vertice)vertices.obtener(indiceVerticeActual); //obtenemos el objeto vértice
                recorridoP.agregar(verticeActual.getContenido());//agregamos en la salida el contenido del vértice

                //3.- Los vértices adyacentes (vecinos directos) no marcados y que están enlazados al nodo que actualmente
                // se procesa (el paso 2) se marcan y se meten en la pila.
                enpilarYMarcarVerticesAdyacentes(indiceVerticeActual, pila, marcadosRecorridoProfundidad);
            }
        }else{ //no existe
            return null;
        }
        return recorridoP;
    }

    ///////////////////////////////////////////////////DIJKSTRA

    // Paso 1
    private void inicializarEtiquetasGrafo(ListaEstatica etiquetasOptimas, int indiceVerticeOrigen, double metricaIndiceOrigen,
                                           double metricaVertices, int verticeAnterior){
        for(int cadaVertice=0; cadaVertice<vertices.numeroElementos(); cadaVertice++){
            EtiquetaGrafo etiqueta=new EtiquetaGrafo();
            etiqueta.setMetricaAcumulada(metricaVertices); // en este caso en nuestro ejemplo era infinito....
            etiqueta.setVerticeAnterior(verticeAnterior); // por ejemplo en nuestro caso - (-1)
            etiqueta.setInteracion(0);
            etiquetasOptimas.agregar(etiqueta);//agregarla a nuestro arreglo de de etiquetas
        }
        //en particular falta cambiar el valor e la métrica en el vértice origen -> 0
        EtiquetaGrafo etiquetaVerticeOrigen=(EtiquetaGrafo) etiquetasOptimas.obtener(indiceVerticeOrigen);
        etiquetaVerticeOrigen.setMetricaAcumulada(metricaIndiceOrigen); // por ejemplo con 0.0
    }

    // Paso 2
    private void actualizarMetricaAcumuladaEtiquetas(int verticeActual, ListaEstatica etiquetasOptimas, ListaEstatica permanentes, int iteracion,
                                                     double infinito){
        //recorrer todos los vértices
        for(int cadaPosibleVecino=0; cadaPosibleVecino<aristas.obtenerColumnas();cadaPosibleVecino++){
            //checar cuáles son vecinos no marcados
            Double flechaMetricaOrigenActualDestino=(Double)aristas.obtenerValor(verticeActual, cadaPosibleVecino);
            if(flechaMetricaOrigenActualDestino!=null && flechaMetricaOrigenActualDestino!=0 && flechaMetricaOrigenActualDestino!=infinito &&
                    (boolean)permanentes.obtener(cadaPosibleVecino)==false){
                //calcularemos las métricas acumuladas desde el vértice actual a este cada vecino adyacente y si resulta mejor la métrica
                //se actualizará en la etiqueta

                //sacar la métrica acumulada del vértice actual
                EtiquetaGrafo etiquetaVerticeActual=(EtiquetaGrafo) etiquetasOptimas.obtener(verticeActual);
                double metricaAcumuladaVerticeActual=etiquetaVerticeActual.getMetricaAcumulada();
                //sumar la métrica acumulada del vértice actual +  la métrica del vértice actual hacia el vecino
                double metricaAcumuladaVerticeActualDestino= metricaAcumuladaVerticeActual + flechaMetricaOrigenActualDestino;

                //sacar la métrica acumulada del vecino
                EtiquetaGrafo etiquetaVerticeDestino=(EtiquetaGrafo) etiquetasOptimas.obtener(cadaPosibleVecino);
                double metricaVerticeDestino=etiquetaVerticeDestino.getMetricaAcumulada();

                //comparar si es mejor la métrica acumulada hacia un vecino desde el origen actual
                //no olvidad si es DEC y INC
                boolean banderaActualizarEtiqueta=false;
                if(orden==TipoOrdenGrafo.DEC){ //más chico es mejor
                    if(metricaAcumuladaVerticeActualDestino<metricaVerticeDestino){ //si es mejor, actualizar
                        banderaActualizarEtiqueta=true;
                    }
                }else{ //INC, más grande es mejor
                    if(metricaAcumuladaVerticeActualDestino>metricaVerticeDestino){ //si es mejor, actualizar
                        banderaActualizarEtiqueta=true;
                    }
                } //if
                if(banderaActualizarEtiqueta==true){ //cambiar los valores de la etiqueta
                    etiquetaVerticeDestino.setInteracion(iteracion);
                    etiquetaVerticeDestino.setMetricaAcumulada(metricaAcumuladaVerticeActualDestino);
                    etiquetaVerticeDestino.setVerticeAnterior(verticeActual);
                }
            }//if vecino
        } //for
    }

    // Paso 3
    private int obtenerVerticeMetricaOptima(ListaEstatica etiquetasOptimas, ListaEstatica permanentes, double infinito){
        double mejorMetrica = infinito; // Es la metrica mejor incial
        int mejorVertice = -1; // Es el mejor vértice
        for (int cadaVertice = 0; cadaVertice < vertices.numeroElementos(); cadaVertice++) {
            // checar solo los no marcados como permanentes
            if ((boolean)permanentes.obtener(cadaVertice) == false) {
                // obtener la etiqueta de cada vértice
                EtiquetaGrafo etiquetaCadaVertice = (EtiquetaGrafo) etiquetasOptimas.obtener(cadaVertice);
                // obtener la métrica de cada vértice
                double metricaCadaVertice = etiquetaCadaVertice.getMetricaAcumulada();
                // comparamos esta métrica con la mejor métrica y si es mejor, se sustituye
                // considerar el orden inc o dec
                if (orden == TipoOrdenGrafo.DEC) { // más pequeño es mejor
                    if (metricaCadaVertice < mejorMetrica) { // si es mejor, cambiamos
                        mejorMetrica = metricaCadaVertice; // cambiamos la mejor métrica
                        mejorVertice = cadaVertice; // cambiamos el mejor vértice
                    }
                } else { // INC, más grande es mejor
                    if (metricaCadaVertice > mejorMetrica) { // si es mejor, cambiamos
                        mejorMetrica = metricaCadaVertice; // cambiamos la mejor métrica
                        mejorVertice = cadaVertice; // cambiamos el mejor vértice
                    }
                } // if
            } // if
        } // for
        return mejorVertice;
    }

    // Etiquetas de Dijkstra
    public ListaEstatica etiquetasOptimasDijkstra (Object origen) {
        ListaEstatica etiquetasOptimas=new ListaEstatica(vertices.numeroElementos()); //arreglo paralelo para las etiquetas
        ListaEstatica permanentes=new ListaEstatica(vertices.numeroElementos()); //arreglo paralelo para los marcados como permanentes
        //definimos el infinito
        double infinito=0;
        if(orden==TipoOrdenGrafo.DEC){ //decremental, más chico es mejor,  +infinito
            infinito=Double.MAX_VALUE;
        }else{ //incremental, más grande es mejor, -infinito
            infinito=Double.MIN_VALUE;
        }
        //Pasos:
        // 0.- Validar que el oirgen exista
        Integer indiceVerticeOrigen=(Integer)vertices.buscar(origen);
        if(indiceVerticeOrigen!=null) { //si existe el origen

            // 1.- Inicializar etiquetas partiendo de un nodo origen, marcándolo como permanente.
            inicializarEtiquetasGrafo(etiquetasOptimas, indiceVerticeOrigen, 0.0, infinito, -1);
            //marcar el vértice origen como permanente
            permanentes.rellenar(false, vertices.numeroElementos());//rellenado con falsos
            permanentes.cambiar(indiceVerticeOrigen,true); //marcamos el origen

            //indicar que el vértice origen es el vértice Actual, para la primera itreración
            int verticeActual=indiceVerticeOrigen;
            for(int iteracion=1; iteracion< vertices.numeroElementos(); iteracion++) { // el último vértice no se necesita procesar en el paso 2 y 3
                //2.- Calcular los nuevos valores de las etiquetas de los vértices a partir de las métricas acumuladas hacia
                //    los vecinos (adyacentes) no marcados como permanentes; todo esto partiendo del vértice acrtual. si se mejora
                //    la métrica, se actualiza la etiqueta de ese vértice.
                actualizarMetricaAcumuladaEtiquetas(verticeActual, etiquetasOptimas, permanentes,iteracion, infinito);

                //3.- Elegir el vértice con la mejor métrica acumulada (óptima), tomando en cuenta vértices no marcados como permanentes.
                //    entonces ese vértice elegido se marca y se convierte en el vértice actual.
                verticeActual = obtenerVerticeMetricaOptima(etiquetasOptimas, permanentes, infinito);
                permanentes.cambiar(verticeActual, true); //marcar el vertice actual como permanente
            }
            return etiquetasOptimas;
        }else{ //no existe el origen
            return null;
        }
    }

    // Métria Origen -> Destino
    public Double obtenerMetricaOptimaDijkstra(Object origen, Object destino) {
        // buscar que exista los vértices origen y destino
        Integer indiceDestino = (Integer) vertices.buscar(destino);
        ListaEstatica etiquetasOptimas = etiquetasOptimasDijkstra(origen);
        if (indiceDestino != null && etiquetasOptimas != null) { // si existe el destino y el origen
            // obtenemos la metrica de la etiqueta del destino
            EtiquetaGrafo etiquetaDestino = (EtiquetaGrafo) etiquetasOptimas.obtener(indiceDestino);
            return etiquetaDestino.getMetricaAcumulada();
        } else { // no existe el destino o el origen
            return null;
        }
    }

    // Ruta de Origen -> Destino
    public ListaDinamica obtenerRutaOptimaDijkstra(Object origen, Object destino) {
        ListaDinamica rutaOrigenDestino = new ListaDinamica();
        // buscar que exista los vértices origen y destino
        Integer indiceDestino = (Integer) vertices.buscar(destino);
        ListaEstatica etiquetasOptimas = etiquetasOptimasDijkstra(origen);
        if (indiceDestino != null && etiquetasOptimas != null) { // si existe el destino y el origen
            // hacer el backtrace
            // primero declaramos una variable para el vértice actual
            int indiceVerticeActual = indiceDestino;
            // ciclo que recorre hasta que encontremos un vertice anterior = indeterminado: -1
           do {
               // agregamos el vértice actual a la ruta
               Vertice verticeActual = (Vertice) vertices.obtener(indiceVerticeActual);
               rutaOrigenDestino.agregarInicio(verticeActual.getContenido());
               // nos vamos hacia atrás en la ruta, usando como referencia el vertice anterior de la etiqueta del vértice actual
               // ese vertice anterior se combierte en el nuevo vertice actual en la siguiente iteración
               EtiquetaGrafo etiquetaVerticeActual = (EtiquetaGrafo) etiquetasOptimas.obtener(indiceVerticeActual);
               indiceVerticeActual = etiquetaVerticeActual.getVerticeAnterior();
           } while (indiceVerticeActual != -1); // hasta no llegar a un -1 en el campo anterior
            return rutaOrigenDestino;
        } else { // no existe el destino o el origen
            return null;
        }
    }

    /**
     * Este método elimina un vértice del grafo.
     * @param vertice Es el vértice a eliminar.
     * @return Regresa el vertice eliminado.
     */
    public Object eliminarVertice(Object vertice) {
        // 1.- Buscar el vértice en el grafo (lista de vértices)
        Integer indiceVertice = (Integer) vertices.buscar(vertice);
        if (indiceVertice != null) {
            // 2.- Eliminar el vértice del grafo (lista de vértices).
            Object verticeEliminado = vertices.eliminar(vertice);
            // 3.- Eliminar las aristas que tienen el vértice como origen o destino
            aristas.eliminarRenglon(indiceVertice);
            // 4.- Eliminar las aristas que tienen ese vértice como origen o destino
            aristas.eliminarColumna(indiceVertice );
            return verticeEliminado;
        } else {
            return null;
        }
    }

    /**
     * Este método comprueba si un vértice origen es adyacente a un vértice destino.
     * @param origen Es el vértice origen.
     * @param destino Es el vértice destino.
     * @return Regresa <b>true</b> si el vértice origen es adyacente al vértice destino, <b>false</b> en caso contrario.
     */
    public boolean esAdyacente(Object origen, Object destino) {
        // 1.- Buscar el vértice en el grafo (origen y destino)
        Integer indiceVerticeOrigen = (Integer) vertices.buscar(origen);
        Integer indiceVerticeDestino = (Integer) vertices.buscar(destino);
        if (indiceVerticeOrigen != null && indiceVerticeDestino != null) {
            // 2.- Comprobar si hay una arista entre el vértice origen y el vértice destino
            Double arista = (Double) aristas.obtenerValor(indiceVerticeOrigen, indiceVerticeDestino);
            if (arista != 0.0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método elimina una arista del grafo.
     * @param origen Es el vértice origen de la arista.
     * @param destino Es el vértice destino de la arista.
     * @return Regresa <b>true</b> si la arista fue eliminada, <b>false</b> en caso contrario.
     */
    public boolean eliminarArista(Object origen, Object destino) {
        // 1.- Buscar el vértice en el grafo (origen y destino)
        Integer indiceVerticeOrigen = (Integer) vertices.buscar(origen);
        Integer indiceVerticeDestino = (Integer) vertices.buscar(destino);
        if (indiceVerticeOrigen != null && indiceVerticeDestino != null) {
            // 2.- Comprobar si hay una arista entre los vertices origen y destino
            Double arista = (Double) aristas.obtenerValor(indiceVerticeOrigen, indiceVerticeDestino);
            if (arista != 0.0) {
                // 3.- Eliminar la arista entre los vertices origen y destino
                aristas.cambiar(indiceVerticeOrigen, indiceVerticeDestino, 0.0);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método busca un vértice en el grafo y regresa su información.
     * @param vertice Es el vértice a buscar.
     * @return Regresa una <b>lista Estatica</b> con la información del vértice.
     */
    public ListaEstatica buscarVertice(Object vertice) {
        ListaEstatica infoVertice = new ListaEstatica(vertices.numeroElementos() + 1);
        // 1.- Buscar el vértice en el grafo (lista de vértices)
        Integer indiceVertice = (Integer) vertices.buscar(vertice);
        // Si existe debe regresar la información en cuestión, en caso contrario null.
        if (indiceVertice != null) {
            // 2.- Buscar la información del vértice
            for (int cadaDestino = 0; cadaDestino < aristas.obtenerRenglones(); cadaDestino++) {
                Double arista = (Double) aristas.obtenerValor(indiceVertice, cadaDestino);
                if (arista != 0.0) {
                    String infoArista = "Arista: (" + aristas.obtenerValor(indiceVertice, cadaDestino) +
                            ") De " + vertices.obtener(indiceVertice) + " a " + vertices.obtener(cadaDestino);
                    infoVertice.agregar(infoArista);
                }
            }
            infoVertice.agregar("Vertice: " + vertices.obtener(indiceVertice));
            return infoVertice;
        } else {
            return null;
        }
    }

    /**
     * Este método valida si el grafo es pseudoGrafo.
     * @return Regresa <b>true</b> si el grafo es pseudoGrafo, <b>false</b> en caso contrario.
     */
    public boolean esPseudografo() {
        boolean esPseudografo = false;
        // Regresa verdadero si el grafo que tiene lazos o bucles, es un pseudografo, falso en caso contrario.
        int columna = 0;
        for (int fila = 0; fila < aristas.obtenerRenglones(); fila++) {
            Double arista = (Double) aristas.obtenerValor(fila, columna);
            if (arista != 0.0) {
                esPseudografo = true;
            }
            columna++;
        }
        return esPseudografo;
    }

    /**
     * Este método valida si el grafo es multígrafo.
     * @return Regresa <b>true</b> si el grafo es multígrafo, <b>false</b> en caso contrario.
     */
    public boolean esMultigrafo() {
        boolean esMultigrafo = false;
        // Regresa verdadero si el grafo que tiene más de una arista entre dos vértices,
        // es un multígrafo, falso en caso contrario.
        for (int fila = 0; fila < aristas.obtenerRenglones(); fila++) {
            int contador = 0;
            for (int columna = 0; columna < aristas.obtenerColumnas(); columna++) {
                Double arista = (Double) aristas.obtenerValor(fila, columna);
                if (arista != 0.0) {
                    contador++;
                }
            }
            if (contador > 1) {
                esMultigrafo = true;
            }
        }
        return esMultigrafo;
    }

    /**
     * Este método calcula el grado de un vértice (numero de aristas que sale de él).
     * @param vertice Es el vértice del que se quiere calcular el grado.
     * @return Regresa el grado del vértice.
     */
    public int gradoVertice(Object vertice) {
        //  Regresa el número de aristas que contiene el vértice. Si es 0, es nodo aislado.
        int grado = 0; // Inicializar el grado
        Integer indiceVertice = (Integer) vertices.buscar(vertice);
        if (indiceVertice != null) {
            for (int cadaDestino = 0; cadaDestino < aristas.obtenerColumnas(); cadaDestino++) {
                Double arista = (Double) aristas.obtenerValor(indiceVertice, cadaDestino); // Obtener la arista
                if (arista != 0.0) { // Si hay arista
                    grado++; // Incrementar el grado
                }
            }
        }
        return grado;
    }

    public boolean esAciclico() {
        boolean esAciclico = true;
        int columna = 0;
        for (int fila = 0; fila < aristas.obtenerRenglones(); fila++) {
            Double arista = (Double) aristas.obtenerValor(fila, columna);
            if (arista != 0.0) {
                esAciclico = false;
            }
            columna++;
        }
        return esAciclico;
    }


    /**
     * Este método verifica si existe un camino entre dos vértices (origen y destino).
     * @param origen Es el vértice origen.
     * @param destino Es el vértice destino.
     * @return Regresa <b>true</b> si existe un camino entre los vértices, <b>false</b> si no existe.
     */
    public boolean hayRuta(Object origen, Object destino) {
        ListaDinamica recorridoProfundidad = recorridoProfunidad(origen);
        if (recorridoProfundidad != null) {
            Object verticeDestino = recorridoProfundidad.buscar(destino);
            if (verticeDestino != null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite saber si el grafo es conexo.
     * @return Regresa <b>true</b> si el grafo es conexo, <b>false</b> si no es conexo.
     */
    public boolean esConexo() {
        // Un grafo es conexo si desde cualquier vértice existe un camino hasta
        // cualquier otro vértice del grafo. Puede apoyarse en el procedimiento explicado
        // en clase que usa los recorridos (profundidad o anchura).
        // 1.- Buscar el primer vértice
        boolean esConexo = true;
        int columna = 0;
        for (int fila = 0; fila < aristas.obtenerRenglones(); fila++) {
            Double arista = (Double) aristas.obtenerValor(fila, columna);
            if (arista != 0.0) {
                esConexo = false;
            }
            columna++;
        }
        return esConexo;
    }

    /**
     * Este método valida si hay un camino cerrado de un vértice origen.
     * @param origen Es el vértice origen.
     * @return Regresa <b>true</b> si hay un camino cerrado de un vértice origen, <b>false</b> en caso contrario.
     */
    public boolean hayCaminoCerrado(Object origen) {
        return false;
    }

    /**
     * Este método valida si hay un camino simple entre un vértice origen y un vértice destino.
     * @param origen Es el vértice origen.
     * @param destino Es el vértice destino.
     * @return Regresa <b>true</b> si hay un camino simple entre el vértice origen y el vértice destino,
     * <b>false</b> si no hay camino simple entre el vértice origen y el vértice destino.
     */
    public boolean esCaminoSimple(Object origen, Object destino) {
        return false;
    }

    /**
     * Este método verifica si el grafo es dirigido o no.
     * @return Regresa <b>true</b> si el grafo es dirigido, <b>false</b> en caso contrario.
     */
    public boolean esDirigido() {
        //  Este método indica si el grafo es dirigido o no dirigido
        // Para un grafo no dirigido la matriz de adyacencia es simétrica.
        boolean esDirigido = true;
        for (int fila = 0; fila < aristas.obtenerRenglones(); fila++) {
            for (int columna = 0; columna < aristas.obtenerColumnas(); columna++) {
                Double arista = (Double) aristas.obtenerValor(fila, columna);
                if (arista != 0.0) {
                    if (fila != columna) {
                        esDirigido = false;
                    }
                }
            }
        }
        return esDirigido;
    }

    /**
     * Este método valida si el grafo es arbol.
     * @return Regresa <b>true</b> si el grafo es arbol, <b>false</b> si no lo es.
     */
    public boolean esArbol() {
        /*
        Se dice que un grafo no dirigido es un árbol si es conexo, acíclico y
        donde cada vértice solo debe tener un padre. En el caso de un grafo dirigido, no
        debe tener ciclos y cada vértice solo debe tener un padre.
         */
        return false;
    }

    /**
     * Este método lista las aristas del grafo.
     */
    public void listarAristas() {
        // Este método muestra en el formato (origen, destino, peso), un listado de todas las
        // aristas que tiene un grafo. Por ejemplo: [(A,B,12), (C,D,3), …, (G,H,9)].
        SalidaPorDefecto.consola("[ ");
        for (int origen = 0; origen < aristas.obtenerRenglones(); origen++) {
            for (int destino = 0; destino < aristas.obtenerColumnas(); destino++) {
                Double arista = (Double) aristas.obtenerValor(origen, destino);
                if (arista != 0.0) {
                    SalidaPorDefecto.consola("(" + vertices.obtener(origen) + "," +
                            vertices.obtener(destino) + "," + arista + ") ");
                }
            }
        }
        SalidaPorDefecto.consola("]");
    }

    /**
     * Este método lista las aristas de un vértice del grafo.
     * @param vertice Es el vértice del que se quiere listar sus aristas.
     */
    public void listarAristas(Object vertice) {
        // Lista las aristas del vértice proporcionado
        Integer indiceVertice = (Integer) vertices.buscar(vertice);
        if (indiceVertice != null) {
            SalidaPorDefecto.consola("[ ");
            for (int cadaDestino = 0; cadaDestino < aristas.obtenerColumnas(); cadaDestino++) {
                Double arista = (Double) aristas.obtenerValor(indiceVertice, cadaDestino);
                if (arista != 0.0) {
                    SalidaPorDefecto.consola("(" + vertices.obtener(indiceVertice) + "," +
                            vertices.obtener(cadaDestino) + "," + arista + ") ");
                }
            }
            SalidaPorDefecto.consola("]");
        } else {
            SalidaPorDefecto.consola("El vértice no existe");
        }
    }

    /**
     * Este método imprime los vértices del grafo.
     */
    public void listarVertices() {
        // Lista los vértices del grafo
        SalidaPorDefecto.consola("[ ");
        for (int cadaVertice = 0; cadaVertice < vertices.numeroElementos(); cadaVertice++) {
            if (cadaVertice != vertices.numeroElementos() - 1) {
                SalidaPorDefecto.consola(vertices.obtener(cadaVertice) + ", ");
            } else {
                SalidaPorDefecto.consola(vertices.obtener(cadaVertice) + "]");
            }
        }
    }

    /**
     * Este método imprime los componentes conexos de un grafo no dirigido.
     * @param vertice Es el vértice del cual se inicia el recorrido en profundidad para encontrar los componentes conexos.
     */
    public void mostrarComponentesConexasNoDirigido(Object vertice) {
        boolean esConexo = true;
        // Un grafo no dirigido es conexo si existe un camino entre cualquier par de vértices que forman el grafo.
        // 1. Realizar un recorrido (anchura o profundidad) del grafo a partir de cualquier vértice w.
        // 2. Si en el recorrido se han marcado todos los vértices, entonces el grafo es conexo.
        // 3. Si el grafo no es conexo, los vértices marcados forman un componente conexo.
        // 4. Se toma un vértice no marcado, z y se realiza de nuevo el recorrido del grafo a partir de z.
        // Los nuevos vértices marcados forman otro componente conexo.
        // 5. El algoritmo termina cuando todos los vértices han sido marcados.
        recorridoProfunidad(vertice).imprimir();
        for (int cadaVerticeMarcado = 0; cadaVerticeMarcado < marcadosRecorridoProfundidad.numeroElementos(); cadaVerticeMarcado++) {
            boolean esVerticeMarcado = (boolean) marcadosRecorridoProfundidad.obtener(cadaVerticeMarcado);
            if (esVerticeMarcado == false) {
                SalidaPorDefecto.consola("\n");
                recorridoProfunidad(vertices.obtener(cadaVerticeMarcado)).imprimir();
                SalidaPorDefecto.consola("\n");
                esConexo = false;
            }
        }
        if (esConexo) {
            SalidaPorDefecto.consola("El grafo es conexo\n");
        } else {
            SalidaPorDefecto.consola("El grafo no es conexo\n");
        }
    }

    /**
     * Este verifica si un grafo dirigido es fuertemente conexo.
     * @return Regresa <b>true</b> si el grafo es fuertemente conexo, <b>false</b> si no lo es.
     */
    public boolean esFuertamenteConexo() {
        return false;
    }

    /**
     * Este método imprime los componentes fuertemente conexos de un grafo dirigido.
     * @param vertice Es el vértice del cual se inicia el recorrido en profundidad para encontrar los componentes conexos.
     */
    public void mostrarComponentesFuertementeConexas(Object vertice) {

    }

    // obtener el vertice con el indice dado
    public Object obtenerVertice(int indice) {
        return vertices.obtener(indice);
    }
}