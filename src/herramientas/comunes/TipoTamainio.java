package herramientas.comunes;

/**
 * Este enumerado representa los tipos de tamaños que se pueden usar en una imagen.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public enum TipoTamainio {
    AU_DOBELE("DOBLE", 1), AU_TRIPLE("TRIPLE", 2), DI_MITAD("MITAD", 3);

    private String nombre;
    private int valor;

    /**
     * Constructor del enumerado TipoTamainio que recibe el nombre del tipo de tamaño y un valor.
     * @param nombre Es el nombre del tipo de tamaño.
     * @param valor Es el valor del tipo de tamaño.
     */
    TipoTamainio(String nombre, int valor) {
        this.nombre = nombre;
        this.valor = valor;
    }

    /**
     * Metodo que retorna el nombre del tipo de tamaño.
     * @return Retorna el nombre del tipo de tamaño.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que retorna el valor del tipo de tamaño.
     * @return Retorna el valor del tipo de tamaño.
     */
    public int getValor() {
        return valor;
    }
}
