package estructurasLineales;

import entradaSalida.SalidaPorDefecto;

/**
 * Esta clase nos representa un TDA para una lista estática que almacena valores numéricos.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class ListaEstaticaNumerica extends ListaEstatica{

    protected int MAXIMO;
    protected int ultimo;
    protected Number[] datos;

    public ListaEstaticaNumerica(int maximo) {
        super(maximo);
        this.MAXIMO = maximo;
        this.ultimo = -1;
        this.datos = new Number[MAXIMO];
    }

    @Override
    public boolean vacia() {
        return ultimo == -1;
    }

    @Override
    public boolean llena() {
        return ultimo == MAXIMO - 1;
    }

    @Override
    public int numeroElementos() {
        return ultimo + 1;
    }

    protected boolean enRango(int indice){
        return indice >= 0 && indice <= ultimo;
    }

    @Override
    public Object obtener(int indice){
        if(enRango(indice)){
            return datos[indice];
        }else{
            return -1;
        }
    }

    /**
     * Este método nos permite ingresar valores numericos a nuestra lista estática de números.
     * @param valor Es el valor que vamos a ingresar a nuestra lista estática de números.
     * @return Regresa la posicion en donde se inserto el valor.
     */
    @Override
    public int agregar(Object valor) {
        if (!llena() && valor instanceof Number) { // hay espacio y el valor es un numero
            ultimo++;
            datos[ultimo] = (Number) valor;
            return ultimo;
        } else { // no hay espacio o el valor no es un numero
            return -1;
        }
    }

    /**
     * Este método nos permite bucar un valor en nuestra lista estática de números.
     * @param valor Es el valor que vamos a buscar en nuestra lista estática de números.
     * @return Regresa la posicion en donde se encuentra el valor.
     */
    @Override
    public Object buscar(Object valor) {
       // Buscar un valor numérico en la lista y devolver su posición en la lista
        int posicionValor = -1;
        if (!vacia() && valor instanceof Number) {
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                if (datos[posicion].equals(valor)) {
                    posicionValor = posicion;
                    break;
                }
            }
            return posicionValor;
        } else {
            return posicionValor;
        }
    }

    /**
     * Este método nos permite eliminar un elemento de nuestra lista estática de números.
     * @param indice Es la posicion del elemento que vamos a eliminar.
     * @return Regresa el valor eliminado.
     */
    @Override
    public Object eliminar(int indice) {
        if (obtener(indice) != null) { // si lo encontramos
            double valorEliminado = (Double) datos[indice];
            ultimo--;
            for (int movimiento = indice; movimiento <= ultimo; movimiento++) {
                datos[movimiento] = datos[movimiento + 1];
            }
            return valorEliminado;
        } else { // no lo encontramos
            return null;
        }
    }

    /**
     * Este método realiza la multiplicación de un escalar por cada elemento de la lista.
     * @param escalar Es el numero que multiplica a cada elemento de la lista.
     * @return Regresa <b>true</b> si se multiplicó correctamente, <b>false</b> si la lista está vacía.
     */
    public boolean porEscalar(Number escalar){
        if (!vacia()){
            // multiplicar cada elemento de la lista por el escalar
            double escalarAMultiplicar = escalar.doubleValue();
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                datos[posicion] = datos[posicion].doubleValue() * escalarAMultiplicar;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método realiza la suma de un escalar más cada elemento de la lista.
     * @param escalar Es el numero que suma a cada elemento de la lista.
     * @return Regresa <b>true</b> si se sumó correctamente, <b>false</b> si la lista está vacía.
     */
    public boolean sumarEscalar(Number escalar){
        if (!vacia()){
            // sumar cada elemento de la lista por el escalar
            double escalarASumar = escalar.doubleValue();
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                datos[posicion] = datos[posicion].doubleValue() + escalarASumar;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método realiza la suma de los valores de la lista actual con los valores de la lista pasada como parámetro.
     * @param lista2 Es la lista que contiene los valores que se suman con los valores de la lista actual.
     * @return Regresa <b>true</b> si se sumó correctamente, <b>false</b> si la lista está vacía.
     */
    public boolean sumar(ListaEstaticaNumerica lista2){
        if (numeroElementos() == lista2.numeroElementos() && !vacia()){
            // sumar cada elemento de la lista actual con el elemento de la lista2
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                // asignar el resultado de la suma a la posicion de la lista actual
                datos[posicion] = datos[posicion].doubleValue() + lista2.datos[posicion].doubleValue();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método realiza la multiplicación de los valores de la lista actual con los valores de
     * la lista pasada como parámetro.
     * @param lista2 Es la lista que contiene los valores que se multiplican con los valores de la lista actual.
     * @return Regresa <b>true</b> si se multiplicarón correctamente los valores, <b>false</b> si la lista está
     * vacía o si la lista pasada como parámetro no tiene el mismo número de elementos que la lista actual.
     */
    public boolean multiplicar(ListaEstaticaNumerica lista2){
        if (numeroElementos() == lista2.numeroElementos() && !vacia()){
            // multiplicar cada elemento de la lista actual con el elemento de la lista2
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                // asignar el resultado de la multiplicacion a la posicion de la lista actual
                datos[posicion] = datos[posicion].doubleValue() * lista2.datos[posicion].doubleValue();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método realiza la potenciación de los valores de la lista actual con el valor que se le pasa como
     * parámetro.
     * @param escalar Es exponente por el cual se eleva cada elemento de la lista actual.
     * @return Regresa <b>true</b> si se elevaron correctamente los valores, <b>false</b> si la lista está vacía.
     */
    public boolean aplicarPotencia(Number escalar){
        double base = 0;
        if (!vacia()){
            double escalarAPotenciar = escalar.doubleValue();
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                base = datos[posicion].doubleValue();
                datos[posicion] = Math.pow(base, escalarAPotenciar);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método realiza la potenciación de los valores de la lista actual con los valores de la lista pasada como
     * parámetro.
     * @param lista2 Es la lista que contiene los valores con que se eleva cada elemento de la lista actual.
     * @return Regresa <b>true</b> si se elevaron correctamente los valores, <b>false</b> si la lista está vacía.
     */
    public boolean aplicarPotencia(ListaEstaticaNumerica lista2){
        double exponente = 0;
        double base = 0;
        if (numeroElementos() == lista2.numeroElementos() && !vacia()){
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                base = datos[posicion].doubleValue();
                exponente = lista2.datos[posicion].doubleValue();
                datos[posicion] = Math.pow(base, exponente);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método realiza la multiplicación de los valores de la lista actual con el valor que se le pasa como
     * parámetro y suma cada rsultado de la multiplicación.
     * @param lista2 Es la lista que contiene los valores que se multiplican con los valores de la lista actual.
     * @return Regresa el producto de la multiplicación y suma de los valores de la lista actual con los valores de la lista2.
     */
    public double productoEscalar(ListaEstaticaNumerica lista2){
        double productoEscalar = 0;
        if (numeroElementos() == lista2.numeroElementos() && !vacia()){
            // multiplicar cada elemento de la lista actual con el elemento de la lista2
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                // asignar el resultado de la multiplicacion a la posicion de la lista actual
                productoEscalar += datos[posicion].doubleValue() * lista2.datos[posicion].doubleValue();
            }
        }
        return productoEscalar;
    }

    /**
     * Este método realiza la opearcion (Norma de un vector).
     * @return Regresa el valor de la norma del vector.
     */
    public double norma(){
        double norma = 0;
        double sumaElementosAlCuadrado = 0;
        if (!vacia()){
            // calcular la norma de la lista
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                sumaElementosAlCuadrado += Math.pow(datos[posicion].doubleValue(), 2);
            }
            norma = Math.sqrt(sumaElementosAlCuadrado);
        }
        return norma;
    }

    /**
     * Este método calcula la norma euclidiana de dos vectores.
     * @param arreglo2 Es la lista con la cual se efectura la el cálculo de la norma euclidiana.
     * @return Regresa el valor de la norma euclidiana.
     */
    public double normaEuclidiana(ListaEstaticaNumerica arreglo2){
        double normaEuclidiana = 0;
        double sumaElementosAlCuadrado = 0;
        if (numeroElementos() == arreglo2.numeroElementos() && !vacia()){
            // calcular la norma euclidiana de la lista
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                double elemento1 = datos[posicion].doubleValue();
                double elemento2 = arreglo2.datos[posicion].doubleValue();
                sumaElementosAlCuadrado += Math.pow(elemento1 - elemento2, 2);
            }
            normaEuclidiana = Math.sqrt(sumaElementosAlCuadrado);
        }
        return normaEuclidiana;
    }

    /**
     * Este método suma los valores de una lista numerica.
     * @param listaNumerica Es la lista de la cual se suman sus valores.
     * @return Regresa el valor de la suma de los valores de la lista.
     */
    private double sumarElementosListaNumerica(ListaEstaticaNumerica listaNumerica){
        double sumaElementos = 0;
        if (!listaNumerica.vacia()){
            for (int posicion = 0; posicion <= listaNumerica.ultimo; posicion++) {
                double elemento = listaNumerica.datos[posicion].doubleValue();
                sumaElementos += elemento;
            }
        }
        return sumaElementos;
    }

    /**
     * Este método realiza la suma de el conjunto de listas que se le pasa como parámetro más la suma de los valores
     * de la lista actual.
     * @param listas Es una lista estática que contiene las listas numerica que se suman con la lista actual.
     * @return Regresa el valor de la suma.
     */
    public double sumarListaEstatica(ListaEstatica listas){
        double sumaListasEstaticas = 0;
        if (!vacia()){
            sumaListasEstaticas =  sumarElementosListaNumerica(this);
            for (int posicion = 0; posicion <= listas.ultimo; posicion++) {
                ListaEstaticaNumerica listaNumericaTmp = (ListaEstaticaNumerica) listas.datos[posicion];
                sumaListasEstaticas += sumarElementosListaNumerica(listaNumericaTmp);
            }
        }
        return sumaListasEstaticas;
    }

    /**
     * Este método realiza la suma de los escalares de la lista que se le pasa como parámetro más la suma de los
     * valores de la lista actual.
     * @param escalares Es la lista que contiene los escalares que se suman con la lista actual.
     * @return Regresa el valor de la suma de los escalares.
     */
    public double sumarEscalares(ListaEstaticaNumerica escalares){
        double sumaEscalares = 0;
        if (!vacia() && numeroElementos() == escalares.numeroElementos()){
            sumaEscalares = sumarElementosListaNumerica(escalares) + sumarElementosListaNumerica(this);
        }
        return sumaEscalares;
    }

    /**
     * Este método realiza la suma de los indices que tiene la lista que se le pasa como parámetro, de la
     * lista actual.
     * @param listaIndices Es la lista que contiene los indices que se suman de la lista actual.
     * @return Regresa el resultado de la suma de los indices.
     */
    public double sumarIndices(ListaEstaticaNumerica listaIndices){
        double sumaDeIndices = 0;
        if (!vacia()){
            for (int posicion = 0; posicion <= listaIndices.ultimo; posicion++) {
                int indice = listaIndices.datos[posicion].intValue();
                if (enRango(indice)){
                    sumaDeIndices += datos[indice].doubleValue();
                }
            }
        }
        return sumaDeIndices;
    }

    /**
     * Este método nos permite obtener un sub arreglo de la lista actual.
     * @param listaIndices Es el arreglo que contiene los indices de los elementos que se desean obtener.
     * @return Regresa una lista estática numerica con los elementos que se desean obtener.
     */
    public ListaEstaticaNumerica subLista(ListaEstaticaNumerica listaIndices){
        ListaEstaticaNumerica subListaNumerica = new ListaEstaticaNumerica(listaIndices.numeroElementos());
        if (!vacia() && listaIndices.numeroElementos() > 0){
            for (int posicion = 0; posicion <= listaIndices.ultimo; posicion++) {
                int indice = listaIndices.datos[posicion].intValue();
                if (enRango(indice)){
                    subListaNumerica.agregar(datos[indice]);
                }
            }
        }
        return subListaNumerica;
    }

    /**
     * Este método multiplica un vector por un escalar.
     * @param vector Es el vector que se multiplica por el escalar.
     * @param escalar Es el escalar que se multiplica por el vector.
     * @return Regresa una lista estática numerica con el resultado de la multiplicación.
     */
    private ListaEstaticaNumerica vectorXEscalar(ListaEstaticaNumerica vector, double escalar){
        ListaEstaticaNumerica vectorXEscalar = new ListaEstaticaNumerica(vector.numeroElementos());
        if (!vector.vacia()){
            for (int posicion = 0; posicion <= vector.ultimo; posicion++) {
                double elemento = vector.datos[posicion].doubleValue();
                vectorXEscalar.agregar(escalar * elemento);
            }
        }
        return vectorXEscalar;
    }

    /**
     * Este método suma un conjunto de vectores.
     * @param vectores Es la lista que contiene los vectores que se suman.
     * @return Regresa una lista estática numerica con el resultado de la suma de los vectores.
     */
    private ListaEstaticaNumerica sumaDeVectores(ListaEstaticaNumerica vectores){
        ListaEstaticaNumerica sumaDeVectores = new ListaEstaticaNumerica(vectores.numeroElementos());
        if (!vacia() && vectores.numeroElementos() == numeroElementos()){
            for (int posicion = 0; posicion <= vectores.ultimo; posicion++) {
                double elemento = vectores.datos[posicion].doubleValue();
                sumaDeVectores.agregar(datos[posicion].doubleValue() + elemento);
            }
        }
        return sumaDeVectores;
    }

    /**
     * Este método verifica si un conjunto de vectores son linealmiente dependientes.
     * @param listaVectores Es la lista que contiene los vectores que se verifican.
     * @return Regresa <b>true</b> si los vectores son linealmente dependientes, <b>false</b> en caso contrario.
     */
    public boolean sonLinealmenteDependientes(ListaEstatica listaVectores){
        return true;
    }

    /**
     * Este método verifica si un conjunto de vectores son linealmiente independientes.
     * @param listaVectores Es la lista que contiene los vectores que se verifican.
     * @return Regresa <b>true</b> si los vectores son linealmente independientes, <b>false</b> en caso contrario.
     */
    public boolean sonLinealmenteIndependientes(ListaEstatica listaVectores){
        return true;
    }

    /**
     * Este método verifica si dos vectores son ortogonales o no.
     * @param lista2 Es la lista que contiene el segundo vector.
     * @return Regresa <b>true</b> si los vectores son ortogonales, <b>false</b> en caso contrario.
     */
    public boolean esOrtogonal(ListaEstaticaNumerica lista2){
        boolean esOrtogonal = false;
        if (!vacia() && !lista2.vacia() && numeroElementos() == lista2.numeroElementos()){
            double productoEscalar = productoEscalar(lista2);
            esOrtogonal = productoEscalar == 0;
        }
        return esOrtogonal;
    }

    /**
     * Este método verifica si dos vectores, el actual y el parámetro, son paralelos.
     * @param lista2 Es la lista que contiene el segundo vector.
     * @return Regresa <b>true</b> si los vectores son paralelos, <b>false</b> en caso contrario.
     */
    public boolean esParalelo(ListaEstaticaNumerica lista2){
        return true;
    }

    @Override
    public void imprimir() {
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            SalidaPorDefecto.consola(datos[posicion] + "\n");
        }
    }

    /**
     * Este método agrega el contenido de una lista a la lista actual.
     * @param buffer Es la lista que contiene los elementos que se desean agregar a la lista actual.
     * @return Regresa true si la lista fue agregada correctamente, false en caso contrario.
     */
    public boolean agregarBuffer(double[] buffer) {
        boolean agregado = false;
       if (vacia()){
           for (int posicion = 0; posicion < buffer.length; posicion++) {
               agregar(buffer[posicion]);
           }
           agregado = true;
       }
       return agregado;
    }

    /**
     * Este método nos permite obtener una copia de la lista actual.
     * @return Regresa un arreglo de tipo <b>double</b> con los elementos de la lista.
     */
    public double[] leerArreglo() {
        double[] copia = new double[datos.length];
        if (!vacia()) {
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                copia[posicion] = datos[posicion].doubleValue();
            }
        }
        return copia;
    }

    /**
     * Este método remplaza un elemento del la lista por otro elemento.
     * @param posicion Es la posición del elemento que se remplaza.
     * @param elemento Es el elemento que se remplaza.
     * @return Regresa <b>true</b> si el elemento se remplaza, <b>false</b> en caso contrario.
     */
    public boolean remplazar(int posicion, double elemento) {
        boolean remplazado = false;
        if (!vacia() && posicion <= ultimo) {
            datos[posicion] = elemento;
            remplazado = true;
        }
        return remplazado;
    }


    /**
     * Este método nos indica el tamaño de la lista.
     * @return Regresa el tamaño de la lista.
     */
    public int tamanio() {
        return datos.length;
    }

    /**
     * Este método remplaza todo el contenido de la lista por nuevos elementos de una lista.
     * @param listaNueva Es la lista que contiene los nuevos elementos.
     */
    public void remplazar(double[] listaNueva) {
        if (!vacia() && listaNueva.length == datos.length) {
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                datos[posicion] = listaNueva[posicion];
            }
        }
    }
}