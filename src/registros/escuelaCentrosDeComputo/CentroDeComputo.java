package registros.escuelaCentrosDeComputo;

import estructurasLineales.ListaDinamica;

/**
 * Esta clase representa un centro de cómputo de una escuela.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class CentroDeComputo {

    protected int numero;
    protected String nombre;
    protected ListaDinamica computadoras;

    public CentroDeComputo(int numero, String nombre) {
        this.numero = numero;
        this.nombre = nombre;
        computadoras = new ListaDinamica();
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ListaDinamica getComputadoras() {
        return computadoras;
    }

    public void setComputadoras(ListaDinamica computadoras) {
        this.computadoras = computadoras;
    }

    /**
     * Este método nos permite agregar una computadora al centro de cómputo.
     * @param computadora Es la computadora que se va a agregar al centro de cómputo.
     * @return Retorna true si la computadora se agregó al centro de cómputo, false en caso contrario.
     */
    public boolean agregarComputadora(Computadora computadora){
        int valorRetornoAgregado = computadoras.agregar(computadora);
        if (valorRetornoAgregado == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Este método nos permite eliminar una computadora del centro de cómputo.
     * @param numeroDecomputadora Es el número de la computadora que se va a eliminar del centro de cómputo.
     * @return Retorna true si la computadora se eliminó del centro de cómputo, false en caso contrario.
     */
    public boolean eliminarComputadora(int numeroDecomputadora) {
        Object valorRetornoEliminado = computadoras.eliminar(numeroDecomputadora+"");
        if (valorRetornoEliminado == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Este método nos permite buscar una computadora en el centro de cómputo.
     * @param numeroDecomputadora Es el número de la computadora que se va a buscar en el centro de cómputo.
     * @return Retorna la computadora que se buscó en el centro de cómputo, null en caso contrario.
     */
    public Computadora buscarComputadora(int numeroDecomputadora) {
        Object computadoraEncontrada = computadoras.buscar(numeroDecomputadora+"");
        if (computadoraEncontrada == null) {
            return null;
        } else {
            return (Computadora) computadoraEncontrada;
        }
    }

    /**
     * Este método nos permite saber que computadoras tienen la aplicación Chrome instalada.
     * @return Regresa una lista dinámica con las computadoras que tienen Chrome instalado.
     */
    public ListaDinamica buscarComputadorasConChromeInstalado() {
        ListaDinamica computadorasConChromeInstalado = new ListaDinamica();
        computadoras.iniciarIterador();
        while (computadoras.hayNodos() == true) {
            Computadora computadoraIterada = (Computadora) computadoras.obtenerNodo();
            if (computadoraIterada.estaInstaladoChrome() == true) {
                computadorasConChromeInstalado.agregar(computadoraIterada);
            }
        }
        return computadorasConChromeInstalado;
    }

    /**
     * Este método muestra el listado de computadoras del centro de cómputo.
     */
    public void mostrarListadoDeComputadoras() {
        computadoras.iniciarIterador();
        while(computadoras.hayNodos() == true){
            Computadora computadoraIterada = (Computadora) computadoras.obtenerNodo();
            computadoraIterada.mostrarDatos();
        }
    }

    @Override
    public String toString() {
        return numero + "";
    }
}