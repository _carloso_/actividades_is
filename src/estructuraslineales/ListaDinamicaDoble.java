package estructurasLineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.auxiliares.NodoDoble;

/**
 * Esta clase representa un TDA de una lista doblemente enlazada.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class ListaDinamicaDoble implements ListaAlmacenamiento{

    protected NodoDoble primero;
    protected NodoDoble ultimo;
    protected NodoDoble iterador;

    public ListaDinamicaDoble() {
        primero = null;
        ultimo = null;
        iterador = null;
    }

    @Override
    public boolean vacia() {
        if (primero == null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int agregar(Object valor) {
        NodoDoble nuevoNodo = new NodoDoble(valor); // paso 1
        if (nuevoNodo != null) { // hay espacio en memoria.
            if (!vacia()) { // b)
                ultimo.setNodoDer(nuevoNodo); // paso 2
                nuevoNodo.setNodoIzq(ultimo); // paso 3
                ultimo = nuevoNodo; // paso 4
            } else { // a)
                primero = nuevoNodo; // paso 2
                ultimo = nuevoNodo;
            }
            return 0;
        } else { // no hay espacio en memoria.
            return -1;
        }
    }

    /**
     * Este método agrega un elemento al inicio de la lista.
     * @param valor Es el valor del elemento que se va a agregar a la lista.
     * @return Retorna 0 si se pudo agregar el elemento, -1 si no se pudo agregar.
     */
    public int agregarInicio(Object valor) {
        NodoDoble nuevoNodo = new NodoDoble(valor);
        if (nuevoNodo != null) {
            if (!vacia()) {
                nuevoNodo.setNodoDer(primero);
                primero.setNodoIzq(nuevoNodo);
                primero = nuevoNodo;
            } else {
                primero = nuevoNodo;
                ultimo = nuevoNodo;
            }
            return 0;
        } else {
            return -1;
        }
    }

    /**
     * Este método elimina el primer elemento de la lista.
     * @return Regresa el valor del elemento eliminado, null si la lista esta vacía.
     */
    public Object eliminarInicio() {
        if (!vacia()) {
            Object contenidoEliminado = primero.getContenido();
            if (primero == ultimo) { // Si solo hay un elemento en la lista.
                primero = null;
                ultimo = null;
            } else { // Si hay mas de un elemento en la lista.
                primero = primero.getNodoDer();
                primero.setNodoIzq(null);
            }
            return contenidoEliminado;
        } else {
            return null;
        }
    }

    @Override
    public void imprimir() {
        if (!vacia()) { // hay elementos en la lista.
            NodoDoble iterador = primero;
            SalidaPorDefecto.consola("null <- ");
            while (iterador != ultimo) { // se repite desde el primero hasta el penultimo elemento.
                Object contenido = iterador.getContenido();
                SalidaPorDefecto.consola(contenido + " <-> ");
                iterador = iterador.getNodoDer();
            }
            SalidaPorDefecto.consola(ultimo.getContenido() + " -> null"); // se imprime el ultimo elemento.
        } else { // La lista está vacía.
            SalidaPorDefecto.consola("null");
        }
    }

    @Override
    public void imprimirOI() {
        if (!vacia()) { // hay elementos en la lista.
            NodoDoble iterador = ultimo;
            SalidaPorDefecto.consola("null <- ");
            while (iterador != primero) { // se repite desde el ultimo hasta el segundo elemento.
                Object contenido = iterador.getContenido();
                SalidaPorDefecto.consola(contenido + " <-> ");
                iterador = iterador.getNodoIzq();
            }
            SalidaPorDefecto.consola(primero.getContenido() + " -> null");
        } else { // La lista está vacía.
            SalidaPorDefecto.consola("null");
        }
    }

    @Override
    public Object buscar(Object valor) {
        if (!vacia()) {
            NodoDoble iterador = primero;
            while (iterador != null && !iterador.getContenido().toString().equalsIgnoreCase(valor.toString())) {
                iterador = iterador.getNodoDer();
            }
            if (iterador != null) {
                return iterador.getContenido();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Este método busca un elemento en la lista, comenzando desde el final.
     * @param valor Es el valor del elemento que se va a buscar.
     * @return Regresa el contenido del elemento si se encuentra, null si no se encuentra.
     */
    public Object buscarFinal(Object valor) {
        if (!vacia()) {
            NodoDoble iterador = ultimo;
            while (iterador != null && !iterador.getContenido().toString().equalsIgnoreCase(valor.toString())) {
                iterador = iterador.getNodoIzq();
            }
            if (iterador != null) {
                return iterador.getContenido();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Object eliminar(Object valor) {
        if (!vacia()) { // hay elementos en la lista.
            NodoDoble iterador = primero;
            while (iterador != null && !iterador.getContenido().toString().equalsIgnoreCase(valor.toString())) {
                iterador = iterador.getNodoDer();
            }
            if (iterador != null) { // Si el elemento se encuentra en la lista.
                Object contenidoEliminado = iterador.getContenido();
                if (primero == ultimo) { // Si solo hay un elemento en la lista.
                    primero = null;
                    ultimo = null;
                } else if (iterador == primero) { // Si el elemento a eliminar es el primero.
                    primero = primero.getNodoDer();
                    primero.setNodoIzq(null);
                } else if (iterador == ultimo) { // Si el elemento a eliminar es el ultimo.
                    ultimo = ultimo.getNodoIzq();
                    ultimo.setNodoDer(null);
                } else { // Si el elemento a eliminar está en el medio.
                    NodoDoble nodoIzq = iterador.getNodoIzq();
                    NodoDoble nodoDer = iterador.getNodoDer();
                    nodoIzq.setNodoDer(nodoDer);
                    nodoDer.setNodoIzq(nodoIzq);
                }
                return contenidoEliminado;
            } else {
                return null;
            }
        } else { // La lista está vacía.
            return null;
        }
    }

    /**
     * Este método cuenta el número de elementos que hay en la lista.
     * @return Regresa el número de elementos que hay en la lista.
     */
    public int contarCantidadDeNodosExistentes(){
        int cantidad = 0;
        iniciarIteradorDelInicio();
        while (hayNodos() == true){
            obtenerNodoDer();
            cantidad++;
        }
        return cantidad;
    }

    @Override
    public boolean esIgual(ListaAlmacenamiento lista2) {
       if (lista2.getClass().equals(this.getClass())) { // Si las listas son del mismo tipo.
           ListaDinamicaDoble listaDoble2 = (ListaDinamicaDoble) lista2;
           if (listaDoble2.vacia() && this.vacia()) { // Si ambas listas están vacías.
               return true;
           } else if (!listaDoble2.vacia() && !this.vacia()) { // Si ambas listas no están vacías.
               if (listaDoble2.contarCantidadDeNodosExistentes() == this.contarCantidadDeNodosExistentes()) { // Si la cantidad de elementos es la misma.
                   NodoDoble iterador1 = this.primero;
                   NodoDoble iterador2 = listaDoble2.primero;
                   while (iterador1 != null && iterador2 != null) { // Mientras no se llegue al final de las listas.
                       if (!iterador1.getContenido().toString().equalsIgnoreCase(iterador2.getContenido().toString())) { // Si los elementos no son iguales.
                           return false;
                       }
                       iterador1 = iterador1.getNodoDer();
                       iterador2 = iterador2.getNodoDer();
                   }
                   return true;
               } else {
                   return false;
               }
           } else {
               return false;
           }
        } else {
            return false;
        }
    }

    @Override
    public boolean cambiar(Object valorViejo, Object valorNuevo, int numVeces) {
        int numVecesValorViejo = contar(valorViejo); // numero de veces que se repite el valor viejo en la lista.
        if (!vacia() && numVeces > 0 && numVeces <= numVecesValorViejo) { // hay elementos en la lista y se cambia por lo menos una vez.
            NodoDoble iterador = primero;
            while (iterador != null && !iterador.getContenido().toString().equalsIgnoreCase(valorViejo.toString())) {
                iterador = iterador.getNodoDer();
            }
            if (iterador != null) {
                if (numVeces == 1) { // Si se cambia el valor una vez.
                    iterador.setContenido(valorNuevo);
                    return true;
                } else { // Si se cambia el valor una cantidad de veces.
                    int contador = 0;
                    while (iterador != null && contador < numVeces) {
                        iterador.setContenido(valorNuevo);
                        iterador = iterador.getNodoDer();
                        contador++;
                    }
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public ListaEstatica buscarValores(Object valor) {
        ListaEstatica listaDePosiciones = new ListaEstatica(contarCantidadDeNodosExistentes());
        iniciarIteradorDelInicio(); // Se inicia el iterador en el primer nodo.
        int posicion = 0;
        while (hayNodos() == true){ // Mientras haya nodos en la lista.
            Object contenido = obtenerNodoDer(); // Se obtiene el contenido del nodo actual y se avanza al siguiente nodo Der.
            if (contenido.toString().equalsIgnoreCase(valor.toString())) {
                listaDePosiciones.agregar(posicion);
            }
            posicion++;
        }
        return listaDePosiciones;
    }

    @Override
    public Object eliminar() { // Elimina el último elemento de la lista.
        if (!vacia()) {
            Object contenidoEliminado = ultimo.getContenido(); // paso 1
            if (primero == ultimo) { // b) único
                primero = null; // paso 2
                ultimo = null;
           } else { // c) varios
                ultimo = ultimo.getNodoIzq(); // paso 2
                ultimo.setNodoDer(null); // paso 3
           }
            return contenidoEliminado;
        } else { // a)
            return null;
        }
    }

    @Override
    public boolean agregarLista(ListaAlmacenamiento lista2) {
        if (lista2 instanceof ListaEstatica) { // Si la lista 2 es una lista estática.
            ListaEstatica lista2Estatica = (ListaEstatica) lista2;
            for (int pos = 0; pos < lista2Estatica.numeroElementos(); pos++) {
                agregar(lista2Estatica.obtener(pos));
            }
            return true;
        } else if (lista2 instanceof ListaDinamica) { // Si la lista 2 es una lista dinámica simple.
            ListaDinamica lista2Dinamica = (ListaDinamica) lista2;
            lista2Dinamica.iniciarIterador();
            while (lista2Dinamica.hayNodos() == true) {
                agregar(lista2Dinamica.obtenerNodo());
            }
            return true;
        } else if (lista2 instanceof ListaDinamicaDoble) { // Si la lista 2 es una lista dinámica doble.
            ListaDinamicaDoble lista2DinamicaDoble = (ListaDinamicaDoble) lista2;
            lista2DinamicaDoble.iniciarIteradorDelInicio();
            while (lista2DinamicaDoble.hayNodos() == true) {
                agregar(lista2DinamicaDoble.obtenerNodoDer());
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void invertir() {
       // cambiar el orden de los elementos de la lista por el inverso.
        ListaEstatica listaInvertida = new ListaEstatica(contarCantidadDeNodosExistentes());
        iniciarIteradorDelFinal(); // Se inicia el iterador en el último nodo.
        while (hayNodos() == true) { // Mientras haya nodos en la lista.
            Object contenido = obtenerNodoIzq(); // Se obtiene el contenido del nodo actual y se avanza al siguiente nodo Izq.
            listaInvertida.agregar(contenido); // agregamos el elemento actual a la lista estática invertida.
            eliminar(); // Eliminamos el último elemento de la lista.
        }
        // agregamos los elementos de la lista invertida a la lista dinámica doble.
        for (int pos = 0; pos < listaInvertida.numeroElementos(); pos++) {
            agregar(listaInvertida.obtener(pos));
        }
    }

    @Override
    public int contar(Object valor) {
        int contador = 0;
        if (!vacia()) {
            NodoDoble iterador = primero;
            while (iterador != null) {
                if (iterador.getContenido().toString().equalsIgnoreCase(valor.toString())) {
                    contador++;
                }
                iterador = iterador.getNodoDer();
            }
            return contador;
        } else {
            return contador;
        }
    }

    @Override
    public boolean eliminarLista(ListaAlmacenamiento lista2) {
        if (lista2 instanceof ListaEstatica) { // Si la lista 2 es una lista estática.
            ListaEstatica lista2Estatica = (ListaEstatica) lista2;
            for (int pos = 0; pos < lista2Estatica.numeroElementos(); pos++) {
                Object contenidoAEliminar = lista2Estatica.obtener(pos);
                eliminar(contenidoAEliminar);
            }
            return true;
        } else if (lista2 instanceof ListaDinamica) { // Si la lista 2 es una lista dinámica simple.
            ListaDinamica lista2Dinamica = (ListaDinamica) lista2;
            lista2Dinamica.iniciarIterador();
            while (lista2Dinamica.hayNodos() == true) {
                Object contenidoAEliminar = lista2Dinamica.obtenerNodo();
                eliminar(contenidoAEliminar);
            }
            return true;
        } else if (lista2 instanceof ListaDinamicaDoble) { // Si la lista 2 es una lista dinámica doble.
            ListaDinamicaDoble lista2DinamicaDoble = (ListaDinamicaDoble) lista2;
            lista2DinamicaDoble.iniciarIteradorDelInicio();
            while (lista2DinamicaDoble.hayNodos() == true) {
                Object contenidoAEliminar = lista2DinamicaDoble.obtenerNodoDer();
                eliminar(contenidoAEliminar);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void rellenar(Object valor, int cantidad) {
        for (int pos = 0; pos < cantidad; pos++) {
            agregar(valor);
        }
    }

    @Override
    public Object clonar() {
        if (!vacia()) {
            ListaDinamicaDoble listaClonada = new ListaDinamicaDoble();
            iniciarIteradorDelInicio();
            while (hayNodos() == true) {
                Object contenido = obtenerNodoDer();
                listaClonada.agregar(contenido);
            }
            return listaClonada;
        } else {
            return null;
        }
    }

    /**
     * Este método permite obtener el contenido del nodo que se encuentra en la posición indicada.
     * @param indice Es el índice del nodo que se desea obtener.
     * @return Regresa el contenido del nodo que se encuentra en la posición indicada.
     */
    public Object obtenerNodo(int indice) {
        if (!vacia()) {
            NodoDoble iterador = primero;
            for (int pos = 0; pos < indice; pos++) {
                iterador = iterador.getNodoDer();
            }
            return iterador.getContenido();
        } else {
            return null;
        }
    }

    /**
     * Este método verifica si un índice es válido para la lista.
     * @param indice Es el índice a verificar.
     * @return Regresa <b>true</b> si el indice es válido, <b>false</b> si no lo es.
     */
    private boolean validarIndice(int indice) {
        if (indice >= 0 && indice < contarCantidadDeNodosExistentes()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object subLista(int indiceInicial, int indiceFinal) {
        ListaDinamicaDoble listaSubLista = new ListaDinamicaDoble();
        if (validarIndice(indiceInicial) == true && validarIndice(indiceFinal) == true) {
            for (int pos = indiceInicial; pos <= indiceFinal; pos++) {
                Object contenido = obtenerNodo(pos);
                listaSubLista.agregar(contenido);
            }
            return listaSubLista;
        } else {
            return null;
        }
    }

    @Override
    public boolean esSublista(ListaAlmacenamiento lista2) {
        return false;
    }

    @Override
    public boolean insertar(Object valor, int indice) {
        if (indice == 0) { // agregar al inicio
            int valorRetornoAgregar = agregarInicio(valor);
            if (valorRetornoAgregar == 0) {
                return true;
            } else {
                return false;
            }
        } else if (validarIndice(indice) == true) { // posicion valida, se puede agregar
            NodoDoble temp = primero;
            int posActual = 0;
            while (posActual < indice - 1) {
                temp = temp.getNodoDer();
                posActual++;
            }
            NodoDoble nuevoNodo = new NodoDoble(valor);
            nuevoNodo.setNodoIzq(temp);
            nuevoNodo.setNodoDer(temp.getNodoDer());
            if (temp.getNodoDer() != null) {
                temp.getNodoDer().setNodoIzq(nuevoNodo);
            }
            temp.setNodoDer(nuevoNodo);
            return true;
        } else if (indice < 0){ // la posicion es negativa, no se puede agregar
            return false;
        } else { // la posicion es mayor que la cantidad de nodos se agrega al final de la lista
            int valorRetornoAgregar = agregar(valor);
            if (valorRetornoAgregar == 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public void rellenar(Object valor) {

    }

    @Override
    public Object verUltimo() {
        if (!vacia()) {
            return ultimo.getContenido();
        } else {
            return null;
        }
    }

    @Override
    public Object verPrimero() {
        if (!vacia()) {
            return primero.getContenido();
        } else {
            return null;
        }
    }

    /**
     * Este método vacía la lista dinámica doble.
     */
    public void vaciarLista(){
        primero = null;
        ultimo = null;
        iterador = null;
    }

    /**
     * Este método inicializa un iterador para recorrer la lista, comenzando desde el primer elemento.
     */
    public void iniciarIteradorDelInicio() {
        iterador = primero;
    }

    /**
     * Este método inicializa un iterador para recorrer la lista, comenzando desde el último elemento.
     */
    public void iniciarIteradorDelFinal() {
        iterador = ultimo;
    }

    /**
     * Este método nos indica si aún hay nodos por recorrer en la lista.
     * @return Regresa <b>true</b> si hay nodos por recorrer en la lista, <b>false</b> en caso contrario.
     */
    public boolean hayNodos() {
        if (iterador != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método nos regresa el contenido del nodo actual del iterador y avanza al siguiente nodo derecho de la lista.
     * @return Regresa el contenido del nodo actual del iterador.
     */
    public Object obtenerNodoDer() {
       if (hayNodos() == true) {
           Object contenido = iterador.getContenido();
           iterador = iterador.getNodoDer();
           return contenido;
       } else {
           return null;
       }
    }

    /**
     * Este método nos regresa el contenido del nodo actual del iterador y avanza al nodo izquierdo de la lista.
     * @return Regresa el contenido del nodo actual del iterador.
     */
    public Object obtenerNodoIzq() {
       if (hayNodos() == true) {
           Object contenido = iterador.getContenido();
           iterador = iterador.getNodoIzq();
           return contenido;
       } else {
           return null;
       }
    }
}