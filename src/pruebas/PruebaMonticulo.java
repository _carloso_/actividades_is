package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasnolineales.Monticulo;
import herramientas.comunes.TipoOrden;

public class PruebaMonticulo {
    public static void main(String[] args) {
        Monticulo monticulo = new Monticulo(TipoOrden.DESC);

        monticulo.agregar(30);
        monticulo.agregar(48);
        monticulo.agregar(15);
        monticulo.agregar(67);
        monticulo.agregar(24);
        monticulo.agregar(17);
        monticulo.agregar(5);
        // Datos: 30, 48, 15, 67, 24, 17, 5

        SalidaPorDefecto.consola("Recorrido del monticulo: " + "\n");
        SalidaPorDefecto.consola("Inorden: ");
        monticulo.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: ");
        monticulo.preOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: ");
        monticulo.postOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Amplitud: ");
        monticulo.recorridoAmplitudRecursivo();

        //monticulo.eliminar(5);

        /*
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorrido del monticulo: " + "\n");
        SalidaPorDefecto.consola("Inorden: ");
        monticulo.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: ");
        monticulo.preOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: ");
        monticulo.postOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Amplitud: ");
        monticulo.recorridoAmplitudRecursivo();

         */

    }
}
