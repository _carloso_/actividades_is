package registros.campaniavacunacioncovid;

public class Vacuna {

    private int numVacuna;
    private String nombre;

    public Vacuna(int numVacuna, String nombre) {
        this.numVacuna = numVacuna;
        this.nombre = nombre;
    }

    public int getNumVacuna() {
        return numVacuna;
    }

    public void setNumVacuna(int numVacuna) {
        this.numVacuna = numVacuna;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Override
    public String toString() {
        return  "NO. Vacuna:" + numVacuna + "\n" +
                "Nombre:" + nombre + "\n";
    }
}
