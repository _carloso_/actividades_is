package herramientas.matematicas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.PilaEstatica;

public class ExpresionesAritmeticas {
    public static char tokenPrioritario2;

    /**
     *  Este metodo convierte la infija en postfija
     * @param infija este metodo recibe la infija de parametro tipo string
     * @return regresa la postija
     */
    public static String infijaAPostfija(String infija) {
        PilaEstatica pila2 = new PilaEstatica(infija.length());
        String postfija = "";
        for (int indiceToken = 0; indiceToken < infija.length(); indiceToken++) {
            char token = infija.charAt(indiceToken);
            if (esOperando(token) == true) {  //se verifica que el valor de la posicion sea operando
                postfija += token;     //si es operando se le suma al String
            }
            else{
                pila2.poner(token); //si no lo es, se va a la pila para usarlo despues
            }
            if (token == '('){ //se va a verificar si hay un token de mayor o igual prioridad para sacarlo de la pila y sumarlo al string
                char tokenPrioritario = infija.charAt(indiceToken-1); //me muevo una posicion atras del parentesis para ver si encuentro un operador de importancia
                if (tokenPrioritario == '*' || tokenPrioritario == '/' || tokenPrioritario == '^'){
                    pila2.quitar();
                    tokenPrioritario2 = tokenPrioritario; //a la variable static se le asigna el valor local para asi poderlo usar en otro bloque
                }
            }
            if (token == ')' ){
                pila2.quitar(); //cuando hay parentesis, inmediatamente se quita para no sumarlo al String al final
                for (int cont = indiceToken-1; cont >= 0; cont--){    //se busca el operador (, empezando a recorrer la pila desde el operando )
                    char token2 = infija.charAt(cont); //se crea un nuevo token para este for, asi no se confunidiran
                    if (esOperando(token2) == false) {  //como estamos verificando los valores dentro de el parentesis, los operadores que esten entre ellos se van a sumar al string
                        postfija +=  token2; //se suma el operador al string
                        pila2.quitar(); //despues de sumar el operador al string, ya no lo necesitamos mas en la pila, asi que se quita
                    }
                    if (token2 == '(' ){
                        postfija = postfija.replace("(", ""); //como se suman los operadores, es imposible que se sume el parentesis (, por lo que lo remplazare por un valor nulo
                        break; //cuando encuentra el parentesis, se rompe el ciclo
                    }
                }
                //se agrega en esta parte del codigo, ya que primero se tienen que sumar al string los valores que estan adentro del parentesis
                postfija += tokenPrioritario2; //se le suma el token prioritario al string
            }

        }
        for (int cont = 0; cont < pila2.maximo()-1  ; cont++) {  //al terminar el ciclo, al string se le suman los valores de la pila, empezando desde el ultimo
            postfija += pila2.verUltimo(); //se obtiene el ultimo valor de la pila y se suma en el string
            pila2.quitar(); //se quita el ultimo valor para segior recorriendo la pila con el ultimo valor
            if (pila2.verUltimo() == null){
                break;
            }
        }
        pila2.imprimir();
        return postfija;
    }

    public static String infijaPrefija(String infija) {
        return "";
    }

    /**
     * este metodo resuelve la postfija, dando el resultado de la operacion
     * @param postfija este metodo recibe la postfija de parametro tipo string
     * @return regresa el producto de la postfija
     */
    public static Double evaluarPostfija(String postfija) {
        PilaEstatica pila = new PilaEstatica(postfija.length());
        //1) Tokenizar la expresión postfija.
        //  a b c d - * e f ^ /  +
        //  0 1 2 3 4 5 6 7 8 9 10, índiceToken
        for (int indiceToken = 0; indiceToken < postfija.length(); indiceToken++) {
            //2) Analizar de uno por uno los tokens.
            char token = postfija.charAt(indiceToken);
            if (esOperando(token) == true) {
                //2.1) Si el token es Operando, se mete en la pila.
                if (pila.poner("" + token) == false) { //no hay espacio en la pila
                    return null;
                }
            } else { //es operador
                //2.2) Si el token es Operador, se sacan de la pila dos operandos
                //(el primer operando que se saque equivale a OP2).
                // Se aplica la operación del token y el resultado se mete en la
                //pila.
                String operando2 = (String) pila.quitar();
                String operando1 = (String) pila.quitar();

                if (operando1 == null || operando2 == null) { //no había elementos que sacar
                    return null;
                } else { //seguimos con el proceso
                    Double operacionParcial = aplicarOperacionA(token, Double.parseDouble(operando1),
                            Double.parseDouble(operando2));
                    if (operacionParcial == null) { //el operador es inválido
                        return null;
                    } else { //seguimos
                        if (pila.poner("" + operacionParcial) == false) { //no hab{ia espacio
                            return null;
                        }
                    }
                }
            }
        } //for
        //3) El resultado de la operación queda en la pila.
        String resultadoEvaluacion = (String) pila.quitar();

        if (resultadoEvaluacion == null) { //hubo error
            return null;
        } else {
            return Double.parseDouble(resultadoEvaluacion);
        }
    }

    public static Double aplicarOperacionA(char operador, double operando1, double operando2) {
        if (operador == '+') {
            return operando1 + operando2;
        } else if (operador == '-') {
            return operando1 - operando2;
        } else if (operador == '*') {
            return operando1 * operando2;
        } else if (operador == '/') {
            if (operando2 == 0) {
                return null;
            } else {
                return operando1 / operando2;
            }
        } else if (operador == '%') {
            return operando1 % operando2;
        } else if (operador == '^') {
            return Math.pow(operando1, operando2);
        } else { //no es un operador del catálogo
            return null;
        }
    }

    public static boolean esOperando(char token) {
        if (token == '+' || token == '-' || token == '*' || token == '/' || token == '^' || token == '(' || token == ')' || token == '%') {
            return false; //no es operando
        } else { //como no es operador y otro simbolo, es operando
            return true;
        }
    }

    public static boolean esParentesisIzq(char token) {
        if (token == ')' ) {
            return true; //es parentesis
        } else { //no es operando
            return false;
        }
    }

    public static double evaluarPrefija(String prefija) {
        return 0.0;
    }

    /**
     * Este metodo verifica que el la variable no tenga mas de 50 caracteres
     * @param valor es el valor de tipo cade,a ya que asi podemos verificar su longitud
     * @return regresa el valor de tipo double, si es que cumple con la verificación, si no, regresa 0
     */
    public static double agregarValor(String valor){
        double valorVerificado = 0;
        if (valor.length() <= 50 ){ //se verifica que los caracteres de la variable no pasen de 50
            valorVerificado = Double.parseDouble(valor); //se castea el valor de String a double para realizar operaciones con el.
            return valorVerificado;
        }
        else{
            SalidaPorDefecto.consola("los caracteres de la variable sobrepasan el limite, intentelo de nuevo \n");
            return 0;
        }
    }

}
