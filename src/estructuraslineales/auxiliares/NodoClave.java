package estructurasLineales.auxiliares;

/**
 * Esta clase representa un nodo de una lista enlazada de claves y valores.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class NodoClave {

    protected Object clave;
    protected Object contenido;
    protected NodoClave nodoDer;

    public NodoClave(Object clave, Object valor) {
        this.clave = clave;
        this.contenido = valor;
        nodoDer = null;
    }

    /**
     * Este método retorna la clave del nodo.
     * @return Regresa la clave del nodo.
     */
    public Object getClave() {
        return clave;
    }

    /**
     * Este método retorna el valor del nodo.
     * @return Regresa el contenido del nodo.
     */
    public Object getContenido() {
        return contenido;
    }

    /**
     * Este método retorna la referencia al siguiente nodo derecho.
     * @return Regresa la referencia al siguiente nodo derecho.
     */
    public NodoClave getNodoDer() {
        return nodoDer;
    }

    /**
     * Este método establece la referencia al siguiente nodo derecho.
     * @param nodoDer Referencia al siguiente nodo derecho.
     */
    public void setNodoDer(NodoClave nodoDer) {
        this.nodoDer = nodoDer;
    }

    /**
     * Este método asigna el contenido del nodo.
     * @param contenido Es el contenido que se asignará al nodo.
     */
    public void setContenido(Object contenido) {
        this.contenido = contenido;
    }

    /**
     * Este método asigna la clave del nodo.
     * @param clave Es la clave que se asignará al nodo.
     */
    public void setClave(Object clave) {
        this.clave = clave;
    }

    @Override
    public String toString() {
        return clave.toString() + " " + contenido.toString();
    }
}