package estructurasnolineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.auxiliares.NodoDoble;
import herramientas.generales.Utilerias;

/**
 * Esta clase representa un TDA Arbol Binario de Búsqueda (ABB).
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class ArbolBinarioBusqueda extends ArbolBinario {

    public ArbolBinarioBusqueda() {
        super();
    }

    /**
     * Este metodo agrega un nuevo nodo al arbol ABB.
     * @param valor Es el valor del nodo que se agregara al arbol.
     * @return Regresa <b>true</b> si el nodo se agrego correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregar(Object valor) {
        if (raiz == null) { // el valor lo agregamos como la nueva raiz
            NodoDoble nuevoNodo = new NodoDoble(valor);
            if (nuevoNodo != null) { // si hay espacio
                raiz = nuevoNodo;
                return true;
            } else { // no hay espacio o hay un error
                return false;
            }
        } else { // ya hay nodos en el arbol
            return agregar(raiz, valor);
        }
    }

    /**
     * Este metodo agrega un nuevo nodo al arbol ABB.
     * @param subRaiz Es la raiz del subarbol en donde se buscara el nodo.
     * @param valor Es el valor del nodo a insertar.
     * @return Regresa <b>true</b> si el nodo se agrego correctamente, <b>false</b> si no se pudo agregar.
     */
    private boolean agregar(NodoDoble subRaiz, Object valor) {
        // comparar si el valor es menor o mayor a la subraiz
        if (Utilerias.compararObjetos(valor, subRaiz.getContenido()) < 0) { // valor < subraiz
            if (subRaiz.getNodoIzq() == null) { // llegamos a la posicion en donde le toca ser insertado
                NodoDoble nuevoNodo = new NodoDoble(valor);
                if (nuevoNodo != null) { // si hay espacio
                    subRaiz.setNodoIzq(nuevoNodo);
                    return true;
                } else { // no hay espacio o hay un error
                    return false;
                }
            } else { // hacemos llamada recursiva hacia esa rama
                return agregar(subRaiz.getNodoIzq(), valor);
            }
        } else if (Utilerias.compararObjetos(valor, subRaiz.getContenido()) > 0) { // valor > subraiz
            if (subRaiz.getNodoDer() == null) { // llegamos a la posicion en donde le toca ser insertado
                NodoDoble nuevoNodo = new NodoDoble(valor);
                if (nuevoNodo != null) { // si hay espacio
                    subRaiz.setNodoDer(nuevoNodo);
                    return true;
                } else { // no hay espacio o hay un error
                    return false;
                }
            } else { // hacemos llamada recursiva hacia esa rama
                return agregar(subRaiz.getNodoDer(), valor);
            }
        } else { // el valor ya existe en el arbol (valor == subraiz)
            return false;
        }
    }

    /**
     * Este metodo busca un nodo en el arbol ABB.
     * @param valor Es el valor del nodo a buscar.
     * @return Regresa el contenido del nodo buscado, <b>null</b> si no se encontro el nodo.
     */
    public Object buscar(Object valor) {
        return buscar(raiz, valor);
    }

    /**
     * Este metodo busca un nodo en el arbol ABB.
     * @param subRaiz Es la raiz del subarbol en donde se buscara el nodo.
     * @param valor Es el valor del nodo a buscar.
     * @return Regresa el contenido del nodo buscado, <b>null</b> si no se encontro el nodo.
     */
    private Object buscar(NodoDoble subRaiz, Object valor) {
        if (subRaiz != null) { // hay donde buscar
            if (Utilerias.compararObjetos(valor, subRaiz.getContenido()) < 0) { // valor < subraiz
                return buscar(subRaiz.getNodoIzq(), valor);
            } else if (Utilerias.compararObjetos(valor, subRaiz.getContenido()) > 0) { // valor > subraiz
                return buscar(subRaiz.getNodoDer(), valor);
            } else { // valor == subraiz (encontramos el nodo buscado)
                return subRaiz.getContenido();
            }
        } else { // no hay donde buscar
            return null;
        }
    }

    /**
     * Este metodo elimina un nodo del arbol ABB.
     * @param valor Es el valor del nodo a eliminar.
     */
    public void eliminar(Object valor) {
        if (raiz != null) { // hay nodos en el arbol
            eliminar(raiz, null, valor);
        } else { // no hay nodos en el arbol
            SalidaPorDefecto.consola("No hay nodos en el arbol.");
        }
    }

    /**
     * Este metodo elimina un nodo del arbol ABB.
     * @param subRaiz Es la raiz del subarbol que se esta recorriendo.
     * @param anterior Es el nodo anterior al que se esta recorriendo.
     * @param valor Es el valor del nodo a eliminar.
     */
    public void eliminar(NodoDoble subRaiz, NodoDoble anterior, Object valor) {
        NodoDoble aux1 = null;
        NodoDoble aux2 = null;
        NodoDoble temp = null;
        if (Utilerias.compararObjetos(valor, subRaiz.getContenido()) < 0) { // valor < subraiz
            eliminar(subRaiz.getNodoIzq(), subRaiz, valor);
        } else {
            if (Utilerias.compararObjetos(valor, subRaiz.getContenido()) > 0) { // valor > subraiz
                eliminar(subRaiz.getNodoDer(), subRaiz, valor);
            } else { // valor == subraiz
                if (subRaiz.getNodoIzq() != null && subRaiz.getNodoDer() != null) { // subraiz tiene dos hijos
                    // buscamos el nodo que va a reemplazar al nodo a eliminar (se saca el mas a la derecha del subarbol izquierdo)
                    aux1 = subRaiz.getNodoIzq();
                    boolean encontrado = false;
                    while (aux1.getNodoDer() != null) {
                        aux2 = aux1;
                        aux1 = aux1.getNodoDer();
                        encontrado = true;
                    }
                    subRaiz.setContenido(aux1.getContenido());
                    if (encontrado == true) {
                        aux2.setNodoDer(aux1.getNodoIzq());
                    } else {
                        subRaiz.setNodoIzq(aux1.getNodoIzq());
                    }
                } else { // el derecho, izquierdo o ambos son null
                    temp = subRaiz;
                    if (temp.getNodoDer() == null) {
                        if (temp.getNodoIzq() != null) {
                            temp = subRaiz.getNodoIzq();
                            if (anterior != null) {
                                if (Utilerias.compararObjetos(valor, anterior.getContenido()) < 0) { // valor < anterior
                                    anterior.setNodoIzq(temp);
                                } else { // valor > anterior
                                    anterior.setNodoDer(temp);
                                }
                            } else { // anterior es null
                                raiz = temp;
                            }
                        } else { // ambos son null
                            if (anterior == null) { // raiz solamente
                                raiz = null;
                            } else { // anterior no es null
                                if (Utilerias.compararObjetos(valor, anterior.getContenido()) < 0) { // valor < anterior
                                    anterior.setNodoIzq(null);
                                } else { // valor > anterior
                                    anterior.setNodoDer(null);
                                }
                            }
                        }
                    } else { // la derecha no es null
                        if (temp.getNodoIzq() == null) { // solo la izquierda es null
                            temp = subRaiz.getNodoDer();
                            if (anterior != null) {
                                if (Utilerias.compararObjetos(valor, anterior.getContenido()) < 0) { // valor < anterior
                                    anterior.setNodoIzq(temp);
                                } else { // valor > anterior
                                    anterior.setNodoDer(temp);
                                }
                            } else { // anterior es null
                                raiz = temp;
                            }
                        } else { // ambos son null
                            if (anterior == null) { // raiz solamente
                                raiz = null;
                            } else { // anterior no es null
                                if (Utilerias.compararObjetos(valor, anterior.getContenido()) < 0) { // valor < anterior
                                    anterior.setNodoIzq(null);
                                } else { // valor > anterior
                                    anterior.setNodoDer(null);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
