package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaDePrioridad;
import estructurasLineales.ColaEstatica;

public class PruebaColaDePrioridad {
    public static void main(String[] args) {
        ColaDePrioridad colaP = new ColaDePrioridad(5);
        colaP.poner(5);
        colaP.poner(4);
        colaP.poner(3);
        colaP.poner(7);
        colaP.poner(9);
        colaP.poner(1);

        colaP.imprimir();
        SalidaPorDefecto.consola("\n");
        colaP.ordenarPrioridad();
        colaP.imprimir();
    }
}
