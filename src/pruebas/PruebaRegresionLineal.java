package pruebas;

import entradaSalida.SalidaPorDefecto;
import examenes.parcial1.RegresionLineal;

public class PruebaRegresionLineal {
    public static void main(String[] args) {

        RegresionLineal rl = new RegresionLineal("C:\\Users\\Cristian\\Desktop\\x.txt", "C:\\Users\\Cristian\\Desktop\\y.txt");
        SalidaPorDefecto.consola("--- Ecuación de la recta de regresion lineal ---" + "\n");
        rl.mostrarEcuacionDeRegresion();

        SalidaPorDefecto.consola("Sumatoria (xi - promedio(x))^2: " + rl.sumatoriaCuadradaDeXiMenosPromedioX() + "\n");
        SalidaPorDefecto.consola("Sumatoria (xi - promedio(x)) * (yi - promedio(y)): " + rl.sumatoriaXiMenosPromedioXPorYiMenosPromedioY() + "\n");

        rl.precioEstimado();

    }
}
