package estructurasLineales.auxiliares;

/**
 * Esta clase representa un nodo doble, que contiene un elemento y un puntero al nodo derecho y al nodo izquierdo.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class NodoDoble {

    protected Object contenido;
    protected NodoDoble nodoDer;
    protected NodoDoble nodoIzq;

    public NodoDoble(Object contenido) {
        this.contenido = contenido;
        nodoDer = null;
        nodoIzq = null;
    }

    public Object getContenido() {
        return contenido;
    }

    public NodoDoble getNodoDer() {
        return nodoDer;
    }

    public NodoDoble getNodoIzq() {
        return nodoIzq;
    }

    public void setContenido(Object contenido) {
        this.contenido = contenido;
    }

    public void setNodoDer(NodoDoble nodoDer) {
        this.nodoDer = nodoDer;
    }

    public void setNodoIzq(NodoDoble nodoIzq) {
        this.nodoIzq = nodoIzq;
    }

    @Override
    public String toString() {
        return contenido.toString();
    }
}