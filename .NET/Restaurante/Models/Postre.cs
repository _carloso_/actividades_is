using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restaurante.Models
{
    public class Postre
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Nombre del Postre")]
        public string Nombre { get; set; }
        [Required]
        [Display(Name = "Descripción del Postre")]
        public string Descripcion { get; set; }
        public double? Precio { get; set; }
        [Display(Name = "Imagen del Postre")]
        public string? UrlImagen { get; set; }
    }
}