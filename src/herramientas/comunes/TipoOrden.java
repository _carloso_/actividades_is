package herramientas.comunes;

/**
 * Este enumerado representa los tipos de ordenamiento que se pueden realizar en una TDA.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public enum TipoOrden {
    ASC("ASENDENTE", 1), DESC("DESENDENTE", 2);

    private String nombre;
    private int valor;

    /**
     * Constructor de la clase.
     * @param nombre Nombre del tipo de ordenamiento.
     * @param valor Valor del tipo de ordenamiento.
     */
    private TipoOrden(String nombre, int valor) {
        this.nombre = nombre;
        this.valor = valor;
    }

    /**
     * Metodo que retorna el nombre del tipo de ordenamiento.
     * @return Nombre del tipo de ordenamiento.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que retorna el valor del tipo de ordenamiento.
     * @return Valor del tipo de ordenamiento.
     */
    public int getValor() {
        return valor;
    }
}