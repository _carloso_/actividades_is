package estructurasLineales;

public class ColaDePrioridad extends ColaEstatica {
    protected Object datos[];
    protected int primero;
    protected int ultimo;
    protected int MAXIMO;
    public ColaDePrioridad(int maximo) {
        super(maximo);
        MAXIMO=maximo;
        datos=new Object[MAXIMO];
        primero=-1;
        ultimo=-1;
    }

    @Override
    public void ordenarPrioridad() {
        super.ordenarPrioridad();
    }

    @Override
    public boolean poner(Object valor) {
        return super.poner(valor);
    }

    @Override
    public Object quitar() {
        return super.quitar();
    }

    @Override
    public Object verUltimo() {
        return super.verUltimo();
    }

    @Override
    public Object verPrimero() {
        return super.verPrimero();
    }

    @Override
    public void imprimir() {
        super.imprimir();
    }
}
