package registros.ordenacionpersonas;

/**
 * Este enumerado define los criterios de ordenación de las personas.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public enum TipoCriterio {
    NOMBRE("nombre", 1), EDAD("edad", 2);

    private String nombre;
    private int valor;

    private TipoCriterio(String nombre, int valor) {
        this.nombre = nombre;
        this.valor = valor;
    }

    public String getNombre() {
        return nombre;
    }

    public int getValor() {
        return valor;
    }
}
