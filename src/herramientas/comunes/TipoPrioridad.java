package herramientas.comunes;

/**
 * Este enumerado representa los tipos de prioridad que puede tener una tarea.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public enum TipoPrioridad {
   MENOR_A_MAYOR("Menor a mayor", 1), MAYOR_A_MENOR("Mayor a menor", 2);

    private String nombre;
    private int valor;

    private TipoPrioridad(String nombre, int valor) {
        this.nombre = nombre;
        this.valor = valor;
    }

    /**
     * Nos permite obtener el nombre del tipo de prioridad.
     * @return Regresa el nombre del tipo de prioridad.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Nos permite obtener el valor del tipo de prioridad.
     * @return Regresa el valor del tipo de prioridad.
     */
    public int getValor() {
        return valor;
    }
}