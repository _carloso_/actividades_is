package estructurasLineales;

public class ColaDinamica implements LoteAlmacenamiento{

    private ListaDinamica cola;

    public ColaDinamica(){
        cola = new ListaDinamica();
    }

    @Override
    public boolean vacio() {
        return cola.vacia();
    }

    @Override
    public boolean lleno() {
        return false;
    }

    @Override
    public boolean poner(Object valor) {
        int valorRetorno = cola.agregarInicio(valor);
        if (valorRetorno == 0){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object quitar() {
        return cola.eliminar();
    }

    @Override
    public void imprimir() {
        cola.imprimir();
    }

    @Override
    public Object verUltimo() {
        return cola.verUltimo();
    }
}