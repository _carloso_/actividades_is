package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasNoLineales.ArbolExpAritm;

public class PruebaAEA {
    public static  void main(String [] args){
        ArbolExpAritm arbol=new ArbolExpAritm();

        arbol.generarArbolExpresiones("z^n - e / g + (b * d) -");

        SalidaPorDefecto.consola("Imprimiendo el árbol: \n");
        SalidaPorDefecto.consola("Inorden: ");
        arbol.inorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: ");
        arbol.postorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: ");
        arbol.preorden();
        SalidaPorDefecto.consola("\n");

    }
}
