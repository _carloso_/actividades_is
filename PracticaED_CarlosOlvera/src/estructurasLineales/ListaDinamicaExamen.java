package estructurasLineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.auxiliares.Nodo;
/*
Actividad 3 examen 2
 */
public class ListaDinamicaExamen {

    protected int contador = 0; //contador que contara todos los nodos de la lista
    ListaDinamica listaSuperior;//lista que estara en la posicion superior
    ListaDinamica listaInferior;//lista que estara en la posicion inferior
    Nodo primero;
    Nodo ultimo;
    /*
     Se inicializan dos listas dinamicas, una inferior y otra superior
    */
    public ListaDinamicaExamen(){
        listaSuperior = new ListaDinamica();
        listaInferior = new ListaDinamica();
        primero = null;
        ultimo = null;
    }

    /*
    Metodo que agrega al final de la lista
     */
    public int agregar(Object valor){
        if(valor!=null){ //hay espacio
            if (contador % 2 == 0){
                listaSuperior.agregar(valor);
                primero = listaSuperior.primero; //se inicializa la variable primero, ya que el primer valor de la lista esta en la parte superior
                contador++;
            }
            else{
                listaInferior.agregar(valor);
                primero = listaInferior.primero;
                contador++;
            }
            return 0;
        }
        else{  //no hay espacio
            return -1;
        }
    }

    /*
    Metodo que agrega un valor al inicio
     */
    public int agregarInicio(Object valor){
        ListaDinamica listaDinamicaTemporalSuperior = new ListaDinamica();
        ListaDinamica listaDinamicaTemporalInferior = new ListaDinamica();
        Nodo iterador1 = listaSuperior.primero;  //se inicializa el nodo de la lista Superior
        Nodo iterador2 = listaInferior.primero;  //Se inicializa el nodo de la lista inferior
        int contadortemporal = 0;
        if (valor != null){
            listaDinamicaTemporalSuperior.agregar(valor); //se agrega el valor al principio de la lista superior
            contadortemporal++;
            while (iterador1 != null || iterador2 != null){ //se guardan los valores respectivos de cada lista en una lista temporal
                if(contadortemporal % 2 != 0){ //de esta forma se reacomodan los valores
                    listaDinamicaTemporalSuperior.agregar(iterador2.getContenido());
                    listaDinamicaTemporalInferior.agregar(iterador1.getContenido());
                }
                else{
                    listaDinamicaTemporalInferior.agregar(iterador1.getContenido());
                    listaDinamicaTemporalSuperior.agregar(iterador2.getContenido());
                }
                contadortemporal++;//contador temporal para saber en que lista deben ir
                iterador1=iterador1.getNodoDer();
                iterador2=iterador2.getNodoDer();
            }
            listaSuperior = listaDinamicaTemporalSuperior; //se reemplazan las viejas listas por las nuevas ya modificadas
            primero = listaSuperior.primero; //se inicializa la variable primero, ya que el primer valor de la lista esta en la parte superior
            listaInferior = listaDinamicaTemporalInferior;
            contador++; //se le suma al contador general de la clase
            return 0;
        }
        else{
            return -1;
        }
    }
    /*
    Metodo que elimina el ultimo valor de la lista
     */
    public void eliminar(){
        if (contador%2 == 0){ //elimina el ultimo valor de la lista superior si el contador es inpar
            listaInferior.eliminar();
            contador--;
        }
        else {
            listaSuperior.eliminar();//elimina el ultimo valor de la lista inferior si el contador es par
            contador--;
        }
    }
    /*
    Metodo que elimina un valor especifico de una lista
     */
    public void eliminar(Object valor) {
        ListaDinamica listaDinamicaTemporalSuperior = new ListaDinamica(); //se crea una lista temporal que almacenara los datos temporalmente
        ListaDinamica listaDinamicaTemporalInferior = new ListaDinamica();
        Nodo iterador1 = listaSuperior.primero;  //se inicializa el nodo de la lista Superior
        Nodo iterador2 = listaInferior.primero;  //Se inicializa el nodo de la lista inferior
        int contadortemporal = 0;
        if (listaInferior.eliminar(valor) != null) {//primero vemos si el valor que queremos eliminar esta en la lista inferior (elimna de ultimos a primeros valores)
            listaInferior.eliminar(valor);
            contador--;
        } else {//si no se elimina en la inferior, procedemos a eliminarlo en la superior
            listaSuperior.eliminar(valor);
            contador--;
        }
        while ( iterador1 != null && iterador2 != null) { //se guardan los valores respectivos de cada lista en una lista temporal
            if (contadortemporal % 2 != 0) { //de esta forma se reacomodan los valores
                listaDinamicaTemporalSuperior.agregar(iterador2.getContenido());
                listaDinamicaTemporalInferior.agregar(iterador1.getContenido());
            } else {
                listaDinamicaTemporalInferior.agregar(iterador1.getContenido());
                listaDinamicaTemporalSuperior.agregar(iterador2.getContenido());
            }
            contadortemporal++;//contador temporal para saber en que lista deben ir
            iterador1 = iterador1.getNodoDer();
            iterador2 = iterador2.getNodoDer();
        }
        listaSuperior = listaDinamicaTemporalSuperior; //se reemplazan las viejas listas por las nuevas ya modificadas
        listaInferior = listaDinamicaTemporalInferior;
    }

    /*
    Se elimina el primer valor de la lista
     */
    public void eliminarInicio(){
        ListaDinamica listaDinamicaTemporalSuperior = new ListaDinamica();
        ListaDinamica listaDinamicaTemporalInferior = new ListaDinamica();
        listaSuperior.eliminarInicio();//se elimina el primer valor de la lista superior
        Nodo iterador1 = listaSuperior.primero;  //se inicializa el nodo de la lista Superior
        Nodo iterador2 = listaInferior.primero;  //Se inicializa el nodo de la lista inferior
        int contadortemporal = 0;
            while (iterador2 != null){ //se guardan los valores respectivos de cada lista en una lista temporal
                if(contadortemporal % 2 != 0){ //de esta forma se reacomodan los valores
                    listaDinamicaTemporalSuperior.agregar(iterador2.getContenido());
                    listaDinamicaTemporalInferior.agregar(iterador1.getContenido());
                }
                else {
                    listaDinamicaTemporalInferior.agregar(iterador1.getContenido());
                    listaDinamicaTemporalSuperior.agregar(iterador2.getContenido());
                }
                contadortemporal++;//contador temporal para saber en que lista deben ir
                iterador1=iterador1.getNodoDer();
                iterador2=iterador2.getNodoDer();
            }
            listaSuperior = listaDinamicaTemporalSuperior; //se reemplazan las viejas listas por las nuevas ya modificadas
            primero = listaSuperior.primero; //se inicializa la variable primero, ya que el primer valor de la lista esta en la parte superior
            listaInferior = listaDinamicaTemporalInferior;
            contador--; //se le suma al contador general de la clase
    }
    /*
    Metodo para buscar un valor especifico
     */
    public Object buscar(Object valor){
        Object encontrado = listaInferior.buscar(valor); //primero busca el valor en la segunda lista y lo almacena en una variable
        if (encontrado != null){ //si encuentra el valor lo regresa
            return encontrado;
        }
        else{
            encontrado = listaSuperior.buscar(valor); //si no, lo busca en la otra lista y almacena el resultado en una variable
            if (encontrado != null){ //si encuentra el valor lo regresa
                return encontrado;//si encuentra el valor lo regresa
            }
            else {
                return null; //si no lo encuentra, regresa null
            }
        }
    }
    /*
    Metodo que imprime las listas, y las flechas que las relacionan
     */
    public void imprimir(){
        if(listaSuperior.vacia()==true){
            SalidaPorDefecto.consola("null");
        }else{
            Nodo iterador1 = listaSuperior.primero; //el nodo temporal empieza del inicio
            while (iterador1 != listaSuperior.ultimo){ //MIENTRAS EL ITERADOR NO SEA EL ULTIMO
                SalidaPorDefecto.consola(iterador1.getContenido()+ " -> ");
                iterador1=iterador1.getNodoDer();
            }
            SalidaPorDefecto.consola(iterador1.getContenido()+ " -> null\n");

            //esto nos servira solo para imprmimir las flechas
            Nodo iteradorFlechas = listaSuperior.primero; //el nodo temporal empieza del inicio
            SalidaPorDefecto.consola( "↑↓   ");
            while (iteradorFlechas != listaSuperior.ultimo){ //MIENTRAS EL ITERADOR NO SEA EL ULTIMO
                SalidaPorDefecto.consola( "↑↓   ");
                iteradorFlechas=iteradorFlechas.getNodoDer();
            }
            SalidaPorDefecto.consola("\n ");

            Nodo iterador2 = listaInferior.primero; //el nodo temporal empieza del inicio
            while (iterador2 != listaInferior.ultimo){ //MIENTRAS EL ITERADOR NO SEA EL ULTIMO
                SalidaPorDefecto.consola(iterador2.getContenido()+ " -> ");
                iterador2=iterador2.getNodoDer();
            }
            SalidaPorDefecto.consola(iterador2.getContenido()+ " -> null");
        }
    }
}
