package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasLineales.ListaEstaticaNumerica;

public class PruebaListaEstaticaNumerica {
    public static void main(String[] args) {
        ListaEstaticaNumerica listaNumerica1 = new ListaEstaticaNumerica(8);
        listaNumerica1.agregar(60);
        listaNumerica1.agregar(298.23);
        listaNumerica1.agregar(340);
        listaNumerica1.agregar(-20);
        listaNumerica1.agregar(72.5);
        listaNumerica1.agregar("H");
        listaNumerica1.agregar("T");
        SalidaPorDefecto.consola("****** Lista Numerica 1 ******" + "\n");
        listaNumerica1.imprimir();

        SalidaPorDefecto.consola(">Buscando el elemento 340 en la lista: " + listaNumerica1.buscar(340) + "\n");
        SalidaPorDefecto.consola(">Eliminar el elemento de indice 1: " + listaNumerica1.eliminar(1) + "\n");
        listaNumerica1.imprimir();
        SalidaPorDefecto.consola(">Multiplicar la lista Por 2: " + listaNumerica1.porEscalar(2) + "\n");
        listaNumerica1.imprimir();
        SalidaPorDefecto.consola(">Sumar la lista con 10: " + listaNumerica1.sumarEscalar(10) + "\n");
        listaNumerica1.imprimir();

        ListaEstaticaNumerica listaNumerica2 = new ListaEstaticaNumerica(8);
        listaNumerica2.agregar(20);
        listaNumerica2.agregar(-100);
        listaNumerica2.agregar(15);
        listaNumerica2.agregar(5);
        SalidaPorDefecto.consola("****** Lista Numerica 2 ******" + "\n");
        listaNumerica2.imprimir();

        SalidaPorDefecto.consola(">Suma de las listas 1 y 2: " +
                listaNumerica1.sumar(listaNumerica2) + "\n");
        listaNumerica1.imprimir();
        SalidaPorDefecto.consola(">Multiplicacion de las listas 1 y 2: " +
                listaNumerica1.multiplicar(listaNumerica2) + "\n");
        listaNumerica1.imprimir();

        SalidaPorDefecto.consola(">Aplicar potencia 3 a la lista 2: " +
                listaNumerica2.aplicarPotencia(3) + "\n");
        listaNumerica2.imprimir();

        SalidaPorDefecto.consola("*** Lista Numerica 3 ***" + "\n");
        ListaEstaticaNumerica listaNumerica3 = new ListaEstaticaNumerica(6);
        listaNumerica3.agregar(5);
        listaNumerica3.agregar(3);
        listaNumerica3.agregar(8);
        listaNumerica3.agregar(4);
        listaNumerica3.imprimir();

        SalidaPorDefecto.consola("*** Lista Numerica 4 ***" + "\n");
        ListaEstaticaNumerica listaNumerica4 = new ListaEstaticaNumerica(6);
        listaNumerica4.agregar(4);
        listaNumerica4.agregar(1);
        listaNumerica4.agregar(0);
        listaNumerica4.agregar(2);
        listaNumerica4.imprimir();

        SalidaPorDefecto.consola("*** Lista Numerica 5 ***" + "\n");
        ListaEstaticaNumerica listaNumerica5 = new ListaEstaticaNumerica(6);
        listaNumerica5.agregar(3);
        listaNumerica5.agregar(2);
        listaNumerica5.agregar(7);
        listaNumerica5.agregar(5);
        listaNumerica5.imprimir();

        SalidaPorDefecto.consola(">Aplicar potecias de la lista 3 a la lista 4: " +
                listaNumerica3.aplicarPotencia(listaNumerica4) + "\n" + "*** Lista 3: ***" + "\n");
        listaNumerica3.imprimir();

        SalidaPorDefecto.consola(">Producto Escalar de la lista 3 y 4: " +
                listaNumerica3.productoEscalar(listaNumerica4) + "\n");

        SalidaPorDefecto.consola(">Norma de la lista 4: " + listaNumerica4.norma() + "\n");
        SalidaPorDefecto.consola(">Norma Euclidiana de la lista 4 y 5: " +
                listaNumerica4.normaEuclidiana(listaNumerica5) + "\n");

        // Esta listaEstatica almacena listasEstaticasNumericas
        ListaEstatica listaEstatica1 = new ListaEstatica(3);
        listaEstatica1.agregar(listaNumerica4);
        listaEstatica1.agregar(listaNumerica5);
        listaEstatica1.agregar(listaNumerica3);

        SalidaPorDefecto.consola(">Sumar listas con lista 3: " +
                listaNumerica3.sumarListaEstatica(listaEstatica1) + "\n");

        ListaEstaticaNumerica escalares = new ListaEstaticaNumerica(5);
        escalares.agregar(20);
        escalares.agregar(47);
        escalares.agregar(5);
        escalares.agregar(3);
        SalidaPorDefecto.consola("*** Lista de escalares ***" + "\n");
        escalares.imprimir();
        SalidaPorDefecto.consola(">Suma de la lista escalares más la lista 5: " +
                listaNumerica5.sumarEscalares(escalares) + "\n");

        SalidaPorDefecto.consola("*** Lista Numerica 5 ***" + "\n");
        listaNumerica5.imprimir();
        SalidaPorDefecto.consola("*** Lista De Indices ***" + "\n");
        ListaEstaticaNumerica listaDeIndices = new ListaEstaticaNumerica(5);
        listaDeIndices.agregar(0);
        listaDeIndices.agregar(2);
        listaDeIndices.agregar(8);
        listaDeIndices.agregar(3);
        listaDeIndices.agregar(6);
        listaDeIndices.imprimir();
        SalidaPorDefecto.consola("Sumar los indices 0, 2, 3 de la lista 5: " +
                listaNumerica5.sumarIndices(listaDeIndices) + "\n");

        ListaEstatica listaEstatica2 = new ListaEstatica(6);
        listaEstatica2.agregar("G");
        listaEstatica2.agregar("F");
        listaEstatica2.agregar("A");
        listaEstatica2.agregar("K");
        listaEstatica2.agregar("W");
        listaEstatica2.agregar("N");
        SalidaPorDefecto.consola("*** Lista Estatica ***" + "\n");
        listaEstatica2.imprimirOI();
        SalidaPorDefecto.consola("*** Sublista De La Lista Estatica 2 ***" + "\n");
        ListaEstatica sublistaEstatica = listaEstatica2.subLista(listaDeIndices);
        sublistaEstatica.imprimirOI();

        SalidaPorDefecto.consola("*** Lista Numerica 2 ***" + "\n");
        listaNumerica2.imprimir();
        SalidaPorDefecto.consola("*** Sublista De La Lista Numérica 2 ***" + "\n");
        ListaEstaticaNumerica sublistaNumerica = listaNumerica2.subLista(listaDeIndices);
        sublistaNumerica.imprimir();

        ListaEstaticaNumerica vectro1 = new ListaEstaticaNumerica(5);
        vectro1.agregar(0);
        vectro1.agregar(-1);
        vectro1.agregar(1);
        vectro1.agregar(2);
        SalidaPorDefecto.consola("*** Vector 1 ***" + "\n");
        vectro1.imprimir();

        ListaEstaticaNumerica vectro2 = new ListaEstaticaNumerica(5);
        vectro2.agregar(2);
        vectro2.agregar(1);
        vectro2.agregar(1);
        vectro2.agregar(0);
        SalidaPorDefecto.consola("*** Vector 2 ***" + "\n");
        vectro2.imprimir();

        SalidaPorDefecto.consola("los Vectores 1 y 2 Son Ortogonales: " +
                vectro1.esOrtogonal(vectro2) + "\n");


    }
}
