package estructurasLineales;

import estructurasLineales.auxiliares.Nodo;

import static estructurasLineales.TipoOrden.*;

public class ListaDinamicaOrdenada extends ListaDinamica{
    TipoOrden tipoOrden;
    public ListaDinamicaOrdenada(TipoOrden orden){
        super();
        tipoOrden = orden;
    }

    public int agregar(Object valor) {
        Nodo nuevoNodo = new Nodo(valor);
        int valorRetorno = -1;
        if (nuevoNodo != null) {
            if (!vacia()) {
                if (tipoOrden == TipoOrden.INC) {
                    boolean agregadoINC = agregarINC(valor, nuevoNodo);
                    if (agregadoINC == true){
                        valorRetorno = 0;
                    }
                } else {
                    boolean agrgadoDEC = agregarDEC(valor, nuevoNodo);
                    if (agrgadoDEC == true){
                        valorRetorno = 0;
                    }
                }
            } else {
                primero = nuevoNodo;
                ultimo = nuevoNodo;
                valorRetorno = 0;
            }
        }
        return valorRetorno;
    }

    private boolean agregarDEC(Object valor, Nodo nuevoNodo){
        if (nuevoNodo != null){
            Nodo iterador = null;
            Nodo nodoActual = primero;
            while (nodoActual != null && nodoActual.getContenido().toString().compareTo(valor.toString()) > 0) {
                iterador = nodoActual;
                nodoActual = nodoActual.getNodoDer();
            }
            if (iterador == null) {
                nuevoNodo.setNodoDer(primero);
                primero = nuevoNodo;
            } else {
                nuevoNodo.setNodoDer(nodoActual);
                iterador.setNodoDer(nuevoNodo);
            }
            return true;
        } else {
            return false;
        }
    }

    private boolean agregarINC(Object valor, Nodo nuevoNodo) {
        if (nuevoNodo != null){
            Nodo nodoAnterior = null;
            Nodo iterador = primero;
            while (iterador != null && iterador.getContenido().toString().compareTo(valor.toString()) < 0) {
                nodoAnterior = iterador;
                iterador = iterador.getNodoDer();
            }
            if (nodoAnterior == null) {
                nuevoNodo.setNodoDer(primero);
                primero = nuevoNodo;
            } else {
                nuevoNodo.setNodoDer(iterador);
                nodoAnterior.setNodoDer(nuevoNodo);
            }
            return true;
        } else {
            return false;
        }
    }

    public Object buscar(Object valor) {
        if (!vacia()) {
            if (tipoOrden == TipoOrden.INC) { // buscar de forma ascendente
                Nodo iterador = primero;
                while (iterador != null && iterador.getContenido().toString().compareTo(valor.toString()) < 0) {
                    iterador = iterador.getNodoDer();
                }
                if (iterador != null && iterador.getContenido().toString().compareTo(valor.toString()) == 0) {
                    return iterador.getContenido();
                } else {
                    return null;
                }
            } else { // buscar de forma descendente
                Nodo iterador = primero;
                while (iterador != null && iterador.getContenido().toString().compareTo(valor.toString()) > 0) {
                    iterador = iterador.getNodoDer();
                }
                if (iterador != null && iterador.getContenido().toString().compareTo(valor.toString()) == 0) {
                    return iterador.getContenido();
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
    }

    public boolean cambiar(Object valorViejo, Object valorNuevo, int numVeces){
        boolean valorRetorno = super.cambiar(valorViejo, valorNuevo, numVeces);
        if (valorRetorno == true){
            ordenarLista();
            return true;
        } else {
            return false;
        }
    }

    private void ordenarLista(){
        if (tipoOrden == TipoOrden.INC){
            ordenarINC();
        } else {
            ordenarDEC();
        }
    }

    private void ordenarINC() {
        Nodo iterador1 = primero;
        Nodo iterador2;
        Object contenidoAux;
        while(iterador1 != null){
            iterador2 = iterador1.getNodoDer();
            while(iterador2 != null){
                if(iterador1.getContenido().toString().compareTo(iterador2.toString()) > 0){
                    contenidoAux = iterador1.getContenido();
                    iterador1.setContenido(iterador2.getContenido());
                    iterador2.setContenido(contenidoAux);
                }
                iterador2 = iterador2.getNodoDer();
            }
            iterador1 = iterador1.getNodoDer();
        }
    }

    private void ordenarDEC() {
        Nodo iterador1 = primero;
        Nodo iterador2;
        Object contenidoAux;
        while(iterador1 != null){
            iterador2 = iterador1.getNodoDer();
            while(iterador2 != null){
                if(iterador1.getContenido().toString().compareTo(iterador2.toString()) < 0){
                    contenidoAux = iterador1.getContenido();
                    iterador1.setContenido(iterador2.getContenido());
                    iterador2.setContenido(contenidoAux);
                }
                iterador2 = iterador2.getNodoDer();
            }
            iterador1 = iterador1.getNodoDer();
        }
    }

}
