package registros.galeriaDeArte;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasNoLineales.Matriz3;

public class ControlDeActividades {
    protected Matriz3 historialActividades;
    protected ListaEstatica pintores;
    protected ListaEstatica actividades;
    protected ListaEstatica anios;

    public ControlDeActividades(int numActividades, int numPintores) {

        pintores = new ListaEstatica(numPintores);
        actividades = new ListaEstatica(numActividades);
        anios = new ListaEstatica(22);
        historialActividades = new Matriz3(numPintores,numActividades , 22, "0"); //tamaño para asi tener cadi dia durante 21 años
    }

    public boolean agregarPintor(Pintor pintor) {
        Integer posicionBusqueda = (Integer) pintores.buscar(pintor);
        if (posicionBusqueda == null) { //no existe, se puede agregar
            int retorno = pintores.agregar(pintor);
            if (retorno == -1) { //no pudo agregarlo
                return false;
            } else { //si pudo
                return true;
            }
        } else {  //ya existe
            return false;
        }
    }

    public boolean agregarActividad(Actividades actividad) {
        Integer posicionBusqueda = (Integer) actividades.buscar(actividad);
        if (posicionBusqueda == null) { //si no existe
            int retorno = actividades.agregar(actividad);
            if (retorno == -1) { // está llena
                return false;
            } else { // hay espacio
                return true;
            }
        } else {//si ya existe
            return false;
        }
    }

    public void agregarAnio() {
        int anio = 2000;
        for (int cont = 0; cont <= 21; cont++) { //los años no se manipularan, ya que es el historial del 2000 L 2021
            anios.agregar(anio);
            anio++;
        }
    }

    public boolean agregarHistorialActividades(String actividad, int pintor, int anio) {
        //buscar los índices que les corresponden a los datos de usuario dados como argumentos
        ListaEstatica indicesBusqueda = obtenerIndicesCeldas(actividad, pintor, anio);
        if (indicesBusqueda != null) { //se puede agregar
            return historialActividades.cambiar((int) indicesBusqueda.obtener(0), (int) indicesBusqueda.obtener(1),
                    (int) indicesBusqueda.obtener(2), actividad);
        } else { //algún datos no existe
            return false;
        }
    }


    private ListaEstatica obtenerIndicesCeldas(String actividad, int pintor, int anio) {
        Integer indicePintor = (Integer) pintores.buscar(pintor);
        Integer indiceActividad = (Integer) actividades.buscar(actividad);
        Integer indiceAnio = (Integer) anios.buscar(anio);

        if (indicePintor != null && indiceActividad != null && indiceAnio != null) { //los índices son válidos, existen los datos
            ListaEstatica indicesArgumentos = new ListaEstatica(3);
            indicesArgumentos.agregar(indicePintor.intValue());
            indicesArgumentos.agregar(indiceActividad.intValue());
            indicesArgumentos.agregar(indiceAnio.intValue());
            return indicesArgumentos;
        } else { //no existe alguno o todos
            return null;
        }
    }

    public void imprimirListadoPintores() {
        pintores.imprimirOI();
    }

    public void imprimirListadoActividades() {
        actividades.imprimirOI();
    }

    public void imprimirListadoAnios() {
        anios.imprimirOI();
    }

    public void imprimirHistorial() {
        historialActividades.imprimirXColumnas();
    }

    public void imprimirDatosCompetencias() {
        SalidaPorDefecto.consola("Los datos de los pintores y sus actividades son las siguientes: \n");
        SalidaPorDefecto.consola("Las actividades en los que se participa: \n");
        imprimirListadoActividades();
        SalidaPorDefecto.consola("Los pintores participantes son: \n");
        imprimirListadoPintores();
        SalidaPorDefecto.consola("Los años de participación son:\n");
        imprimirListadoAnios();
        SalidaPorDefecto.consola("Los dias que registaron las actividades son:\n");
        imprimirHistorial();
        SalidaPorDefecto.consola("\n");
    }

    public String actividadPopular(){
        Object valor;
        int pintar = 0;
        int exponer = 0;
        int descansar = 0;
        int autografos = 0;
        int viajar = 0;
        String actividadPopular = "";
        for(int fila=0; fila<historialActividades.obtenerRenglones(); fila++){ //recorre fila por fila
            for(int columna=0;columna<historialActividades.obtenerColumnas(); columna++){ //recorremos una por una las columnas de un solo renglòn
                for(int prof=0;prof<historialActividades.obtenerProfundidad();prof++){ //recorrer una por una las celdas de profundidad
                    valor = historialActividades.obtener(fila,columna,prof);
                    if (valor == "pintar"){
                        pintar += 1;
                    }
                    else if (valor == "exponer"){
                        exponer += 1;
                    }
                    else if (valor == "viajar"){
                        viajar += 1;
                    }
                    else if (valor == "descansar"){
                        descansar += 1;
                    }
                    else if (valor == "autografos"){
                        autografos += 1;
                    }
                }
            }
        }
        if (pintar>exponer && pintar>viajar && pintar>descansar && pintar>autografos){
            actividadPopular = "pintar";
        }
        if (autografos>exponer && autografos>viajar && autografos>descansar && autografos>pintar){
            actividadPopular = "autografos";
        }
        if (descansar>exponer && descansar>viajar && descansar>autografos && descansar>pintar){
            actividadPopular = "descansar";
        }
        if (exponer>descansar && exponer>viajar && exponer>autografos && exponer>pintar){
            actividadPopular = "exponer";
        }
        if (viajar>descansar && viajar>exponer && viajar>autografos && viajar>pintar){
            actividadPopular = "viajar";
        }
        return actividadPopular;
    }

    public String actividadPopularPintor(Pintor pintor){
        Object valor;
        Integer indicePintor = (Integer) pintores.buscar(pintor); //ya no recorre los renglones, ya que solo nos interesa la poscion de un pintor
        int pintar = 0;
        int exponer = 0;
        int descansar = 0;
        int autografos = 0;
        int viajar = 0;
        String actividadPopular = "";
            if (indicePintor != null){
                for(int columna=0;columna<historialActividades.obtenerColumnas(); columna++){ //recorremos una por una las columnas de un solo renglòn
                    for(int prof=0;prof<historialActividades.obtenerProfundidad();prof++){ //recorrer una por una las celdas de profundidad
                        valor = historialActividades.obtener(indicePintor,columna,prof);
                        if (valor == "pintar"){
                            pintar += 1;
                        }
                        else if (valor == "exponer"){
                            exponer += 1;
                        }
                        else if (valor == "viajar"){
                            viajar += 1;
                        }
                        else if (valor == "descansar"){
                            descansar += 1;
                        }
                        else if (valor == "autografos"){
                            autografos += 1;
                        }
                    }
                }
            }
            else{
                return null;
            }
        if (pintar>exponer && pintar>viajar && pintar>descansar && pintar>autografos){
            actividadPopular = "pintar";
        }
        if (autografos>exponer && autografos>viajar && autografos>descansar && autografos>pintar){
            actividadPopular = "autografos";
        }
        if (descansar>exponer && descansar>viajar && descansar>autografos && descansar>pintar){
            actividadPopular = "descansar";
        }
        if (exponer>descansar && exponer>viajar && exponer>autografos && exponer>pintar){
            actividadPopular = "exponer";
        }
        if (viajar>descansar && viajar>exponer && viajar>autografos && viajar>pintar){
            actividadPopular = "viajar";
        }
        return actividadPopular;
    }

    public int contadorDescanso(Pintor pintor){
        Object valor;
        Integer indicePintor = (Integer) pintores.buscar(pintor); //ya no recorre los renglones, ya que solo nos interesa la poscion de un pintor
        int descansar = 0;
        if (indicePintor != null){
            for(int columna=0;columna<historialActividades.obtenerColumnas(); columna++){ //recorremos una por una las columnas de un solo renglòn
                for(int prof=0;prof<historialActividades.obtenerProfundidad();prof++){ //recorrer una por una las celdas de profundidad
                    valor = historialActividades.obtener(indicePintor,columna,prof);
                    if (valor == "descansar"){
                        descansar += 1;
                    }
                }
            }
            return descansar;
        }
        else{
            return descansar;
        }
    }

    public String infoActividad(Pintor pintor, Object actividad, int anio){
        Object valor;
        Integer indiceAnio = (Integer) anios.buscar(anio);
        Integer indiceActividad = (Integer) actividades.buscar(actividad);
        if (indiceAnio != null ){
            for(int fila=0; fila<historialActividades.obtenerRenglones(); fila++) { //recorre fila por fila
                for (int columna = 0; columna < historialActividades.obtenerColumnas(); columna++) { //recorremos una por una las columnas de un solo renglòn
                    valor = historialActividades.obtener(fila,columna,indiceAnio); //no recorremos al año, ya que se proporciono la fecha
                    if (valor == actividad){ //si el valor obtendio es el mismo que el argumento
                        return "Nombre del pintor: "+ pintor.getNombre() +", Edad: "+ pintor.getEdad();
                    }
                }
            }
        }
        else{
            return null;
        }
        return null;
    }

    public String fechaXevento(Pintor pintor, Actividades expo){
        Object valor;
        Integer indicePintor = (Integer) pintores.buscar(pintor);
        if (indicePintor != null && expo != null){     //el evento tiene que ser exclusivamente de exponer
            for(int columna=0;columna<historialActividades.obtenerColumnas(); columna++) { //recorremos una por una las columnas de un solo renglòn
                for (int prof = 0; prof < historialActividades.obtenerProfundidad(); prof++) { //recorrer una por una las celdas de profundidad
                    valor = historialActividades.obtener(indicePintor, columna, prof);
                    if (valor == "exponer"){
                        return expo.obtenerDatos();
                    }
                }
            }
        }
        else{
            return null;
        }
        return null;
    }

    public String contadorExposiciones(){
        ListaEstatica ordenar = new ListaEstatica(22);
        Object valor;
        String anio = "";
        int cero = 0;
        int uno = 0;
        int dos= 0;
        int tres= 0;
        int cuatro= 0;
        int cinco= 0;
        int seis= 0;
        int siete= 0;
        int ocho= 0;
        int nueve= 0;
        int diez= 0;
        int once= 0;
        int doce= 0;
        int trece= 0;
        int catorce= 0;
        int quince= 0;
        int diezseis= 0;
        int diezsiete= 0;
        int diezocho= 0;
        int dieznueve= 0;
        int veinte= 0;
        int veintuno= 0;
        for(int fila=0; fila<historialActividades.obtenerRenglones(); fila++) { //recorre fila por fila
            for (int prof = 0; prof < historialActividades.obtenerProfundidad(); prof++) { //recorrer una por una las celdas de profundidad
                valor = historialActividades.obtener(fila, 2, prof); //no se recorren las actividades, ya que solo necesitamos la de exponer
                if(valor == "exponer" && prof == 0){
                    cero += 1;
                }
                else if(valor == "exponer" && prof == 1){
                    uno +=1;
                }
                else if(valor == "exponer" && prof == 2){
                    dos +=1;
                }
                else if(valor == "exponer" && prof == 3){
                    tres +=1;
                }
                else if(valor == "exponer" && prof == 4){
                    cuatro +=1;
                }
                else if(valor == "exponer" && prof == 5){
                    cinco +=1;
                }
                else if(valor == "exponer" && prof == 6){
                    seis +=1;
                }
                else if(valor == "exponer" && prof == 7){
                   siete +=1;
                }
                else if(valor == "exponer" && prof == 8){
                    ocho +=1;
                }
                else if(valor == "exponer" && prof == 9){
                   nueve +=1;
                }
                else if(valor == "exponer" && prof == 10){
                    diez +=1;
                }
                else if(valor == "exponer" && prof == 11){
                    once +=1;
                }
                else if(valor == "exponer" && prof == 12){
                    doce +=1;
                }
                else if(valor == "exponer" && prof == 13){
                    trece +=1;
                }
                else if(valor == "exponer" && prof == 14){
                    catorce +=1;
                }
                else if(valor == "exponer" && prof == 15){
                    quince +=1;
                }
                else if(valor == "exponer" && prof == 16){
                    diezseis +=1;
                }
                else if(valor == "exponer" && prof == 17){
                    diezsiete +=1;
                }
                else if(valor == "exponer" && prof == 18){
                    diezocho +=1;
                }
                else if(valor == "exponer" && prof == 19){
                    dieznueve +=1;
                }
                else if(valor == "exponer" && prof == 20){
                    veinte +=1;
                }
                else if(valor == "exponer" && prof == 21){
                    veintuno +=1;
                }
            }
        }
        ordenar.agregar(cero);
        ordenar.agregar(uno);
        ordenar.agregar(dos);
        ordenar.agregar(tres);
        ordenar.agregar(cuatro);
        ordenar.agregar(cinco);
        ordenar.agregar(seis);
        ordenar.agregar(siete);
        ordenar.agregar(ocho);
        ordenar.agregar(nueve);
        ordenar.agregar(diez);
        ordenar.agregar(once);
        ordenar.agregar(doce);
        ordenar.agregar(trece);
        ordenar.agregar(catorce);
        ordenar.agregar(quince);
        ordenar.agregar(diezseis);
        ordenar.agregar(diezsiete);
        ordenar.agregar(diezocho);
        ordenar.agregar(dieznueve);
        ordenar.agregar(veinte);
        ordenar.agregar(veintuno);
        int mayor = (int) ordenar.obtener(0);
        for (int cont = 1; cont < ordenar.numeroElementos(); cont++) {
            if ((int)ordenar.obtener(cont) > mayor) {
                mayor = (int)ordenar.obtener(cont);
            }
        }
        int indice = (int) ordenar.buscar(mayor);
        if(indice == 0){ //se verifica el indice para saber que año poner
            anio = "2000";
        }
        else if(indice == 1){
            anio = "2001";
        }
        else if(indice == 2){
            anio = "2002";
        }
        else if(indice == 3){
            anio = "2003";
        }
        else if(indice == 4){
            anio = "2004";
        }
        else if(indice == 5){
            anio = "2005";
        }
        else if(indice == 6){
            anio = "2006";
        }
        else if(indice == 7){
            anio = "2007";
        }
        else if(indice == 8){
            anio = "2008";
        }
        else if(indice == 9){
            anio = "2009";
        }
        else if(indice == 10){
            anio = "2010";
        }
        else if(indice == 11){
            anio = "2011";
        }
        else if(indice == 12){
            anio = "2012";
        }
        else if(indice == 13){
            anio = "2013";
        }
        else if(indice == 14){
            anio = "2014";
        }
        else if(indice == 15){
            anio = "2015";
        }
        else if(indice == 16){
            anio = "2016";
        }
        else if(indice == 17){
            anio = "2017";
        }
        else if(indice == 18){
            anio = "2018";
        }
        else if(indice == 19){
            anio = "2019";
        }
        else if(indice == 20){
            anio = "2020";
        }
        else if(indice == 21){
            anio = "2021";
        }
        return anio;
    }

    public String infoPintor(Pintor pintor){
        String datos =pintor.obtenerDatos();
        return datos;
    }

}

