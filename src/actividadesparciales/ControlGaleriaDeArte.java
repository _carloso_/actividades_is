package actividadesparciales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaEstatica;
import estructurasLineales.ListaEstatica;
import estructurasnolineales.Matriz3;

/**
 * Este clase representa una galería de arte.
 * @author Cristina Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class ControlGaleriaDeArte {

    private Matriz3 actividadesDesarrolladas;
    private ListaEstatica pintores;
    private ColaEstatica actividades;
    private ListaEstatica fechas;
    private int numeroActividad;

    public ControlGaleriaDeArte(int numActividades, int numPintores, int numFechas) {
       pintores = new ListaEstatica(numPintores);
       actividades = new ColaEstatica(numActividades);
       fechas = new ListaEstatica(numFechas);
       actividadesDesarrolladas = new Matriz3(numActividades, numPintores, numFechas);
       numeroActividad = -1;
    }

    /**
     * Este método agrega un pintor a la lista de pintores.
     * @param pintor Es el pintor que se desea agregar.
     * @return Regresa <b>true</b> si el pintor se agregó correctamente, <b>false</b> en caso contrario.
     */
    public boolean agregarPintor(Pintor pintor) {
        Integer posicionBusqueda = (Integer) pintores.buscar(pintor);
        if (posicionBusqueda == null) { // no existe el pintor, se puede agregar
            int retorno = pintores.agregar(pintor);
            if (retorno == -1) { // no se pudo agregar
                return false;
            } else { // si se pudo agregar
                return true;
            }
        } else { // ya existe el pintor
            return false;
        }
    }

    /**
     * Este método agrega una actividad a la cola de actividades.
     * @param actividad Es la actividad que se desea agregar.
     * @return Regresa <b>true</b> si la actividad se agregó correctamente, <b>false</b> en caso contrario.
     */
    public boolean agregarActividad(ActividadGaleria actividad) {
      Integer posicionBusqueda = (Integer) actividades.buscar(actividad);
      if (posicionBusqueda == null) { // no existe la actividad, se puede agregar
          boolean retorno = actividades.poner(actividad);
          if (!retorno) { // no se pudo agregar
              return false;
          } else { // si se pudo agregar
              return true;
          }
      } else { // ya existe la actividad
          return false;
      }
    }

    /**
     * Este método agrega una fecha a la lista de fechas.
     * @param fecha Es la fecha que se desea agregar.
     * @return Regresa <b>true</b> si la fecha se agregó correctamente, <b>false</b> en caso contrario.
     */
    public boolean agregarFecha(Fecha fecha) {
      Integer posicionBusqueda = (Integer) fechas.buscar(fecha);
      if (posicionBusqueda == null) { // no existe la fecha, se puede agregar
          int retorno = fechas.agregar(fecha);
          if (retorno == -1) { // no se pudo agregar
              return false;
          } else { // si se pudo agregar
              return true;
          }
      } else { // ya existe la fecha
          return false;
      }
    }

    /**
     * Este método imprime el listado de pintores.
     */
    public void imprimirListadoPintores() {
        for (int posicion = 0; posicion < pintores.numeroElementos(); posicion++) {
            Pintor pintor = (Pintor) pintores.obtener(posicion);
            SalidaPorDefecto.consola(pintor.obtenerDatos() + "\n");
            SalidaPorDefecto.consola("----------------------" + "\n");
        }
    }

    /**
     * Este método imprime el listado de actividades.
     */
    public void imprimirListadoActividades() {
        actividades.imprimir();
    }

    /**
     * Este método imprime el listado de fechas.
     */
    public void imprimirListadoFechas() {
        fechas.imprimir();
    }

    /**
     * Este método imprime los datos de la galería de arte.
     */
    public void imprimirDatosDeGaleria() {
        SalidaPorDefecto.consola("***** Datos de la galeria *****" + "\n");
        SalidaPorDefecto.consola("-> Los pintores son: " + "\n");
        imprimirListadoPintores();
        SalidaPorDefecto.consola("-> Las actividades son: " + "\n");
        imprimirListadoActividades();
        SalidaPorDefecto.consola("-> Las fechas son: " + "\n");
        imprimirListadoFechas();
    }

    /**
     * Este método agrega una actividad a la matriz de actividades desarrolladas por los pintores en diferentes fechas.
     * @param pintor Es el pintor que desarrolló la actividad.
     * @param fecha Es la fecha en la que se desarrolló la actividad.
     * @return Regresa <b>true</b> si la actividad se agregó correctamente, <b>false</b> en caso contrario.
     */
    public boolean asignarActividadAPintor(int pintor, Fecha fecha) {
        ActividadGaleria actividadAAsignar = (ActividadGaleria) actividades.quitar();
        Integer posicionPintor = (Integer) pintores.buscar(pintor);
        int posicionFecha = fechas.obtenerPosicion(fecha);
        Pintor pintorAAsignar = (Pintor) pintores.obtener(posicionPintor);
        if (actividadAAsignar != null && posicionFecha != -1 && pintorAAsignar != null) {
            numeroActividad++;
            boolean retorno = actividadesDesarrolladas.cambiar(numeroActividad, posicionPintor, posicionFecha, actividadAAsignar);
            SalidaPorDefecto.consola("-> Se asignó la actividad " + actividadAAsignar +
                                    " al pintor " + pintorAAsignar.nombreCompleto() +
                                    " en la fecha " + fecha + "\n");
            return retorno;
        } else {
            return false;
        }
    }

    /**
     * Este método imprime al pintor que mas actividades ha realizado.
     */
    public void imprimirPintorQueDesarrolloMasActividades() {
        // filas -> actividades
        // columnas -> pintores
        // profundidad -> fechas
        int[] cantidadActividades = new int[pintores.numeroElementos()];
        for (int fila = 0; fila < actividadesDesarrolladas.obtenerRenglones(); fila++) {
            for (int col = 0; col < actividadesDesarrolladas.obtenerColumnas(); col++) {
                for (int prof = 0; prof < actividadesDesarrolladas.obtenerProfundidad(); prof++) {
                    if (actividadesDesarrolladas.obtener(fila, col, prof) != null) {
                        cantidadActividades[col]++;
                    }
                }
            }
        }
        // buscar el pintor que desarrolló más actividades
        int pintorQueDesarrolloMasActividades = 0;
        for (int pos = 1; pos < cantidadActividades.length; pos++) {
            if (cantidadActividades[pos] > cantidadActividades[pintorQueDesarrolloMasActividades]) {
                pintorQueDesarrolloMasActividades = pos;
            }
        }
        Pintor pintor = (Pintor) pintores.obtener(pintorQueDesarrolloMasActividades);
        SalidaPorDefecto.consola("-> El pintor que desarrolló más actividades es " + pintor.nombreCompleto() +
                                " con " + cantidadActividades[pintorQueDesarrolloMasActividades] + " actividades\n");
    }

    /**
     * Este método imprime el listado de actividades desarrolladas por un pintor en específico.
     * @param pintor Es el pintor del cual se desea conocer las actividades que ha desarrollado.
     */
    public void listaActividadesDesarrolladasPorUnPintor(Pintor pintor) {
        // filas -> actividades
        // columnas -> pintores
        // profundidad -> fechas
        int posicionPintor = pintores.obtenerPosicion(pintor);
        if (posicionPintor != -1) {
            Pintor pintorABuscar = (Pintor) pintores.obtener(posicionPintor);
            SalidaPorDefecto.consola("-> El pintor " + pintorABuscar.nombreCompleto() +
                    " desarrolló las siguientes actividades: " + "\n");
            for (int fila = posicionPintor; fila < actividadesDesarrolladas.obtenerRenglones(); fila++) {
                for (int col = 0; col < actividadesDesarrolladas.obtenerColumnas(); col++) {
                    for (int profu = 0; profu < actividadesDesarrolladas.obtenerProfundidad(); profu++) {
                        if (actividadesDesarrolladas.obtener(fila, col, profu) != null) {
                            if (col == posicionPintor) {
                                SalidaPorDefecto.consola(actividadesDesarrolladas.obtener(fila, col, profu) +
                                        " en la fecha " + fechas.obtener(profu) + "\n");
                            }
                        }
                    }
                }
            }
        } else {
            SalidaPorDefecto.consola("-> El pintor no existe\n");
        }
    }

    /**
     * Este método imprime el nombre y la edad del pintor que realizó alguna actividad en una fecha determinada.
     * @param actividad Es la actividad de la cual se quiere saber el pintor que la realizó.
     * @param fecha Es la fecha en la que se realizó la actividad.
     */
    public void pintorQueDesarrolloUnaActividad(ActividadGaleria actividad, Fecha fecha) {
        // Buscar la actividad en la matriz
        // filas -> actividades
        // columnas -> pintores
        // profundidad -> fechas
        int posicionFecha = fechas.obtenerPosicion(fecha);
        if (actividad != null && posicionFecha != -1) {
            for (int fila = 0; fila < actividadesDesarrolladas.obtenerRenglones(); fila++) {
                for (int col = 0; col < actividadesDesarrolladas.obtenerColumnas(); col++) {
                    for (int profu = 0; profu < actividadesDesarrolladas.obtenerProfundidad(); profu++) {
                        if (actividadesDesarrolladas.obtener(fila, col, profu) != null) {
                            if (actividadesDesarrolladas.obtener(fila, col, profu).equals(actividad) &&
                                    profu == posicionFecha) {
                                Pintor pintor = (Pintor) pintores.obtener(col);
                                SalidaPorDefecto.consola("-> El pintor que desarrolló la actividad "
                                        + actividad + " fue " + pintor.nombreCompleto() +
                                        " de " + pintor.getEdad() + " años, En la fecha " + fechas.obtener(profu) + "\n");
                                break;
                            }
                        }
                    }
                }
            }
        } else {
            SalidaPorDefecto.consola("-> La actividad esta vacia \n");
        }
    }

    /**
     * Este método muestra la cantidad de exposiciones que ha realizado un pintor.
     * @param pintor Es el pintor del cual se quiere saber la cantidad de exposiciones que ha realizado.
     */
    public void cantidadDeExpociocionesDeUnPintor(Pintor pintor) {
        // filas -> actividades
        // columnas -> pintores
        // profundidad -> fechas
        int posicionPintor = pintores.obtenerPosicion(pintor);
        if (posicionPintor != -1) {
            int cantidadExpociciones = 0;
            for (int fila = 0; fila < actividadesDesarrolladas.obtenerRenglones(); fila++) {
                for (int col = 0; col < actividadesDesarrolladas.obtenerColumnas(); col++) {
                    for (int profu = 0; profu < actividadesDesarrolladas.obtenerProfundidad(); profu++) {
                        if (actividadesDesarrolladas.obtener(fila, col, profu) != null) {
                            ActividadGaleria actividad = (ActividadGaleria) actividadesDesarrolladas.obtener(fila, col, profu);
                            if (col == posicionPintor) {
                                if (actividad.getTipoActividad() == TipoActividad.EXPONER) {
                                    cantidadExpociciones++;
                                }
                            }
                        }
                    }
                }
            }
            SalidaPorDefecto.consola("-> El pintor " + pintor.nombreCompleto() +
                    " desarrolló " + cantidadExpociciones + " expociciones\n");
        } else {
            SalidaPorDefecto.consola("-> El pintor no existe\n");
        }
    }

    /**
     * Este método muestra la cantidad de aforo que tuvo una expocición en una fecha.
     * @param exposicion Es la expocición de la cual se quiere saber la cantidad de aforo.
     * @param fechaExpo Es la fecha en que se realizó la expocición.
     */
    public void cantidadDeAforoDeUnaExpocicion(ActividadGaleria exposicion, Fecha fechaExpo) {
        // Buscar la actividad en la matriz
        // filas -> actividades
        // columnas -> pintores
        // profundidad -> fechas
        int posicionFecha = fechas.obtenerPosicion(fechaExpo);
        if (exposicion != null && posicionFecha != -1) {
            for (int fila = 0; fila < actividadesDesarrolladas.obtenerRenglones(); fila++) {
                for (int col = 0; col < actividadesDesarrolladas.obtenerColumnas(); col++) {
                    for (int profu = 0; profu < actividadesDesarrolladas.obtenerProfundidad(); profu++) {
                        if (actividadesDesarrolladas.obtener(fila, col, profu) != null) {
                            if (actividadesDesarrolladas.obtener(fila, col, profu).equals(exposicion) &&
                                        profu == posicionFecha) {
                                SalidaPorDefecto.consola("-> La expocicion " + exposicion.getNombre()
                                        + " que se realizo en " + exposicion.getLugar() + " en la fecha " + fechaExpo +
                                        " tuvo un aforo de " + exposicion.getAforoPersonas() + " personas\n");
                            }
                        }
                    }
                }
            }
        } else {
            SalidaPorDefecto.consola("-> La actividad esta vacia \n");
        }
    }

    /**
     * Este método imprime todas las actividades que se realizaron en un año en especifico.
     * @param anio Es el año en el cual se desean mostrar las actividades que se realizaron.
     */
    public void imprimirActividaesDesarrolldasEnUnAnio(int anio) {
        // filas -> actividades
        // columnas -> pintores
        // profundidad -> fechas
        for (int fila = 0; fila < actividadesDesarrolladas.obtenerRenglones(); fila++) {
            for (int col = 0; col < actividadesDesarrolladas.obtenerColumnas(); col++) {
                for (int profu = 0; profu < actividadesDesarrolladas.obtenerProfundidad(); profu++) {
                    if (actividadesDesarrolladas.obtener(fila, col, profu) != null) {
                        Fecha fechaTem = (Fecha) fechas.obtener(profu);
                        Pintor pintorTem = (Pintor) pintores.obtener(col);
                        if (fechaTem.getAnio() == anio && pintorTem != null) {
                            SalidaPorDefecto.consola("-> La actividad " + actividadesDesarrolladas.obtener(fila, col, profu) +
                                    " fue desarrollada por el pintor " + pintorTem.nombreCompleto() + " en el " + fechaTem + "\n");
                        }
                    }
                }
            }
        }
    }

    /**
     * Este método imprime todas las actividades desarrolladas por un pintor en un año determinado.
     * @param anio Es el año en el que se desarrollaron las actividades del pintor.
     * @param pintor Es el pintor que desarrollo las actividades.
     */
    public void imprimirActividaesDesarrolldasEnUnAnioPorPintor(int anio, Pintor pintor) {
        // filas -> actividades
        // columnas -> pintores
        // profundidad -> fechas
        for (int fila = 0; fila < actividadesDesarrolladas.obtenerRenglones(); fila++) {
            for (int col = 0; col < actividadesDesarrolladas.obtenerColumnas(); col++) {
                for (int profu = 0; profu < actividadesDesarrolladas.obtenerProfundidad(); profu++) {
                    if (actividadesDesarrolladas.obtener(fila, col, profu) != null) {
                        Fecha fechaTem = (Fecha) fechas.obtener(profu);
                        if (fechaTem.getAnio() == anio && pintor.equals(pintores.obtener(col))) {
                            SalidaPorDefecto.consola("-> La actividad " + actividadesDesarrolladas.obtener(fila, col, profu) +
                                    " fue desarrollada por el pintor " + pintor.nombreCompleto() + " en el " + fechaTem + "\n");
                        }
                    }
                }
            }
        }
    }

    /**
     * Este método imprime el pintor que más descansos a tenido durante su trabajo en la galería.
     */
    public void pintorQueMasDescansa() {
       // filas -> actividades
       // columnas -> pintores
       // profundidad -> fechas
       // contar el numero de descansos de cada pintor
       // pintor con mas descansos
        int[] contadorDescansos = new int[pintores.numeroElementos()];
        for (int fila = 0; fila < actividadesDesarrolladas.obtenerRenglones(); fila++) {
            for (int col = 0; col < actividadesDesarrolladas.obtenerColumnas(); col++) {
                for (int profu = 0; profu < actividadesDesarrolladas.obtenerProfundidad(); profu++) {
                    if (actividadesDesarrolladas.obtener(fila, col, profu) != null) {
                        ActividadGaleria actividadTem = (ActividadGaleria) actividadesDesarrolladas.obtener(fila, col, profu);
                        if (actividadTem.getTipoActividad().equals(TipoActividad.DESCANSAR)) {
                            contadorDescansos[col]++;
                        }
                    }
                }
            }
        }
        int mayor = 0;
        for (int i = 0; i < contadorDescansos.length; i++) {
            if (contadorDescansos[i] > mayor) {
                mayor = contadorDescansos[i];
            }
        }
        for (int i = 0; i < contadorDescansos.length; i++) {
            if (contadorDescansos[i] == mayor) {
                Pintor pintorTem = (Pintor) pintores.obtener(i);
                SalidaPorDefecto.consola("-> El pintor " + pintorTem.nombreCompleto() +
                        " tiene " + contadorDescansos[i] + " descansos\n");
                // mostrar los descansos y las fechas en las que se realizaron
                int aux = 1;
                for (int fila = 0; fila < actividadesDesarrolladas.obtenerRenglones(); fila++) {
                    for (int col = 0; col < actividadesDesarrolladas.obtenerColumnas(); col++) {
                        for (int profu = 0; profu < actividadesDesarrolladas.obtenerProfundidad(); profu++) {
                            if (actividadesDesarrolladas.obtener(fila, col, profu) != null) {
                                ActividadGaleria actividadTem = (ActividadGaleria) actividadesDesarrolladas.obtener(fila, col, profu);
                                if (actividadTem.getTipoActividad().equals(TipoActividad.DESCANSAR) &&
                                        pintorTem.equals(pintores.obtener(col))) {
                                    Fecha fechaTem = (Fecha) fechas.obtener(profu);
                                    SalidaPorDefecto.consola((aux) + ") " + actividadTem.getNombre()
                                            + " en el " + fechaTem + "\n");
                                    aux++;
                                }

                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Este método muestra las actividades que realizaron los pintores en una fecha determinada.
     */
     public void imprimirActividadesDesarrolladas() {
        SalidaPorDefecto.consola("--- Matriz De Actividades desarrolladas por los pintores en diferentes fechas ---" + "\n");
        SalidaPorDefecto.consola("-> Las actividades desarrolladas por cada pintor: \n");
        actividadesDesarrolladas.imprimirXColumnas();
    }
}