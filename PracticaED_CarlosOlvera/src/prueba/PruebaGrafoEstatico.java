package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasNoLineales.GrafoEstatico;

public class PruebaGrafoEstatico {
    public static  void main(String[] args){
        GrafoEstatico grafo1=new GrafoEstatico(5);

        grafo1.agregarVertice("Z");
        grafo1.agregarVertice("M");
        grafo1.agregarVertice("S");
        grafo1.agregarVertice("T");
        grafo1.agregarVertice("A");

        grafo1.agregarArista("Z","S");
        grafo1.agregarArista("M","Z");
        grafo1.agregarArista("M","T");
        grafo1.agregarArista("T","Z");
        grafo1.agregarArista("T","S");
        grafo1.agregarArista("T","A");
        grafo1.agregarArista("S","S");
        grafo1.agregarArista("A","A");
        grafo1.imprimir();

        SalidaPorDefecto.consola("-----ELIMINAR VERTICE-----\n");
        SalidaPorDefecto.consola("Eliminar la letra Z\n");
        SalidaPorDefecto.consola("Se elimino el valor: "+grafo1.eliminarVertice("Z")+"\n");
        grafo1.imprimir();

        SalidaPorDefecto.consola("-----ES ADYACENTE-----\n");
        SalidaPorDefecto.consola("M y T son adyacentes: "+grafo1.esAdyacente("M","T") + "\n");
        SalidaPorDefecto.consola("T y A son adyacentes: "+grafo1.esAdyacente("T","A") + "\n");

        SalidaPorDefecto.consola("-----ELIMINAR ARISTA-----\n");
        SalidaPorDefecto.consola("Eliminar la arista M,T\n");
        SalidaPorDefecto.consola("Se elimino el valor: "+ grafo1.eliminarArista("M","T")+"\n");
        grafo1.imprimir();

        SalidaPorDefecto.consola("-----BUSCAR VERTICE-----\n");
        SalidaPorDefecto.consola("Buscar vertice T:\n");
        grafo1.buscarVertice("T");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Buscar vertice Z:\n");
        grafo1.buscarVertice("Z");
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----PSEUDOGRAFO-----\n");
        SalidaPorDefecto.consola("Es Pseudografo: "+grafo1.esPseudografo());
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----MULTIGRAFO-----\n");
        SalidaPorDefecto.consola("Es Multigrafo: "+grafo1.esMultiografo());
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----GRADO VERTICE-----\n");
        SalidaPorDefecto.consola("Numero de arista del vertice T: "+grafo1.gradoVertice("M"));
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----HAY RUTA-----\n");
        SalidaPorDefecto.consola("Ruta de S a T. ");
        SalidaPorDefecto.consola("Hay ruta: "+ grafo1.hayRuta("T","S"));
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----CONEXO-----\n");
        //SalidaPorDefecto.consola("Es conexo: "+ grafo1.esConexo());
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----LISTA ARISTAS-----\n");
        SalidaPorDefecto.consola("Lista para vertice S\n");
        grafo1.listarAristas();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----LISTA ARISTAS DE UN VERTICE-----\n");
        SalidaPorDefecto.consola("Lista para vertice S\n");
        grafo1.listarAristas("S");
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----LISTA VERTICEs-----\n");
        grafo1.listarVertices();
        SalidaPorDefecto.consola("\n");
    }
}
