package registros.indicesBD;

import entradaSalida.SalidaPorDefecto;
import estructurasnolineales.ArbolBinarioBusqueda;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Esta clase se encarga de generar los indices de una tabla de registros en un arbol binario de búsqueda.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class GeneradorIndicesBD {

    protected String rutaArchivo;
    protected int indice;
    protected long direccion;
    protected ArbolBinarioBusqueda arbolABB;

    public GeneradorIndicesBD(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
        indice = 0;
        direccion = 0;
        arbolABB = new ArbolBinarioBusqueda();
    }

    /**
     * Este método se encarga de leer y mostrar el contenido de un archivo de texto.
     * @throws IOException Si hay un error de entrada/salida al leer el archivo.
     */
    // Este método solo es de prueba para ver el contenido de un archivo de texto.
    public void leerArchivo() throws IOException {
        RandomAccessFile archivo = null;
        try {
            archivo = new RandomAccessFile(rutaArchivo, "r");
            SalidaPorDefecto.consola("El tamaño es: " + archivo.length() + "\n");
            String cad="";
            while(true) {
                SalidaPorDefecto.consola("Pos: " + archivo.getFilePointer() + " - ");
                cad = archivo.readLine();
                if (cad==null) {
                    break;
                }
                SalidaPorDefecto.consola(cad + "\n");
            }
        } catch (IOException e) {
            System.out.println("No se encontro el archivo");
        }
        /*
        System.out.println("\n");
        archivo.seek(9071);
        System.out.println(archivo.readLine());
        System.out.println("\n");
         */
        archivo.close();
    }

    /**
     * Este método se encarga de generar los índices de una tabla de registros en un arbol binario de búsqueda.
     * @return Regresa <b>true</b> si se generaron los índices correctamente, de lo contrario regresa <b>false</b>.
     * @throws IOException Si hay un error de entrada/salida al leer el archivo.
     */
    public boolean generarIndices() throws IOException {
        NodoBusquedaArbol nuevoNodo = null;
        RandomAccessFile archivo = null;
        try {
            archivo = new RandomAccessFile(rutaArchivo, "r");
            String cad="";
            while(true) {
                indice++; // incrementa el indice de registro es como una llave primaria autoincremental de una BD
                direccion = archivo.getFilePointer(); // se guarda la direccion del registro
                // Nuevo nodo que guarda el nombre del índice, el índice y la dirección del registro
                nuevoNodo = new NodoBusquedaArbol(indice, direccion);
                arbolABB.agregar(nuevoNodo);
                cad = archivo.readLine();
                if (cad==null) {
                    break;
                }
            }
        } catch (IOException e) {
            SalidaPorDefecto.consola("No se encontro el archivo" );
            return false;
        } finally {
            archivo.close();
        }
        return true;
    }

    /**
     * Este método se encarga de buscar la dirección de un registro en un árbol binario de búsqueda de acuerdo a un índice dado.
     * @param indice Es el índice del registro que se desea buscar.
     * @throws IOException Si hay un error de entrada/salida al leer el archivo.
     */
    public void buscarRegistro(int indice) throws IOException {
        // buscar el nodo en el arbol y buscar su contenido en el archivo
        NodoBusquedaArbol nodoBusqado = (NodoBusquedaArbol) arbolABB.buscar(indice);
        if (nodoBusqado != null) {
            SalidaPorDefecto.consola("Registro encontrado: Indice: " + nodoBusqado.getIndice() +
                    " - Direccion: " + nodoBusqado.getDireccion() + "\n");
            // obtener el registro de la dirección del nodo encontrado y mostrarlo
            SalidaPorDefecto.consola(obtenerContenido(nodoBusqado.getDireccion()) + "\n");
        } else {
            SalidaPorDefecto.consola("No se encontro el nodo (Registro) con el indice: " + indice + "\n");
        }
    }

    /**
     * Este método se encarga de obtener el contenido de una dirección dada del archivo.
     * @param direccion Es la dirección del registro que se desea obtener.
     * @return Regresa el contenido del registro.
     * @throws IOException Si hay un error de entrada/salida al leer el archivo.
     */
    private String obtenerContenido(long direccion) throws IOException {
        RandomAccessFile archivo = null;
        String cad="";
        try {
            archivo = new RandomAccessFile(rutaArchivo, "r");
            archivo.seek(direccion); // posicionarse en la direccion del registro
            cad = archivo.readLine();
        } catch (IOException e) {
            SalidaPorDefecto.consola("No se encontro el archivo");
        } finally {
            archivo.close();
        }
        return cad;
    }

    /**
     * Este método se encarga de insertar un nuevo registro en el ABB y en el archivo.
     * @param registro Es el registro que se desea insertar.
     * @return Regresa <b>true</b> si se insertó el registro correctamente, de lo contrario regresa <b>false</b>.
     * @throws IOException Si hay un error de entrada/salida al leer el archivo.
     */
    public boolean insertarRegistro(String registro) throws IOException {
        // obtener la direccion del ultimo registro del archivo y sumarle uno para el nuevo registro a insertar
        long direccion = obtenerDireccionUltimoRegistro() + 1;
        NodoBusquedaArbol nuevoNodo = new NodoBusquedaArbol(indice, direccion);
        indice++;
        arbolABB.agregar(nuevoNodo);
        RandomAccessFile archivo = null;
        try {
            archivo = new RandomAccessFile(rutaArchivo, "rw");
            archivo.seek(archivo.length()); // posicionarse en el final del archivo
            archivo.writeBytes(registro + "\n");
            return true;
        } catch (IOException e) {
            SalidaPorDefecto.consola("No se encontro el archivo");
            return false;
        } finally {
            archivo.close();
        }
    }

    /**
     * Este método se encarga de obtener la dirección del último registro del archivo.
     * @return Regresa la dirección del último registro del archivo.
     * @throws IOException Si hay un error de entrada/salida al leer el archivo.
     */
    private long obtenerDireccionUltimoRegistro() throws IOException {
        RandomAccessFile archivo = null;
        long direccion = 0;
        try {
            archivo = new RandomAccessFile(rutaArchivo, "r");
            direccion = archivo.length();
        } catch (IOException e) {
            SalidaPorDefecto.consola("No se encontro el archivo");
        } finally {
            archivo.close();
        }
        return direccion;
    }

    /**
     * Este método se encarga de eliminar un registro en el ABB y en el archivo de acuerdo a un índice dado.
     * @param indice Es el índice del registro que se desea eliminar.
     * @return Regresa <b>true</b> si se eliminó el registro correctamente, de lo contrario regresa <b>false</b>.
     * @throws IOException Si hay un error de entrada/salida al leer el archivo.
     */
    public boolean eliminarRegistro (int indice) throws IOException {
        NodoBusquedaArbol nodoBusqado = (NodoBusquedaArbol) arbolABB.buscar(indice);
        if (nodoBusqado != null) {
            // eliminar el nodo del archivo y del arbol
            eliminarRegistroDeArchivo(nodoBusqado.getDireccion());
            arbolABB.eliminar(nodoBusqado);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método se encarga de eliminar un registro en el archivo de acuerdo a una dirección dada.
     * @param direccion Es la dirección del registro que se desea eliminar.
     * @throws IOException Si hay un error de entrada/salida al leer el archivo.
     */
    private void eliminarRegistroDeArchivo(long direccion) throws IOException {
        RandomAccessFile archivo = null;
        try {
            archivo = new RandomAccessFile(rutaArchivo, "rw");
            archivo.seek(direccion); // posicionarse en la direccion del registro
            // escribir una cadena vacia en la direccion del registro para eliminarlo
            archivo.writeBytes("");
        } catch (IOException e) {
            SalidaPorDefecto.consola("No se encontro el archivo");
        } finally {
            archivo.close();
        }
    }

    /**
     * Este método se encarga de imprimir el árbol ABB.
     */
    public void imprimirArbol() {
        SalidaPorDefecto.consola("Inorden: ");
        arbolABB.inOrden();
    }
}