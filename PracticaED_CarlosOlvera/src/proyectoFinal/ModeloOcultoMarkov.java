package proyectoFinal;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasNoLineales.Matriz2;

/**
 * Esta clase representa el modelo oculto de Markov de los estados de ánimo de una persona.
 * @author Cristian Omar Alvarado Rodriguez y Carlos Eduardo Olvera Mayorga.
 * @version 1.0
 */
public class ModeloOcultoMarkov {

    protected GrafoMOM grafoMOM; // grafo de Markov de los estados de ánimo de una persona

    public ModeloOcultoMarkov(int numeroNodos, int numeroObservaciones) {
        grafoMOM = new GrafoMOM(numeroNodos, numeroObservaciones);
    }

    /**
     * Este método agrega un nuevo nodo al grafo de Markov.
     * @param nodoEstado Es el nodo que se desea agregar al grafo de Markov.
     * @return Regresa <b>true</b> si el nodo se agregó correctamente al grafo o <b>false</b> si no se pudo agregar.
     */
    public boolean agregarNodoEstado(NodoMOM nodoEstado) {
        return grafoMOM.agregarNodo(nodoEstado);
    }

    /**
     * Este método agrega una arista al grafo de Markov.
     * @param estadoOrigen Es el estado origen de la arista.
     * @param estadoDestino Es el estado destino de la arista.
     * @param probabilidad Es la probabilidad de transición entre los estados.
     * @return Regresa <b>true</b> si la arista se agregó correctamente al grafo o <b>false</b> si no se pudo agregar.
     */
    public boolean agregarArista(Object estadoOrigen, Object estadoDestino, double probabilidad) {
        return grafoMOM.agregarArista(estadoOrigen, estadoDestino, probabilidad);
    }

    /**
     * Este método muestra un menu de opciones para el usuario.
     */
    public void mostrarMenu() {
        int opcion = 0;
        do {
            SalidaPorDefecto.consola("----- Menu de opciones -----\n");
            SalidaPorDefecto.consola("1. Mostrar información del grafo\n");
            SalidaPorDefecto.consola("2. Probabilidad de que el escenario inicie con un estado de animo específico\n");
            SalidaPorDefecto.consola("3. Actividades Según Estado de Animo\n");
            SalidaPorDefecto.consola("4. Probabilidad de que en un dia este en un cierto estado de animo\n");
            SalidaPorDefecto.consola("5. Probabilidad de que se de cierta secuencia de estados de animo\n");
            SalidaPorDefecto.consola("6. Salir\n");
            SalidaPorDefecto.consola(">>>Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    grafoMOM.imprimirGrafo();
                    break;
                case 2:
                    subMenuProbabilidadInicio();
                    break;
                case 3:
                    SalidaPorDefecto.consola("Estado de animo actual (feliz/enojado/triste/aburrido): ");
                    String estadoAnimoActual = EntradaPorDefecto.consolaCadenas();
                    if (validarEstadoAnimo(estadoAnimoActual)) {
                        subMenuProbabilidadActividad(estadoAnimoActual);
                    }
                    break;
                case 4:
                    subMenuProbabilidadTransicionEmociones();
                    break;
                case 5:
                    subMenuProbabilidadSecuencia();
                    break;
                case 6:
                    SalidaPorDefecto.consola("Saliendo...");
                    break;
                default:
                    SalidaPorDefecto.consola("Opción no válida\n");
                    break;
            }
        } while (opcion != 6);
    }

    /**
     * Este método muestra un submenu de opciones para el usuario, para mostrar la probabilidad
     * de que el escenario inicie con un estado de ánimo específico.
     */
    private void subMenuProbabilidadInicio() {
        int opcion = 0;
        do {
            SalidaPorDefecto.consola("----- Probabilidad de inicio específico -----\n");
            SalidaPorDefecto.consola("1. Feliz\n");
            SalidaPorDefecto.consola("2. Enojado\n");
            SalidaPorDefecto.consola("3. Triste\n");
            SalidaPorDefecto.consola("4. Aburrido\n");
            SalidaPorDefecto.consola("5. Regresar...\n");
            SalidaPorDefecto.consola(">>>Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    NodoMOM nodoBusqueda = grafoMOM.obtenerNodo("feliz");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de iniciar con un estado de animo feliz: " +
                                nodoBusqueda.getProbaInicial() + " %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo feliz\n");
                    }
                    break;
                case 2:
                    nodoBusqueda = grafoMOM.obtenerNodo("enojado");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de iniciar con un estado de animo enojado: " +
                                nodoBusqueda.getProbaInicial() + " %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo enojado\n");
                    }
                    break;
                case 3:
                    nodoBusqueda = grafoMOM.obtenerNodo("triste");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de iniciar con un estado de animo triste: " +
                                nodoBusqueda.getProbaInicial() + " %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo triste\n");
                    }
                    break;
                case 4:
                    nodoBusqueda = grafoMOM.obtenerNodo("aburrido");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de iniciar con un estado de animo aburrido: " +
                                nodoBusqueda.getProbaInicial() + " %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo aburrido\n");
                    }
                    break;
                default:
                    SalidaPorDefecto.consola("Opción no válida\n");
                    break;
            }
        } while (opcion != 5);
    }

    /**
     * Este método muestra un submenu de opciones para el usuario, para mostrar la probabilidad
     * de un cierta actividad según un estado de ánimo específico.
     * @param estadoAnimoActual Es el estado de ánimo actual del usuario.
     */
    private void subMenuProbabilidadActividad(String estadoAnimoActual) {
        int opcion = 0;
        do {
            SalidaPorDefecto.consola("----- Actividades Según Estado de Animo -----\n");
            SalidaPorDefecto.consola("1. Probabilidad de que su amigo este Bailando si esta " + estadoAnimoActual + "\n");
            SalidaPorDefecto.consola("2. Probabilidad de que su amigo este Leyendo si esta " + estadoAnimoActual + "\n");
            SalidaPorDefecto.consola("3. Probabilidad de que su amigo este Escuchando Musica si esta " + estadoAnimoActual + "\n");
            SalidaPorDefecto.consola("4. Regresar...\n");
            SalidaPorDefecto.consola(">>>Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    SalidaPorDefecto.consola("Probabilidad de que su amigo este Bilando si esta "
                            + estadoAnimoActual + ": " + grafoMOM.probabilidadActividad(estadoAnimoActual, 0) + "\n");
                    break;
                case 2:
                    SalidaPorDefecto.consola("Probabilidad de que su amigo este Leyendo si esta "
                            + estadoAnimoActual + ": " + grafoMOM.probabilidadActividad(estadoAnimoActual, 1) + "\n");
                    break;
                case 3:
                    SalidaPorDefecto.consola("Probabilidad de que su amigo este Escuchando Musica si esta "
                            + estadoAnimoActual + ": " +
                            grafoMOM.probabilidadActividad(estadoAnimoActual, 2) + "\n");
                    break;
                default:
                    SalidaPorDefecto.consola("Opción no válida\n");
                    break;
            }
        } while (opcion != 4);
    }

    /**
     * Este método válida que el usuario ingrese el estado de ánimo correcto.
     * @param estadoAnimo Es el estado de ánimo que el usuario ingresó.
     * @return Regresa <b>true</b> si el estado de ánimo es correcto, <b>false</b> en caso contrario.
     */
    private boolean validarEstadoAnimo(String estadoAnimo) {
        return estadoAnimo.equalsIgnoreCase("feliz") || estadoAnimo.equalsIgnoreCase("enojado")
                || estadoAnimo.equalsIgnoreCase("triste") || estadoAnimo.equalsIgnoreCase("aburrido");
    }

    /**
     * Este método muestra un submenu de opciones para el usuario, para mostrar la probabilidad
     * de que en un dia esté en un cierto estado de ánimo.
     */
    private void subMenuProbabilidadTransicionEmociones() {
        int opcion = 0;
        int dia = 0;
        Matriz2 condicionFutura = grafoMOM.aristas;
        do {
            SalidaPorDefecto.consola("----- Probabilidad de que en un dia este en un cierto estado de animo -----\n");
            SalidaPorDefecto.consola("1. Feliz\n");
            SalidaPorDefecto.consola("2. Enojado\n");
            SalidaPorDefecto.consola("3. Triste\n");
            SalidaPorDefecto.consola("4. Aburrido\n");
            SalidaPorDefecto.consola("5. Regresar...\n");
            SalidaPorDefecto.consola(">>>Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            SalidaPorDefecto.consola(">>>Ingrese el numero de día: ");
            dia = EntradaPorDefecto.consolaInteger();
            for(int cont = 1; cont < dia; cont++){
                condicionFutura = Matriz2.multiplicacion(grafoMOM.aristas,condicionFutura);
            }
            switch (opcion) {
                case 1:
                    NodoMOM nodoBusqueda = grafoMOM.obtenerNodo("feliz");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de estar con un estado de animo feliz en el dia "+ dia+ ": "+ condicionFutura.obtener(1,0)+" %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo feliz\n");
                    }
                    break;
                case 2:
                    nodoBusqueda = grafoMOM.obtenerNodo("enojado");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de estar con un estado de animo feliz en el dia "+ dia+ ": "+ condicionFutura.obtener(1,1)+" %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo enojado\n");
                    }
                    break;
                case 3:
                    nodoBusqueda = grafoMOM.obtenerNodo("triste");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de estar con un estado de animo feliz en el dia "+ dia+ ": "+ condicionFutura.obtener(1,2)+" %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo triste\n");
                    }
                    break;
                case 4:
                    nodoBusqueda = grafoMOM.obtenerNodo("aburrido");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de estar con un estado de animo feliz en el dia "+ dia+ ": "+ condicionFutura.obtener(1,3)+" %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo aburrido\n");
                    }
                    break;
                default:
                    SalidaPorDefecto.consola("Opción no válida\n");
                    break;
            }
        } while (opcion != 5);
    }

    /**
     * Este método muestra un submenu de opciones para el usuario, para mostrar la probabilidad
     * de secuencia de estados de ánimo.
     */
    private void subMenuProbabilidadSecuencia() {
        //se utiliza la regla de la suma de las probabilidades
        SalidaPorDefecto.consola("----- ¿Cuál es la probabilidad de que el estado de animo sea feliz, enojado, triste, aburrido, feliz, enojado, triste, aburrido etc? -----\n");
        double probabilidadFelizEnojado = (double)grafoMOM.aristas.obtener(0,1);
        double probabilidadEnojadoTriste = (double)grafoMOM.aristas.obtener(1,2);
        double probabilidadTristeAburrido = (double)grafoMOM.aristas.obtener(2,3);
        double probabilidadAburridoFeliz = (double)grafoMOM.aristas.obtener(3,0);
        //la probhabilidad de que al menos dos de estos estados de animo se repitan, ya que cada estado tiene un porcentaje del 25%
        double probabilidadeAlMenos = 0.5;
        double suma = probabilidadFelizEnojado + probabilidadEnojadoTriste + probabilidadTristeAburrido
                + probabilidadAburridoFeliz;
        double resultado = suma-probabilidadeAlMenos;
        SalidaPorDefecto.consola("La probabilidad de que se repita este ciclo es de : " +resultado+ " %\n");
    }
}
