package registros.escuela;

import estructurasLineales.ListaDinamica;

public class Computadoras {
    protected String noComputadora;
    protected int NoCC;
    protected int ram;
    protected String discoDuro;
    protected String procesador;
    protected String marca;
    protected ListaDinamica usuarios;
    protected String aplicaciones;
    public Computadoras(String noComputadora, int noCC, int ram, String discoDuro, String procesador, String marca, ListaDinamica aplicaciones, ListaDinamica usuarios ){
        this.noComputadora = noComputadora;
        this.NoCC = noCC;
        this.ram = ram;
        this. discoDuro = discoDuro;
        this.procesador = procesador;
        this.marca = marca;
        this.usuarios = usuarios;
        this. aplicaciones = aplicaciones.toString();
    }

    public String getAplicaciones() {
        return "Aplicaciones: " + aplicaciones;
    }

    public ListaDinamica getUsuarios() {
        return usuarios;
    }

    public String getDiscoDuro() {
        return discoDuro;
    }

    public String getMarca() {
        return marca;
    }

    public int getNoCC() {
        return NoCC;
    }

    public String getNoComputadora() {
        return noComputadora;
    }

    public String getProcesador() {
        return procesador;
    }

    public int getRam() {
        return ram;
    }

    public void setAplicaciones(ListaDinamica aplicaciones) {
        this.aplicaciones = String.valueOf(aplicaciones);
    }

    public void setDiscoDuro(String discoDuro) {
        this.discoDuro = discoDuro;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setNoCC(int noCC) {
        NoCC = noCC;
    }

    public void setNoComputadora(String noComputadora) {
        this.noComputadora = noComputadora;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public void setUsuarios(ListaDinamica usuarios) {
        this.usuarios = usuarios;
    }

    public String datos() {
        return "No. de Computadora: " + noComputadora + ", No. Centro de Computo: " + NoCC + ", RAM: " + ram +"GB" + ", Disco Duro: " + discoDuro +", Procesador: " + procesador +
                ", Marca: " + marca + ", Aplicaciones: " + aplicaciones + ", Usuarios: " + usuarios+ "\n";
    }

    @Override
    public String toString() {
        return noComputadora;
    }
}
