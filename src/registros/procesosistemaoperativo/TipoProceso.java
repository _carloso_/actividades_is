package registros.procesosistemaoperativo;

/**
 * Este enumerado representa los tipos de proceso que puede tener el sistema operativo.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public enum TipoProceso {
    DESCARGA("Descarga", 1), EJECUCION("Ejecución", 2), IMPRESION("Impresión", 3);

    private String nombre;
    private int id;

    private TipoProceso(String nombre, int id) {
        this.nombre = nombre;
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public int getId() {
        return id;
    }
}