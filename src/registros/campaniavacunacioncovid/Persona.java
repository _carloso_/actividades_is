package registros.campaniavacunacioncovid;

/**
 * Clase que representa a una persona en la campaña de vacunación contra COVID-19.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class Persona {

    private String nombre;
    private String apellido;
    private String curp;
    private String fechaNacimiento;
    private String sexo;
    private String estadoResidencia;
    private int edad;

    public Persona(String nombre, String apellido, String curp, String fechaNacimiento, String sexo, String estadoResidencia, int edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.curp = curp;
        this.fechaNacimiento = fechaNacimiento;
        this.sexo = sexo;
        this.estadoResidencia = estadoResidencia;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getEstadoResidencia() {
        return estadoResidencia;
    }

    public void setEstadoResidencia(String estadoResidencia) {
        this.estadoResidencia = estadoResidencia;
    }

    @Override
    public String toString(){
        return  "Nombre:" + nombre + "\n" +
                "Apellido:" + apellido + "\n" +
                "CURP:" + curp + "\n" +
                "Fecha de nacimiento:" + fechaNacimiento + "\n" +
                "Sexo:" + sexo + "\n" +
                "Estado de residencia:" + estadoResidencia + "\n" +
                "Edad:" + edad + "\n";
    }
}
