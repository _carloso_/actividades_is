package registros.escuelas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;

public class Escuela2 {

    protected String nombre;
    protected ListaDinamica alumnos;

    public Escuela2(String nombre) {
        this.nombre = nombre;
        this.alumnos = new ListaDinamica();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ListaDinamica getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(ListaDinamica alumnos) {
        this.alumnos = alumnos;
    }

    /**
     * Metodo que agrega a un alumno a la lista de alumnos de la escuela.
     * @param alumno Es el alumno a agregar.
     * @return Regresa <b>true</b> si se agrego correctamente el alumno o <b>false</b> si no se agrego.
     */
    public boolean agregarAlumno(Alumno2 alumno) {
        int valorRetornoAgregado = alumnos.agregar(alumno);
        if (valorRetornoAgregado == -1) { // no se pudo agregar
            return false;
        } else { // si se pudo agregar
            return true;
        }
    }

    /**
     * Este metodo imprime a un alumno.
     */
    public void imprimirListadoAlumnos() {
        alumnos.imprimir();
    }

    /**
     * Metodo que imprime los datos de un alumno.
     */
    public void imprimirListadoDatosAlumnos() {
        alumnos.iniciarIterador();
        while (alumnos.hayNodos() == true) { // iterador esta en una posicion distinta de null
            Alumno2 alumnoIterador = (Alumno2) alumnos.obtenerNodo(); // obtener el nodo actual
            SalidaPorDefecto.consola(alumnoIterador.getMatricula() + " " +
                                     alumnoIterador.getNombre() + " " + alumnoIterador.getApellido() + " " +
                                     alumnoIterador.getEdad() + "\n");
            // imprimir las calificaciones del alumno
            alumnoIterador.imprimirCalificaciones();
        }
    }

    /**
     * Método que calcula el promedio de un alumno dada su matricula.
     * @param matricula Es la matricula del alumno.
     * @return El promedio de calificaciones del alumno.
     */
    public Double calcularPromedioAlumno(String matricula) {
        // Buscar al alumno con esa matricula
        Alumno2 alumnoSolicitado = (Alumno2) alumnos.buscar(matricula);

        if (alumnoSolicitado != null) { // si esta
            return alumnoSolicitado.obtenerPromedio();
        } else { // no esta
            return null;
        }
    }
}