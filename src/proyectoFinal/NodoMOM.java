package proyectoFinal;

import estructurasLineales.ListaEstatica;

public class NodoMOM {

    protected String nombre;
    protected double probaInicial;
    protected ListaEstatica observaciones;

    public NodoMOM(String nombre, double probaInicial, ListaEstatica observaciones){
        this.nombre = nombre;
        this.probaInicial = probaInicial;
        this.observaciones = observaciones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getProbaInicial() {
        return probaInicial;
    }

    public void setProbaInicial(double probaInicial) {
        this.probaInicial = probaInicial;
    }

    public ListaEstatica getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(ListaEstatica observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public String toString() {
        return nombre;
    }
}