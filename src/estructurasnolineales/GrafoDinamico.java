package estructurasnolineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import estructurasLineales.PilaDinamica;
import estructurasnolineales.auxiliares.Vertice;

/**
 * Esta clase representa un TDA grafo dinámico.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class GrafoDinamico {

    protected ListaDinamica listaAdyacencia; // gurada as listas secundarias (vertices origen y hacia los destinos)
    protected Matriz2 matrizAdyacencia; // guarda la matriz de adyacencia
    protected int numVertice; // guarda el numero de vertice

    public GrafoDinamico(int numVertices) {
        listaAdyacencia = new ListaDinamica();
        matrizAdyacencia = new Matriz2(numVertices, numVertices,0.0);
        numVertice = 0;
    }

    /**
     * Este método busca la sublista de adyacencia de un vertice dado.
     * @param contenidoVertice Es el vertice al que se le busca la sublista de adyacencia.
     * @return Retorna la sublista de adyacencia del vertice dado.
     */
    private ListaDinamica buscarVerticeListaAdyacencia(Object contenidoVertice) {
        listaAdyacencia.iniciarIterador();
        while (listaAdyacencia.hayNodos() == true) {
            ListaDinamica subListaCadaVertice = (ListaDinamica) listaAdyacencia.obtenerNodo();
            Vertice primerVerticeCadaSubLista = (Vertice) subListaCadaVertice.verPrimero();
            if (primerVerticeCadaSubLista.toString().equalsIgnoreCase(contenidoVertice.toString()) == true) {
                return subListaCadaVertice;
            }
        }
        return null;
    }

    /**
     * Este método agrega un vertice al grafo.
     * @param contenido Es el contenido del vertice a agregar.
     * @return Regresa <b>true</b> si el vertice se agregó correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregarVertice(Object contenido) {
        // Checar si existe el vertice
        // Para esto debemos buscar en la primera posicion de cada sublista
        ListaDinamica subListaVerticeAEncontrar = buscarVerticeListaAdyacencia(contenido);
        if (subListaVerticeAEncontrar == null) { // no existe el vertice
            ListaDinamica subListaVerticeNuevo = new ListaDinamica(); // creamos la sublista del vertice nuevo
            Vertice verticeNuevo = new Vertice(); // creamos un vertice nuevo
            numVertice++;
            verticeNuevo.setContenido(contenido);
            verticeNuevo.setNumVertice(numVertice);
            subListaVerticeNuevo.agregar(verticeNuevo); // agregamos el vertice nuevo a su propia sublista
            // agregamos la sublista a la lista de adyacencia
            if (listaAdyacencia.agregar(subListaVerticeNuevo) == 0) { // si se pudo agregar la sublista
                return true;
            } else { // no se pudo agregar la sublista
                return false;
            }
        } else { // ya existe el vertice
            return false;
        }
    }

    /**
     * Este método agrega una arista al grafo.
     * @param origen Es el contenido del vertice origen de la arista.
     * @param destino Es el contenido del vertice destino de la arista.
     * @return Regresa <b>true</b> si la arista se agregó correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregarArista(Object origen, Object destino) {
        /*
        // se ocupa saber si existe el origen y el destino
        ListaDinamica sublistaOrigen = buscarVerticeListaAdyacencia(origen);
        ListaDinamica sublistaDestino = buscarVerticeListaAdyacencia(destino);
        if (sublistaOrigen != null && sublistaDestino != null) { // si existen
            // sacamos el primer vertice de la sublista del destino
            Vertice verticeDestino = (Vertice) sublistaDestino.verPrimero();
            // agregar este vertice al final de la sublista del origen
            if (sublistaOrigen.agregar(verticeDestino) == 0) { // si se pudo agregar
                return true;
            } else { // no se pudo agregar
                return false;
            }
        } else { // no existen
            return false;
        }
         */
        return agregarArista(origen, destino, 1.0);
    }

    /**
     * Este método agrega una arista al grafo con un peso.
     * @param origen Es el contenido del vertice origen de la arista.
     * @param destino Es el contenido del vertice destino de la arista.
     * @param peso Es el peso de la arista.
     * @return Regresa <b>true</b> si la arista se agregó correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregarArista(Object origen, Object destino, double peso) {
        // se ocupa saber si existe el origen y el destino
        ListaDinamica sublistaOrigen = buscarVerticeListaAdyacencia(origen);
        ListaDinamica sublistaDestino = buscarVerticeListaAdyacencia(destino);
        if (sublistaOrigen != null && sublistaDestino != null) { // si existen
            // sacamos el primer vertice de la sublista del destino
            Vertice verticeDestino = (Vertice) sublistaDestino.verPrimero();
            // agregar este vertice al final de la sublista del origen
            if (sublistaOrigen.agregar(verticeDestino) == 0) { // si se pudo agregar
                // agregar el peso a la matriz de adyacencia
                Vertice verticeOrigen = (Vertice) sublistaOrigen.verPrimero();
                int posicionOrigen = verticeOrigen.getNumVertice() - 1;
                int posicionDestino = verticeDestino.getNumVertice() - 1;
                matrizAdyacencia.cambiar(posicionOrigen, posicionDestino, peso);
                return true;
            } else { // no se pudo agregar
                return false;
            }
        } else { // no existen
            return false;
        }
    }

    /**
     * Este método imprime la matriz de adyacencia del grafo.
     */
    public void imprimirMatrizAdyacencia() {
        matrizAdyacencia.imprimirXReglones();
    }

    /**
     * Este métdodo realiza el algoritmo de Kruskal para encontrar el árbol recubridor minimal y su costo mínimo.
     * @return Regresa el costo mínimo del árbol recubridor minimal.
     */
    public double algoritmoKruskal() {
        double costoTotal = 0.0;
        listaAdyacencia.iniciarIterador();
        while (listaAdyacencia.hayNodos() == true) {
            ListaDinamica subListaCadaVertice = (ListaDinamica) listaAdyacencia.obtenerNodo();
            Vertice primerVerticeCadaSubLista = (Vertice) subListaCadaVertice.verPrimero();
            int posicionOrigen = primerVerticeCadaSubLista.getNumVertice() - 1;
            subListaCadaVertice.iniciarIterador();
            subListaCadaVertice.obtenerNodo();
            while (subListaCadaVertice.hayNodos() == true) {
                Vertice verticeCadaSubLista = (Vertice) subListaCadaVertice.obtenerNodo();
                int posicionDestino = verticeCadaSubLista.getNumVertice() - 1;
                costoTotal += (double) matrizAdyacencia.obtenerValor(posicionOrigen, posicionDestino);
            }
        }
        return costoTotal;
    }

    /**
     * Este método imprime el grafo.
     */
    public void imprimir() {
        // recorrer la lista de adyacencia y sacar cada una de las sublistas
        listaAdyacencia.iniciarIterador();
        while (listaAdyacencia.hayNodos() == true) {
            ListaDinamica subListaCadaVertice = (ListaDinamica) listaAdyacencia.obtenerNodo();
            subListaCadaVertice.imprimir(); // imprimimos cada sublista
            SalidaPorDefecto.consola("\n");
        }
    }

    /**
     * Este método realiza el paso 3 del recorrido en profundidad (marca y empila vertices).
     * @param sublistaVerticeActual Es la sublista de adyacencia del vertice actual.
     * @param pila Es la pila donde se guardan los vertices.
     * @param marcados Es la lista de vertices marcados.
     */
    private void enpilarYMarcarVerticesAdyacentes(ListaDinamica sublistaVerticeActual, PilaDinamica pila, ListaDinamica marcados) {
        sublistaVerticeActual.iniciarIterador();
        sublistaVerticeActual.obtenerNodo(); // nos brincamos el primero, que es el vertice actual
        // empezaremos a partir del segundo, que son los vecinos del primero (vertice actual)
        while (sublistaVerticeActual.hayNodos() == true) {
            // obtener el elemento de la sublista (vecino)
            Vertice verticeVecino = (Vertice) sublistaVerticeActual.obtenerNodo();
            // checar si esta marcado
            if (marcados.buscar(verticeVecino.getContenido()) == null) { // no esta, por lo tanto no esta marcado
                // empilamos
                pila.poner(verticeVecino);
                // marcamos
                marcados.agregar(verticeVecino);
            }
        }
    }

    /**
     * Este método recorre el grafo en profundidad.
     * @param origen Es el vertice origen de donde empezará el recorrido en profundidad.
     * @return Regresa una lista dinámica con los vertices recorridos en profundidad.
     */
    public ListaDinamica recorridoProfundidad(Object origen) {
        ListaDinamica recorridoP = new ListaDinamica();
        PilaDinamica pila = new PilaDinamica();
        ListaDinamica marcados=new ListaDinamica();
        //Pasos:
        // 0. Validar la existencia del origen.
        ListaDinamica sublistaOrigen = buscarVerticeListaAdyacencia(origen);
        if (sublistaOrigen != null) { //existe
            // 1.- Partir de un vértice origen. Este vértice se marca y se mete en una pila.
            // sacar el vertice de la sublista
            Vertice verticeOrigen = (Vertice) sublistaOrigen.verPrimero();
            // marcamos este vértice origen
            marcados.agregar(verticeOrigen);
            // meter el origen en la pila
            pila.poner(verticeOrigen);
            while(pila.vacio() == false) {
                // 2.- Mientras existan vértices en la pila, se van a extraer (de uno por uno) y se procesarán (imprimir).
                Vertice verticeActual = (Vertice) pila.quitar(); // sacamos el vertice de la pila
                recorridoP.agregar(verticeActual.getContenido());// agregamos en la salida el contenido del vértice
                // 3.- Los vértices adyacentes (vecinos directos) no marcados y que están enlazados al nodo que actualmente
                // se procesa (el paso 2) se marcan y se meten en la pila.
                // obtener la sublista del vertice actual
                ListaDinamica sublistaVerticeActual = buscarVerticeListaAdyacencia(verticeActual.getContenido());
                enpilarYMarcarVerticesAdyacentes(sublistaVerticeActual, pila, marcados);
            }
        } else { // no existe
            return null;
        }
        return recorridoP;
    }
}