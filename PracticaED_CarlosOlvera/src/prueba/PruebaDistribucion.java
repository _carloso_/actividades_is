package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import herramientas.matematicas.DistribucionNormal;

public class PruebaDistribucion {
    public static void main(String[] args) {
        DistribucionNormal distribucionNormal = new DistribucionNormal("src/xEvaluacion2.txt");
        distribucionNormal.tabX.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Promedio = "+distribucionNormal.promedio());
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Desviacion Estandar = "+distribucionNormal.desviacionEstandar());
        SalidaPorDefecto.consola("\n");
        distribucionNormal.fx();
        ListaEstatica listaValores = distribucionNormal.valoresFx;
        listaValores.imprimir();
    }
}
