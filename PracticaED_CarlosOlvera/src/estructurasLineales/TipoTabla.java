package estructurasLineales;

public enum TipoTabla {
    FILA("RENGLON",1), COL("COLUMNA",2);

    private String nombre;
    private int valor;

    private TipoTabla(String renglon, int valor) {
        this.nombre = nombre;
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }

    public String getNombre() {
        return nombre;
    }
}
