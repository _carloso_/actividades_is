package pruebas.menus;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import registros.campesinodearroz.Campesino;

/**
 * Esta clase nos ayudara a probar el funcionamiento de nuestro programa con
 * un menu de opciones y metodos de prueba.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class MenuCampesinos {

    // Constantes para dar color al texto que se muestra en consola.
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_WHITE = "\u001B[37m";

    /**
     * Este metodo no muestra un menu de opciones, para probar el funcionamiento del programa.
     */
    public static void mostrarMenu() {
        SalidaPorDefecto.consola(ANSI_RED + "Menu de opciones" + ANSI_WHITE + "\n");
        SalidaPorDefecto.consola("1. Promedio anual de toneladas por campesino." + "\n");
        SalidaPorDefecto.consola("2. Cuántos meses tuvieron toneladas menores al promedio anual." + "\n");
        SalidaPorDefecto.consola("3. Mes que obtuvo más toneladas por cada año de un campesino." + "\n");
        SalidaPorDefecto.consola("4. Toneladas que se obtuvieron el último mes de cada" +
                                " año de un campesino." + "\n");
        SalidaPorDefecto.consola("5. Toneladas el primer trimestre de cada año de " +
                                "un campesino." + "\n");
        SalidaPorDefecto.consola("6. ¿Qué campesino ha realizado mejor su trabajo en cada año?." + "\n");
        SalidaPorDefecto.consola("7. ¿Qué mes es el que produce mejores dividendos a los campesinos?."
                                + "\n");
        SalidaPorDefecto.consola("8. ¿Qué época del año (primavera, verano, otoño o invierno) " +
                        "genera mayor ganancia en producción?." + "\n");
        SalidaPorDefecto.consola("9. Salir." + "\n");
        SalidaPorDefecto.consola("Elija una opción: " );
    }

    /**
     * Este metodo nos ayuda a probar el funcionamiento del programa llamando a los metodos
     * que se encuentran en el menu.
     * @param opcion Es la opcion que selecciono el usuario.
     */
    public static void menuCampesinos(int opcion, ListaEstatica listaCampesinos) {
        switch (opcion) {
            case 1 -> promedioAnual(listaCampesinos);
            case 2 -> mesesMenoresPromedio();
            case 3 -> mesCampesino(listaCampesinos);
            case 4 -> toneladasUltimoMes();
            case 5 -> toneladasPrimerTrimestre();
            case 6 -> mejorCampesino();
            case 7 -> mesMejorDividendo();
            case 8 -> epocaMejoresGanacias();
            case 9 -> SalidaPorDefecto.consola( ANSI_RED + "Saliendo...");
            default -> SalidaPorDefecto.consola("--- Opción inválida. ---" + "\n");
        }
    }

    /**
     * Este metodo calcula el promedio anual de toneladas que se obtuvieron por cada campesino.
     * @param listaCampesinos Es la lista de campesinos que se encuentra en el programa.
     */
    public static void promedioAnual(ListaEstatica listaCampesinos) {
        for (int posCampesino = 0; posCampesino < listaCampesinos.numeroElementos(); posCampesino++) {
            Campesino campesino = (Campesino) listaCampesinos.obtener(posCampesino);
            ListaEstatica listaAniosCampesino = campesino.getListaAnios();
            SalidaPorDefecto.consola( ANSI_GREEN + "--- Promedios anuales del campesino " +
                    campesino.getNumCampesino() + ", " +
                    campesino.getNombre() +
                    " " + campesino.getApellido() +
                    " ---" + ANSI_WHITE +"\n");
            for (int posicion = 0; posicion < listaAniosCampesino.numeroElementos(); posicion++) {
                double sumaToneladasAnio = 0.0;
                double promedioToneladasAnio = 0.0;
                ListaEstatica listaMesesAnio = (ListaEstatica) listaAniosCampesino.obtener(posicion);
                for (int posicionMes = 0; posicionMes < listaMesesAnio.numeroElementos(); posicionMes++) {
                    sumaToneladasAnio += Double.parseDouble(listaMesesAnio.obtener(posicionMes).toString());
                }
                promedioToneladasAnio = sumaToneladasAnio / listaMesesAnio.numeroElementos();
                SalidaPorDefecto.consola("->Promedio Año " + (posicion + 1) + ": "
                        + promedioToneladasAnio + "\n");
            }
        }
    }

    /**
     * Metodo que nos indica cuantos meses tuvieron toneladas menores al promedio anual de un campesino.
     */
    private static void mesesMenoresPromedio() {
        SalidaPorDefecto.consola("Metodo no implementado.");
    }

    /**
     * Este metodo calcula cuantos meses tuvieron toneladas menores al promedio anual de un determinado campesino.
     * @param listaCampesinos La lista de campesinos.
     */
    // No terminado al 100%
    public static void mesCampesino(ListaEstatica listaCampesinos) {
        for (int posCampesino = 0; posCampesino < listaCampesinos.numeroElementos(); posCampesino++) {
            Campesino campesino = (Campesino) listaCampesinos.obtener(posCampesino);
            ListaEstatica listaAniosCampesino = campesino.getListaAnios();
            SalidaPorDefecto.consola( ANSI_GREEN + "--- Promedios anuales del campesino " +
                    campesino.getNumCampesino() + ", " +
                    campesino.getNombre() +
                    " " + campesino.getApellido() +
                    " ---" + ANSI_WHITE +"\n");
            calcularPromedioUnAnio(listaAniosCampesino);
        }

    }

    /**
     * Este metodo calcula cuantas toneladas se obtuvieron el ultimo mes de cada anio de cada campesino.
     */
    public static void toneladasUltimoMes() {
        SalidaPorDefecto.consola("Metodo no implementado.");
    }

    /**
     * Metodo que cuenta las toneladas que se obtuvieron en el primer trimestre de cada anio de cada campesino.
     */
    public static void toneladasPrimerTrimestre() {
        SalidaPorDefecto.consola("Metodo no implementado.");
    }

    /**
     * Este metodo calcula cual fue el campesiono que mas producion obtuvo en cada anio.
     */
    public static void mejorCampesino() {
        SalidaPorDefecto.consola("Metodo no implementado.");
    }

    /**
     * Este metodo calcula cual fue el mes que mas producion obtuvo en cada anio.
     */
    public static void mesMejorDividendo() {
        SalidaPorDefecto.consola("Metodo no implementado.");
    }

    /**
     * Metodo que nos indica que epoca del anio (primavera, verano, otonio o invierno) genera mayor ganancia
     * en produccion de cada campesino.
     */
    public static void epocaMejoresGanacias() {
        SalidaPorDefecto.consola("Metodo no implementado.");
    }

    /**
     * Este metodo lee la opcion que el usuario ingreso y la retorna.
     * @return La opcion que el usuario ingreso <b>int</b>.
     */
    public static int leerOpcion() {
        return EntradaPorDefecto.consolaInteger();
    }

    /**
     * Este metodo calcula el promedio anual de toneladas de un determinado anio y campesino.
     * @param listaMesesAnio Es la lista que contine la poroducion por meses de un anio.
     * @return Regresa el promedio de toneladas de un anio <b>double</b>.
     */
    // Metodo no terminado
    private static Double calcularPromedioUnAnio(ListaEstatica listaMesesAnio) {
        double sumaToneladasAnio = 0.0;
        double promedioToneladasAnio = 0.0;
        for (int posicionMes = 0; posicionMes < listaMesesAnio.numeroElementos(); posicionMes++) {
            sumaToneladasAnio += Double.parseDouble(listaMesesAnio.obtener(posicionMes).toString());
        }
        promedioToneladasAnio = sumaToneladasAnio / listaMesesAnio.numeroElementos();
        return promedioToneladasAnio;
    }
}