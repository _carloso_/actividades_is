package estructurasNoLineales;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaEstatica;
import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaDinamicaClave;
import estructurasLineales.PilaEstatica;
import estructurasLineales.auxiliares.NodoDoble;
import herramientas.generales.Utilerias;

import java.util.Objects;

public class ArbolBinario {
    protected NodoDoble raiz;
    protected ListaDinamicaClave operadores = new ListaDinamicaClave();
    protected ListaDinamica operandos = new ListaDinamica();
    protected String in = "";
    protected String pre ="";
    protected String post ="";


    public ArbolBinario(){
        raiz=null;
    }

    public boolean generarArbol(){
        SalidaPorDefecto.consola("Indica la raíz: ");
        String contenidoNodo= EntradaPorDefecto.consolaCadenas();
        NodoDoble nuevoNodo=new NodoDoble(contenidoNodo);
        if(nuevoNodo!=null){ //hay espacio de memoria
            raiz=nuevoNodo;
            generarArbol(raiz);
            agruparOperadores(contenidoNodo); //se agrupa dependiendo su tipo de dato
            return true;
        }else{ //no hay espacio de memoria
            return false;
        }
    }
    private void generarArbol(NodoDoble subRaiz){ //Por cada Subraiz
        //Agregar hijo izquierdo
        SalidaPorDefecto.consola("¿El nodo "+ subRaiz.getContenido()+ " tiene hijo izquierdo ? ");
        String respuestaPreguntaHijoIzq=EntradaPorDefecto.consolaCadenas();
        if(respuestaPreguntaHijoIzq.equalsIgnoreCase("s")){ //si quiuere agregar un hijo izquierdo
            SalidaPorDefecto.consola("Indica el dato del hijo izquierdo: ");
            String contenidoNodoIzquierdo=EntradaPorDefecto.consolaCadenas();
            NodoDoble nuevoNodoIzquierdo=new NodoDoble(contenidoNodoIzquierdo);
            if(nuevoNodoIzquierdo!=null){ //si hay espacio
                agruparOperadores(contenidoNodoIzquierdo); //se agrupa dependiendo su tipo de dato
                subRaiz.setNodoIzq(nuevoNodoIzquierdo); //ligamos la subraiz a este hijo nuevo
                //llamada recursiva para ver si a este nodo izquierdo le corresponden tener hijo también
                generarArbol(subRaiz.getNodoIzq());
            }
        }//si no quiere tener hijo izquierdo, no hacemos nada, es decir, caso base

        //Agregar hijo derecho
        SalidaPorDefecto.consola("¿El nodo "+ subRaiz.getContenido()+ " tiene hijo derecho ? ");
        String respuestaPreguntaHijoDer=EntradaPorDefecto.consolaCadenas();
        if(respuestaPreguntaHijoDer.equalsIgnoreCase("s")){ //si quiuere agregar un hijo derecho
            SalidaPorDefecto.consola("Indica el dato del hijo derecho: ");
            String contenidoNodoDerecho=EntradaPorDefecto.consolaCadenas();
            NodoDoble nuevoNodoDerecho=new NodoDoble(contenidoNodoDerecho);
            if(nuevoNodoDerecho!=null){ //si hay espacio
                agruparOperadores(contenidoNodoDerecho); //se agrupa dependiendo su tipo de dato
                subRaiz.setNodoDer(nuevoNodoDerecho); //ligamos la subraiz a este hijo nuevo
                //llamada recursiva para ver si a este nodo derecho le corresponden tener hijo también
                generarArbol(subRaiz.getNodoDer());
            }
        }//si no quiere tener hijo derecho, no hacemos nada, es decir, caso base
    }

    public void inorden(){
        inorden(raiz);
    }

    private void inorden(NodoDoble subRaiz){
        //IRD
        if(subRaiz!=null) {
            inorden(subRaiz.getNodoIzq());
            SalidaPorDefecto.consola(subRaiz.getContenido() + " ");
            if (subRaiz.getContenido() != null) {
                in += (subRaiz.getContenido() + " "); //esta variable se utilizara para cambiar los valores despues

            }
            inorden(subRaiz.getNodoDer());
        } //en el else el caso base, donde n ose hace nada
    }


    public void preorden(){
        preorden(raiz);
    }

    private void preorden(NodoDoble subRaiz){
        //RID
        if(subRaiz!=null) {
            SalidaPorDefecto.consola(subRaiz.getContenido() + " ");
            if (subRaiz.getContenido() != null) {
                pre += (subRaiz.getContenido() + " "); //esta variable se utilizara para cambiar los valores despues

            }
            preorden(subRaiz.getNodoIzq());
            preorden(subRaiz.getNodoDer());
        } //en el else el caso base, donde n ose hace nada
    }

    public void postorden(){
        postorden(raiz);
    }

    private void postorden(NodoDoble subRaiz){
        //IDR
        if(subRaiz!=null) {
            postorden(subRaiz.getNodoIzq());
            postorden(subRaiz.getNodoDer());
            SalidaPorDefecto.consola(subRaiz.getContenido() + " ");
            post += (subRaiz.getContenido() + " "); //esta variable se utilizara para cambiar los valores despues

        } //en el else el caso base, donde n ose hace nada
    }

    public void postordenIterativo() {
        // I,D,R
        PilaEstatica pila;
        NodoDoble cur;
        NodoDoble pre;
        if (raiz != null) {
            pila = new PilaEstatica(obtenerAltura());
            cur = raiz;
            pre = raiz;
            pila.poner(raiz);
            while (!pila.vacio()) {
                cur = (NodoDoble) pila.verUltimo();
                if (cur == pre || cur == pre.getNodoIzq() || cur == pre.getNodoDer()) {
                    if (cur.getNodoIzq() != null) {
                        pila.poner(cur.getNodoIzq());
                    } else if (cur.getNodoDer() != null) {
                        pila.poner(cur.getNodoDer());
                    }
                    if (cur.getNodoIzq() == null && cur.getNodoDer() == null) {
                        SalidaPorDefecto.consola(pila.quitar() + " ");
                    }
                } else if (pre == cur.getNodoIzq()) {
                    if (cur.getNodoDer() != null) {
                        pila.poner(cur.getNodoDer());
                    } else if (cur.getNodoDer() == null) {
                        SalidaPorDefecto.consola(pila.quitar() + " ");
                    }
                } else if (pre == cur.getNodoDer()) {
                    SalidaPorDefecto.consola(pila.quitar() + " ");
                }
                pre = cur;
            }
        } else {
            SalidaPorDefecto.consola("El árbol está vacío.");
        }
    }


    public void anchuraCOLA(){
        ColaEstatica cola = new ColaEstatica(obtenerAltura());
        cola.poner(raiz);
        while (!cola.vacio()){
            NodoDoble nodo = (NodoDoble) cola.quitar(); //se agrega el valor eliminado al arbol
            SalidaPorDefecto.consola(nodo.getContenido() + " ");
            if(nodo.getNodoIzq() != null && nodo.getNodoDer() != null) {//si tiene AMBOS hijos
                cola.poner(nodo.getNodoIzq());
                cola.poner(nodo.getNodoDer());
            }
            else if(nodo.getNodoIzq() != null){
                cola.poner(nodo.getNodoIzq());
            }
            else if(nodo.getNodoDer() != null){
                cola.poner(nodo.getNodoDer());
            }
        }

    }

    public void anchuraPILA(){
        PilaEstatica pila = new PilaEstatica(obtenerAltura());
        pila.poner(raiz);
        while (!pila.vacio()){
            NodoDoble nodo = (NodoDoble) pila.quitar(); //se agrega el valor eliminado al arbol
            SalidaPorDefecto.consola(nodo.getContenido() + " ");
            if(nodo.getNodoIzq() != null && nodo.getNodoDer() != null) {//si tiene AMBOS hijos
                pila.poner(nodo.getNodoIzq());
                pila.poner(nodo.getNodoDer());
            }
            else if(nodo.getNodoIzq() != null){
                pila.poner(nodo.getNodoIzq());
            }
            else if(nodo.getNodoDer() != null){
                pila.poner(nodo.getNodoDer());
            }
        }

    }

    public void agruparOperadores(String valor){
        if (Objects.equals(valor, "*") || Objects.equals(valor, "/") || Objects.equals(valor, "+") || Objects.equals(valor, "-") || Objects.equals(valor, "(") || Objects.equals(valor, ")") || Objects.equals(valor, "^")){
            operandos.agregar(valor);
        }
        else {
            SalidaPorDefecto.consola("Asignele valor a la variable: ");
            int valorAsignado = EntradaPorDefecto.consolaInteger();
            operadores.agregar(valor, valorAsignado);
        }

    }

    public void imprimirListas(){
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Lista 1 (Lista Dinamica Clave): ");
        operadores.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Lista 2 (Lista Dinamica): ");
        operandos.imprimir();
        SalidaPorDefecto.consola("\n");
    }

    public void cambiarValores(String valorViejo, String valorNuevo){
        in = in.replace(valorViejo,valorNuevo);
        pre = pre.replace(valorViejo,valorNuevo);
        post = post.replace(valorViejo,valorNuevo);
        operadores.cambiarClave(valorViejo,valorNuevo); //se cambia el valor en la lista clave
    }

    public void imprimirCambios(){
        SalidaPorDefecto.consola("InOrden: "+in+"\n");
        SalidaPorDefecto.consola("PreOrden: "+pre+"\n");
        SalidaPorDefecto.consola("PostOrden: "+post+"\n");

    }

    public int obtenerAltura(){
        return obtenerAltura(raiz);
    }

    /**
     * Este metodo recorre el arbol para obtener su altura
     * @param subRaiz Es la subRaiz del arbol que se recorre
     * @return regresa la altura del arbol
     */
    private int obtenerAltura(NodoDoble subRaiz){
        if (subRaiz == null){
            return 0;
        }
        int alturaIzquierda = obtenerAltura(subRaiz.getNodoIzq());
        int alturaDerecha = obtenerAltura(subRaiz.getNodoDer());
        if (alturaIzquierda > alturaDerecha){
            return alturaIzquierda+1;
        }
        else {
            return alturaDerecha+1;
        }
    }

    public int obtenerNivel(String contenido){
        return  obtenerNivel(raiz,contenido, 1);
    }

    /**
     * Este mnetodo recorre el arbol para obtener el nivel en el que se encuentra algun nodo
     * @param subRaiz Es la subraiz del arbol que se esta recorriendo
     * @param contenido el el valor del nodo buscado
     * @param nivel es el nivel del nodo
     * @return regresa el nivel
     */
    private int obtenerNivel(NodoDoble subRaiz, String contenido, int nivel) {
        if (subRaiz != null) {
            if (subRaiz.getContenido().equals(contenido)) {
                return nivel;
            }
            int nivelIzquierdo = obtenerNivel(subRaiz.getNodoIzq(), contenido, nivel + 1);
            if (nivelIzquierdo != -1) {
                return nivelIzquierdo;
            }
            int nivelDerecho = obtenerNivel(subRaiz.getNodoDer(), contenido, nivel + 1);
            if (nivelDerecho != -1) {
                return nivelDerecho;
            }
        }
        return  -1;
    }

    public void contarNodosXnivel(){
        SalidaPorDefecto.consola("Nodos por nivel: "+"\n");
        int numNiveles = obtenerAltura();
        for(int i = 1 ; i <= numNiveles; i++){
            SalidaPorDefecto.consola("El nivel "+i + "tiene "+ contarNodosXnivel(raiz,i) + " nodos" + "\n");
        }
    }

    private int contarNodosXnivel(NodoDoble subRaiz, int nivel){
        int contador = 0;
        if (subRaiz != null){
            if(nivel == 1){
                contador++;
            }
            else {
                contador += contarNodosXnivel(subRaiz.getNodoIzq(), nivel-1);
                contador += contarNodosXnivel(subRaiz.getNodoDer(), nivel-1);
            }
        }
        return contador;
    }

    /*public void obtenerInfoNodo(String contenido){
        obtenerInfoNodo(raiz,contenido);
    }

    private void obtenerInfoNodo(NodoDoble subRaiz, String contenido){
        NodoDoble nodoPadre = null;
        if (subRaiz != null){
            if (subRaiz.getContenido().equals(contenido)){
                if (subRaiz == raiz){
                    SalidaPorDefecto.consola("Nodo raiz, no tiene padre");
                }
                else if (subRaiz.getNodoIzq() == null && subRaiz.getNodoDer() == null){
                    SalidaPorDefecto.consola("Nodo hoja, ");
                    nodoPadre = obtenerNodoPadre(raiz,subRaiz);
                    if (nodoPadre != null){
                        SalidaPorDefecto.consola("Su padre es "+ nodoPadre.getContenido());
                    }
                    else{
                        SalidaPorDefecto.consola("no tiene padre");
                    }
                }
                else{
                    SalidaPorDefecto.consola("Nodo intermedio, ");
                    nodoPadre = obtenerNodoPadre(raiz,subRaiz);
                    if (nodoPadre != null){
                        SalidaPorDefecto.consola("Su padre es "+ nodoPadre.getContenido());
                    }
                    else{
                        SalidaPorDefecto.consola("no tiene padre");
                    }
                }
            }
            obtenerInfoNodo(subRaiz.getNodoIzq(),contenido);
            obtenerInfoNodo(subRaiz.getNodoDer(),contenido);
        }
    }*/
}
