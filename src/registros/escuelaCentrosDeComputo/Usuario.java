package registros.escuelaCentrosDeComputo;

import estructurasLineales.ListaDinamica;

/**
 * Esta clase representa un usuario de una computadora de un centro de cómputo.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class Usuario {

    protected int numUsuario;
    protected String nombre;
    protected String apellido;
    protected String correo;
    protected boolean estatusDeUso; // true = uso, false = no uso
    protected String horaInicio;
    protected String horaFin;
    protected String fechaDeUso;
    protected ListaDinamica aplicacionesUsadas;

    public Usuario(int numUsuario, String nombre, String apellido, String correo, boolean estatusDeUso, String horaInicio, String horaFin, String fechaDeUso) {
        this.numUsuario = numUsuario;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.estatusDeUso = estatusDeUso;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
        this.fechaDeUso = fechaDeUso;
        this.aplicacionesUsadas = new ListaDinamica();
    }

    public int getNumUsuario() {
        return numUsuario;
    }

    public void setNumUsuario(int numUsuario) {
        this.numUsuario = numUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public boolean isEstatusDeUso() {
        return estatusDeUso;
    }

    public void setEstatusDeUso(boolean estatusDeUso) {
        this.estatusDeUso = estatusDeUso;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getFechaDeUso() {
        return fechaDeUso;
    }

    public void setFechaDeUso(String fechaDeUso) {
        this.fechaDeUso = fechaDeUso;
    }

    public ListaDinamica getAplicacionesUsadas() {
        return aplicacionesUsadas;
    }

    public void setAplicacionesUsadas(ListaDinamica aplicacionesUsadas) {
        this.aplicacionesUsadas = aplicacionesUsadas;
    }

    /**
     * Este método nos permite agregar una aplicación usada por el usuario.
     * @param aplicacionUsada Es la aplicación que fue usada por el usuario.
     * @return Regresa true si se pudo agregar la aplicación, false si no se pudo agregar.
     */
    public boolean agregarAplicacionUsada(Aplicacion aplicacionUsada){
        int valorRetornoAgregar = aplicacionesUsadas.agregar(aplicacionUsada);
        if (valorRetornoAgregar == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Este método nos permite obtener las aplicaciones usadas por el usuario.
     * @return Regresa una cadena con las aplicaciones usadas por el usuario.
     */
    private String aplicacinesUsadas() {
        String aplicaciones = "";
        aplicacionesUsadas.iniciarIterador();
        while(aplicacionesUsadas.hayNodos() == true) {
            Aplicacion aplicacionUsada = (Aplicacion) aplicacionesUsadas.obtenerNodo();
            aplicaciones += aplicacionUsada.getNombre() + " ";
        }
        return aplicaciones;
    }

    /**
     * Este método nos permite obtener la información del usuario.
     * @return Regresa una cadena con la información del usuario.
     */
    public String obtenerDatos(){
        return "No. de usuario: " + numUsuario +
                "\nNombre: " + nombre +
                "\nApellido: " + apellido +
                "\nCorreo: " + correo +
                "\nEstatus de uso: " + estatusDeUso +
                "\nHora de inicio: " + horaInicio +
                "\nHora de fin: " + horaFin +
                "\nAplicaciones usadas: " + aplicacinesUsadas() + "\n";
    }

    @Override
    public String toString(){
        return numUsuario + "";
    }
}