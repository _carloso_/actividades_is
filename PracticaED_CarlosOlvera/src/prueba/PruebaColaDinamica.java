package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaDinamica;

public class PruebaColaDinamica {
    public static void main(String[] args) {
        ColaDinamica cola=new ColaDinamica();

        cola.poner("F");
        cola.poner("J");
        cola.poner("A");
        cola.poner("K");
        cola.poner("Y");
        cola.poner("Z");

        cola.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("Ultimo valor: "+cola.verUltimo());

        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("Atendiendo al siguiente: " + cola.quitar()+ "\n");

        cola.imprimir();
        SalidaPorDefecto.consola("\n");
    }
}
