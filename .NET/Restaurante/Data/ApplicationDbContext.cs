﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Restaurante.Models;


namespace Restaurante.Data;

public class ApplicationDbContext : IdentityDbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public DbSet<Postre> Postres  {get; set;}
    public DbSet<Bebidas> Bebidas  {get; set;}
    public DbSet<Comida> Comidas  {get; set;}
    public DbSet<Entradas> Entradas  {get; set;}
}
