package estructurasLineales;

/**
 * Esta interfaz gestiona la funcinalidad de un vector.
 * La clase hereda de la interfaz ListaAlmacenamineto.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public interface VectorLista extends ListaAlmacenamiento {
    /**
     * Este método determina si un vector esta lleno.
     * @return Regresa <b>true</b> si el vector esta lleno, <b>false</b> en caso contrario.
     */
    public boolean llena();

    /**
     * Este metodo nos indica el tamanio maximo del vector.
     * @return Regresa el tamanio maximo del vector.
     */
    public int maximo();

    /**
     * Este metodo nos indica el numero de elementos que tiene el vector.
     * @return Regresa el numero de elementos del vector.
     */
    public int numeroElementos();

    /**
     * Este metodo nos regresa el objeto de una determinada posicion.
     * @param indice Posicion del objeto que se desea obtener.
     * @return Regresa el objeto de la posicion indicada y <p>null</p> en caso de que no exista.
     */
    public Object obtener(int indice);

    /**
     * Este metodo modifica el elemento de una determinada posicion por uno nuevo valor.
     * @param indice Es la posicion del elemento que se desea modificar.
     * @param valor Es el nuevo elemento que se desea asignar.
     * @return Regresa <b>true</b> si se pudo modificar el elemento, <p>false</p> en caso contrario.
     */
    public boolean cambiar(int indice, Object valor);

    /**
     * Este metodo modifica el elemento del arreglo actual en las posiciones indicadas por los indices,
     * por el contenido de valoresNuevos.
     * @param indicesBusqueda Es un arreglo con los indices de las posiciones que se desean modificar.
     * @param valoresNuevos Es un arreglo con los valores que se desean asignar a las posiciones indicadas.
     * @return Regresa <b>true</b> si se pudieron modificar los elementos, <p>false</p> en caso contrario.
     */
    public boolean cambiarListaEstatica(ListaEstatica indicesBusqueda, ListaEstatica valoresNuevos);

    /**
     * Este metodo elimina un elemento del la lista.
     * @param indice Es la posicion del elemento que se desea eliminar.
     * @return Regresa el elemento eliminado o <p>null</p> en caso de que no exista o no se pudo eliminar.
     */
    public Object eliminar(int indice);

    /**
     * Este metodo redimensiona el tamaño del arreglo a un nuevo tamaño indicado.
     * @param maximo Es el nuevo tamaño del arreglo.
     * @return Regresa un arreglo con el nuevo tamaño.
     */
    public Object redimensionar(int maximo);
}