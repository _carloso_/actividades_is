package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasNoLineales.ArbolBinarioBusqueda;
import estructurasNoLineales.ArbolMonticulo;
import estructurasNoLineales.TipoOrdenRecorrido;

import static estructurasNoLineales.TipoOrdenRecorrido.*;

public class PruebaAM {
    public static void main(String[] args) {
        ArbolBinarioBusqueda arbolBinario = new ArbolBinarioBusqueda();
        ArbolMonticulo arbolASCENDENTE = new ArbolMonticulo(ASC);
        ArbolMonticulo arbolDESCENDENTE = new ArbolMonticulo(DESC);
        SalidaPorDefecto.consola("-----ARBOL BINARIO DE BUSQUEDA: \n");
        arbolBinario.agregar("20");
        arbolBinario.agregar("18");
        arbolBinario.agregar("9");
        arbolBinario.agregar("8");
        arbolBinario.agregar("10");
        arbolBinario.agregar("12");
        SalidaPorDefecto.consola("InOrden: ");
        arbolBinario.inorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PreOrden: ");
        arbolBinario.preorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PostOrden recursivo: ");
        arbolBinario.postorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Anchura: ");
        arbolBinario.anchuraCOLA();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----FORMA ASCENDENTE: \n");
        arbolASCENDENTE.agregar("20");
        arbolASCENDENTE.agregar("18");
        arbolASCENDENTE.agregar("9");
        arbolASCENDENTE.agregar("8");
        arbolASCENDENTE.agregar("10");
        arbolASCENDENTE.agregar("12");
        SalidaPorDefecto.consola("InOrden: ");
        arbolASCENDENTE.inorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PreOrden: ");
        arbolASCENDENTE.preorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PostOrden recursivo: ");
        arbolASCENDENTE.postorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Anchura: ");
        arbolASCENDENTE.anchuraCOLA();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----FORMA DESCENDENTE: \n");
        arbolDESCENDENTE.agregar("20");
        arbolDESCENDENTE.agregar("18");
        arbolDESCENDENTE.agregar("9");
        arbolDESCENDENTE.agregar("8");
        arbolDESCENDENTE.agregar("10");
        arbolDESCENDENTE.agregar("12");
        SalidaPorDefecto.consola("InOrden: ");
        arbolDESCENDENTE.inorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PreOrden: ");
        arbolDESCENDENTE.preorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PostOrden recursivo: ");
        arbolDESCENDENTE.postorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Anchura: ");
        arbolDESCENDENTE.anchuraCOLA();
        SalidaPorDefecto.consola("\n");
    }

}

