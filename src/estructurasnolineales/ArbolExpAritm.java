package estructurasnolineales;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaDinamicaClave;
import estructurasLineales.PilaEstatica;
import estructurasLineales.auxiliares.NodoDoble;
import herramientas.matematicas.ExpresionesAritmeticas;

import java.util.Objects;
/**
 * Esta clase representa un árbol de expresiones aritméticas.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class ArbolExpAritm extends ArbolBinario {

    private ListaDinamicaClave operadores;
    private ListaDinamica operandos;

    public ArbolExpAritm() {
        super();
        operadores = new ListaDinamicaClave();
        operandos = new ListaDinamica();
    }

    /**
     * Este método se encarga de generar el arbol de forma manual por parte del usuario.
     * @return Regresea true si el arbol se genero correctamente, false si nos se pudo generar el arbol.
     */
    @Override
        public boolean generarArbol(){
        SalidaPorDefecto.consola("Indica la raiz del arbol: ");
        String contenidoNodo = EntradaPorDefecto.consolaCadenas();
        NodoDoble nuevoNodo = new NodoDoble(contenidoNodo);
        if(nuevoNodo != null){ //hay espacio de memoria
            raiz = nuevoNodo;
            generarArbol(raiz);
            agruparOperadores(contenidoNodo); //se agrupa dependiendo su tipo de dato
            return true;
        }else{ //no hay espacio de memoria
            return false;
        }
    }

    /**
     * Este Este método se encarga de generar el arbol de forma manual por parte del usuario (método recursivo).
     * @param subRaiz Es una subRaiz del arbol que se va a crear.
     */
    private void generarArbol(NodoDoble subRaiz){ //Por cada Subraiz
        //Agregar hijo izquierdo
        SalidaPorDefecto.consola("¿El nodo " + subRaiz.getContenido() + " tiene hijo izquierdo? (s/n): ");
        String respuestaPreguntaHijoIzq=EntradaPorDefecto.consolaCadenas();
        if (respuestaPreguntaHijoIzq.equalsIgnoreCase("s")) { //si quiuere agregar un hijo izquierdo
            SalidaPorDefecto.consola("Indica el contenido del hijo izquierdo de " + subRaiz.getContenido() + ": ");
            String contenidoNodoIzquierdo=EntradaPorDefecto.consolaCadenas();
            NodoDoble nuevoNodoIzquierdo=new NodoDoble(contenidoNodoIzquierdo);
            if (nuevoNodoIzquierdo!=null) { //si hay espacio
                agruparOperadores(contenidoNodoIzquierdo); //se agrupa dependiendo su tipo de dato
                subRaiz.setNodoIzq(nuevoNodoIzquierdo); //ligamos la subraiz a este hijo nuevo
                //llamada recursiva para ver si a este nodo izquierdo le corresponden tener hijo también
                generarArbol(subRaiz.getNodoIzq());
            }
        }//si no quiere tener hijo izquierdo, no hacemos nada, es decir, caso base

        //Agregar hijo derecho
        SalidaPorDefecto.consola("¿El nodo " + subRaiz.getContenido() + " tiene hijo derecho? (s/n): ");
        String respuestaPreguntaHijoDer=EntradaPorDefecto.consolaCadenas();
        if (respuestaPreguntaHijoDer.equalsIgnoreCase("s")) { //si quiuere agregar un hijo derecho
            SalidaPorDefecto.consola("Indica el contenido del hijo derecho de " + subRaiz.getContenido() + ": ");
            String contenidoNodoDerecho=EntradaPorDefecto.consolaCadenas();
            NodoDoble nuevoNodoDerecho=new NodoDoble(contenidoNodoDerecho);
            if (nuevoNodoDerecho!=null) { //si hay espacio
                agruparOperadores(contenidoNodoDerecho); //se agrupa dependiendo su tipo de dato
                subRaiz.setNodoDer(nuevoNodoDerecho); //ligamos la subraiz a este hijo nuevo
                //llamada recursiva para ver si a este nodo derecho le corresponden tener hijo también
                generarArbol(subRaiz.getNodoDer());
            }
        }//si no quiere tener hijo derecho, no hacemos nada, es decir, caso base
    }

    /**
     * Este método genera un arbol con base en una expresion aritmética inFija.
     * @param infija Es la expresion aritmética inFija.
     * @return Regresa true si el arbol se genero correctamente o false si nos se pudo generar.
     */
    public boolean generarArbolInfija(String infija) {
        // Paso 1: Convertir de manera sistemática una expresión infija a postfija (prefija).
        //invocamos a un método que convierta infija -> postfija
        //generarArbolPostfija(postfija)
        return true;
    }

    /**
     * Este método genera un arbol con base en una expresion aritmética postFija.
     * @param postfija Es la expresion aritmética postFija.
     * @return Regresa true si el arbol se genero correctamente o false si nos se pudo generar.
     */
    public boolean generarArbolPostfija(String postfija) {
        PilaEstatica pila = new PilaEstatica(postfija.length());

        for(int posToken = 0; posToken < postfija.length();posToken++) {
            // Paso 2: (Asumiendo que se tiene una expresión postfija) Tokenizar la expresión izq -> der.
            char token=postfija.charAt(posToken);
            // Paso 3: Comparar
            if(ExpresionesAritmeticas.esOperando(token)==true) {
                // 3.1 Si el token es un operando, crear un nodo con el token como contenido y se mete en la pila.
                NodoDoble nuevoNodo= new NodoDoble(token);
                if(pila.poner(nuevoNodo)==false) {
                    return false;
                }
            } else {
                // 3.2 Si el token es un operador, crear un nodo con el token como contenido; sacando dos nodos
                // (operandos) de la pila (la primera extracción es op2, la siguiente es op1),
                // enlazándolos a la subraíz creada (nodo creado). El nodo nuevo (subraíz) se mete en la pila.
                NodoDoble nuevoNodo=new NodoDoble(token);
                NodoDoble operando2=(NodoDoble) pila.quitar(); //hijo derecho
                NodoDoble operando1=(NodoDoble) pila.quitar(); //hijo izquyierdo
                nuevoNodo.setNodoIzq(operando1);
                nuevoNodo.setNodoDer(operando2);
                if(pila.poner(nuevoNodo)==false) {
                    return false;
                }
            }
        }
        // Paso 4: La raíz del árbol quedará guardada en la pila (solo debe haber un solo elemento).
        NodoDoble nodoRaiz= (NodoDoble) pila.quitar();
        if(nodoRaiz != null){
            raiz = nodoRaiz;
            return true;
        }else{
            return false;
        }
    }

    /**
     * Este método agrupa los operadores y los operandos del arbol de expresión.
     * @param valor Es el valor que se va a agrupar.
     */
    private void agruparOperadores(String valor){
        if (Objects.equals(valor, "*") || Objects.equals(valor, "/") || Objects.equals(valor, "+") ||
                Objects.equals(valor, "-") || Objects.equals(valor, "(") || Objects.equals(valor, ")") ||
                Objects.equals(valor, "^")){
            operandos.agregar(valor);
        }
        else {
            SalidaPorDefecto.consola("Ingresa el valor de la variable `" + valor + "´: ");
            int valorAsignado = EntradaPorDefecto.consolaInteger();
            operadores.agregar(valor, valorAsignado);
        }
    }

    /**
     * Este método muestra las listas enlazadas con los operadores y operandos del arbol.
     */
    public void mostrarListasDinamicas() {
        SalidaPorDefecto.consola("Lista 1 (Lista Dinamica Clave): ");
        operadores.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Lista 2 (Lista Dinamica): ");
        operandos.imprimir();
        SalidaPorDefecto.consola("\n");
    }

    /**
     * Este método se encarga de verificar si un caracter es un operador.
     * @param token Es el caracter que se va a verificar.
     * @return Regresa <b>true</b> si el caracter es un operador, <b>false</b> si no lo es.
     */
    public boolean esOperador(char token) {
        char[] operadores = {'+', '-', '*', '/', '^'};
        int cont = 0;
        for (int oper = 0; oper < operadores.length; oper++) {
            if (token == operadores[oper]) {
                cont += 1;
            }
        }
        if (cont == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método se encarga de generar un arbol de expresión a partir de una expresión ingresada por el usuario.
     * @param expresion Es la expresión aritmética con la que se va a generar el arbol.
     * @return Regresa <b>true</b> si se generó el arbol correctamente, <b>false</b> si no se pudo generar.
     */
    public boolean generarArbolExpresiones(String expresion) {
        // z^n-e/g+(b*d) ----------> (((z^n)-(e/g))+(b*d))
        // 1.- La primera vez que se encuentre un paréntesis a la izquierda, crear un nodo que será el nodo raíz.
        // Se llama a éste nodo actual y se mete en la pila.
        // 2.- Cada vez que se encuentre un nuevo paréntesis a la izquierda, crear un nuevo nodo.
        // Si el nodo actual no tiene un hijo izquierdo, hacer el nuevo nodo el hijo izquierdo; en caso contrario hacerlo hijo derecho.
        // Hacer el nuevo nodo el nodo actual y meterlo en la pila.
        // 3.- Cuando se encuentra un operando, crear un nuevo nodo y asignar el operando a su componente de datos.
        // Si el nodo actual no tiene un hijo izquierdo, hacer el nuevo nodo el hijo izquierdo;
        // en caso contrario, hacerlo el hijo derecho.
        // 4.- Cuando se encuentre un operador, sacar un nodo de la pila (ahora será el nodo actual)
        // y asignarlo en el campo de datos del nodo.
        // 5.- Ignorar paréntesis derecho y espacios.

        PilaEstatica pila = new PilaEstatica(expresion.length());

        for (int i = 0; i < expresion.length(); i++) {
            char token = expresion.charAt(i);
            if (token == '(' && raiz == null) {
                if (pila.vacio()) {
                    NodoDoble nodoRaiz = raiz;
                    NodoDoble nodoActual = new NodoDoble(token);
                    nodoRaiz.setNodoIzq(nodoActual);
                    pila.poner(nodoActual);
                } else {
                    NodoDoble nodoActual = new NodoDoble(token);
                    NodoDoble nodoRaiz = (NodoDoble) pila.quitar();
                    if (nodoRaiz.getNodoIzq() == null) {
                        nodoRaiz.setNodoIzq(nodoActual);
                    } else {
                        nodoRaiz.setNodoDer(nodoActual);
                    }
                }
            } else if (esOperador(token)) {
                NodoDoble nodoActual = new NodoDoble(token);
                NodoDoble nodoRaiz = (NodoDoble) pila.quitar();
                if (nodoRaiz.getNodoIzq() == null) {
                    nodoRaiz.setNodoIzq(nodoActual);
                } else {
                    nodoRaiz.setNodoDer(nodoActual);
                }
                pila.poner(nodoActual);
            } else {
                NodoDoble nodoActual = new NodoDoble(token);
                NodoDoble nodoRaiz = (NodoDoble) pila.quitar();
                if (nodoRaiz.getNodoIzq() == null) {
                    nodoRaiz.setNodoIzq(nodoActual);
                } else {
                    nodoRaiz.setNodoDer(nodoActual);
                }
                pila.poner(nodoActual);
            }
        }
        return true;
    }
}