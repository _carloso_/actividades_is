package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;

public class PruebaListaEstatica {
    public static void main(String[] args) {
        ListaEstatica lista = new ListaEstatica(6);
        ListaEstatica lista2 = new ListaEstatica(6);
        ListaEstatica lista3 = new ListaEstatica(3);

        SalidaPorDefecto.consola("------ Lista 1 ------" + "\n");
        lista.agregar("F");
        lista.agregar("D");
        lista.agregar("A");
        lista.agregar("S");
        lista.agregar("S");
        lista.agregar("S");
        lista.imprimir();


        SalidaPorDefecto.consola("------ Lista 2 ------" + "\n");
        lista2.agregar("F");
        lista2.agregar("D");
        lista2.agregar("A");
        lista2.agregar("S");
        lista2.agregar("S");
        lista2.agregar("S");
        lista2.imprimir();

        SalidaPorDefecto.consola("------ Lista 3 ------" + "\n");
        lista3.agregarLista(lista);
        lista3.imprimir();

        SalidaPorDefecto.consola("Cantidad maxima de elementos de la lista 2: " +
                lista2.maximo() + "\n");

        SalidaPorDefecto.consola("Numeros de elementos de la lista 1: " +
                lista.numeroElementos() + "\n");

        SalidaPorDefecto.consola("Obtener el elemento en la posicion 2 de la lista 1: " +
                lista.obtener(2) + "\n");

        SalidaPorDefecto.consola("Las lista 1 y 2 son iguales: " +
                lista.esIgual(lista2) + "\n");

        lista3.invertir();
        SalidaPorDefecto.consola("------ Lista 3 invertida ------" + "\n");
        lista3.imprimir();

        SalidaPorDefecto.consola("las listas son iguales: " + lista.esIgual(lista2) + "\n");

        SalidaPorDefecto.consola("Eliminar elemnto del indice 2, lista 1: " +
                lista.eliminar(2) + "\n");
        SalidaPorDefecto.consola("------ Lista 1 ------" + "\n");
        lista.imprimir();

        SalidaPorDefecto.consola("Ultimo elemento de la lista2: " + lista2.eliminar() + "\n");

        SalidaPorDefecto.consola("Cuantas veces aparece el elemento 'S' en la lista1: " +
                lista.contar("S") + "\n");

        SalidaPorDefecto.consola("Se eliminaron todos los elementos de la lista3 en la lista2: " +
                lista2.eliminarLista(lista) + "\n");
        lista2.imprimir();

        SalidaPorDefecto.consola("----- Copia de la lista 3 -----\n");
        ListaEstatica listaCopia = (ListaEstatica) lista3.clonar();
        listaCopia.imprimir();

        SalidaPorDefecto.consola("----- SubLista de la lista 1 -----" + "\n");
        ListaEstatica subLista = (ListaEstatica) lista.subLista(1, 4);
        if (subLista != null) {
            subLista.imprimir();
        } else {
            SalidaPorDefecto.consola("No se pudo crear la sublista" + "\n");
        }

        SalidaPorDefecto.consola("----- SubLista de la lista 2 -----" + "\n");
        ListaEstatica subLista2 = (ListaEstatica) lista2.subLista(0, 2);
        if (subLista2 != null) {
            subLista2.imprimir();
        } else {
            SalidaPorDefecto.consola("No se pudo crear la sublista" + "\n");
        }

        SalidaPorDefecto.consola("Contar cuntas veces aparece el elemento 'S' en la lista 1: " +
                lista.contar("S") + "\n");

        lista.imprimir();
      SalidaPorDefecto.consola("Cambiar ValoresViejos por ValoresNuevos: " +
              lista.cambiar("S","W", 2) + "\n");
      lista.imprimir();
      SalidaPorDefecto.consola("\n");

      ListaEstatica listaIndices = new ListaEstatica(8);
      listaIndices.agregar(0);
      listaIndices.agregar(1);
      listaIndices.agregar(2);
      listaIndices.agregar(3);
      listaIndices.agregar(4);
      listaIndices.agregar(5);
      listaIndices.agregar(6);
      ListaEstatica listaValores = new ListaEstatica(8);
      listaValores.agregar("L");
      listaValores.agregar("O");
      listaValores.agregar("Z");
      listaValores.agregar("V");
      listaValores.agregar("E");
      listaValores.agregar("R");
      listaValores.agregar("K");
      lista.imprimir();
      SalidaPorDefecto.consola("Cambiar Lista Estatica: " +
              lista.cambiarListaEstatica(listaIndices, listaValores) + "\n");
      lista.imprimir();

      SalidaPorDefecto.consola("Rellenar Lista Estatica con M una cantidad de 3: " + "\n");
      lista.rellenar("M", 3);
      lista.imprimir();
    }
}