package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamicaOrdenada;
import estructurasLineales.ListaEstatica;
import estructurasNoLineales.Matriz2;

import static estructurasLineales.TipoOrden.*;

public class PruebaListaDinamicaOrdenada {
    public static void main(String[] args) {
        ListaDinamicaOrdenada listaIncremental = new ListaDinamicaOrdenada(INC);
        listaIncremental.agregar("H");
        listaIncremental.agregar("P");
        listaIncremental.agregar("F");
        listaIncremental.agregar("U");

        SalidaPorDefecto.consola("--- ORDENAR DE FORMA ASCENDENTE ---" + "\n");
        listaIncremental.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola( "----INVERSA---" + "\n");
        listaIncremental.imprimirOI();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola( "----BUSCAR---" + "\n");
        SalidaPorDefecto.consola("Buscar el valor U: " + listaIncremental.buscar("U"));

        // esta lista se va agregar a la lista dinámica 1
        ListaEstatica listaEstatica1 = new ListaEstatica(5);
        listaEstatica1.agregar("A");
        listaEstatica1.agregar("B");
        listaEstatica1.agregar("W");
        listaEstatica1.agregar("Z");

        SalidaPorDefecto.consola("\n\n---LISTA ESTATICA---" + "\n");
        listaEstatica1.imprimir();


        listaIncremental.agregarLista(listaEstatica1);
        SalidaPorDefecto.consola("\n---LISTA DINAMICA CON LISTA ESTATICA---" + "\n");
        listaIncremental.imprimir();

        ListaDinamicaOrdenada listaDecremental = new ListaDinamicaOrdenada(DEC);
        listaDecremental.agregar("H");
        listaDecremental.agregar("P");
        listaDecremental.agregar("F");
        listaDecremental.agregar("U");
        listaDecremental.agregar("U");

        SalidaPorDefecto.consola("\n\n");
        SalidaPorDefecto.consola("---ORDENAR DE FORMA DESENDENTE ---" + "\n");
        listaDecremental.imprimir();


        SalidaPorDefecto.consola("\n\n*Cambiar el valor U por G 2 Veces: " +
                listaDecremental.cambiar("U", "G", 2) + "\n");
        listaDecremental.imprimir();
        SalidaPorDefecto.consola("\n");

        Matriz2 tabla = new Matriz2(3, 3);
        tabla.cambiar(0, 0, "R");
        tabla.cambiar(0, 1, "M");
        tabla.cambiar(0, 2, "A");
        tabla.cambiar(1, 0, "P");
        tabla.cambiar(1, 1, "B");
        tabla.cambiar(1, 2, "G");
        tabla.cambiar(2, 0, "C");
        tabla.cambiar(2, 1, "N");
        tabla.cambiar(2, 2, "H");

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("---MATRIZ 2D----" + "\n");
        tabla.imprimirXRenglones();
        SalidaPorDefecto.consola("\n---LISTA DINAMICA CON MATRIZ 2D---" + "\n");
        listaDecremental.agregar("R");
        listaDecremental.agregar("M");
        listaDecremental.agregar("A");
        listaDecremental.agregar("P");
        listaDecremental.agregar("B");
        listaDecremental.agregar("G");
        listaDecremental.agregar("C");
        listaDecremental.agregar("N");
        listaDecremental.agregar("H");
        listaDecremental.imprimir();

    }

}
