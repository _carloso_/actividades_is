﻿/*Ejercicio 2

1.- Heredar de la clase Automóvil y crear la clase AutomóvilLujo con las siguientes propiedades
	-AireAcondicionado 10,000
	-VentanaElectrica 20,000
	-AsientosPremium 15,00
	-PantallaTouch 20,00
	
2.- Crear constructor que llame al constructor base y además reciba todos los valores de esta clase.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio
{
	internal class AutomovilDeLujo : Automovil
	{
		private Boolean _AireAcondicionado;
		private Boolean _VentanaElectrica;
		private Boolean _AsientosPremium;
		private Boolean _PantallaTouch;

		public AutomovilDeLujo(int id, string marca, string modelo, int km, float precio) : base(id, marca, modelo, km, precio)
		{
			this._AireAcondicionado = false;
			this._VentanaElectrica = false;
			this._AsientosPremium = false;
			this._PantallaTouch = false;
		}
		public void agregarAireAcondicionado()
		{
			this._AireAcondicionado = true;
			this.precio += 10000;
		}
		public void agregarVentanaElectrica()
		{
			this._VentanaElectrica = true;
			this.precio += 20000;
		}
		public void agregaAsietosPremium()
		{
			this._AsientosPremium = true;
			this.precio += 15000;
		}
		public void agregaPantallaTouch()
		{
			this._PantallaTouch = true;
			this.precio += 20000;
		}
		/* "ID: ,Marca: ,Modelo: ,KM: ,Precio: " + "Extras: Aire Acondicionado, Ventana Electrica, Asientos Premium, PantallaTouch"*/
		/* "ID: ,Marca: ,Modelo: ,KM: ,Precio: " + "Extras: Aire Acondicionado"*/
		/* "ID: ,Marca: ,Modelo: ,KM: ,Precio: " + "Extras: Ventana Electrica, Asientos Premium"*/
		/* "ID: ,Marca: ,Modelo: ,KM: ,Precio: " */
		public override string ToString()
		{

			string extras = " incluye: ";
			if (_AireAcondicionado)
			{
				extras += " Aire Acondicionado,";

			}
			if (_VentanaElectrica)
			{
				extras += " Ventan Electrica,";
			}
			if (_AsientosPremium)
			{
				extras += " Asientos Premium,";
			}
			if (_PantallaTouch)
			{
				extras += " Pantalla Touch,";
			}
			
			return base.ToString() + extras;
		}

	}
}
