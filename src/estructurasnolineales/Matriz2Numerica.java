package estructurasnolineales;

import estructurasLineales.ListaEstaticaNumerica;
import herramientas.comunes.TipoColumna;
import herramientas.comunes.TipoLogaritmo;
import herramientas.comunes.TipoRenglon;

/**
 * Clase que representa un TDA de una matriz de 2D que solo almacena numeros.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class Matriz2Numerica extends Matriz2 {

    public Matriz2Numerica(int filas, int columnas) {
        super(filas, columnas, 0);
    }

    public Matriz2Numerica(int filas, int columnas, Object valor) {
        super(filas, columnas, valor);
    }

    /**
     * Este método permite validar si un indice esta dentro de un limite.
     * @param indice Es el indice que se validara.
     * @param limiteDimension Es el limite de la dimension.
     * @return Regresa <b>true</b> si el indice esta dentro del limite, <b>false</b> si no esta dentro del limite.
     */
    private boolean enRango(int indice, int limiteDimension) {
        if (indice >= 0 && indice < limiteDimension) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método permite agregar/camabiar un valor en la matriz en la posición indicada por los parámetros.
     * @param fila Es la fila de la matriz donde se asignara el valor.
     * @param col Es la columna de la matriz donde se asignara el valor.
     * @param valor Es el valor que se asignara a la posición (fila, col).
     * @return Regresa <b>true</b> si se pudo asignar el valor, <b>false</b> si no se pudo asignar el valor.
     */
    @Override
    public boolean cambiar(int fila, int col, Object valor) {
       if (enRango(fila, renglones) && enRango(col, columnas) && valor instanceof Number) {
           double valorAInsertar = ((Number) valor).doubleValue();
           datos[fila][col] = valorAInsertar;
           return true;
       } else {
           return false;
       }
    }

    /**
     * Este método permite multiplicar cada elemento de la matriz por un escalar.
     * @param escalar Es el escalar por el cual se multiplicará la matriz.
     * @return Regresa <b>true</b> si se pudo multiplicar la matriz por el escalar,
     * <b>false</b> si no se pudo multiplicar la matriz por el escalar.
     */
    public boolean porEscalar(Number escalar){
        boolean seMultiplico = false;
        if (escalar != null) {
            double escalarAMultiplicar = escalar.doubleValue();
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    double valorNuevo = ((Number) datos[fila][col]).doubleValue();
                    valorNuevo *= escalarAMultiplicar;
                    cambiar(fila, col, valorNuevo);
                }
            }
            seMultiplico = true;
        }
        return seMultiplico;
    }

    /**
     * Este método permite multiplicar cada posicion de la matriz por cada escalar de la lista de escalares.
     * @param escalares Es la lista de escalares por los cuales se multiplicará la matriz.
     * @return Regresa <b>true</b> si se pudo multiplicar la matriz por los escalares,
     * <b>false</b> si no se pudo multiplicar la matriz por los escalares.
     */
    public boolean porEscalares(ListaEstaticaNumerica escalares){
        // Se verifica que la lista de escalares tenga la misma cantidad de elementos que la matriz.
        // Multiplicar el escalar de la posicon 0 de la lista por el valor de la matriz 0,0 y así sucesivamente.
        boolean seMultiplicaron = false;
        if (escalares.numeroElementos() == numElementos()) {
            int posEscalar = 0;
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    double valorNuevo = ((Number) datos[fila][col]).doubleValue();
                    valorNuevo *= ((Number) escalares.obtener(posEscalar)).doubleValue();
                    posEscalar++;
                    cambiar(fila, col, valorNuevo);
                }
            }
            seMultiplicaron = true;
        }
        return seMultiplicaron;
    }

    /**
     * Este método permite sumar un escalar a cada posicion de la matriz.
     * @param escalar Es el escalar que se sumará a cada posicion de la matriz.
     * @return Regresa <b>true</b> si se pudo sumar el escalar a cada posicion de la matriz,
     * <b>false</b> si no se pudo sumar el escalar a cada posicion de la matriz.
     */
    public boolean sumarEscalar(Number escalar){
        boolean seSumo = false;
        if (escalar != null) {
            double escalarASumar = escalar.doubleValue();
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    double valorNuevo = ((Number) datos[fila][col]).doubleValue();
                    valorNuevo += escalarASumar;
                    cambiar(fila, col, valorNuevo);
                }
            }
            seSumo = true;
        }
        return seSumo;
    }

    /**
     * Este método permite sumar cada posicion de la matriz con cada escalar de la lista de escalares.
     * @param escalares Es la lista de escalares con los cuales se sumará cada posicion de la matriz.
     * @return Regresa <b>true</b> si se pudo sumar los escalares a cada posicion de la matriz,
     * <b>false</b> si no se pudo sumar los escalares a cada posicion de la matriz.
     */
    public boolean sumarEscalares(ListaEstaticaNumerica escalares){
        boolean seSumaron = false;
        if (escalares.numeroElementos() == numElementos()) {
            int posEscalar = 0;
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    double valorNuevo = ((Number) datos[fila][col]).doubleValue();
                    valorNuevo += ((Number) escalares.obtener(posEscalar)).doubleValue();
                    posEscalar++;
                    cambiar(fila, col, valorNuevo);
                }
            }
            seSumaron = true;
        }
        return seSumaron;
    }

    /**
     * Este método permite multiplicar 2 matrices, la matriz actual por la matriz pasada como parámetro.
     * @param matriz2 Es la matriz con la cual se multiplicará la matriz actual.
     * @return Regresa <b>true</b> si se multiplicaron las matrices, <b>false</b> si las matrices no son
     * de la misma dimensión.
     */
    public boolean multiplicar(Matriz2Numerica matriz2){
        boolean seMultiplicoron = false;
        if (matriz2.numElementos() == numElementos()) {
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    double valorNuevo = ((Number) datos[fila][col]).doubleValue();
                    valorNuevo *= ((Number) matriz2.obtenerValor(fila, col)).doubleValue();
                    cambiar(fila, col, valorNuevo);
                }
            }
            seMultiplicoron = true;
        }
        return seMultiplicoron;
    }

    /**
     * Este método permite sumar 2 matrices, la matriz actual por la matriz pasada como parámetro.
     * @param matriz2 Es la matriz con la cual se sumará la matriz actual
     * @return Regresa <b>true</b> si se multiplicaron las matrices, <b>false</b> si las matrices no son de la misma dimensión.
     */
    public boolean sumar(Matriz2Numerica matriz2){
        boolean seSumaron = false;
        if (matriz2.numElementos() == numElementos()) {
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    double valorNuevo = ((Number) datos[fila][col]).doubleValue();
                    valorNuevo += ((Number) matriz2.obtenerValor(fila, col)).doubleValue();
                    cambiar(fila, col, valorNuevo);
                }
            }
            seSumaron = true;
        }
        return seSumaron;
    }

    /**
     * Este método eleva cada elemento de la matriz a la potencia pasada como parámetro.
     * @param escalar Es el escalar con el que se elevará cada elemento de la matriz.
     * @return Regresa <b>true</b> si se pudieron elevar los elementos de la matriz, <b>false</b> si el escalar es nulo.
     */
    public boolean aplicarPotencia(Number escalar){
        boolean seAplicoPotencia = false;
        if (escalar != null) {
            double exponente = escalar.doubleValue();
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    double base = ((Number) datos[fila][col]).doubleValue();
                    double valorNuevo = Math.pow(base, exponente);
                    cambiar(fila, col, valorNuevo);
                }
            }
            seAplicoPotencia = true;
        }
        return seAplicoPotencia;
    }

    /**
     * Este método nos permite saber si la matriz actual tiene únicamente valores diferntes de cero.
     * @return Regresa <b>true</b> si la matriz tiene por lo menos un valor igual a cero,
     * <b>false</b> si tiene todos los valores diferentes de cero.
     */
    private boolean tieneValoresDiferentesDeCero(){
        boolean tieneValoresDiferentesDeCero = false;
        for (int fila = 0; fila < renglones; fila++) {
            for (int col = 0; col < columnas; col++) {
                if (((Number) datos[fila][col]).doubleValue() == 0) {
                    tieneValoresDiferentesDeCero = true;
                    break;
                }
            }
        }
        return tieneValoresDiferentesDeCero;
    }

    /**
     * Este método calcula el logaritmo de cada elemento de la matriz actual, ya sea logaritmo natural, logaritmo
     * base 10 o logaritmo base 2.
     * @param tipoLogaritmo Es el tipo de logaritmo que se desea aplicar a la matriz actual.
     * @return Regresa <b>true</b> si se pudo calcular el logaritmo, <b>false</b> si no se pudo calcular el logaritmo.
     */
    public boolean aplicarLogaritmo(TipoLogaritmo tipoLogaritmo){
        boolean seAplicoLogaritmo = false;
       if (!tieneValoresDiferentesDeCero()) {
           if (tipoLogaritmo == TipoLogaritmo.NATURAL) {
               for (int fila = 0; fila < renglones; fila++) {
                   for (int col = 0; col < columnas; col++) {
                       double base = ((Number) datos[fila][col]).doubleValue();
                       double valorNuevo = Math.log(base);
                       cambiar(fila, col, valorNuevo);
                   }
               }
               seAplicoLogaritmo = true;
           } else if (tipoLogaritmo == TipoLogaritmo.BASE10) {
               for (int fila = 0; fila < renglones; fila++) {
                   for (int col = 0; col < columnas; col++) {
                       double base = ((Number) datos[fila][col]).doubleValue();
                       double valorNuevo = Math.log10(base);
                       cambiar(fila, col, valorNuevo);
                   }
               }
               seAplicoLogaritmo = true;
           } else {
               for (int fila = 0; fila < renglones; fila++) {
                   for (int col = 0; col < columnas; col++) {
                       double base = ((Number) datos[fila][col]).doubleValue();
                       double valorNuevo = Math.log(base) / Math.log(2);
                       cambiar(fila, col, valorNuevo);
                   }
               }
               seAplicoLogaritmo = true;
           }
       }
       return seAplicoLogaritmo;
    }

    /**
     * Este método nos permite generar una matriz diagonal con un valor especificado.
     * @param contenido Es el valor que se desea que tome la diagonal de la matriz.
     * @return Regresa <b>true</b> si se pudo generar la matriz diagonal, <b>false</b> si no se pudo generar.
     */
    public boolean matrizDiagonal(Number contenido){
        boolean sePudoPonerDiagonal = false;
        if (contenido != null) {
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    if (fila == col) {
                        cambiar(fila, col, contenido);
                    } else {
                        cambiar(fila, col, 0);
                    }
                }
            }
            sePudoPonerDiagonal = true;
        }
        return sePudoPonerDiagonal;
    }

    /**
     * Este método nos permite saber si una matriz es triangular superior.
     * @return Regresa <b>true</b> si la matriz es triangular superior, <b>false</b> si no lo es.
     */
    public boolean esDiagonalSuperior(){
        boolean esDiagonalSuperior = false;
        if (renglones == columnas) {
            esDiagonalSuperior = true;
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    if (fila > col) {
                        if (((Number) datos[fila][col]).doubleValue() != 0) {
                            esDiagonalSuperior = false;
                            break;
                        }
                    }
                }
            }
        }
        return esDiagonalSuperior;
    }

    /**
     * Este método nos permite saber si una matriz es triangular inferior.
     * @return Regresa <b>true</b> si la matriz es triangular inferior, <b>false</b> si no lo es.
     */
    public boolean esDiagonalInferior(){
        boolean esDiagonalInferior = false;
        if (renglones == columnas) {
            esDiagonalInferior = true;
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    if (fila < col) {
                        if (((Number) datos[fila][col]).doubleValue() != 0) {
                            esDiagonalInferior = false;
                            break;
                        }
                    }
                }
            }
        }
        return esDiagonalInferior;
    }

    /**
     * Este método nos permite calcular la potencia de una matriz.
     * @param exponente Es el exponente al que se desea elevar la matriz.
     * @return Regresa <b>true</b> si se pudo elevar la matriz, <b>false</b> si no se pudo elevar.
     */
    public boolean potencia(int exponente){
       return false;
    }

    /**
     * Este método nos permite redimensionar la matriz doblando las columnas.
     * @return Regresa <b>true</b> si se pudo redimensionar la matriz, <b>false</b> si no se pudo redimensionar.
     */
    public boolean doblarColumnas() {
        boolean sePudoDoblar = false;
        int mitadMatriz;
        if (renglones % 2 == 0) {
            mitadMatriz = renglones / 2;
            for (int col = 0; col < columnas; col++) {
                for (int fila = 0; fila < mitadMatriz - 1; fila++) {
                    double valorNuevoSuma = ((Number)datos[fila + 1][col]).doubleValue() + ((Number)datos[fila][col]).doubleValue();
                    cambiar(fila + 1, col, valorNuevoSuma);
                }
                for (int fila = mitadMatriz ; fila < renglones - 1; fila++) {
                    double valorNuevoSuma = ((Number)datos[fila][col]).doubleValue() + ((Number)datos[fila + 1][col]).doubleValue();
                    cambiar(fila, col, valorNuevoSuma);
                }
            }
            for(int fila = 0; fila < mitadMatriz - 1; fila++){
                eliminarRenglon(TipoRenglon.SUPE);
            }
            for(int fila = mitadMatriz ; fila < renglones ; fila++) {
                eliminarRenglon(TipoRenglon.INFE);
            }
            sePudoDoblar = true;
        }
        else{
            mitadMatriz = (renglones - 1) / 2;
            for (int col = 0; col < columnas; col++) {
                for (int fila = 0; fila < mitadMatriz - 1; fila++) {
                    double valorNuevoSuma = ((Number)datos[fila + 1][col]).doubleValue() + ((Number)datos[fila][col]).doubleValue();
                    cambiar(fila + 1, col, valorNuevoSuma);
                }
                for (int fila = mitadMatriz ; fila < renglones - 1; fila++) {
                    double valorNuevoSuma = ((Number)datos[fila][col]).doubleValue() + ((Number)datos[fila + 1][col]).doubleValue();
                    cambiar(fila, col, valorNuevoSuma);
                }
            }
            for(int fila = 0; fila < mitadMatriz - 1; fila++){
                eliminarRenglon(TipoRenglon.SUPE);
            }
            for(int fila = mitadMatriz ; fila < renglones ; fila++) {
                eliminarRenglon(TipoRenglon.INFE);
            }
            sePudoDoblar = true;
        }
        return sePudoDoblar;
    }

    /**
     * Este método nos permite redimensionar la matriz doblando los renglones.
     * @return Regresa <b>true</b> si se pudo redimensionar la matriz, <b>false</b> si no se pudo redimensionar.
     */
    public boolean doblarRenglones(){
        boolean doblado = false;
        int mitadMatriz;
        if (columnas % 2 == 0) {
            mitadMatriz = columnas / 2;
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < mitadMatriz - 1; col++) {
                    double valorNuevoSuma = ((Number)datos[fila][col+1]).doubleValue() + ((Number)datos[fila][col]).doubleValue();
                    cambiar(fila, col + 1, valorNuevoSuma);
                }
                for (int col = mitadMatriz ; col < columnas -1; col++) {
                    double valorNuevoSuma = ((Number)datos[fila][col]).doubleValue() + ((Number)datos[fila][col+1]).doubleValue();
                    cambiar(fila, col, valorNuevoSuma);
                }
            }
            for(int col = 0; col < mitadMatriz - 1; col++){
                quitarReglon(TipoColumna.IZQ);
            }
            for(int col = mitadMatriz ; col < columnas; col++) {
                quitarReglon(TipoColumna.DER);
            }
        } else {
            mitadMatriz = (columnas - 1) / 2;
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < mitadMatriz - 1; col++) {
                    double valorNuevoSuma = ((Number)datos[fila][col + 1]).doubleValue() + ((Number)datos[fila][col]).doubleValue();
                    cambiar(fila, col + 1, valorNuevoSuma);
                }
                for (int col = mitadMatriz ; col < columnas - 1; col++) {
                    double valorNuevoSuma = ((Number)datos[fila][col]).doubleValue() + ((Number)datos[fila][col + 1]).doubleValue();
                    cambiar(fila, col, valorNuevoSuma);
                }
            }
            for(int col = 0; col < mitadMatriz - 1; col++){
                quitarReglon(TipoColumna.IZQ);
            }
            for(int col = mitadMatriz ; col < columnas; col++) {
                quitarReglon(TipoColumna.DER);
            }
        }
        doblado = true;
        return doblado;
    }
}