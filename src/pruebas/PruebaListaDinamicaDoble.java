package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaDinamicaDoble;
import estructurasLineales.ListaEstatica;

public class PruebaListaDinamicaDoble {
    public static void main(String[] args) {
        ListaDinamicaDoble lista1 = new ListaDinamicaDoble();
        lista1.agregar("H");
        lista1.agregar("K");
        lista1.agregar("F");
        lista1.agregar("A");
        lista1.agregar("W");
        lista1.agregar("N");
        lista1.agregarInicio("E");

        SalidaPorDefecto.consola("--- Lista Dinámica Doble #1 ---" + "\n");
        lista1.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("--- Lista Dinámica Doble #1 Impresa en reversa ---" + "\n");
        lista1.imprimirOI();


       SalidaPorDefecto.consola("\n*Eliminando el ultimo elemento: " + lista1.eliminar());
       SalidaPorDefecto.consola("\n*Eliminando el primer elemento: " + lista1.eliminarInicio() + "\n");
       lista1.imprimir();

       SalidaPorDefecto.consola("\n--- Lista 1 Recorrida desde el inicio hasta el final ---\n");
       lista1.iniciarIteradorDelInicio();
       while (lista1.hayNodos() == true) {
           SalidaPorDefecto.consola(lista1.obtenerNodoDer() + "\n");
       }

       SalidaPorDefecto.consola("\n--- Lista 1 Recorrida desde el final hasta el inicio ---\n");
       lista1.iniciarIteradorDelFinal();
       while (lista1.hayNodos() == true) {
           SalidaPorDefecto.consola(lista1.obtenerNodoIzq() + "\n");
       }

       SalidaPorDefecto.consola("\n*Bucar el elemento 'A' en la lista: " + lista1.buscarFinal("A") + "\n");
       SalidaPorDefecto.consola("*Eliminar el elemento 'F' de la lista: " + lista1.eliminar("F") + "\n");
       lista1.imprimir();
       SalidaPorDefecto.consola("\n");

        ListaDinamica listaDinamicaSimple = new ListaDinamica();
        listaDinamicaSimple.agregar("A");
        listaDinamicaSimple.agregar("B");
        listaDinamicaSimple.agregar("C");

        SalidaPorDefecto.consola("\n--- Lista  Dinamica Simple ---" + "\n");
        listaDinamicaSimple.imprimir();
        SalidaPorDefecto.consola("\n");

        ListaDinamicaDoble lista2 = new ListaDinamicaDoble();
        lista2.agregar("A");
        lista2.agregar("B");
        lista2.agregar("C");

        SalidaPorDefecto.consola("--- Lista Dinámica Doble #2 ---" + "\n");
        lista2.imprimir();
        SalidaPorDefecto.consola("\n");

        // salida esperada: false, porque las listas no son del mismo tipo
        SalidaPorDefecto.consola("*La lista Doble #2 es igual a la lista Simple: " + lista2.esIgual(listaDinamicaSimple) + "\n");

        ListaDinamicaDoble lista3 = new ListaDinamicaDoble();
        lista3.agregar("A");
        lista3.agregar("B");
        lista3.agregar("C");

        SalidaPorDefecto.consola("--- Lista Dinámica Doble #3 ---" + "\n");
        lista3.imprimir();
        SalidaPorDefecto.consola("\n");

        // salida esperada: true, porque las listas son del mismo tipo y tienen los mismos elementos
        SalidaPorDefecto.consola("*La lista Doble #3 es igual a la lista doble #2: " + lista3.esIgual(lista2) + "\n");

        SalidaPorDefecto.consola("\n");
        ListaDinamicaDoble lista4 = new ListaDinamicaDoble();
        lista4.agregar("K");
        lista4.agregar("F");
        lista4.agregar("F");
        lista4.agregar("F");
        lista4.agregar("X");

        SalidaPorDefecto.consola("--- Lista Dinámica Doble #4 ---" + "\n");
        lista4.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("*Cambiar el elemento 'F' por 'G' en la lista 2 veces: " +
                lista4.cambiar("F", "G", 2) + "\n");

        SalidaPorDefecto.consola("--- Lista Dinámica Doble #4 ---" + "\n");
        lista4.imprimir();

        ListaEstatica listaDePosiciones = lista4.buscarValores("G");
        SalidaPorDefecto.consola("\n--- Lista Estática Con las Posiciones de los elementos 'G' ---" + "\n");
        listaDePosiciones.imprimirOI();

        SalidaPorDefecto.consola("\n");
        ListaDinamicaDoble lista5 = new ListaDinamicaDoble();
        lista5.agregar("T");
        lista5.agregar("E");
        lista5.agregar("S");

        SalidaPorDefecto.consola("--- Lista Dinámica Doble #5 ---" + "\n");
        lista5.imprimir();
        SalidaPorDefecto.consola("\n");

        ListaEstatica listaEstatica2 = new ListaEstatica(3);
        listaEstatica2.agregar("A");
        listaEstatica2.agregar("B");
        listaEstatica2.agregar("C");

        SalidaPorDefecto.consola("--- Lista Estática #2 ---" + "\n");
        listaEstatica2.imprimirOI();

        SalidaPorDefecto.consola("*Agregar los elementos de la lista estática #2 a la lista dinámica #5: " +
                lista5.agregarLista(listaEstatica2) + "\n");

        SalidaPorDefecto.consola("--- Lista Dinámica Doble #5 ---" + "\n");
        lista5.imprimir();

        lista5.invertir();
        SalidaPorDefecto.consola("\n--- Lista Dinámica Doble #5 Invertida ---" + "\n");
        lista5.imprimir();

        SalidaPorDefecto.consola("\n");
        ListaDinamicaDoble lista6 = new ListaDinamicaDoble();
        lista6.agregar("R");
        lista6.agregar("R");
        lista6.agregar("D");
        lista6.agregar("R");
        lista6.agregar("F");

        SalidaPorDefecto.consola("\n--- Lista Dinámica Doble #6 ---" + "\n");
        lista6.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("*Contar Cuantas veces se repite el elemento 'R' en la lista #6: " +
                lista6.contar("R") + " veces" + "\n");


        ListaDinamica listaDinamicaSimple2 = new ListaDinamica();
        listaDinamicaSimple2.agregar("K");
        listaDinamicaSimple2.agregar("H");
        listaDinamicaSimple2.agregar("W");

        SalidaPorDefecto.consola("\n--- Lista Dinámica Simple #2 ---" + "\n");
        listaDinamicaSimple2.imprimir();
        SalidaPorDefecto.consola("\n");

        ListaDinamicaDoble lista7 = new ListaDinamicaDoble();
        lista7.agregar("A");
        lista7.agregar("H");
        lista7.agregar("B");
        lista7.agregar("W");
        lista7.agregar("C");
        lista7.agregar("K");

        SalidaPorDefecto.consola("\n--- Lista Dinámica Doble #7 ---" + "\n");
        lista7.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("*Eliminar los elementos de la lista dinámica simple #2 de la lista dinámica doble #7: " +
                lista7.eliminarLista(listaDinamicaSimple2) + "\n");

        SalidaPorDefecto.consola("\n--- Lista Dinámica Doble #7 ---" + "\n");
        lista7.imprimir(); // salida esperada: null <- A <-> B <-> C -> null

        SalidaPorDefecto.consola("\n");
        ListaDinamicaDoble listaClonada = (ListaDinamicaDoble) lista7.clonar();
        SalidaPorDefecto.consola("--- Lista Dinámica Doble #7 Clonada ---" + "\n");
        listaClonada.imprimir();

        ListaDinamicaDoble lista8 = new ListaDinamicaDoble();
        lista8.agregar("D");
        lista8.agregar("F");
        lista8.agregar("G");
        lista8.agregar("H");
        lista8.agregar("J");

        SalidaPorDefecto.consola("\n\n--- Lista Dinámica Doble #8 ---" + "\n");
        lista8.imprimir();
        SalidaPorDefecto.consola("\n");

        ListaDinamicaDoble subLista = (ListaDinamicaDoble) lista8.subLista(1, 3);
        SalidaPorDefecto.consola("--- SubLista de la Lista Dinámica Doble #8, Indice_Inicial:1 Indice_Final:3 ---" + "\n");
        subLista.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("\n*Insertar el elemento 'X' en la posición 2 de la sublista: " +
                subLista.insertar("X", 2) + "\n");
        SalidaPorDefecto.consola("\n--- SubLista de la Lista Dinámica Doble #8 ---" + "\n");
        subLista.imprimir();
    }
}