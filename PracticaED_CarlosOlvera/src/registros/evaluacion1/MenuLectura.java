package registros.evaluacion1;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import entradaSalida.archivos.ArchivoTexto;
import estructurasLineales.ColaEstatica;
import estructurasLineales.ListaEstatica;
import estructurasLineales.PilaEstatica;

/**
 * Clase que nos ayudara a encontrar problemas de paridad en Clases u operaciones
 */
public class MenuLectura {
    protected PilaEstatica pilaArchivo; //pila donde se almacenan los caracteres de el archivo 1
    protected PilaEstatica pilaCadena; //pila donde se almacenan los caracteres de una cadena
    protected ListaEstatica archivo; //donde se guardaran los caracteres de el archivo
    protected String archivo1; //variable donde se guardara la ruta del archivo
    protected String archivo2; //variable donde se guardara la ruta de la cadena

    /**
     * El menu nos mostrara las opciones que podemos hacer
     */
    public MenuLectura() {
        SalidaPorDefecto.consola("-BIENVENIDO AL MENU DE LA EVALUACIÓN 1-\n"); //menu donde se eligira la opcion
        int opcion;
        do {
            SalidaPorDefecto.consola("1-.Analizar Clase\n" +
                    "2-.Analizar Cadena\n" +
                    "0.-Salir\n" +
                    "POR FAVOR, ELIJA UNA OPCION\n");
            double opcionD = EntradaPorDefecto.consolaDouble();
            opcion = (int) opcionD;
            switch (opcion) {
                case 1:
                    SalidaPorDefecto.consola("Ingrese la ruta del archivo .txt con la Clase: \n"); //en caso de que sea 1, pide la ruta de la clase
                    archivo1 = EntradaPorDefecto.consolaCadenas();
                    analizarArchivo();
                    break;
                case 2:
                    SalidaPorDefecto.consola("Ingrese la cadena de texto: \n");//en caso de que sea 1, pide la ruta de la cadena
                    archivo2 = EntradaPorDefecto.consolaCadenas();
                    analizarCadena();
                    break;
            }
        }
        while (opcion != 0);
    }

    /**
     * Metodo que analiza el archivo y encuentra los errores
     */
    public void analizarArchivo(){
        archivo = ArchivoTexto.leer(archivo1); //variable tipo ListaEstatica donde se guardara el contenido del archivo1
        pilaArchivo = new PilaEstatica(archivo1.length());  //se le da el tamaño a las pilas con el numero de caracteres
        ColaEstatica coladeErrores = new ColaEstatica(archivo1.length());
        PilaEstatica pilaLinea = new PilaEstatica(archivo1.length()); //pila donde se guardara la linea de el la llave
        String texto = ""; //variable que usaremos para guardar los caracteres
        int maximoLineas = archivo.numeroElementos()-1;
        for (int cont = 0; cont < archivo.numeroElementos(); cont++){
            texto+=archivo.obtener(cont);     //se guardan todas los los caracteres en una cadena para poder analizar caracter por caracter
        }
        for (int cont = 0; cont < texto.length() ; cont++){
            char token = texto.charAt(cont);
            if (token == '{' || token == '(' || token == '['){ //si es alguno de estos caracteres se pone en la pila
                pilaArchivo.poner(token);
                Object valor = archivo.obtener(maximoLineas);
                int indice = (int) archivo.buscar(valor);
                pilaLinea.poner(indice);
            }
            else if(token == '}' || token == ')' || token == ']') {
                pilaArchivo.quitar(); //si el parentesis es cerrado, quitamos un valor de la pila
                pilaLinea.quitar();
            }
        }
        if(pilaArchivo.vacio() == false){ //si se quedo un valor en la pila, significa que falto un valor para cerra una llave
            archivo.imprimirOI();
            for (int contCola = 0; contCola <pilaArchivo.maximo(); contCola++){
                if (pilaLinea.verUltimo() != null && pilaArchivo.verUltimo() != null){ //si el valor de la pila es null no se imprime ni se elimina nada
                    SalidaPorDefecto.consola("Linea "+((int)pilaLinea.verUltimo()+1)+" Falta cerrar "+pilaArchivo.verUltimo()+"\n"); //se vera el valor de la pila que no se elimino, significa que no tiene cierre
                    pilaLinea.quitar();
                    pilaArchivo.quitar();//se quitan para que no slaga el mismo resultado una y otra vez
                }
            }

        }
        else {
            archivo.imprimirOI();
            SalidaPorDefecto.consola("Todo esta perfecto :)\n");
        }


    }

    /**
     * metodo que analiza la cadena y encuentra los errores
     */
    public void analizarCadena(){
        pilaCadena = new PilaEstatica(archivo2.length()); //se la da la dimension a la pila con el numero de caracteres de la cadena recibida
        int contadorEspacios = 0; //variable que se utilizara para saber cuantos espacios habra que ponerse
        String espacios = ""; //esta variable sumara espacios hasta llegar al error de cierre de llaves
        for (int cont = 0; cont < archivo2.length(); cont++) {
            char token = archivo2.charAt(cont);
            if (token == '{' || token == '(' || token == '[') { //si es alguno de estos caracteres se pone en la pila
                pilaCadena.poner(token);

            } else if (token == '}' || token == ')' || token == ']') {
                pilaCadena.quitar(); //si el parentesis es cerrado, quitamos un valor de la pila
            }
        }
        for (int cont2 =archivo2.length() - 1; cont2 >= 0; cont2--) { //for que verifica en que espacio esta el error, empando desde la ultima posicion hasta la primera
            char token = archivo2.charAt(cont2);
            if (token == '{' || token == '(' || token == '[') { //cuando llegue al signo que no cerro se acaba el ciclo
                break;
            }
            else{
                contadorEspacios += 1; //se suma por 1 por cada posicion hasta llegar a la llave
            }
        }
        for(int cont2 = 0; cont2 < archivo2.length()-contadorEspacios-1; cont2 ++){ //se le resta el numero de espacios obtenido al numero total de la pila
            espacios += " ";//se le suma un espacio a la variable de tipo String
        }
        if(pilaCadena.vacio() == false){ //si se quedo un valor en la pila, significa que falto un valor para cerra una llave
            SalidaPorDefecto.consola(espacios+"^Falta cerrar "+pilaCadena.verUltimo()+"\n"); //se vera el valor de la pila que no se elimino, significa que no tiene cierre
        }
        else {
            SalidaPorDefecto.consola("Todo esta perfecto :)\n");
        }
    }
}
