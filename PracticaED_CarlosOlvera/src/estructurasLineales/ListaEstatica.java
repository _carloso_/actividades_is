package estructurasLineales;

import audio.AudioFileRecord;
import entradaSalida.SalidaPorDefecto;

public class ListaEstatica implements VectorLista{
    protected int MAXIMO;
    protected int ultimo;
    protected Object datos[];


    public ListaEstatica(int maximo){
        MAXIMO=maximo;
        datos=new Object[MAXIMO];
        ultimo=-1;
    }

    public boolean vacia() {
        return ultimo == -1;
    }

    public boolean llena() {
        if(ultimo== (MAXIMO-1)){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int maximo() {
        return 0;
    }



    public int agregar(Object valor) {
        if(llena()==false){ //hay espacio
            ultimo=ultimo+1;
            datos[ultimo]=valor;
            return ultimo;
        }
        else{ //no hay espacio
            return -1;
        }
    }

    public void imprimir() {
        for(int posicion=ultimo; posicion>=0; posicion--){
            SalidaPorDefecto.consola(datos[posicion]+ "\n");
        }
    }

    public void imprimirOI() {
        for(int posicion=0;posicion<=ultimo;posicion++){
            SalidaPorDefecto.consola(datos[posicion]+ "\n");
        }
    }

    public Object buscar(Object valor) {
        int posicion=0;
        //buscamos mientras haya donde buscar y mientras no lo encontremos
        while(posicion<=ultimo && !valor.toString().equalsIgnoreCase(datos[posicion].toString())){
            posicion=posicion+1;
        }
        if(posicion>ultimo){ //no lo encontramos
            return null;
        }else{  //si lo encontramos
            return posicion;
        }
    }

    public Object eliminar(Object valor) {
        Integer posicion=(Integer)buscar(valor);
        if(posicion!=null){ //si lo encontramos
            Object valorEliminado=datos[posicion];
            ultimo--;
            for(int movimiento=posicion;movimiento<=ultimo;movimiento++){
                datos[movimiento]=datos[movimiento+1];
            }
            return valorEliminado;
        }else{  //no lo encontramos
            return null;
        }
    }
    public int numeroElementos(){
        return ultimo+1;
    }


    public boolean esIgual(Object lista2){
        return false;
    }

    protected boolean enRango(int indice){
        if(indice>=0 && indice<=ultimo){
            return true;
        }else{
            return false;
        }
    }

    public Object obtener (int indice){
        if(enRango(indice)){
            return datos[indice]; //devuelve el objeto de la posicion indice
        }else{
            return null;
        }
    }

    public boolean cambiar(Object valorViejo, Object valorNuevo, int numVeces) {
        int veces;
        boolean cambio = false;
        if (numVeces <= 0) {
            cambio = false;
        } else {
            veces = contar(valorViejo);
            if (veces == 0) {
                cambio = false;
            } else {
                if (veces < numVeces) {
                    cambio = false;
                } else {
                    for (int posicion = 0; posicion <= ultimo; posicion++) {
                        if (datos[posicion].toString().equalsIgnoreCase(valorViejo.toString())) {
                            datos[posicion] = valorNuevo;
                            numVeces--;
                            if (numVeces == 0) {
                                cambio = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return cambio;
    }

    public boolean cambiar(int indice, Object valor){
        Integer posicion=(Integer)buscar(datos[indice]);
        if (posicion != null){ //se encontro el valor
            datos[indice] = valor;
            return true; //se modifico el elemento
        }
        else{ //no se hizo ninguna modificación
            return false;
        }
    }
    public boolean cambiarListaEstatica(ListaEstatica indicesBusqueda, ListaEstatica valoresNuevos){
        return false;
    }

    public ListaEstatica buscarValores(Object valor){
        int posicion=0;
        //buscamos mientras haya donde buscar y mientras no lo encontremos
        while(posicion<=ultimo && !valor.toString().equalsIgnoreCase(datos[posicion].toString())){
            posicion=posicion+1;
        }
        if(posicion>ultimo){ //no lo encontramos
            return null;
        }else{  //si lo encontramos
            ListaEstatica lisTmp = new ListaEstatica(ultimo);
            lisTmp.agregar(valor);
            return lisTmp;
        }
    }

    public Object eliminar() {
        if(vacia()==false){ //hay algo
            Object valorEliminado=datos[ultimo];
            ultimo--;
            return valorEliminado;
        }else{  //no hay nada
            return null;
        }
    }

    public Object eliminar(int indice){
        Integer posicion=(Integer)buscar(datos[indice]);
        if(posicion!=null){ //si lo encontramos
            Object valorEliminado=datos[posicion];
            ultimo--;
            for(int movimiento=posicion;movimiento<=ultimo;movimiento++){
                datos[movimiento]=datos[movimiento+1];
            }
            return valorEliminado;
        }else{  //no lo encontramos
            return null;
        }
    }
    public void vaciar(){
        for (int posicion = 0; posicion<=MAXIMO-1;posicion++){ //se recorre toda la lista de datos
            datos[posicion]=null; //todos los valores se sustituyen por null (nada)

        }
    }

    public boolean agregarLista(Object lista2) {
        if (lista2 instanceof ListaEstatica) { //valida que lista2 sea una lista estatica
            ListaEstatica listaTem = (ListaEstatica) lista2;  //se le da a lista2 una variable temporal
            for (int posicion = 0; posicion <= listaTem.ultimo; posicion++) {
                agregar(listaTem.obtener(posicion));
            }
            return true;
        } else {
            return false;
        }
    }

    public void invertir(){ //invierte la lista
        for (int posicion= 0; posicion<=ultimo/2; posicion++){
            Object tmp = datos[posicion];
            datos[posicion] = datos[ultimo - posicion];
            datos[ultimo - posicion] = tmp;;
        }

    }

    public int contar(Object valor) {
        int numVeces = 0;
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            if (valor.toString().equalsIgnoreCase(datos[posicion].toString())) {
                numVeces++;
            }
        }
        return numVeces;
    }

    public boolean eliminarLista(Object lista2){
        return false;
    }

    public void rellenar(Object valor, int cantidad){

    }

    public Object clonar(){ //regresa una copia de la lista actual
        return datos;
    }

    public Object subLista(int indiceInicial, int indiceFinal){
        //aqui se valida que el indice no este fuera de rango
        if (indiceInicial >= 0 && indiceInicial <= ultimo && indiceFinal >=0 && indiceFinal <= ultimo){
            //Se crea una lista estatica
            ListaEstatica lisTmp = new ListaEstatica(indiceFinal - indiceInicial + 1);
            for (int posicion = indiceInicial; posicion <= indiceFinal; posicion++){
                lisTmp.agregar(datos[posicion]);
            }
            return lisTmp;
        }
        else{
            return null;
        }
    }


    public Object redimensionar(int maximo){
        if (maximo > 0 && maximo <= maximo()){
            Object[] espacios = new Object[maximo]; //crear objeto que reemplazara la lista a una mas grande o chica
            for (int posicion = 0; posicion <= ultimo; posicion++){ //un for que recorre el los espacios
                espacios[posicion] = datos[posicion];
            }
            datos = espacios; //la lista datos toma los espacios
            MAXIMO = maximo; //se modifica el maximo
            return datos;
        }
        else if (maximo > maximo()){
            MAXIMO = maximo; //si es mas chico de maximo
            return datos;
        }
        else{
            return null; //si no cumple no se modifica nada
        }
    }

    public void agregarBuffer(double [] buffer) {
        if (vacia() == true) { //si la lista esta vacia, se agrega
            for (int posicion=0; posicion <=ultimo; posicion++){
                agregar(buffer[posicion]);
            }
        }
        else {
            SalidaPorDefecto.consola("No se puede agregar a una lista que no este vacia");
        }
    }

    public void reemplazar(int indice, double valor){
        Integer posicion=(Integer)buscar(datos[indice]);
        if (posicion != null){ //se encontro el valor
            datos[indice] = valor;
        }
        else{ //no se hizo ninguna modificación
            SalidaPorDefecto.consola("no se modifico por que no se encontro el indice");
        }
    }

    public Object verUltimo(){
        if (vacia()==false){
            return datos[ultimo];
        }else{
            return  null;
        }
    }

    public int numeroMaximo(){
        int numeroMaximo = 0;
        for (int cont = 0; cont < datos.length; cont++){
            if ((Integer.valueOf((String) obtener(cont)) > numeroMaximo)){
                numeroMaximo = Integer.valueOf((String) obtener(cont));

            }
        }
        return numeroMaximo;
    }

}
