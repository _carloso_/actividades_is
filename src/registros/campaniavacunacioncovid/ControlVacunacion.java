package registros.campaniavacunacioncovid;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaEstatica;
import estructurasLineales.ColaEstaticaDePrioridad;
import herramientas.comunes.TipoPrioridad;

/***
 * Clase que controla la campaña de vacunación contra COVID-19.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class ControlVacunacion {

    private ColaEstaticaDePrioridad personas;
    private ColaEstatica vacunas;

    public ControlVacunacion(int cantidadVacunas, int cantidadPersonas) {
        // Cola de personas que se van a vacunar, con prioridad de vacunación personas de mayor a menor edad.
        personas = new ColaEstaticaDePrioridad(cantidadPersonas, TipoPrioridad.MAYOR_A_MENOR);
        vacunas = new ColaEstatica(cantidadVacunas);
    }

    /**
     * Agrega una persona a la cola de personas a vacunar.
     * @param persona Es la persona a agregar.
     * @return Retorna true si la persona se agrego correctamente, false si no se pudo agregar.
     */
    public boolean agregarPersona(Persona persona) {
        return personas.insertar(persona, persona.getEdad());
    }

    /**
     * Agrega una vacuna a la cola de vacunas.
     * @param vacuna Es la vacuna a agregar.
     * @return Retorna true si la vacuna se agrego correctamente, false si no se pudo agregar.
     */
    public boolean agregarVacuna(Vacuna vacuna) {
        return vacunas.poner(vacuna);
    }

    /**
     * Imprime a las personas que se estan vacunando con una tipo de vacuna.
     */
    public void vacunarPersonas() {
        while (!personas.vacio() && !vacunas.vacio()) {
            Persona personaAVacunar = (Persona) personas.quitar(); // se obtiene la persona a vacunar.
            Vacuna vacuna = (Vacuna) vacunas.quitar(); // se obtiene la vacuna.
            SalidaPorDefecto.consola("->Vacunando a " + personaAVacunar.getNombre() + " " +
                    personaAVacunar.getApellido() + " de " + personaAVacunar.getEdad() +
                    " años " + "con la vacuna " + vacuna.getNombre() + "\n");
        }
        // verificar por que se salio del while, si fue porque ya no hay mas personas o si se terminaron las vacunas
        if (personas.vacio()) { // ya no hay mas personas
            SalidaPorDefecto.consola("*** No hay mas personas para vacunar ***\n");
        } else { // se terminaron las vacunas
            SalidaPorDefecto.consola("*** No hay mas vacunas ***\n");
        }
    }
}