package estructurasLineales.auxiliares;

public class NodoBusquedaArbol {
    protected Object indice;
    protected Object direccion;
    protected NodoBusquedaArbol nodoDer;
    protected NodoBusquedaArbol nodoIzq;

    public NodoBusquedaArbol(int indice,Object direccion){
        this.indice = indice;
        nodoDer=null;
        this.direccion=direccion;
    }

    public NodoBusquedaArbol getNodoIzq() {
        return nodoIzq;
    }

    public void setNodoIzq(NodoBusquedaArbol nodoIzq) {
        this.nodoIzq = nodoIzq;
    }


    public NodoBusquedaArbol getNodoDer() {
        return nodoDer;
    }

    public void setNodoDer(NodoBusquedaArbol nodoDer) {
        this.nodoDer = nodoDer;
    }


    public void setDireccion(long direccion) {
        this.direccion = direccion;
    }

    public Object getDireccion() {
        return direccion;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public Object getIndice() {
        return indice;
    }

    @Override
    public String toString(){
        return "("+indice.toString() + ", " + direccion.toString()+")";
    }
}
