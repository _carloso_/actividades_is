package registros.escuelaCentrosDeComputo;

import estructurasLineales.ListaDinamica;

/**
 * Esta clase representa una escuela.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class Escuela {

    protected String nombre;
    protected ListaDinamica centrosDeComputo;

    public Escuela(String nombre) {
        this.nombre = nombre;
        this.centrosDeComputo = new ListaDinamica();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ListaDinamica getCentrosDeComputo() {
        return centrosDeComputo;
    }

    public void setCentrosDeComputo(ListaDinamica centrosDeComputo) {
        this.centrosDeComputo = centrosDeComputo;
    }

    /**
     * Este método permite agregar un centro de cómputo a la escuela.
     * @param centroDeComputo Es el centro de cómputo que se desea agregar.
     * @return Retorna true si el centro de cómputo fue agregado, false en caso contrario.
     */
    public boolean agregarCentroDeComputo(CentroDeComputo centroDeComputo){
        int valorRetornoAgregado = centrosDeComputo.agregar(centroDeComputo);
        if (valorRetornoAgregado == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Este método permite eliminar un centro de cómputo de la escuela.
     * @param numeroDeCentroDeComputo Es el número del centro de cómputo que se desea eliminar.
     * @return Retorna true si el centro de cómputo fue eliminado, false en caso contrario.
     */
    public boolean eliminarCentroDeComputo(int numeroDeCentroDeComputo) {
        Object valorRetornoEliminado = centrosDeComputo.eliminar(numeroDeCentroDeComputo + "");
        if (valorRetornoEliminado == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Este método permite obtener el centro de cómputo con un nombre específico.
     * @param numeroDeCentroDeComputo Es el número del centro de cómputo que se desea obtener.
     * @return Retorna el centro de cómputo con el nombre especificado, null en caso contrario.
     */
    public CentroDeComputo buscarCentroDeComputo(int numeroDeCentroDeComputo){
        Object valorRetornoBusqueda = centrosDeComputo.buscar(numeroDeCentroDeComputo + "");
        if (valorRetornoBusqueda == null) {
            return null;
        } else {
            return (CentroDeComputo) valorRetornoBusqueda;
        }
    }

    /**
     * Este método muestra el listado de los centros de cómputo de la escuela.
     */
    public void mostrarListadoDeCentrosDeComputo(){
       centrosDeComputo.iniciarIterador();
       while(centrosDeComputo.hayNodos() == true){
           CentroDeComputo centroDeComputoIterador = (CentroDeComputo) centrosDeComputo.obtenerNodo();
           System.out.println("No. Centro De Computo: " + centroDeComputoIterador.getNumero() +
                   ", Nombre: " + centroDeComputoIterador.getNombre() + "\n");
           // imprimir las computadoras
           centroDeComputoIterador.mostrarListadoDeComputadoras();
       }
    }

    @Override
    public String toString() {
        return "Nombre: " + nombre;
    }
}