package prueba;

import estructurasLineales.ListaEstaticaOrdenada;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasLineales.TipoOrden;

import java.lang.String;

public class PruebaOrdenanda {
    public static void main(String argumentos[]){
        ListaEstaticaOrdenada listaOrdenada = new ListaEstaticaOrdenada(5, TipoOrden.INC);
        listaOrdenada.agregar(1);
        listaOrdenada.agregar(2);
        listaOrdenada.agregar(5);
        listaOrdenada.agregar(4);

        SalidaPorDefecto.consola("---Lista Ordenada-- \n");
        listaOrdenada.imprimir();
        SalidaPorDefecto.consola("\n");

        listaOrdenada.eliminar(2);
        SalidaPorDefecto.consola("---Eliminar Numero--- \n");
        SalidaPorDefecto.consola("-eliminar Indice 2- \n");
        listaOrdenada.imprimir();
        SalidaPorDefecto.consola("\n");


        SalidaPorDefecto.consola("---Agregar Numero 11--- \n");
        listaOrdenada.agregar(11);
        listaOrdenada.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("----------");
        SalidaPorDefecto.consola("Cambiando el orden de ordenamiento... \n");
        listaOrdenada.imprimirOI();
        SalidaPorDefecto.consola("\n");

        listaOrdenada.buscar(10);
    }
}
