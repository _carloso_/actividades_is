package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasnolineales.GrafoEstatico;

public class PruebaGrafoEstatico {
    public static void main(String[] args) {
        GrafoEstatico grafo = new GrafoEstatico(4);

        // Agregar vertices
        grafo.agregarVertice("Z");
        grafo.agregarVertice("M");
        grafo.agregarVertice("S");
        grafo.agregarVertice("T");

        // Agregar aristas
        grafo.agregarArista("Z", "S");
        grafo.agregarArista("M", "Z");
        grafo.agregarArista("M", "T");
        grafo.agregarArista("T", "Z");
        grafo.agregarArista("T", "S");

        // Imprimir grafo
        SalidaPorDefecto.consola("---- Grafo 1 ----" + "\n");
        grafo.imprimir();

        // eliminar vertice
        SalidaPorDefecto.consola("\nEliminar el vertice Z: " + grafo.eliminarVertice("Z"));

        // Imprimir grafo
        SalidaPorDefecto.consola("\n\n");
        SalidaPorDefecto.consola("---- Grafo 1 ----" + "\n");
        grafo.imprimir();

        // es adyacente
        SalidaPorDefecto.consola("\nEs adyacente M y T: " + grafo.esAdyacente("M", "T"));

        // eliminar arista
        SalidaPorDefecto.consola("\n\nEliminar la arista M-T: " + grafo.eliminarArista("M", "T"));

        // Imprimir grafo
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("---- Grafo 1 ----" + "\n");
        grafo.imprimir();

        // Grafo 2
        GrafoEstatico grafo2 = new GrafoEstatico(3);
        grafo2.agregarVertice("A");
        grafo2.agregarVertice("B");
        grafo2.agregarVertice("C");

        grafo2.agregarArista("A", "A");
        grafo2.agregarArista("A", "B");
        grafo2.agregarArista("A", "C");
        grafo2.agregarArista("B", "C");

        // Imprimir grafo2
        SalidaPorDefecto.consola("\n\n---- Grafo 2 ----" + "\n");
        grafo2.imprimir();

        // buscar vertice A en grafo 2
        SalidaPorDefecto.consola("\nBucar vertice A: \n");
        ListaEstatica lista = grafo2.buscarVertice("A");
        lista.imprimir();

        // el grafo 2 es PseudoGrafo
        SalidaPorDefecto.consola("\nEl garfo 2 es esPseudografo: " + grafo2.esPseudografo());

        // el grafo 2 es multigrafo
        SalidaPorDefecto.consola("\nEl garfo 2 es multigrafo: " + grafo2.esMultigrafo());

        // grado de un vertice: numero de aristas que salen de ese vertice
        SalidaPorDefecto.consola("\nGrado del vertice A: " + grafo2.gradoVertice("A"));

        // grafo 3 no dirigido
        GrafoEstatico grafo3 = new GrafoEstatico(4);
        grafo3.agregarVertice("X");
        grafo3.agregarVertice("Z");
        grafo3.agregarVertice("Q");
        grafo3.agregarVertice("W");

        grafo3.agregarArista("X", "Z");
        grafo3.agregarArista("Z", "X");
        grafo3.agregarArista("X", "W");
        grafo3.agregarArista("W", "X");
        grafo3.agregarArista("Z", "W");
        grafo3.agregarArista("W", "Z");
        grafo3.agregarArista("W", "Q");
        grafo3.agregarArista("Q", "W");

        // Imprimir grafo3
        SalidaPorDefecto.consola("\n\n---- Grafo 3 ----" + "\n");
        grafo3.imprimir();

        // el grafo 3 es dirigido
        SalidaPorDefecto.consola("\nEl garfo 3 es dirigido: " + grafo3.esDirigido());

        SalidaPorDefecto.consola("\n\nHay ruta entre X y Q: " + grafo3.hayRuta("X", "Q"));
        // Grafo 4
        GrafoEstatico grafo4 = new GrafoEstatico(5);
        grafo4.agregarVertice("A");
        grafo4.agregarVertice("B");
        grafo4.agregarVertice("C");
        grafo4.agregarVertice("D");
        grafo4.agregarVertice("E");

        grafo4.agregarArista("A", "C");
        grafo4.agregarArista("A", "D");
        grafo4.agregarArista("B", "A");
        grafo4.agregarArista("B", "E");
        grafo4.agregarArista("D", "C");

        // Imprimir grafo4
        SalidaPorDefecto.consola("\n\n---- Grafo 4 ----" + "\n");
        grafo4.imprimir();

        // el grafo 4 es conexo
        SalidaPorDefecto.consola("\nEl garfo 4 es conexo: " + grafo4.esConexo() + "\n");

        // listar aristas del grafo 4
        SalidaPorDefecto.consola("\nAristas del grafo 4: " + "\n");
        grafo4.listarAristas();

        // listar aristas del vertice A del grafo 4
        SalidaPorDefecto.consola("\n\nAristas del vertice A del grafo 4: " + "\n");
        grafo4.listarAristas("A");

        // listar los vertices del grafo 4
        SalidaPorDefecto.consola("\n\nVertices del grafo 4: " + "\n");
        grafo4.listarVertices();
    }
}
