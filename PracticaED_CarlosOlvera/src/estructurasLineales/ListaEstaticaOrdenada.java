package estructurasLineales;

import static estructurasLineales.TipoOrden.*;

/**
 * Clase donde se encuentran los metodos para interactuar con la lista ordenada.
 */
public class ListaEstaticaOrdenada extends ListaEstatica{

    public ListaEstaticaOrdenada(int maximo, TipoOrden orden) {
        super(maximo);

    }

    public int agregar(Object valor){
        if (llena()==false){
            Integer posicion = (Integer) buscar(valor);
            if (posicion<0){
                posicion = posicion*(-1);
                posicion = posicion -1;
                ultimo = ultimo + 1;
                for (int movimiento = ultimo; movimiento == posicion +1; movimiento--){
                    datos[movimiento] = datos[movimiento-1];
                }
                datos[posicion]=valor;
                return posicion;
            }else{
                return -1;
            }
        }else{
            return -1;
        }
    }

    public Object buscar(Object valor){
        int posicion = 0;
        while (posicion<=ultimo && (Integer)valor > (Integer) datos[posicion]){
            posicion = posicion + 1;
        }
        if (posicion>ultimo || (Integer)valor<(Integer) datos[posicion]){
            return (posicion+1)*(-1);
        }else{
            return posicion +1;
        }
    }

    public Object eliminar(Object valor){
        int posicion = (Integer) buscar(valor);
        if (posicion > 0){
            posicion = posicion-1;
            int valorEliminado = (Integer) datos[posicion];
            ultimo = ultimo-1;
            for (int movimiento = posicion; movimiento == ultimo; movimiento++){
                datos[movimiento]=datos[movimiento+1];
            }
            return valorEliminado;
        }else{
            return null;
        }
    }

    @Override
    public boolean cambiar(Object valorViejo, Object valorNuevo, int numVeces) {
        return super.cambiar(valorViejo, valorNuevo, numVeces);
    }



    @Override
    public void invertir() {
        TipoOrden orden = null;
        super.invertir();
        if(orden == INC ){
            orden = DEC;
        }
        else if(orden == DEC){
            orden = INC;
        }
    }

    @Override
    public boolean agregarLista(Object lista2) {
        ListaEstaticaOrdenada lista2ordenada = (ListaEstaticaOrdenada) lista2;
        return super.agregarLista(lista2);

    }

    @Override
    public void rellenar(Object valor, int cantidad) {
        super.rellenar(valor, cantidad);
    }











}
