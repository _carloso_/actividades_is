package estructurasNoLineales;

public enum TipoRenglon {
    SUP("SUPERIOR",1), INF("INFERIOR",2);

    private String nombre;
    private int valor;

    private TipoRenglon (String nombre, int valor){
        this.nombre = nombre;
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }

    public String getNombre() {
        return nombre;
    }
}
