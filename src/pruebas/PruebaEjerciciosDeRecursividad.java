package pruebas;

import entradaSalida.SalidaPorDefecto;
import recursividad.EjerciciosDeRecursividad;

public class PruebaEjerciciosDeRecursividad {
    public static void main(String[] args) {
        SalidaPorDefecto.consola(">Multiplicacion Entera de 5x3= " + EjerciciosDeRecursividad.multiplicar(5, 3) + "\n");
        SalidaPorDefecto.consola(">¿El numero 7 es primo?: " + EjerciciosDeRecursividad.esPrimo(7) + "\n");
        SalidaPorDefecto.consola(">Maximo comun divisor de 15 y 21: " + EjerciciosDeRecursividad.mcd(15, 21) + "\n");
        SalidaPorDefecto.consola(">Convertir 150 a binario: " + EjerciciosDeRecursividad.aBinario(150) + "\n");
        SalidaPorDefecto.consola(">Convertir 65029 a hexadecimal: " + EjerciciosDeRecursividad.aHexadecimal(65029) + "\n");
    }
}
