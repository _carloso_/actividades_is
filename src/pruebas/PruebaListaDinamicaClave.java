package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaDinamicaClave;
import estructurasLineales.ListaEstatica;
import estructurasnolineales.Matriz2;

public class PruebaListaDinamicaClave {
    public static void main(String[] args) {
        ListaDinamicaClave listaClave1 = new ListaDinamicaClave();
        listaClave1.agregar(1, "G");
        listaClave1.agregar(2, "A");
        listaClave1.agregar(2, "M");
        listaClave1.agregar(3, "F");
        listaClave1.agregar(4, "E");
        listaClave1.agregar(5, "O");
        listaClave1.agregar(5, "R");

        SalidaPorDefecto.consola("--- Lista Dinámica Clave #1 ---" + "\n");
        listaClave1.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Claves de la Lista #1 ---" + "\n");
        listaClave1.imprimirClaves();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Valores de la Lista #1 ---" + "\n");
        listaClave1.imprimirContenidos();

        SalidaPorDefecto.consola("\n*Eliminando el elemento con clave 2: " + listaClave1.eliminar(2) + "\n");
        listaClave1.imprimir();

        SalidaPorDefecto.consola("\n*Eliminando el elemento con valor 'R': " + listaClave1.eliminarContenido("R") + "\n");
        listaClave1.imprimir();

        SalidaPorDefecto.consola("\n*Buscar el elemento con clave 3: " + listaClave1.buscar(3) + "\n");
        SalidaPorDefecto.consola("*Buscar el elemento con valor 'G': " + listaClave1.buscarContenido("G") + "\n");

        ListaDinamicaClave listaClave2 = new ListaDinamicaClave();
        listaClave2.agregar(1, "H");
        listaClave2.agregar(2, "Q");
        listaClave2.agregar(3, "W");
        listaClave2.agregar(4, "E");
        listaClave2.agregar(5, "B");

        SalidaPorDefecto.consola("\n\n--- Lista Dinámica Clave #2 ---" + "\n");
        listaClave2.imprimir();


        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("*Cambiar el valor del elemento con clave 2 por 'X': " +
                listaClave2.cambiar(2, "X") + "\n");
        listaClave2.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("*Cambiar el elemento con el valor 'E' por 'Z': " +
                listaClave2.cambiarContenido("E", "Z") + "\n");
        listaClave2.imprimir();

        ListaEstatica listaEstatica1 = listaClave2.aListasEstaticas();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista Estática Con las Claves de la Lista #2 ---" + "\n");
        ListaEstatica listaClaves = (ListaEstatica) listaEstatica1.obtener(0);
        listaClaves.imprimirOI();

        SalidaPorDefecto.consola("--- Lista Estática Con los Valores de la Lista #2 ---" + "\n");
        ListaEstatica listaValores = (ListaEstatica) listaEstatica1.obtener(1);
        listaValores.imprimirOI();

        ListaDinamica listaDinamica1 = listaClave2.aListasDinamicas();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista Dinámica Con las Claves de la Lista #2 ---" + "\n");
        ListaDinamica listaClaves2 = (ListaDinamica) listaDinamica1.obtener(0);
        listaClaves2.imprimir();

        SalidaPorDefecto.consola("\n---Lista Dinámica Con los Valores de la Lista #2 ---" + "\n");
        ListaDinamica listaValores2 = (ListaDinamica) listaDinamica1.obtener(1);
        listaValores2.imprimir();

        ListaDinamicaClave listaClave3 = new ListaDinamicaClave();
        listaClave3.agregar(1, "T");
        listaClave3.agregar(2, "S");
        listaClave3.agregar(3, "D");
        listaClave3.agregar(4, "N");
        listaClave3.agregar(5, "K");

        SalidaPorDefecto.consola("\n\n--- Lista Dinámica Clave #3 ---" + "\n");
        listaClave3.imprimir();

        SalidaPorDefecto.consola("\n");
        Matriz2 matriz1 = listaClave3.aMatriz2();
        SalidaPorDefecto.consola("--- Matriz2 Con las Claves y Valores de la Lista #3 ---" + "\n");
        matriz1.imprimirXReglones();

        SalidaPorDefecto.consola("*Obtener el valor de la clave 4: " + listaClave3.obtener(4) + "\n");

        ListaDinamicaClave listaClave4 = new ListaDinamicaClave();
        listaClave4.agregar(1, "P");
        listaClave4.agregar(2, "A");

        SalidaPorDefecto.consola("\n\n--- Lista Dinámica Clave #4 ---" + "\n");
        listaClave4.imprimir();

        ListaDinamicaClave listaAAgregar = new ListaDinamicaClave();
        listaAAgregar.agregar(1, "A");
        listaAAgregar.agregar(2, "B");
        listaAAgregar.agregar(3, "C");
        listaAAgregar.agregar(4, "D");
        listaAAgregar.agregar(5, "E");
        listaAAgregar.agregar(6, "F");

        SalidaPorDefecto.consola("\n\n--- Lista Dinámica Clave Que Se Agregará a la Lista #4 ---" + "\n");
        listaAAgregar.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("*Agregar a la Lista #4: " + listaClave4.agregarLista(listaAAgregar) + "\n");
        listaClave4.imprimir();


        ListaDinamicaClave listaClave5 = new ListaDinamicaClave();
        listaClave5.agregar(1, "H");

        SalidaPorDefecto.consola("\n\n--- Lista Dinámica Clave #5 ---" + "\n");
        listaClave5.imprimir();

        ListaEstatica listaDeClaves = new ListaEstatica(4);
        listaDeClaves.agregar(3);
        listaDeClaves.agregar(2);
        listaDeClaves.agregar(6);
        listaDeClaves.agregar(1);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista Estática De Claves ---" + "\n");
        listaDeClaves.imprimirOI();

        ListaEstatica listaDeValores = new ListaEstatica(4);
        listaDeValores.agregar("W");
        listaDeValores.agregar("X");
        listaDeValores.agregar("V");
        listaDeValores.agregar("Y");

        SalidaPorDefecto.consola("--- Lista Estática De Valores ---" + "\n");
        listaDeValores.imprimirOI();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("*Agregar listas estáticas a la lista dinámica #5: " +
                listaClave5.agregarListasEstaticas(listaDeClaves, listaDeValores) + "\n");

        listaClave5.imprimir();

        ListaDinamicaClave listaClave6 = new ListaDinamicaClave();
        listaClave6.agregar(1, "A");
        listaClave6.agregar(2, "B");

        SalidaPorDefecto.consola("\n\n--- Lista Dinámica Clave #6 ---" + "\n");
        listaClave6.imprimir();

        ListaDinamica listaDinamicaDeClaves = new ListaDinamica();
        listaDinamicaDeClaves.agregar(3);
        listaDinamicaDeClaves.agregar(4);
        listaDinamicaDeClaves.agregar(5);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista Dinámica Simple De Claves ---" + "\n");
        listaDinamicaDeClaves.imprimir();

        ListaDinamica listaDinamicaDeValores = new ListaDinamica();
        listaDinamicaDeValores.agregar("C");
        listaDinamicaDeValores.agregar("D");
        listaDinamicaDeValores.agregar("E");

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista Dinámica Simple De Valores ---" + "\n");
        listaDinamicaDeValores.imprimir();

        SalidaPorDefecto.consola("\n\n");
        SalidaPorDefecto.consola("*Agregar listas dinámicas a la lista #6: " +
                listaClave6.agregarListasDinamicas(listaDinamicaDeClaves, listaDinamicaDeValores) + "\n");

        listaClave6.imprimir();


        ListaDinamicaClave listaClave7 = new ListaDinamicaClave();
        listaClave7.agregar(1, "D");
        listaClave7.agregar(2, "T");
        listaClave7.agregar(3, "F");


        SalidaPorDefecto.consola("\n\n--- Lista Dinámica Clave #7 ---" + "\n");
        listaClave7.imprimir();


        Matriz2 matriz2 = new Matriz2(3, 2);
        // agregar valores y claves a la matriz, la primera columna tine las claves y la segunda columna tine los valores
        matriz2.cambiar(0, 0, 5);
        matriz2.cambiar(0, 1, "A");
        matriz2.cambiar(1, 0, 7);
        matriz2.cambiar(1, 1, "X");
        matriz2.cambiar(2, 0, 9);
        matriz2.cambiar(2, 1, "W");

        SalidaPorDefecto.consola("\n\n--- Matriz #2 ---" + "\n");
        matriz2.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("*Agregar matriz a la lista #7: " + listaClave7.agregarMatriz2(matriz2) + "\n");

        listaClave7.imprimir();
    }
}