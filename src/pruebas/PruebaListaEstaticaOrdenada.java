package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasLineales.ListaEstaticaOrdenada;
import herramientas.comunes.TipoOrden;

public class PruebaListaEstaticaOrdenada {
    public static void main(String[] args) {
        // Lista ordenada de forma ascendente
        ListaEstaticaOrdenada lista1 = new ListaEstaticaOrdenada(6, TipoOrden.ASC);
        SalidaPorDefecto.consola("Insertar el elemento 1: " + lista1.agregar("B") + "\n");
        SalidaPorDefecto.consola("Insertar el elemento 2: " + lista1.agregar("E") + "\n");
        SalidaPorDefecto.consola("Insertar el elemento 3: " + lista1.agregar("A") + "\n");
        SalidaPorDefecto.consola("Insertar el elemento 4: " + lista1.agregar("G") + "\n");
        SalidaPorDefecto.consola("Insertar el elemento 5: " + lista1.agregar("H") + "\n");
        SalidaPorDefecto.consola("---Lista 1: Ordenada Ascendente---\n");
        lista1.imprimir();

        // Lista ordena de forma descendente
        ListaEstaticaOrdenada lista2 = new ListaEstaticaOrdenada(6, TipoOrden.DESC);
        SalidaPorDefecto.consola("Insertar el elemento 1: " + lista2.agregar("B") + "\n");
        SalidaPorDefecto.consola("Insertar el elemento 2: " + lista2.agregar("E") + "\n");
        SalidaPorDefecto.consola("Insertar el elemento 3: " + lista2.agregar("A") + "\n");
        SalidaPorDefecto.consola("Insertar el elemento 4: " + lista2.agregar("G") + "\n");
        SalidaPorDefecto.consola("Insertar el elemento 5: " + lista2.agregar("H") + "\n");
        lista2.agregar("W");
        SalidaPorDefecto.consola("---Lista 2: Ordenada Descendente---\n");
        lista2.imprimir();


        SalidaPorDefecto.consola("Eliminar el elemento E de la lista 1: " +
                lista1.eliminar("E") + "\n");
        lista1.imprimir();
        SalidaPorDefecto.consola("Eliminar el elemento A de la lista 2: " +
                lista2.eliminar("A") + "\n");
        lista2.imprimir();
        SalidaPorDefecto.consola("Cambiar el elemento A por el elemento Z de la lista 1: " +
                lista1.cambiar("A", "Z", 1) + "\n");
        lista1.imprimir();
        SalidaPorDefecto.consola("Cambiar el elemento H por el elemento C de la lista 2: " +
                lista2.cambiar("H", "C", 1) + "\n");
        lista2.imprimir();

        SalidaPorDefecto.consola("Cambiar el elemento de la posicion 1 por el elemento V de la lista 1: " +
                lista1.cambiar(1, "V") + "\n");
        lista1.imprimir();

        SalidaPorDefecto.consola("Cambiar el elemento de la posicion 3 por el elemento S de la lista 2: " +
                lista2.cambiar(3, "S") + "\n");
        lista2.imprimir();

        ListaEstaticaOrdenada lista3 = new ListaEstaticaOrdenada(5, TipoOrden.ASC);
        lista3.agregar(50);
        lista3.agregar(20);
        lista3.agregar(30);
        lista3.agregar(40);
        SalidaPorDefecto.consola("---Lista 3: Ordenada Ascendente---\n");
        lista3.imprimir();

        SalidaPorDefecto.consola("---Lista 3: Ordenada Inversa---\n");
        lista3.invertir();
        lista3.imprimir();
        lista3.agregar(60);
        SalidaPorDefecto.consola("\n");
        lista3.imprimir();

        SalidaPorDefecto.consola("------Rellenar Lista----\n");
        ListaEstaticaOrdenada lista4 = new ListaEstaticaOrdenada(10, TipoOrden.DESC);
        lista4.rellenar("H");
        lista4.imprimir();

        SalidaPorDefecto.consola("\n---- Lista Desordenada ----\n");
        ListaEstatica listaDesordenada = (ListaEstatica) lista4.arregloDesordenado();
        listaDesordenada.imprimir();
    }
}
