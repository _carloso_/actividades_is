package estructurasNoLineales;

import entradaSalida.SalidaPorDefecto;
import entradaSalida.archivos.AccederArchivo;
import entradaSalida.archivos.ArchivoTexto;
import estructurasLineales.ListaEstatica;
import estructurasLineales.auxiliares.NodoBusquedaArbol;
import estructurasLineales.auxiliares.NodoDoble;
import herramientas.generales.Utilerias;

import java.io.IOException;
import java.io.RandomAccessFile;

public class AplicacionesArbol extends ArbolBinarioBusqueda{
    protected ListaEstatica direccion;
    protected ListaEstatica datos;
    protected int contadorIndice = 0;
    public AplicacionesArbol(String ruta) throws IOException{
         AccederArchivo.leerArchivo(ruta);
         direccion = AccederArchivo.getDireccion(); //se obtienen las direcciones
        datos = AccederArchivo.getDatos(); //se obtienen las datos
        direccion.eliminar();//se elimina el ultimo por que es una poscion vacia
        for (int cont = 0; cont < direccion.numeroElementos();cont++){
            agregar(direccion.obtener(cont));
        }
    }

    public boolean agregar(Object valor){
        NodoBusquedaArbol nuevoNodoIndice = new NodoBusquedaArbol((contadorIndice++/2),valor);
        if(raiz==null){ //el valor lo agregaremos como la nueva raíz
            NodoDoble nuevoNodo=new NodoDoble(nuevoNodoIndice);
            if(nuevoNodo!=null){ //si hay espacio
                raiz=nuevoNodo;
                return true;
            }else{ //no hay espacio o hay error
                return false;
            }
        }else{ //ya hay nodos
            return agregar(raiz,valor);
        }
    }

    private boolean agregar(NodoDoble subRaiz, Object valor){
        //Comparar si el valor es mayor o menor a la subraíz
        if(Utilerias.compararObjetos(valor,subRaiz.getContenido())<0){ //valor < subRaiz
            if(subRaiz.getNodoIzq()==null){ //llegamos a la posicion donde le toca ser insertado
                NodoBusquedaArbol nuevoNodoIndice = new NodoBusquedaArbol((contadorIndice++/2),valor);
                NodoDoble nuevoNodo=new NodoDoble(nuevoNodoIndice);
                if(nuevoNodo!=null){ // si hay espacio
                    subRaiz.setNodoIzq(nuevoNodo);
                    return true;
                }else{ //no hay espacio
                    return false;
                }
            }else{ //hacemos llamada recursiva hacia esa rama
                return agregar(subRaiz.getNodoIzq(),valor);
            }
        }else if(Utilerias.compararObjetos(valor,subRaiz.getContenido())>0){ //valor > subRaiz
            if(subRaiz.getNodoDer()==null){ //llegamos a la posicion donde le toca ser insertado
                NodoBusquedaArbol nuevoNodoIndice = new NodoBusquedaArbol((contadorIndice++/2),valor);
                NodoDoble nuevoNodo=new NodoDoble(nuevoNodoIndice);
                if(nuevoNodo!=null){ // si hay espacio
                    subRaiz.setNodoDer(nuevoNodo);
                    return true;
                }else{ //no hay espacio
                    return false;
                }
            }else{ //hacemos llamada recursiva hacia esa rama
                return agregar(subRaiz.getNodoDer(),valor);
            }
        }else{ //valor = subRaiz
            return false;
        }
    }

    public void buscar(int indice){
        if (indice < 0 || indice > direccion.numeroElementos()){
            SalidaPorDefecto.consola("No se encontro el registro");
        }
        else{
            SalidaPorDefecto.consola("Se encontro la direccion: "+ direccion.obtener(indice) + "\n");
            SalidaPorDefecto.consola(datos.obtener(indice)+"");
        }
    }

    public boolean agregarNuevo(Object dire, String registro){
        NodoBusquedaArbol nuevoNodoIndice = new NodoBusquedaArbol((contadorIndice++/2),dire);
        if(raiz==null){ //el valor lo agregaremos como la nueva raíz
            NodoDoble nuevoNodo=new NodoDoble(nuevoNodoIndice);
            direccion.agregar(dire);
            datos.agregar(registro);
            SalidaPorDefecto.consola("Se agrego el registro en el indice "+contadorIndice++/2+"\n");
            if(nuevoNodo!=null){ //si hay espacio
                raiz=nuevoNodo;
                return true;
            }else{ //no hay espacio o hay error
                return false;
            }
        }else{ //ya hay nodos
            SalidaPorDefecto.consola("Se agrego el registro en el indice "+contadorIndice++/2+"\n");
            return agregarNuevo(raiz,dire, registro);

        }
    }
    public boolean agregarNuevo(NodoDoble subRaiz, Object dire, String registro) {
        if (direccion.buscar(dire) == null) { //se verifica que no se guarde en la misma posicion
            direccion.agregar(dire);
            datos.agregar(registro);
            //Comparar si el valor es mayor o menor a la subraíz
            if (Utilerias.compararObjetos(dire, subRaiz.getContenido()) < 0) { //valor < subRaiz
                if (subRaiz.getNodoIzq() == null) { //llegamos a la posicion donde le toca ser insertado
                    NodoBusquedaArbol nuevoNodoIndice = new NodoBusquedaArbol((contadorIndice++ / 2), dire);
                    NodoDoble nuevoNodo = new NodoDoble(nuevoNodoIndice);
                    if (nuevoNodo != null) { // si hay espacio
                        subRaiz.setNodoIzq(nuevoNodo);
                        return true;
                    } else { //no hay espacio
                        return false;
                    }
                } else { //hacemos llamada recursiva hacia esa rama
                    return agregar(subRaiz.getNodoIzq(), dire);
                }
            } else if (Utilerias.compararObjetos(dire, subRaiz.getContenido()) > 0) { //valor > subRaiz
                if (subRaiz.getNodoDer() == null) { //llegamos a la posicion donde le toca ser insertado
                    NodoBusquedaArbol nuevoNodoIndice = new NodoBusquedaArbol((contadorIndice++ / 2), dire);
                    NodoDoble nuevoNodo = new NodoDoble(nuevoNodoIndice);
                    if (nuevoNodo != null) { // si hay espacio
                        subRaiz.setNodoDer(nuevoNodo);
                        return true;
                    } else { //no hay espacio
                        return false;
                    }
                } else { //hacemos llamada recursiva hacia esa rama
                    return agregar(subRaiz.getNodoDer(), dire);
                }
            } else { //valor = subRaiz
                return false;
            }
        }
        else {
            SalidaPorDefecto.consola("La direccion ya esta ocupada");
            return false;
        }
    }

    public void eliminar(int indice) {
        String valorS = "";
        ArbolBinarioBusqueda arbol = new ArbolBinarioBusqueda();
        valorS = "("+indice+", "+direccion.obtener(indice)+")";
        Object valor = valorS;
        SalidaPorDefecto.consola("Nodo: " +valor);
        super.eliminar(valor);
    }

}
