package pruebas;

import entradaSalida.ArchivoTexto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaEstatica;
import herramientas.matematicas.DistribucionNormal;

public class PruebaDistribucionNormal {
    public static void main(String[] args) {
        DistribucionNormal distribucion = new DistribucionNormal("C:\\Users\\Cristian\\Desktop\\x.txt");

        SalidaPorDefecto.consola("*Promedio: " + distribucion.promedio() + "\n");
        SalidaPorDefecto.consola("*Sumatoria (xi - promedio(x))^2: " + distribucion.sumatoria() + "\n");
        SalidaPorDefecto.consola("*Varianza: " + distribucion.varianza() + "\n");
        SalidaPorDefecto.consola("*Desviacion Estandar: " + distribucion.desviacionEstandar() + "\n");


        distribucion.calcularFx();
        ListaDinamica fx = distribucion.getValoresFx();
        ListaEstatica lista = fx.aListaEstatica();
        SalidaPorDefecto.consola("\n--- Valores de f(x) ---\n");
        lista.imprimirOI();
        ArchivoTexto.escribir(lista, "C:\\Users\\Cristian\\Desktop\\fx.txt");


        distribucion.calcularZDeX();
        ListaDinamica z = distribucion.getValoresZDeX();
        lista = z.aListaEstatica();
        SalidaPorDefecto.consola("\n--- Valores de Z de X ---\n");
        lista.imprimirOI();
        ArchivoTexto.escribir(lista, "C:\\Users\\Cristian\\Desktop\\z.txt");


        DistribucionNormal distribucion2 = new DistribucionNormal("C:\\Users\\Cristian\\Desktop\\z.txt");
        distribucion2.calcularFx();
        ListaDinamica fx2 = distribucion2.getValoresFx();
        lista = fx2.aListaEstatica();
        SalidaPorDefecto.consola("\n--- Valores de F(z) ---\n");
        lista.imprimirOI();
        ArchivoTexto.escribir(lista, "C:\\Users\\Cristian\\Desktop\\fxDeZ.txt");
        SalidaPorDefecto.consola("\n");
    }
}
