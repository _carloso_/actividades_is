package estructurasNoLineales;

public class Pixel {
    private int alpha;
    private int red;
    private int green;
    private int blue;
    public Pixel(int blue, int green, int red, int alpha){
        this.blue = blue;
        this.green = green;
        this.red = red;
        this.alpha = alpha;
    }

    public int getAlpha() {
        return alpha;
    }

    public int getBlue() {
        return blue;
    }

    public int getGreen() {
        return green;
    }

    public int getRed() {
        return red;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public void setRed(int red) {
        this.red = red;
    }
}
