package registros.escuelaCentrosDeComputo;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;

/**
 * Esta clase representa una computadora de un centro de computo de una escuela.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class Computadora {

    protected int numComputadora;
    protected String marca;
    protected String modelo;
    protected String memoriaRam;
    protected String discoDuro;
    protected String procesador;
    protected String sistemaOperativo;
    protected ListaDinamica aplicacionesInstaladas;
    protected ListaDinamica usuarios;

    public Computadora(int numComputadora, String marca, String modelo, String memoriaRam, String discoDuro, String procesador, String sistemaOperativo) {
        this.numComputadora = numComputadora;
        this.marca = marca;
        this.modelo = modelo;
        this.memoriaRam = memoriaRam;
        this.discoDuro = discoDuro;
        this.procesador = procesador;
        this.sistemaOperativo = sistemaOperativo;
        this.aplicacionesInstaladas = new ListaDinamica();
        this.usuarios = new ListaDinamica();
    }

    public int getNumComputadora() {
        return numComputadora;
    }

    public void setNumComputadora(int numComputadora) {
        this.numComputadora = numComputadora;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMemoriaRam() {
        return memoriaRam;
    }

    public void setMemoriaRam(String memoriaRam) {
        this.memoriaRam = memoriaRam;
    }

    public String getDiscoDuro() {
        return discoDuro;
    }

    public void setDiscoDuro(String discoDuro) {
        this.discoDuro = discoDuro;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public String getSistemaOperativo() {
        return sistemaOperativo;
    }

    public void setSistemaOperativo(String sistemaOperativo) {
        this.sistemaOperativo = sistemaOperativo;
    }

    public ListaDinamica getAplicacionesInstaladas() {
        return aplicacionesInstaladas;
    }

    public void setAplicacionesInstaladas(ListaDinamica aplicacionesInstaladas) {
        this.aplicacionesInstaladas = aplicacionesInstaladas;
    }

    public ListaDinamica getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ListaDinamica usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * Este método permite agregar una aplicación a la computadora.
     * @param aplicacion Es la aplicación que se desea agregar.
     * @return Retorna true si la aplicación se agregó correctamente, false en
     */
    public boolean agregarAplicacion(Aplicacion aplicacion) {
        int valorRetornoAgregado = aplicacionesInstaladas.agregar(aplicacion);
        if (valorRetornoAgregado == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Este método permite eliminar una aplicación de la computadora.
     * @param aplicacion Es la aplicación que se desea eliminar.
     * @return Retorna true si la aplicación se eliminó correctamente, false si no se pudo eliminar.
     */
    public boolean eliminarAplicacion(Aplicacion aplicacion) {
        Object valorRetornoEliminado = aplicacionesInstaladas.eliminar(aplicacion);
        if (valorRetornoEliminado == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Este método permite agregar un usuario a la computadora.
     * @param usuario Es el usuario que se desea agregar.
     * @return Retorna true si el usuario se agregó correctamente, false en
     */
    public boolean agregarUsuario(Usuario usuario) {
        int valorRetornoAgregado = usuarios.agregar(usuario);
        if (valorRetornoAgregado == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Este método permite eliminar un usuario de la computadora.
     * @param usuario Es el usuario que se desea eliminar.
     * @return Retorna true si el usuario se eliminó correctamente, false si no se pudo eliminar.
     */
    public boolean eliminarUsuario(Usuario usuario) {
        Object valorRetornoEliminado = usuarios.eliminar(usuario);
        if (valorRetornoEliminado == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Este método permite buscar un usuario de la computadora.
     * @param numUsuario Es el número de usuario que se desea buscar.
     * @return Retorna el usuario que se buscó, null si no se encontró.
     */
    public Usuario buscarUsuario(int numUsuario) {
        Object usuarioEncontrado = usuarios.buscar(numUsuario + "");
        if (usuarioEncontrado == null) {
            return null;
        } else {
            return (Usuario) usuarioEncontrado;
        }
    }

    /**
     * Este método imprime las aplicaciones instaladas en la computadora.
     */
    public void mostrarAplicaciones() {
        aplicacionesInstaladas.iniciarIterador();
        while (aplicacionesInstaladas.hayNodos() == true) {
            Aplicacion aplicacionIterada = (Aplicacion) aplicacionesInstaladas.obtenerNodo();
            SalidaPorDefecto.consola(aplicacionIterada.obtenerDatos());
        }
    }

    /**
     * Este método imprime los usuarios de la computadora.
     */
    public void mostrarUsuarios() {
        usuarios.iniciarIterador();
        while (usuarios.hayNodos() == true) {
            Usuario usuarioIterado = (Usuario) usuarios.obtenerNodo();
            SalidaPorDefecto.consola(usuarioIterado.obtenerDatos());
        }
    }

    /**
     * Este método muestra los datos de la computadora.
     */
    public void mostrarDatos() {
        SalidaPorDefecto.consola("No. de computadora: " + numComputadora + "\n" +
                "Marca: " + marca + "\n" +
                "Modelo: " + modelo + "\n" +
                "Memoria RAM: " + memoriaRam + "\n" +
                "Disco duro: " + discoDuro + "\n" +
                "Procesador: " + procesador + "\n" +
                "Sistema operativo: " + sistemaOperativo + "\n");
        SalidaPorDefecto.consola("Aplicaciones instaladas: " + "\n");
        mostrarAplicaciones();
        SalidaPorDefecto.consola("Usuarios: " + "\n");
        mostrarUsuarios();
        SalidaPorDefecto.consola("\n");
    }

    /**
     * Este método permite saber si la aplicacion "Chrome" está instalada en la computadora.
     * @return Retorna true si la aplicación está instalada, false en caso contrario.
     */
    public boolean estaInstaladoChrome() {
        aplicacionesInstaladas.iniciarIterador();
        while (aplicacionesInstaladas.hayNodos() == true) {
            Aplicacion aplicacionIterada = (Aplicacion) aplicacionesInstaladas.obtenerNodo();
            if (aplicacionIterada.getNombre().equalsIgnoreCase("Chrome")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return numComputadora + "";
    }
}