package estructurasLineales;

import entradaSalida.SalidaPorDefecto;

/**
 * Esta clase implementa un TDA de una lista dinámica unida.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class ListaDinamicaUnida {

    /**
     * Lista enlazada superior.
     */
    ListaDinamica listaSup;
    /**
     * Lista enlazada inferior.
     */
    ListaDinamica listaInf;
    /**
     * Contador de elementos.
     */
    int cont;

    public ListaDinamicaUnida() {
        listaSup = new ListaDinamica();
        listaInf = new ListaDinamica();
        cont = 0;
    }

    /**
     * Este método agrega un elemento al final de la lista.
     * @param valor Es el valor que se agregara a la lista.
     * @return Regresa true si se agrego el elemento, false si no se pudo agregar.
     */
    public boolean agregar(Object valor) {
        if (cont % 2 == 0) {
            int valorRetorno = listaSup.agregar(valor);
            cont++;
            if (valorRetorno == -1) {
                return false;
            } else {
                return true;
            }
        } else {
            int valorRetorno = listaInf.agregar(valor);
            cont++;
            if (valorRetorno == -1) {
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * Este método imprime el contenido de la lista.
     */
    public void imprimir() {
       if (!listaSup.vacia()) {
          listaSup.imprimir();
          SalidaPorDefecto.consola("\n");
          // felecga de dos direcciones
          for (int i = 0; i < listaInf.contarCantidadDeNodos(); i++) {
              SalidaPorDefecto.consola("↑↓   " );
          }
          SalidaPorDefecto.consola("\n");
          listaInf.imprimir();
       } else {
           SalidaPorDefecto.consola("null");
       }
    }
}
