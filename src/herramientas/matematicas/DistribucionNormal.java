package herramientas.matematicas;

import entradaSalida.ArchivoTexto;
import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaEstatica;
import estructurasLineales.auxiliares.Nodo;

/**
 * Esta clase representa una distribucion normal.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class DistribucionNormal {

    /**
     * Este atributo es una lista dinamica que contiene los valores de la variable aleatoria X.
     */
    protected ListaDinamica x;

    // variables globales
    int numElementos;
    double desviacion = 0;
    double promedio = 0;
    ListaDinamica valoresFx;
    ListaDinamica valoresZDeX;
    ListaEstatica listaEstX;

    /**
     * Constructor de la clase que recibe la ruta del archivo que contiene los valores de la variable aleatoria X.
     * @param rutaArchivo Es la ruta del archivo que contiene los valores de la variable aleatoria X.
     */
    public DistribucionNormal(String rutaArchivo) {
        x = ArchivoTexto.leerRecursivo(rutaArchivo);
        listaEstX = ArchivoTexto.leer(rutaArchivo);
        valoresFx = new ListaDinamica();
        valoresZDeX = new ListaDinamica();
        numElementos = x.contarCantidadDeNodos();
    }

    /**
     * Este método calcula el promedio de x.
     * @return Retorna el promedio de x.
     */
    public double promedio() {
        promedio = promedio(x.obtenerPrimero(), 0, 0);
        return promedio;
    }

    /**
     * Este método calcula el promedio de x.
     * @param nodo Es el nodo que se está recorriendo.
     * @param suma Es la sumatoria de los valores de x.
     * @param contador Es el contador de los valores de x.
     * @return Retorna el promedio de x.
     */
    private double promedio(Nodo nodo, double suma, int contador) {
        if (nodo == null) {
            return suma / contador;
        }
        return promedio(nodo.getNodoDer(), suma + Double.parseDouble(nodo.getContenido().toString()), contador + 1);
    }

    /**
     * Este método calcula la sumatoria de (xi - promedio(x))^2.
     * @return Retorna la sumatoria de (xi - promedio(x))^2.
     */
    public double sumatoria() {
        return sumatoria(x.obtenerPrimero(), 0);
    }

    /**
     * Este método calcula la sumatoria de (xi - promedio(x))^2.
     * @param nodo Es el nodo que se está recorriendo.
     * @param suma Es la sumatoria de los valores de x.
     * @return Retorna la sumatoria de (xi - promedio(x))^2.
     */
    private double sumatoria(Nodo nodo, double suma) {
        if (nodo == null) {
            return suma;
        }
        return sumatoria(nodo.getNodoDer(), suma + Math.pow(Double.parseDouble(nodo.getContenido().toString()) - promedio, 2));
    }

    /**
     * Este método calcula la desviación estándar de x.
     * @return Retorna la desviación estándar de x.
     */
    public double desviacionEstandar() {
        desviacion = Math.sqrt(sumatoria() / (numElementos - 1));
        return desviacion;
    }

    /**
     * Este método calcula la varianza de x.
     * @return Retorna la varianza de x.
     */
    public double varianza() {
        return sumatoria() / (numElementos - 1);
    }

    /**
     * Este método calcula la distribucion normal de los valores de x,F(x).
     */
    public void calcularFx() {
        calcularFx(listaEstX, numElementos - 1, 0);
    }

    /**
     * Este método calcula la distribucion normal de los valores de x,F(x).
     * @param listaDeX Es la lista con los valores de x.
     * @param ultimo Indica el último valor de la lista.
     * @param pos Indica la posición actual de la lista.
     */
    private void calcularFx(ListaEstatica listaDeX, int ultimo, int pos) {
        if (pos == ultimo) {
            double sX = desviacionEstandar();
            double xi = Double.parseDouble(listaDeX.obtener(ultimo).toString());
            double fxi = 1 / (sX * Math.sqrt(2 * Math.PI)) * (Math.exp(-(1/ (2*Math.pow(sX, 2)) * Math.pow(xi - promedio, 2))));
            valoresFx.agregar(fxi);
        } else {
            double sX = desviacionEstandar();
            double xi = Double.parseDouble(listaDeX.obtener(pos).toString());
            double fxi = 1 / (sX * Math.sqrt(2 * Math.PI)) * (Math.exp(-(1/ (2*Math.pow(sX, 2)) * Math.pow(xi - promedio, 2))));
            valoresFx.agregar(fxi);
            calcularFx(listaDeX, ultimo, pos + 1);
        }
    }

    /**
     * Este método retorna la lista con los valores de F(x).
     * @return Retorna una lista dinámica con los valores de F(x).
     */
    public ListaDinamica getValoresFx() {
        return valoresFx;
    }

    /**
     * Este método calcula z = (xi - promedio(x)) / desviacion(x) de la variable aleatoria x.
     */
    public void calcularZDeX() {
        calcularZDeX(listaEstX, numElementos - 1, 0);
    }

    /**
     * Este método calcula z = (xi - promedio(x)) / desviacion(x) de la variable aleatoria x.
     * @param listaDeX Es la lista con los valores de x.
     * @param ultimo Indica el último valor de la lista.
     * @param pos Indica la posición actual de la lista.
     */
    private void calcularZDeX(ListaEstatica listaDeX, int ultimo, int pos) {
        if (pos == ultimo) {
            double sX = desviacionEstandar();
            double xi = Double.parseDouble(listaDeX.obtener(ultimo).toString());
            double z = (xi - promedio) / sX;
            valoresZDeX.agregar(z);
        } else {
            double sX = desviacionEstandar();
            double xi = Double.parseDouble(listaDeX.obtener(pos).toString());
            double z = (xi - promedio) / sX;
            valoresZDeX.agregar(z);
            calcularZDeX(listaDeX, ultimo, pos + 1);
        }
    }

    /**
     * Este método retorna la lista con los valores de z = (xi - promedio(x)) / desviacion(x).
     * @return Retorna una lista dinámica con los valores de z = (xi - promedio(x)) / desviacion(x).
     */
    public ListaDinamica getValoresZDeX() {
        return valoresZDeX;
    }
}