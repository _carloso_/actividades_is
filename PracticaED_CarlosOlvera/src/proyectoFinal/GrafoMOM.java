package proyectoFinal;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasNoLineales.Matriz2;

/**
 * Esta clase representa un grafo para un modelo oculto de Markov.
 * @author Cristian Omar Alvarado Rodriguez y Carlos Eduardo Olvera Mayorga.
 * @version 1.0.
 */
public class GrafoMOM {

    protected ListaEstatica vertices; // lista de vertices del grafo
    protected Matriz2 aristas; // gurda las probabilidades de transición entre nodos (matriz de adyacencia)
    protected Matriz2 probaEmisiones; // guarda las probabilidades de emisión de cada observación en cada nodo

    public GrafoMOM(int cantidadNodos, int cantidadObservaciones) {
        vertices = new ListaEstatica(cantidadNodos);
        aristas = new Matriz2(cantidadNodos, cantidadNodos, 0.0);
        probaEmisiones = new Matriz2(cantidadNodos, cantidadObservaciones, 0.0);
    }

    /**
     * Este método agrega un nuevo nodo al grafo.
     * @param nodo Es el nuevo nodo que se agregará al grafo.
     * @return Regresa <b>true</b> si el nodo se agregó correctamente, <b>false</b> en caso contrario.
     */
    public boolean agregarNodo(NodoMOM nodo) {
        Integer numVertice = (Integer) vertices.buscar(nodo);
        if (numVertice == null) {
            int retornoOperacion = vertices.agregar(nodo);
            if (retornoOperacion >= 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método agrega una nueva arista al grafo.
     * @param origen Es el nodo origen de la arista.
     * @param destino Es el nodo destino de la arista.
     * @return Regresa <b>true</b> si la arista se agregó correctamente, <b>false</b> en caso contrario.
     */
    public boolean agregarArista(Object origen, Object destino) {
        return agregarArista(origen, destino, 1.0);
    }

    /**
     * Este método agrega una nueva arista al grafo.
     * @param origen Es el nodo origen de la arista.
     * @param destino Es el nodo destino de la arista.
     * @param peso Es el peso de la arista.
     * @return Regresa <b>true</b> si la arista se agregó correctamente, <b>false</b> en caso contrario.
     */
    public boolean agregarArista(Object origen, Object destino, double peso) {
        //Verificar que existan
        Integer indiceOrigen=(Integer)vertices.buscar(origen);
        Integer indiceDestino=(Integer)vertices.buscar(destino);

        if(indiceOrigen!=null && indiceDestino!=null){ //si existen ambos
            //creamos la arista
            return aristas.cambiar(indiceOrigen,indiceDestino,peso);
        }else{ //uno u otro no existe
            return false;
        }
    }

    /**
     * Este método genera la matriz de probabilidades de emisión de cada observación de cada estado del grafo.
     */
    private void generarMatrizProbaEmisiones() {
        //generamos la matriz de probabilidades de emisiones
        for (int pos1 = 0; pos1 < vertices.numeroElementos(); pos1++) {
            NodoMOM nodo = (NodoMOM) vertices.obtener(pos1);
            ListaEstatica observacionesNodo = nodo.getObservaciones();
            for (int pos2 = 0; pos2 < observacionesNodo.numeroElementos(); pos2++) {
                Integer indiceEstado = (Integer) vertices.buscar(nodo);
                if (indiceEstado != null) {
                    Integer indiceObservacion = (Integer) observacionesNodo.buscar(observacionesNodo.obtener(pos2));
                    if (indiceObservacion != null) {
                        Observacion observacion = (Observacion) observacionesNodo.obtener(pos2);
                        probaEmisiones.cambiar(indiceEstado, pos2, observacion.getProbabilidad());
                    }
                }
            }
        }
    }

    /**
     * Este método muestra la información del grafo.
     */
    public void imprimirGrafo() {
        if (!vertices.vacia()) {
            for (int indiceNodo = 0; indiceNodo < vertices.numeroElementos(); indiceNodo++) {
                NodoMOM nodoTemp = (NodoMOM) vertices.obtener(indiceNodo);
                SalidaPorDefecto.consola("Estado: " + nodoTemp.getNombre() + "\n");
                SalidaPorDefecto.consola("Probabilidad inicial: " + nodoTemp.getProbaInicial() + "\n");
                SalidaPorDefecto.consola("Probabilidad observaciones: \n");
               ListaEstatica observaciones = nodoTemp.getObservaciones();
                for (int obser = 0; obser < observaciones.numeroElementos(); obser++) {
                    SalidaPorDefecto.consola(observaciones.obtener(obser) + "\n");
                }
                SalidaPorDefecto.consola("\n");
            }
            SalidaPorDefecto.consola("Probabilidades de emision:\n");
            generarMatrizProbaEmisiones();
            probaEmisiones.imprimirXRenglones();
            SalidaPorDefecto.consola("Probabilidades de transicion:\n");
            aristas.imprimirXRenglones();
        } else {
            SalidaPorDefecto.consola("No hay nodos en el grafo.\n");
        }
    }

    /**
     * Este método nos permite obtener un nodo del grafo.
     * @param nodo Es el nodo que se desea obtener.
     * @return Regresa el nodo que se desea obtener.
     */
    public NodoMOM obtenerNodo(Object nodo) {
        Integer indiceNodoBuscado = (Integer) vertices.buscar(nodo);
        NodoMOM nodoRetorno = (NodoMOM) vertices.obtener(indiceNodoBuscado);
        if (nodoRetorno != null) {
            return nodoRetorno;
        } else {
            return null;
        }
    }

    /**
     * Este método nos permite obtener la matriz de probabilidades de transición del grafo.
     * @return Regresa la matriz de probabilidades de transición del grafo.
     */
    public Matriz2 getTransiciones(){
        Matriz2 matrizRetorno = new Matriz2(vertices.numeroElementos(),vertices.numeroElementos(),0.0);
        for(int fila = 0; fila < aristas.obtenerRenglones(); fila++){
            for(int col = 0; col < aristas.obtenerColumnas(); col++){
                matrizRetorno.cambiar(fila, col, aristas.obtenerValor(fila, col));
            }
        }
        return matrizRetorno;
    }

    /**
     * Este método nos permite obtener la probabilidad de emisión de una observación de un determinado estado.
     * @param estadoAnimoActual Es el estado del que se desea obtener la probabilidad de emisión.
     * @param indiceActividad Es el índice de la observación que se desea obtener.
     * @return Regresa la probabilidad de emisión de la observación en el estado.
     */
    public double probabilidadActividad(Object estadoAnimoActual, int indiceActividad) {
        double probabilidad = 0.0;
        Integer indiceEstado = (Integer) vertices.buscar(estadoAnimoActual);
        if (indiceEstado != null) {
            NodoMOM nodo = (NodoMOM) vertices.obtener(indiceEstado);
            ListaEstatica observaciones = nodo.getObservaciones();
            Observacion observacion = (Observacion) observaciones.obtener(indiceActividad);
            probabilidad = observacion.getProbabilidad();
        }
        return probabilidad;
    }
}
