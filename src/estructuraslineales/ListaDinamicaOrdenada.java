package estructurasLineales;

import estructurasLineales.auxiliares.Nodo;
import herramientas.comunes.TipoOrden;

/**
 * Esta clase representa un TDA de una lista dinámica ordenada.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class ListaDinamicaOrdenada extends ListaDinamica {

    private TipoOrden tipoOrden;

    public ListaDinamicaOrdenada(TipoOrden tipoOrden) {
        super();
        this.tipoOrden = tipoOrden;
    }

    @Override
    public int agregar(Object valor) {
        // agregar elemento de forma ordenada, segun el tipo de orden ASC o DESC
        Nodo nuevoNodo = new Nodo(valor);
        int valorRetorno = -1;
        if (nuevoNodo != null) {
            if (!vacia()) {
                if (tipoOrden == TipoOrden.ASC) {
                    boolean agregadoASC = agregarASC(valor, nuevoNodo);
                    if (agregadoASC == true){
                        valorRetorno = 0;
                    }
                } else {
                    boolean agrgadoDESC = agregarDESC(valor, nuevoNodo);
                    if (agrgadoDESC == true){
                        valorRetorno = 0;
                    }
                }
            } else {
                primero = nuevoNodo;
                ultimo = nuevoNodo;
                valorRetorno = 0;
            }
        }
        return valorRetorno;
    }

    /**
     * Este método nos permite agregar un nuevo nodo a la lista ordena de forma ascendente.
     * @param valor Es el valor del nuevo nodo, que se agregara a la lista.
     * @param nuevoNodo Es el nuevo nodo que se agregara a la lista.
     * @return Regresa <b>true</b> si se agrego el nuevo nodo a la lista, o <b>false</b> si no se agrego.
     */
    private boolean agregarASC(Object valor, Nodo nuevoNodo) {
        if (nuevoNodo != null){
            Nodo nodoAnterior = null;
            Nodo iterador = primero;
            while (iterador != null && iterador.getContenido().toString().compareTo(valor.toString()) < 0) {
                nodoAnterior = iterador;
                iterador = iterador.getNodoDer();
            }
            if (nodoAnterior == null) {
                nuevoNodo.setNodoDer(primero);
                primero = nuevoNodo;
            } else {
                nuevoNodo.setNodoDer(iterador);
                nodoAnterior.setNodoDer(nuevoNodo);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite agregar un nuevo nodo a la lista ordena de forma descendente.
     * @param valor Es el valor del nuevo nodo, que se agregara a la lista.
     * @param nuevoNodo Es el nuevo nodo que se agregara a la lista.
     * @return Regresa <b>true</b> si se agrego el nuevo nodo a la lista, o <b>false</b> si no se agrego.
     */
    private boolean agregarDESC(Object valor, Nodo nuevoNodo){
        if (nuevoNodo != null){
            Nodo iterador = null;
            Nodo nodoActual = primero;
            while (nodoActual != null && nodoActual.getContenido().toString().compareTo(valor.toString()) > 0) {
                iterador = nodoActual;
                nodoActual = nodoActual.getNodoDer();
            }
            if (iterador == null) {
                nuevoNodo.setNodoDer(primero);
                primero = nuevoNodo;
            } else {
                nuevoNodo.setNodoDer(nodoActual);
                iterador.setNodoDer(nuevoNodo);
            }
            return true;
        } else {
            return false;
        }
    }

    // buscar de manera ordenada, segun el tipo de orden ASC o DESC
    @Override
    public Object buscar(Object valor) {
        if (!vacia()) {
            if (tipoOrden == TipoOrden.ASC) { // buscar de forma ascendente
                Nodo iterador = primero;
                while (iterador != null && iterador.getContenido().toString().compareTo(valor.toString()) < 0) {
                    iterador = iterador.getNodoDer();
                }
                if (iterador != null && iterador.getContenido().toString().compareTo(valor.toString()) == 0) {
                    return iterador.getContenido();
                } else {
                    return null;
                }
            } else { // buscar de forma descendente
                Nodo iterador = primero;
                while (iterador != null && iterador.getContenido().toString().compareTo(valor.toString()) > 0) {
                    iterador = iterador.getNodoDer();
                }
                if (iterador != null && iterador.getContenido().toString().compareTo(valor.toString()) == 0) {
                    return iterador.getContenido();
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
    }

    @Override
    public boolean cambiar(Object valorViejo, Object valorNuevo, int numVeces){
        boolean valorRetorno = super.cambiar(valorViejo, valorNuevo, numVeces);
        if (valorRetorno == true){
            ordenarLista();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método ordena la lista dinámica según el tipo de orden.
     */
    private void ordenarLista(){
        if (tipoOrden == TipoOrden.ASC){
            ordenarASC();
        } else {
            ordenarDESC();
        }
    }

    /**
     * Este método ordena la lista dinámica de forma ascendente (menor a mayor)
     */
    private void ordenarASC() {
        Nodo iterador1 = primero;
        Nodo iterador2;
        Object contenidoAux;
        while(iterador1 != null){
            iterador2 = iterador1.getNodoDer();
            while(iterador2 != null){
                if(iterador1.getContenido().toString().compareTo(iterador2.toString()) > 0){
                    contenidoAux = iterador1.getContenido();
                    iterador1.setContenido(iterador2.getContenido());
                    iterador2.setContenido(contenidoAux);
                }
                iterador2 = iterador2.getNodoDer();
            }
            iterador1 = iterador1.getNodoDer();
        }
    }

    /**
     * Este método ordena la lista dinámica de forma descendente (mayor a menor).
     */
    private void ordenarDESC() {
        Nodo iterador1 = primero;
        Nodo iterador2;
        Object contenidoAux;
        while(iterador1 != null){
            iterador2 = iterador1.getNodoDer();
            while(iterador2 != null){
                if(iterador1.getContenido().toString().compareTo(iterador2.toString()) < 0){
                    contenidoAux = iterador1.getContenido();
                    iterador1.setContenido(iterador2.getContenido());
                    iterador2.setContenido(contenidoAux);
                }
                iterador2 = iterador2.getNodoDer();
            }
            iterador1 = iterador1.getNodoDer();
        }
    }
}