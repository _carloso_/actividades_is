package estructurasLineales;

public class PilaDinamica implements LoteAlmacenamiento{
    protected ListaDinamica pila;
    public PilaDinamica() {

        pila = new ListaDinamica();
    }

    @Override
    public boolean vacio() {
        return pila.vacia();
    }

    @Override
    public boolean lleno() {
        return false;
    }

    @Override
    public boolean poner(Object valor) {
        int valorRetorno = pila.agregar(valor);
        if (valorRetorno >= 0) {
            return true;
        } else { //retorna -1
            return false;
        }
    }

    @Override
    public Object quitar() {
        return pila.eliminar();
    }

    @Override
    public void imprimir() {
        pila.imprimir();
    }

    @Override
    public Object verUltimo() {
        return pila.verUltimo();
    }

    @Override
    public int maximo() {
        return 0;
    }

}
