package entradaSalida;

import javax.swing.*;

/**
 * Esta clase gestina la salida por defecto por consola.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class SalidaPorDefecto {
    /**
     * Este metodo imprime por consola un cadena que se le pasa como parametro.
     * @param cadenaSalida Es la cadena que se va a imprimir por consola.
     */
    public static void consola(String cadenaSalida) {
        System.out.print(cadenaSalida);
    }

    /**
     * Este método muestra un mensaje en un JOptionPane.
     * @param mensaje Es el mensaje que se va a mostrar.
     */
    public static void jOptionPaneMensaje(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje);
    }
}
