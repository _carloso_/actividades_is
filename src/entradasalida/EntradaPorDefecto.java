package entradaSalida;

import java.io.*;

/**
 * Esta clase esta destinada a leer los datos de entrada por defecto.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class EntradaPorDefecto {
    /**
     * Este metodo lee una cadena de caracteres desde la consola.
     * @return La cadena de caracteres leida.
     */
    public static String consolaCadenas(){
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(isr);
        String cadenaEntrada = "";
        try {
            cadenaEntrada = buffer.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return cadenaEntrada;
        }
    }

    /**
     * Este metodo lee un numero <b>double</b> desde la consola.
     * @return El numero double leido.
     */
    public static Double consolaDouble(){
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(isr);
        double entrada = 0.0;
        try {
            entrada = Double.parseDouble(buffer.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return entrada;
        }
    }

    /**
     * Este metodo lee un numero entero <b>Integer</b> desde la consola.
     * @return El numero entero leido desde la consola.
     */
    public static Integer consolaInteger(){
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(isr);
        int entrada = 0;
        try {
            entrada = Integer.parseInt(buffer.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return entrada;
        }
    }
}