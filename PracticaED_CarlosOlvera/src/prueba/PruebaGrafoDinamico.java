package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasNoLineales.GrafoDinamico;

public class PruebaGrafoDinamico {
    public static  void main(String args[]){
        GrafoDinamico grafo=new GrafoDinamico();

        grafo.agregarVertice("A");
        grafo.agregarVertice("B");
        grafo.agregarVertice("C");
        grafo.agregarVertice("D");

        grafo.agregarArista("A","B");
        grafo.agregarArista("B","D");
        grafo.agregarArista("C","A");
        grafo.agregarArista("C","B");
        grafo.agregarArista("D","A");
        grafo.agregarArista("D","C");
        grafo.agregarArista("B","A");

        grafo.imprimir();

        /*SalidaPorDefecto.consola("Recorrido en profundidad a partir de A: ");
        grafo.recorridoProfunidad("A").imprimir();*/
        grafo.algoritmoKruska2();
    }
}
