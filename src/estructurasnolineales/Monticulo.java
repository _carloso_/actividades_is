package estructurasnolineales;

import estructurasLineales.auxiliares.NodoDoble;
import herramientas.comunes.TipoOrden;
import herramientas.generales.Utilerias;

/**
 * Esta clase implementa un TDA montículo binario (heapMax y heapMin).
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class Monticulo extends ArbolBinario {

    protected TipoOrden tipoOrden;

    public Monticulo(TipoOrden tipoOrden) {
        this.tipoOrden = tipoOrden;
    }

    // Monticulo maximo: El valor de cada nodo es mayor o igual al valor de cada uno de sus hijos.
    // Monticulo minimo: El valor de cada nodo es menor o igual al valor de cada uno de sus hijos.

    /**
     * Este método inserta un nuevo nodo en el montículo.
     * @param valor El valor del nuevo nodo.
     * @return Regresa <b>true</b> si el nodo fue insertado correctamente, <b>false</b> si no se pudo insertar.
     */
    public boolean agregar(Object valor) {
        if (raiz == null) { //Si el montículo está vacío
            NodoDoble nuevoNodo = new NodoDoble(valor);
            if (nuevoNodo != null) {
                raiz = nuevoNodo;
                return true;
            } else {
                return false;
            }
        } else { //Si el montículo no está vacío
            NodoDoble nuevoNodo = new NodoDoble(valor);
            if (nuevoNodo != null) {
                NodoDoble nodoActual = raiz;
                NodoDoble nodoPadre = null;
                while (nodoActual != null) {
                    nodoPadre = nodoActual;
                    if (tipoOrden == TipoOrden.ASC) {
                        if (Utilerias.compararObjetos(nuevoNodo.getContenido(), nodoActual.getContenido()) < 0) { //nuevoNodo < nodoActual
                            nodoActual = nodoActual.getNodoIzq();
                        } else { //nuevoNodo > nodoActual
                            nodoActual = nodoActual.getNodoDer();
                        }
                    } else { //tipoOrden == TipoOrden.DESC
                        if (Utilerias.compararObjetos(nuevoNodo.getContenido(), nodoActual.getContenido()) > 0) { //nuevoNodo > nodoActual
                            nodoActual = nodoActual.getNodoIzq();
                        } else { //nuevoNodo < nodoActual
                            nodoActual = nodoActual.getNodoDer();
                        }
                    }
                }
                if (tipoOrden == TipoOrden.ASC) {
                    if (nodoPadre.getNodoIzq() == null) {
                        nodoPadre.setNodoIzq(nuevoNodo);
                    } else {
                        nodoPadre.setNodoDer(nuevoNodo);
                    }
                } else { //tipoOrden == TipoOrden.DESC
                    if (nodoPadre.getNodoIzq() == null) {
                        nodoPadre.setNodoIzq(nuevoNodo);
                    } else {
                        nodoPadre.setNodoDer(nuevoNodo);
                    }
                }
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Este método elimina un nodo del montículo.
     * @param valor El valor del nodo a eliminar.
     * @return Regresa <b>true</b> si el nodo fue eliminado correctamente, <b>false</b> si no se pudo eliminar.
     */
    public boolean eliminar(Object valor) {
        NodoDoble nodoActual = raiz;
        NodoDoble nodoPadre = null;
        while (nodoActual != null) {
            nodoPadre = nodoActual;
            if (Utilerias.compararObjetos(valor, nodoActual.getContenido()) < 0) { //valor < nodoActual
                nodoActual = nodoActual.getNodoIzq();
            } else { //valor > nodoActual
                nodoActual = nodoActual.getNodoDer();
            }
        }
        if (nodoPadre == null) {
            raiz = null;
        } else if (nodoPadre.getNodoIzq() == nodoActual) {
            nodoPadre.setNodoIzq(null);
        } else {
            nodoPadre.setNodoDer(null);
        }
        return true;
    }
}