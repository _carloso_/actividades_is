package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.PilaEstatica;

public class PruebaPilaEstatica {
    public static void main(String[] args) {
        PilaEstatica pila = new PilaEstatica(6);

        pila.poner("G");
        pila.poner("H");
        pila.poner("Z");
        pila.poner("A");

        pila.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("Eliminando el último: " + pila.quitar() + "\n");
        SalidaPorDefecto.consola("Eliminando el último: " + pila.quitar() + "\n");

        pila.imprimir();
        SalidaPorDefecto.consola("\n");
    }
}