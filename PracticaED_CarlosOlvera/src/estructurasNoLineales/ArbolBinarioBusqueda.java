package estructurasNoLineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.auxiliares.NodoDoble;
import estructurasLineales.auxiliares.Nodo;
import estructurasLineales.auxiliares.NodoDoble;
import herramientas.generales.Utilerias;

import javax.security.auth.Subject;

public class ArbolBinarioBusqueda extends ArbolBinario{

    public boolean agregar(Object valor){
        if(raiz==null){ //el valor lo agregaremos como la nueva raíz
            NodoDoble nuevoNodo=new NodoDoble(valor);
            if(nuevoNodo!=null){ //si hay espacio
                raiz=nuevoNodo;
                return true;
            }else{ //no hay espacio o hay error
                return false;
            }
        }else{ //ya hay nodos
            return agregar(raiz,valor);
        }
    }

    private boolean agregar(NodoDoble subRaiz, Object valor){
        //Comparar si el valor es mayor o menor a la subraíz
        if(Utilerias.compararObjetos(valor,subRaiz.getContenido())<0){ //valor < subRaiz
            if(subRaiz.getNodoIzq()==null){ //llegamos a la posicion donde le toca ser insertado
                NodoDoble nuevoNodo=new NodoDoble(valor);
                if(nuevoNodo!=null){ // si hay espacio
                    subRaiz.setNodoIzq(nuevoNodo);
                    return true;
                }else{ //no hay espacio
                    return false;
                }
            }else{ //hacemos llamada recursiva hacia esa rama
                return agregar(subRaiz.getNodoIzq(),valor);
            }
        }else if(Utilerias.compararObjetos(valor,subRaiz.getContenido())>0){ //valor > subRaiz
            if(subRaiz.getNodoDer()==null){ //llegamos a la posicion donde le toca ser insertado
                NodoDoble nuevoNodo=new NodoDoble(valor);
                if(nuevoNodo!=null){ // si hay espacio
                    subRaiz.setNodoDer(nuevoNodo);
                    return true;
                }else{ //no hay espacio
                    return false;
                }
            }else{ //hacemos llamada recursiva hacia esa rama
                return agregar(subRaiz.getNodoDer(),valor);
            }
        }else{ //valor = subRaiz
            return false;
        }
    }

    public Object buscar(Object valor){
        return buscar(raiz, valor);
    }

    private Object buscar(NodoDoble subRaiz, Object valor){
        if(subRaiz!=null){ //hay donde buscar
            if (Utilerias.compararObjetos(valor,subRaiz.getContenido())<0) {   //<
                return buscar(subRaiz.getNodoIzq(), valor);
            }else if (Utilerias.compararObjetos(valor,subRaiz.getContenido())>0){  //>
                return buscar(subRaiz.getNodoDer(), valor);
            }else{ //si son iguales
                return subRaiz.getContenido();
            }
        }else{ //no hay donde buscar
            return null;
        }
    }

    public void eliminar(Object valor){
        if (raiz != null){
            eliminar(raiz,null,valor);
        }
        else{
            SalidaPorDefecto.consola("No hay nodos existentes");
        }
    }
    public void eliminar(NodoDoble subRaiz, NodoDoble anterior, Object valor) {
        NodoDoble aux = null;
        NodoDoble aux1 = null;
        NodoDoble otro = null;
        if (Utilerias.compararObjetos(valor, subRaiz.getContenido()) < 0) { // valor < subraiz
            eliminar(subRaiz.getNodoIzq(), subRaiz, valor);
        } else {
            if (Utilerias.compararObjetos(valor, subRaiz.getContenido()) > 0) { // valor > subraiz
                eliminar(subRaiz.getNodoDer(), subRaiz, valor);
            } else { // valor == subraiz
                if (subRaiz.getNodoIzq() != null && subRaiz.getNodoDer() != null) { // subraiz tiene dos hijos
                    // buscamos el nodo que va a reemplazar al nodo a eliminar (se saca el mas a la derecha del subarbol izquierdo)
                    aux = subRaiz.getNodoIzq();
                    boolean encontrado = false;
                    while (aux.getNodoDer() != null) {
                        aux1 = aux;
                        aux = aux.getNodoDer();
                        encontrado = true;
                    }
                    subRaiz.setContenido(aux.getContenido());
                    if (encontrado == true) {
                        aux1.setNodoDer(aux.getNodoIzq());
                    } else {
                        subRaiz.setNodoIzq(aux.getNodoIzq());
                    }
                } else { // el derecho, izquierdo o ambos son null
                    otro = subRaiz;
                    if (otro.getNodoDer() == null) {
                        if (otro.getNodoIzq() != null) {
                            otro = subRaiz.getNodoIzq();
                            if (anterior != null) {
                                if (Utilerias.compararObjetos(valor, anterior.getContenido()) < 0) { // valor < anterior
                                    anterior.setNodoIzq(otro);
                                } else { // valor > anterior
                                    anterior.setNodoDer(otro);
                                }
                            } else { // anterior es null
                                raiz = otro;
                            }
                        } else { // ambos son null
                            if (anterior == null) { // raiz solamente
                                raiz = null;
                            } else { // anterior no es null
                                if (Utilerias.compararObjetos(valor, anterior.getContenido()) < 0) { // valor < anterior
                                    anterior.setNodoIzq(null);
                                } else { // valor > anterior
                                    anterior.setNodoDer(null);
                                }
                            }
                        }
                    } else { // la derecha no es null
                        if (otro.getNodoIzq() == null) { // solo la izquierda es null
                            otro = subRaiz.getNodoDer();
                            if (anterior != null) {
                                if (Utilerias.compararObjetos(valor, anterior.getContenido()) < 0) { // valor < anterior
                                    anterior.setNodoIzq(otro);
                                } else { // valor > anterior
                                    anterior.setNodoDer(otro);
                                }
                            } else { // anterior es null
                                raiz = otro;
                            }
                        } else { // ambos son null
                            if (anterior == null) { // raiz solamente
                                raiz = null;
                            } else { // anterior no es null
                                if (Utilerias.compararObjetos(valor, anterior.getContenido()) < 0) { // valor < anterior
                                    anterior.setNodoIzq(null);
                                } else { // valor > anterior
                                    anterior.setNodoDer(null);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
