package examenes.parcial1;

import entradaSalida.ArchivoTexto;
import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasLineales.PilaEstatica;

/**
 * Esta calse realiza la verificacion del balanceo de "() [] {}" en una cadena de caracteres o de un archivo de texto.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class BalanceoDeCaracteres {

    /**
     * Atributo pila que almacena los caracteres de la cadena de texto que se lee.
     * @see PilaEstatica
     */
    private PilaEstatica pilaCaracteres;

    // Variables
    int opcion;
    String rutaArchivo, cadena;

    // constantes para los colores de la consola.
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_WHITE = "\u001B[37m";

    /**
     * Este método muestra un menu de opciones para el usuario.
     */
    public void mostrarMenu(){
        do {
            SalidaPorDefecto.consola("1) Leer una archivo" + "\n");
            SalidaPorDefecto.consola("2) Leer una cadena" + "\n");
            SalidaPorDefecto.consola("3) Salir" + "\n");
            SalidaPorDefecto.consola("Elige una opcion: ");
            opcion = EntradaPorDefecto.consolaInteger();

            switch (opcion){
                case 1:
                    SalidaPorDefecto.consola("Ingresa la ruta del archivo: ");
                    rutaArchivo = EntradaPorDefecto.consolaCadenas();
                    balacearArchivo(rutaArchivo);
                    break;
                case 2:
                    SalidaPorDefecto.consola("Ingresa la cadena: ");
                    cadena = EntradaPorDefecto.consolaCadenas();
                    balacearCadena(cadena, 1);
                    break;
                case 3:
                    SalidaPorDefecto.consola("Saliendo...");
                    break;
                default:
                    SalidaPorDefecto.consola("*** Opcion Invalida ***" + "\n");
                    break;
            }
        } while (opcion != 3);
    }

    /**
     * Este método se encarga de verificar si una cadena de texto esta balanceada o no y mostrar los errores.
     * @param cadena Es la cadena de texto que se desea verificar.
     * @param numLinea Es el numero de linea que se esta leyendo.
     */
    private void balacearCadena(String cadena, int numLinea) {
        // Balanceo de	(...),[...],{...}
        pilaCaracteres =  new PilaEstatica(cadena.length());
        ListaEstatica listaCadena = new ListaEstatica(cadena.length());
        for (int pos = 0;  pos < cadena.length(); pos++) {
            char caracter = cadena.charAt(pos);
            listaCadena.agregar(caracter);
        }
        int pos = 0;
        while (pos < listaCadena.numeroElementos()){
            char caracter = (char) listaCadena.obtener(pos);
            if (caracter == '(' || caracter == '[' || caracter == '{') {
                pilaCaracteres.poner(caracter);
            } else if (caracter == ')' || caracter == ']' || caracter == '}') {
                if (pilaCaracteres.vacio()) {
                    break;
                } else {
                    pilaCaracteres.quitar();
                }
            }
            pos++;
        }
        if (pilaCaracteres.vacio() && pos == listaCadena.numeroElementos()) {
            SalidaPorDefecto.consola(ANSI_BLUE + "La linea " + numLinea + " no tiene errores" +
                    ANSI_WHITE + "\n");
        } else {
            while (!pilaCaracteres.vacio()) {
                char caracter = (char) pilaCaracteres.quitar();
                if (caracter == '(') {
                    SalidaPorDefecto.consola(ANSI_RED + "Error: Falta un ')'" + " en la linea " +
                            numLinea + ANSI_WHITE + "\n");
                } else if (caracter == '[') {
                    SalidaPorDefecto.consola(ANSI_RED + "Error: Falta un ']'" + " en la linea " +
                            numLinea + ANSI_WHITE + "\n");
                } else if (caracter == '{') {
                    SalidaPorDefecto.consola(ANSI_RED + "Error: Falta un '}'" + " en la linea " +
                            numLinea + ANSI_WHITE + "\n");
                } else if (caracter == ')') {
                    SalidaPorDefecto.consola(ANSI_RED + "Error: Falta un '('" + " en la linea " +
                            numLinea + ANSI_WHITE + "\n");
                } else if (caracter == ']') {
                    SalidaPorDefecto.consola(ANSI_RED + "Error: Falta un '['" + " en la linea " +
                            numLinea + ANSI_WHITE + "\n");
                } else if (caracter == '}') {
                    SalidaPorDefecto.consola(ANSI_RED + "Error: Falta un '{'" + " en la linea " +
                            numLinea + ANSI_WHITE + "\n");
                } else {
                    SalidaPorDefecto.consola(ANSI_RED + "Error: Caracter invalido" + ANSI_WHITE + "\n");
                }
            }
        }
    }

    /**
     * Este método se encarga de leer un archivo y balancear las llaves, corchetes y paréntesis que contiene.
     * @param rutaArchivo Es la ruta del archivo que se desea leer.
     */
    private void balacearArchivo(String rutaArchivo) {
        ListaEstatica listaArchivo = ArchivoTexto.leer(rutaArchivo);
        if (listaArchivo != null) {
            // recorrer la lista linea por linea y balancear cada linea de la lista, y mostrar los errores en cada linea
            int numLinea = 1;
            while (!listaArchivo.vacia()) {
                String linea = (String) listaArchivo.obtener(0);
                listaArchivo.eliminar(0);
                balacearCadena(linea, numLinea);
                numLinea++;
            }
        } else {
            SalidaPorDefecto.consola("*** No se pudo leer el archivo ***" + "\n");
        }
    }
}