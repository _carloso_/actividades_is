package prueba;

import entradaSalida.SalidaPorDefecto;
import entradaSalida.archivos.AccederArchivo;
import estructurasLineales.ListaEstatica;
import estructurasNoLineales.AplicacionesArbol;

import java.io.IOException;

public class PruebaAplicacionesArbol {

    public static void main(String[] args) throws IOException {
        AplicacionesArbol arbolA = new AplicacionesArbol("src\\datos_ordenes\\categories_tab.txt");
        SalidaPorDefecto.consola("-----INDICE-----\n");
        SalidaPorDefecto.consola("PreOrden: ");
        arbolA.preorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("InOrden: ");
        arbolA.inorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PostOrden: ");
        arbolA.postorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----BUSCAR-----\n");
        SalidaPorDefecto.consola("buscar indice 11\n");
        arbolA.buscar(11);
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----AGREGAR-----\n");
        arbolA.agregarNuevo("1234","\"software\",\"computer software\",11,91\n");
        SalidaPorDefecto.consola("buscar indice 23\n");
        arbolA.buscar(23);
        SalidaPorDefecto.consola("PreOrden: ");
        arbolA.preorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("InOrden: ");
        arbolA.inorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PostOrden: ");
        arbolA.postorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----ELIMINAR-----\n");
        SalidaPorDefecto.consola("eliminar indice 5\n");
        arbolA.eliminar(5);
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PreOrden: ");
        arbolA.preorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("InOrden: ");
        arbolA.inorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PostOrden: ");
        arbolA.postorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");




        /*SalidaPorDefecto.consola("tabla categories_tab\n");
        AccederArchivo.leerArchivo("src\\datos_ordenes\\categories_tab.txt");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("TABLA DIRECCION\n");
        AccederArchivo.getDireccion();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("TABLA DATOS\n");
        AccederArchivo.getDatos();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("tabla customers\n");
        AccederArchivo.leerArchivo("src\\datos_ordenes\\customers.txt");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("tabla order_items\n");
        AccederArchivo.leerArchivo("src\\datos_ordenes\\order_items.txt");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("tabla orders\n");
        AccederArchivo.leerArchivo("src\\datos_ordenes\\orders.txt");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("tabla product_description\n");
        AccederArchivo.leerArchivo("src\\datos_ordenes\\product_description.txt");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("tabla product_information\n");
        AccederArchivo.leerArchivo("src\\datos_ordenes\\product_information.txt");*/
    }
}
