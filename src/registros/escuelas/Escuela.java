package registros.escuelas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;

/**
 * Clase que representa una escuela con una lista de alumnos.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class Escuela {

    protected String nombre;
    protected ListaEstatica alumnos;

    public Escuela(String nombre, int cantidadAlumnos) {
        this.nombre = nombre;
        alumnos = new ListaEstatica(cantidadAlumnos);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ListaEstatica getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(ListaEstatica alumnos) {
        this.alumnos = alumnos;
    }

    /**
     * Metodo que agrega a un alumno a la lista de alumnos de la escuela.
     * @param alumno Es el alumno a agregar.
     * @return Regresa <b>true</b> si se agrego correctamente el alumno o <b>false</b> si no se agrego.
     */
    public boolean agregarAlumno(Alumno alumno) {
        int valorRetornoAgregado = alumnos.agregar(alumno);
        if (valorRetornoAgregado == -1) { // no se pudo agregar
            return false;
        } else { // si se pudo agregar
            return true;
        }
    }

    /**
     * Este metodo imprime a un alumno.
     */
    public void imprimirListadoAlumnos() {
        alumnos.imprimir();
    }

    /**
     * Metodo que imprime los datos de un alumno.
     */
    public void imprimirListadoDatosAlumnos() {
        String datosAlumno = "";
        for (int posicion = 0; posicion < alumnos.numeroElementos(); posicion++) {
            Alumno alumnoBusqueda = (Alumno) alumnos.obtener(posicion);
            datosAlumno = datosAlumno + alumnoBusqueda.getMatricula() +
                    " " + alumnoBusqueda.getNombre() + " " + alumnoBusqueda.getApellido() +
                    " " + alumnoBusqueda.obtenerPromedio() + "\n";
        }
        SalidaPorDefecto.consola(datosAlumno);
    }

    /**
     * Metod que calcula el promedio de un alumno dada su matricula.
     * @param matricula Es la matricula del alumno.
     * @return El promedio de calificaciones del alumno.
     */
    public Double calcularPromedioAlumno(String matricula) {
        // Buscar al alumno con esa matricula
        Integer posicionBusqueda = (Integer) alumnos.buscar(matricula);

        if (posicionBusqueda != null) { // si esta
            Alumno alumnoBusqueda = (Alumno) alumnos.obtener(posicionBusqueda);
            return alumnoBusqueda.obtenerPromedio();
        } else { // no esta
            return null;
        }
    }
}