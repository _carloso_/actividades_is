package proyectoFinal;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasnolineales.Matriz2;

public class ModeloOcultoMarkov {

    protected GrafoMOM grafoMOM;

    public ModeloOcultoMarkov(int numeroNodos, int numeroObservaciones) {
        grafoMOM = new GrafoMOM(numeroNodos, numeroObservaciones);
    }

    public boolean agregarNodoEstado(NodoMOM nodoEstado) {
        return grafoMOM.agregarNodo(nodoEstado);
    }

    public boolean agregarArista(Object estadoOrigen, Object estadoDestino, double probabilidad) {
        return grafoMOM.agregarArista(estadoOrigen, estadoDestino, probabilidad);
    }

    public void mostrarMenu() {
        int opcion = 0;
        do {
            SalidaPorDefecto.consola("----- Menu de opciones -----\n");
            SalidaPorDefecto.consola("1. Mostrar información del grafo\n");
            SalidaPorDefecto.consola("2. Probabilidad de que el escenario inicie con un estado de animo específico\n");
            SalidaPorDefecto.consola("3. Probabilidad de que su amigo haga cierta actividad según su estado de animo\n");
            SalidaPorDefecto.consola("4. Salir\n");
            SalidaPorDefecto.consola(">>>Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    grafoMOM.imprimirGrafo();
                    break;
                case 2:
                    subMenuProbabilidadInicio();
                    break;
                case 3:
                    SalidaPorDefecto.consola("Estado de animo actual (feliz/enojado/triste/aburrido): ");
                    String estadoAnimoActual = EntradaPorDefecto.consolaCadenas();
                    if (validarEstadoAnimo(estadoAnimoActual)) {
                        subMenuProbabilidadActividad(estadoAnimoActual);
                    } else {
                        SalidaPorDefecto.consola("Estado de animo inválido\n");
                    }
                    break;
                case 4:
                    SalidaPorDefecto.consola("Saliendo...\n");
                    break;
                default:
                    SalidaPorDefecto.consola("Opción no válida\n");
                    break;
            }
        } while (opcion != 4);
    }

    private void subMenuProbabilidadActividad(String estadoAnimoActual) {
        int opcion = 0;
        do {
            SalidaPorDefecto.consola("----- Actividades Según Estado de Animo -----\n");
            SalidaPorDefecto.consola("1. Probabilidad de que su amigo este Bilando si esta " + estadoAnimoActual + "\n");
            SalidaPorDefecto.consola("2. Probabilidad de que su amigo este Leyendo si esta " + estadoAnimoActual + "\n");
            SalidaPorDefecto.consola("3. Probabilidad de que su amigo este Escuchando Musica si esta " + estadoAnimoActual + "\n");
            SalidaPorDefecto.consola("4. Regresar...\n");
            SalidaPorDefecto.consola(">>>Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    SalidaPorDefecto.consola("Probabilidad de que su amigo este Bilando si esta "
                            + estadoAnimoActual + ": " + grafoMOM.probabilidadActividad(estadoAnimoActual, 0) + "\n");
                    break;
                case 2:
                    SalidaPorDefecto.consola("Probabilidad de que su amigo este Leyendo si esta "
                            + estadoAnimoActual + ": " + grafoMOM.probabilidadActividad(estadoAnimoActual, 1) + "\n");
                    break;
                case 3:
                    SalidaPorDefecto.consola("Probabilidad de que su amigo este Escuchando Musica si esta "
                            + estadoAnimoActual + ": " +
                            grafoMOM.probabilidadActividad(estadoAnimoActual, 2) + "\n");
                    break;
                default:
                    SalidaPorDefecto.consola("Opción no válida\n");
                    break;
            }
        } while (opcion != 4);
    }

    private void subMenuProbabilidadInicio() {
        int opcion = 0;
        do {
            SalidaPorDefecto.consola("----- Probabilidad de inicio específico -----\n");
            SalidaPorDefecto.consola("1. Feliz\n");
            SalidaPorDefecto.consola("2. Enojado\n");
            SalidaPorDefecto.consola("3. Triste\n");
            SalidaPorDefecto.consola("4. Aburrido\n");
            SalidaPorDefecto.consola("5. Regresar...\n");
            SalidaPorDefecto.consola(">>>Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    NodoMOM nodoBusqueda = grafoMOM.obtenerNodo("feliz");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de iniciar con un estado de animo feliz: " +
                                nodoBusqueda.getProbaInicial() + " %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo feliz\n");
                    }
                    break;
                case 2:
                    nodoBusqueda = grafoMOM.obtenerNodo("enojado");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de iniciar con un estado de animo enojado: " +
                                nodoBusqueda.getProbaInicial() + " %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo enojado\n");
                    }
                    break;
                case 3:
                    nodoBusqueda = grafoMOM.obtenerNodo("triste");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de iniciar con un estado de animo triste: " +
                                nodoBusqueda.getProbaInicial() + " %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo triste\n");
                    }
                    break;
                case 4:
                    nodoBusqueda = grafoMOM.obtenerNodo("aburrido");
                    if (nodoBusqueda != null) {
                        SalidaPorDefecto.consola("Probabilidad de iniciar con un estado de animo aburrido: " +
                                nodoBusqueda.getProbaInicial() + " %\n");
                    } else {
                        SalidaPorDefecto.consola("No existe el estado de animo aburrido\n");
                    }
                    break;
                default:
                    SalidaPorDefecto.consola("Opción no válida\n");
                    break;
            }
        } while (opcion != 5);
    }

    private double obtenerProbabilidadTransicion(Object nodoOrigen, Object nodoDestino) {
        return grafoMOM.obtenerProbabilidadTransicion(nodoOrigen, nodoDestino);
    }

    // calcular los estados transitorios de los nodos elevar la matriz de transiciones a la potencia de numero de dia
    public Matriz2 calcularMatrizTransiciones(int numeroDia) {
        Matriz2 matrizTransiciones1 = grafoMOM.obtenerMatrizProbaTransicion();
        Matriz2 matrizTransiciones2 = multiplicacion(matrizTransiciones1, matrizTransiciones1);
        return matrizTransiciones2;
    }

    public static Matriz2 multiplicacion (Matriz2 A, Matriz2 B){
        // columnas de la matriz A
        int n= A.obtenerColumnas();
        // filas de la matriz A
        int m= A.obtenerRenglones();
        // filas de la matriz B
        int n2= B.obtenerColumnas();
        // columnas de la matriz B
        int o= B.obtenerRenglones();
        // nueva matriz
        Matriz2 C = new Matriz2(m,o);
        // se comprueba si las matrices se pueden multiplicar
        if (n==n2){
            for (int i=0; i<n;i++){

                for (int j=0; j<n2;j++){
                    //aqui se multiplica la matriz
                    double a=0;
                    for(int k=0;k<o;k++){
                        a=a+(double)A.obtenerValor(i,k)*(double)B.obtenerValor(k,j);
                    }
                    C.cambiar(i,j,a);
                }

            }
        }
        /**
         *  si no se cumple la condición se retorna una matriz vacía
         */
        return C;
    }

    private boolean validarEstadoAnimo(String estadoAnimo) {
        return estadoAnimo.equalsIgnoreCase("feliz") || estadoAnimo.equalsIgnoreCase("enojado")
                || estadoAnimo.equalsIgnoreCase("triste") || estadoAnimo.equalsIgnoreCase("aburrido");
    }

}
