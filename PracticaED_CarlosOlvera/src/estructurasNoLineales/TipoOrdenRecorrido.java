package estructurasNoLineales;

public enum TipoOrdenRecorrido {
    ASC("ASCENDENTE",1),DESC("DESCENDENTE",2);

    private String nombre;
    private int valor;

    private TipoOrdenRecorrido (String nombre, int valor){
        this.nombre = nombre;
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }

    public String getNombre() {
        return nombre;
    }


}
