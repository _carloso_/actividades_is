package herramientas.comunes;

/**
 * Este enumerado representa las posisiones de las columnas de una tabla.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public enum TipoColumna {
    IZQ("IZQUIERDA", 1), DER("DERECHA", 2);

    private String nombre;
    private int valor;

    /**
     * Constructor del enumerado TipoColumna.
     * @param nombre Nombre del enumerado.
     * @param valor Valor del enumerado.
     */
    private TipoColumna(String nombre, int valor) {
        this.nombre = nombre;
        this.valor = valor;
    }

    /**
     * Metodo que obtiene el nombre del TipoColumna.
     * @return Nombre del TipoColumna.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que obtiene el valor del TipoColumna.
     * @return Valor del TipoColumna.
     */
    public int getValor() {
        return valor;
    }

}
