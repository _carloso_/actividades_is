package estructurasnolineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;

/**
 * Esta calse representa una TDA de una matatriz de 3 dimensiones.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class Matriz3 {

    protected int renglones;
    protected int columnas;
    protected int profundidad;
    protected Object[][][] datos;

    public Matriz3(int renglones, int columnas, int profundidad) {
        this.renglones = renglones;
        this.columnas = columnas;
        this.profundidad = profundidad;
        datos = new Object[renglones][columnas][profundidad];
    }

    public Matriz3(int renglones, int columnas, int profundidad, Object valor) {
        this.renglones = renglones;
        this.columnas = columnas;
        this.profundidad = profundidad;
        datos = new Object[renglones][columnas][profundidad];
        rellenar(valor);
    }

    /**
     * Este método rellena la matriz con un valor por defecto qur se le pasa por parametro.
     *
     * @param valor Es el valor por defecto, con el que se rellenara la matriz.
     */
    private void rellenar(Object valor) {
        for (int fila = 0; fila < renglones; fila++) { // recorre fila por fila
            // Podemos asumir que en nuestro escenrio hey un solo renglon
            for (int columna = 0; columna < columnas; columna++) { // recorremos una por una las columnas de un solo renglon
                // Asumir que hay un solo renglon y un solo columna
                for (int prof = 0; prof < profundidad; prof++) { // recorrer una por una las celdas de profundidad
                    datos[fila][columna][prof] = valor;
                }
            }
        }
    }

    public Object obtener(int fila, int col, int prof) {
        if (enRango(fila, renglones) == true && enRango(col, columnas) == true && enRango(prof, profundidad) == true) {
            return datos[fila][col][prof];
        } else { // indices fuera de rango
            return null;
        }
    }

    public boolean cambiar(int fila, int col, int prof, Object valor) {
        if (enRango(fila, renglones) == true && enRango(col, columnas) == true && enRango(prof, profundidad) == true) {
            datos[fila][col][prof] = valor;
            return true;
        } else { // indices fuera de rango
            return false;
        }
    }

    private boolean enRango(int indice, int limiteDimension) {
        if (indice >= 0 && indice < limiteDimension) {
            return true;
        } else {
            return false;
        }
    }

    public int obtenerRenglones() {
        return renglones;
    }

    public int obtenerColumnas() {
        return columnas;
    }

    public int obtenerProfundidad() {
        return profundidad;
    }

    public void imprimirXColumnas() {
        // Extraer las rebanadas (columnas)
        for (int col = 0; col < columnas; col++) { // Una por una las rebanadas de columnas
            for (int fila = 0; fila < renglones; fila++) { // Extrae renglon por renglon
                for (int prof = 0; prof < profundidad; prof++) { // Extraemos las columnas de la matriz 2D, (Profundidad)
                    SalidaPorDefecto.consola(datos[fila][col][prof] + " ");
                }
                // Cuando termina todas las columnas, hacemos un salto de linea
                SalidaPorDefecto.consola("\n");
            }
            // Cuando termina todos los renglones, hacemos un salto de linea
            SalidaPorDefecto.consola("\n");
        }
    }

    // Agregar Un metodo que permita pasar de una matriz tridimensional a una matriz bidimensional (Pueden resultar varias)
    public ListaEstatica aMatriz2XColumnas() {
        ListaEstatica matrices2 = new ListaEstatica(columnas); // el numero de columnas (de matriz 3d) es el numero de matrices 2d
        // Primero recorremos todas las rebanadas que seran matrices bidimensionales
        for (int rebanada = 0; rebanada < columnas; rebanada++) {
            // Cada vez que yo obtenga una nueva rebanada, sera una nueva matriz2
            Matriz2 matriz2Rebanada = new Matriz2(renglones, profundidad); // reglones 3d, columnsa 3d
            // Hay que ir metiendo los valores dentro de la matriz2 nueva
            // estos valores se obtienen de la matriz 3d
            for (int fila = 0; fila < renglones; fila++) {
                // por cada fila recorremos cada columna(profundidad del 3d)
                for (int profCol = 0; profCol < profundidad; profCol++) {
                    matriz2Rebanada.cambiar(fila, profCol, datos[fila][rebanada][profCol]);
                }
            }
            // Cuando termino de agregar todos los valores a mi matriz2, jalado de la matriz3
            // guardamos la matriz2 en la lista de matrices2
            matrices2.agregar(matriz2Rebanada);
        }
        return matrices2;
    }

    public int obtenerRenglon(int fila){
        // obtener el renglon de la matriz 3d
        int renglon = 0;
        for (int columna = 0; columna < columnas; columna++) {
            for (int prof = 0; prof < profundidad; prof++) {
                renglon = (int) datos[fila][columna][prof];
            }
        }
        return renglon;
    }

    public Object obtenerColumna(int columna){
        // obtener el columna de la matriz 3d
        Object columnaMatriz = null;
        for (int fila = 0; fila < renglones; fila++) {
            for (int prof = 0; prof < profundidad; prof++) {
                columnaMatriz = datos[fila][columna][prof];
            }
        }
        return columnaMatriz;
    }

}