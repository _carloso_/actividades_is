package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.PilaDinamica;

public class PruebaPilaDinamica {
    public static void main(String[] args) {
        PilaDinamica pila = new PilaDinamica();

        pila.poner("G");
        pila.poner("H");
        pila.poner("Z");
        pila.poner("A");
        pila.poner("F");

        pila.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("Eliminando el último: " + pila.quitar() + "\n");
        SalidaPorDefecto.consola("Eliminando el último: " + pila.quitar() + "\n");

        pila.imprimir();
        SalidaPorDefecto.consola("\n");
    }
}

