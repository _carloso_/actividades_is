package estructurasnolineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import herramientas.comunes.TipoColumna;
import herramientas.comunes.TipoRenglon;

/**
 * Esta clase representa una matriz bidimensional, TDA de una matriz 2D.
 * @author Cristian Omar Alvarado Rodríguez.
 * @version 1.0
 */
public class Matriz2 {

    protected int renglones;
    protected int columnas;
    protected Object[][] datos;

    public Matriz2(int renglones, int columnas) {
        this.renglones = renglones;
        this.columnas = columnas;
        datos = new Object[renglones][columnas];
    }

    public Matriz2(int renglones, int columnas, Object valor) {
        this.renglones = renglones;
        this.columnas = columnas;
        datos = new Object[renglones][columnas];
        rellenar(valor);
    }

    /**
     * Este método rellena la matriz con un valor por defecto qur se le pasa por parametro.
     * @param valor Es el valor por defecto, con el que se rellenara la matriz.
     */
    private void rellenar(Object valor) {
        for (int fila = 0; fila < renglones; fila++) {
            for (int columna = 0; columna < columnas; columna++) {
                datos[fila][columna] = valor;
            }
        }
    }

    /**
     * Este método devuelve el valor de la matriz en la posición (fila, columna).
     * @param fila Es la fila de la matriz.
     * @param col Es la columna de la matriz.
     * @return Regresa el valor que se encuentra en la posición (fila, columna).
     */
    public Object obtenerValor(int fila, int col) {
        if (enRango(fila, renglones) && enRango(col, columnas)) {
            return datos[fila][col];
        } else { // indices fuera de rango
            return null;
        }
    }

    /**
     * Este método nos permite asignar un valor a una posición de la matriz.
     * @param fila Es la fila de la matriz donde se asignara el valor.
     * @param col Es la columna de la matriz donde se asignara el valor.
     * @param valor Es el valor que se asignara a la posición (fila, col).
     * @return Regresa <b>true</b> si se pudo asignar el valor, <b>false</b> en caso contrario.
     */
    public boolean cambiar(int fila, int col, Object valor) {
        if (enRango(fila, renglones) && enRango(col, columnas)) {
            datos[fila][col] = valor;
            return true;
        } else { // indices fuera de rango
            return false;
        }
    }

    /**
     * Este método verifica si una posición esta dentro de los límites de la matriz.
     * @param indice Es el indice que se desea verificar.
     * @param limiteDimension Es el límite de la dimension de la matriz.
     * @return Regresa <b>true</b> si el indice esta dentro de los límites de la matriz, <b>false</b> en caso contrario.
     */
    private boolean enRango(int indice, int limiteDimension) {
        if (indice >= 0 && indice < limiteDimension) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite sabaer la cantidad de renglones que tiene la matriz.
     * @return Regresa el numero de renglones que tiene la matriz.
     */
    public int obtenerRenglones() {
        return renglones;
    }

    /**
     * Este método nos permite saber la cantidad de columnas que tiene la matriz.
     * @return Regresa el numero de columnas que tiene la matriz.
     */
    public int obtenerColumnas() {
        return columnas;
    }

    /**
     * Este método imrpime la matriz columna por columna.
     */
    public void imprimirXColumnas() {
        for (int col = 0; col < columnas; col++) {
            for (int fila = 0; fila < renglones; fila++) {
                SalidaPorDefecto.consola(datos[fila][col] + " ");
            }
            SalidaPorDefecto.consola("\n");
        }
    }

    /**
     * Este método imrpime la matriz renglón por renglón.
     */
    public void imprimirXReglones() {
        for (int fila = 0; fila < renglones; fila++) {
            for (int col = 0; col < columnas; col++) {
                SalidaPorDefecto.consola(datos[fila][col] + " ");
            }
            SalidaPorDefecto.consola("\n");
        }
    }

    /**
     * Este método nos permite hacer la transpuesta de la matriz.
     */
    public void transpuesta() {
        Object[][] transpuesta = new Object[columnas][renglones];
        for (int fila = 0; fila < renglones; fila++) {
            for (int col = 0; col < columnas; col++) {
                transpuesta[col][fila] = datos[fila][col];
            }
        }
        datos = transpuesta;
    }

    /**
     * Este método nos permite obtener una copia de la matriz actual.
     * @return Regresa una copia de la matriz actual.
     */
    public Matriz2 clonar(){
        Matriz2 copiaMatriz = new Matriz2(renglones, columnas);
        for (int fila = 0; fila < renglones; fila++) {
            for (int col = 0; col < columnas; col++) {
                copiaMatriz.cambiar(fila, col, datos[fila][col]);
            }
        }
        return copiaMatriz;
    }

    /**
     * Este método nos permite saber si dos matrices son iguales.
     * @param matriz2 Es la matriz con la que se desea comparar.
     * @return Regresa <b>true</b> si las matrices son iguales, o <b>false</b> si son distintas.
     */
    public boolean esIgual(Matriz2 matriz2){
        boolean sonIguales = false;
        if (renglones == matriz2.obtenerRenglones() && columnas == matriz2.obtenerColumnas()) {
            sonIguales = true;
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    if (!datos[fila][col].equals(matriz2.obtenerValor(fila, col))) {
                        sonIguales = false;
                        break;
                    }
                }
            }
        }
        return sonIguales;
    }

    /**
     * Este método nos permite generar un vector columna de m x 1.
     * @param filas Es el número de filas que tendrá el vector columna.
     * @param valor Es el valor que tendrá cada uno de los elementos del vector.
     * @return Regresa <b>true</b> si se pudo generar el vector columna, o <b>false</b> si no se pudo.
     */
    public boolean vectorColumna(int filas, Object valor){
        boolean sePudoGenerar = false;
        if (filas > 0 && valor != null) {
            Matriz2 vectorColumna = new Matriz2(filas, 1);
            for (int fila = 0; fila < filas; fila++) {
                vectorColumna.cambiar(fila, 0, valor);
            }
            sePudoGenerar = true;
        }
        return sePudoGenerar;
    }

    /**
     * Este método nos permite generar un vector renglón de 1 x n.
     * @param columnas Es el número de columnas que tendrá el vector renglón.
     * @param valor Es el valor que tendrá cada uno de los elementos del vector.
     * @return Regresa <b>true</b> si se pudo generar el vector renglón.
     */
    public boolean vectorRenglon(int columnas, Object valor){
        boolean sePudoGenerar = false;
        if (columnas > 0 && valor != null) {
            Matriz2 vectorRenglon = new Matriz2(1, columnas);
            for (int col = 0; col < columnas; col++) {
                vectorRenglon.cambiar(0, col, valor);
            }
            sePudoGenerar = true;
        }
        return sePudoGenerar;
    }

    /**
     * Este método nos permite crear/redimensionar/substituir la tabla actual por una nueva matriz pasada por parámetro.
     * @param matriz2 Es la matriz que se desea utilizar para reemplazar a la matriz actual.
     * @return Regresa <b>true</b> si la matriz se ha reemplazado correctamente, o <b>false</b> si no se pudo reemplazar.
     */
    public boolean redefinir(Matriz2 matriz2){
        // Obtenemos el numero de renglones y columnas de la matriz2.
       int numReglonesMatriz2 = matriz2.obtenerRenglones();
       int numColumnasMatriz2 = matriz2.obtenerColumnas();
       // Redimensionamos la matriz actual con el numero de renglones y columnas de la matriz2.
       datos = new Object[numReglonesMatriz2][numColumnasMatriz2];
       // Asignamos el numero de renglones y columnas de la matriz2 a la matriz actual.
       renglones = numReglonesMatriz2;
       columnas = numColumnasMatriz2;
       // Recorremos la matriz actual y asignamos los valores de la matriz2 a la matriz actual.
       for (int fila = 0; fila < renglones; fila++) {
           for (int col = 0; col < columnas; col++) {
               datos[fila][col] = matriz2.obtenerValor(fila, col);
           }
       }
       return true;
    }

    /**
     * Este método agrega una fila a la matriz actual y la rellena con los valores de un arreglo pasado por parámetro.
     * @param arreglo Es el arreglo que se desea utilizar para rellenar la fila.
     * @return Regresa <b>true</b> si la fila se ha agregado correctamente, o <b>false</b> si no se pudo agregar.
     */
    public boolean agregarRenglon(ListaEstatica arreglo, int numFila){
        boolean agregada = false;
        // Obtenemos el numero de elementos del arreglo.
        int numElementosArreglo = arreglo.numeroElementos();
        // Verificamos que el numero de elementos del arreglo sea igual al numero de columnas de la matriz actual.
        if (numElementosArreglo == columnas) {
            // Verificamos que el numero de fila que se desea agregar sea menor al numero de renglones de la matriz actual.
            if (numFila < renglones) {
                // Recorremos la matriz actual (columnas) y asignamos los valores del arreglo a la matriz actual.
                for (int col = 0; col < columnas; col++) {
                    datos[numFila][col] = arreglo.obtener(col);
                }
                agregada = true;
            }
        }
        return agregada;
    }

    /**
     * Este método agrega una columna a la matriz actual y la rellena con los valores de un arreglo pasado por parámetro.
     * @param arreglo Es el arreglo que se desea utilizar para rellenar la columna.
     * @return Regresa <b>true</b> si la columna se ha agregado correctamente, o <b>false</b> si no se pudo agregar.
     */
    public boolean agregarColumna(ListaEstatica arreglo, int numColumna){
        boolean agregada = false;
        // Obtenemos el numero de elementos del arreglo.
        int numElementosArreglo = arreglo.numeroElementos();
        // Verificamos que el numero de elementos del arreglo sea igual al numero de renglones de la matriz actual.
        if (numElementosArreglo == renglones) {
            // Verificamos que el numero de columna que se desea agregar sea menor al numero de columnas de la matriz actual.
            if (numColumna < columnas) {
                // Recorremos la matriz actual (filas) y asignamos los valores del arreglo a la matriz actual.
                for (int fila = 0; fila < renglones; fila++) {
                    datos[fila][numColumna] = arreglo.obtener(fila);
                }
                agregada = true;
            }
        }
        return agregada;
    }

    /**
     * Este método agrega una matriz nueva a la matriz actual, se agregan los elementos por columnas.
     * @param matriz2 Es la matriz que se desea utilizar para agregar a la matriz actual.
     * @return Regresa <b>true</b> si la matriz se ha agregado correctamente, o <b>false</b> si no se pudo agregar.
     */
    public boolean agregarMatrizXColumna(Matriz2 matriz2){
        boolean agregada = false;
        int numReglonesMatriz2 = matriz2.obtenerRenglones();
        int numColumnasMatriz2 = matriz2.obtenerColumnas();
        if (numColumnasMatriz2 == renglones && numReglonesMatriz2 == columnas) {
            for (int col = 0; col < columnas; col++) {
                for (int fila = 0; fila < renglones; fila++) {
                    datos[fila][col] = matriz2.obtenerValor(fila, col);
                }
            }
            agregada = true;
        }
        return agregada;
    }

    /**
     * Este método agrega una matriz nueva a la matriz actual, se agregan los elementos de la matriz2 por filas.
     * @param matriz2 Es la matriz que se desea utilizar para agregar a la matriz actual.
     * @return Regresa <b>true</b> si la matriz se ha agregado correctamente, o <b>false</b> si no se pudo agregar.
     */
    public boolean agregarMatrizXRenglon(Matriz2 matriz2){
        // agregar los elementos en filas hacia abajo
        boolean agregada = false;
        int numReglonesMatriz2 = matriz2.obtenerRenglones();
        int numColumnasMatriz2 = matriz2.obtenerColumnas();
        if (numColumnasMatriz2 == renglones && numReglonesMatriz2 == columnas) {
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    datos[fila][col] = matriz2.obtenerValor(fila, col);
                }
            }
            agregada = true;
        }
        return agregada;
    }

    /**
     * Este método convierte la matriz 2D en una matriz 3D, se agregan los elementos de la matriz2 por filas.
     * @param matrices Es la lista que contine las matrices para la profundidad de la matriz 3D.
     * @return Regresa una matriz 3D.
     */
    public Matriz3 aMatriz3(ListaEstatica matrices){
        Matriz3 matriz3 = new Matriz3(renglones, columnas, matrices.numeroElementos());
        return matriz3;
    }

    /**
     * Este método nos permite saber cuantos elementos tiene la matriz actual.
     * @return Regresa el numero de elementos que tiene la matriz actual.
     */
    public int numElementos(){
        return renglones * columnas;
    }

    /**
     * Este método convierte la matriz actual acomodando cada columna una debajo de otra para formar un vector columna.
     * @return Regresa un vector columna con los valores de la matriz actual.
     */
    public Matriz2 aVectorColumna(){
        Matriz2 matriz2 = new Matriz2(numElementos(), 1);
        int contadadorFila = 0;
        for (int col = 0; col < columnas; col++) {
            for (int fila = 0; fila < renglones; fila++) {
                matriz2.datos[contadadorFila][0] = datos[fila][col];
                contadadorFila++;
            }
        }
        return matriz2;
    }

    /**
     * Este método convierte la matriz actual acomodando cada renglon uno enseguida de otro para formar un vector renglon.
     * @return Regresa un vector renglon con los valores de la matriz actual.
     */
    public Matriz2 aVectorRenglon(){
        Matriz2 matriz2 = new Matriz2(1, numElementos());
        int contadorCol = 0;
        for (int fila = 0; fila < renglones; fila++) {
            for (int col = 0; col < columnas; col++) {
                matriz2.datos[0][contadorCol] = datos[fila][col];
                contadorCol++;
            }
        }
        return matriz2;
    }

    /**
     * Este método nos permite obtener una columna de la matriz actual.
     * @param numColumna Es el numero de columna que se desea obtener.
     * @return Regresa una lista estática con los valores de la columna.
     */
    public ListaEstatica obtenerColumna(int numColumna){
        if (numColumna >= 0 && numColumna < columnas) {
            ListaEstatica columna = new ListaEstatica(renglones);
            for (int fila = 0; fila < renglones; fila++) {
                columna.agregar(datos[fila][numColumna]);
            }
            return columna;
        } else {
            return null;
        }
    }

    /**
     * Este método elimina una columna de la matriz actual de la izquierda de la derecha.
     * @param tipoCol Es el tipo de columna que se desea eliminar izquierda o derecha.
     * @return Regresa <b>true</b> si la columna se ha eliminado correctamente, o <b>false</b> si no se pudo eliminar.
     */
    public boolean quitarReglon(TipoColumna tipoCol){
        boolean quitar = false;
        if (tipoCol == TipoColumna.IZQ) {
            if (columnas > 0) {
                for (int fila = 0; fila < renglones; fila++) {
                    for (int col = 0; col < columnas - 1; col++) {
                        datos[fila][col] = datos[fila][col + 1];
                    }
                }
                columnas--;
                quitar = true;
            }
        } else {
            if (columnas > 0) {
                for (int fila = 0; fila < renglones; fila++) {
                    datos[fila][columnas - 1] = null;
                }
                columnas--;
                quitar = true;
            }
        }
        return quitar;
    }

    /**
     * Este método elimina una fila de la matriz actual.
     * @param tipoRenglon Es el tipo de renglon que se desea eliminar superior o inferior.
     * @return Regresa <b>true</b> si la fila se ha eliminado correctamente, o <b>false</b> si no se pudo eliminar.
     */
    public boolean eliminarRenglon(TipoRenglon tipoRenglon){
        boolean eliminado = false;
        if (tipoRenglon == TipoRenglon.SUPE) {
            if (renglones > 0) {
                for (int col = 0; col < columnas; col++) {
                    for (int fila = 0; fila < renglones - 1; fila++) {
                        datos[fila][col] = datos[fila + 1][col];
                    }
                }
                renglones--;
                eliminado = true;
            }
        } else {
            if (renglones > 0) {
                for (int col = 0; col < columnas; col++) {
                    datos[renglones - 1][col] = null;
                }
                renglones--;
                eliminado = true;
            }
        }
        return eliminado;
    }

    /**
     * Este método elimina una fila de la matriz actual en la posición especificada.
     * @param renglon Es el renglon que se desea eliminar.
     * @return Regresa <b>true</b> si la fila se ha eliminado correctamente, o <b>false</b> si no se pudo eliminar.
     */
    public boolean eliminarRenglon(int renglon){
        boolean eliminado = false;
        if (enRango(renglon, renglones)) {
            for (int col = 0; col < columnas; col++) {
                for (int fila = renglon; fila < renglones - 1; fila++) {
                    datos[fila][col] = datos[fila + 1][col];
                }
            }
            renglones--;
            eliminado = true;
        }
        return eliminado;
    }

    /**
     * Este método elimina una columna de la matriz actual en la posición especificada.
     * @param columna Es la columna que se desea eliminar.
     * @return Regresa <b>true</b> si la columna se ha eliminado correctamente, o <b>false</b> si no se pudo eliminar.
     */
    public boolean eliminarColumna(int columna){
        boolean eliminado = false;
        if (enRango(columna, columnas)) {
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = columna; col < columnas - 1; col++) {
                    datos[fila][col] = datos[fila][col + 1];
                }
            }
            columnas--;
            eliminado = true;
        }
        return eliminado;
    }

    /**
     * Este método genera una diagonal en la matriz actual.
     * @param elemento Es el elemento que se desea colocar en la diagonal de la matriz.
     */
    public void matrizDiagonal(Object elemento) {
        if (renglones==columnas) {
            for (int diag=0;diag<renglones;diag++) {
                datos[diag][diag]=elemento;
            }
        } else { //hacer lo que corresponda,

        }
    }

    // determinar si una matriz es simetrica
    public boolean esSimetrica() {
        boolean simetrica = true;
        if (renglones == columnas) {
            for (int fila = 0; fila < renglones; fila++) {
                for (int col = 0; col < columnas; col++) {
                    if (datos[fila][col] != datos[col][fila]) {
                        simetrica = false;
                    }
                }
            }
        } else {
            simetrica = false;
        }
        return simetrica;
    }
}