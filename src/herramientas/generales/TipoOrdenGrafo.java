package herramientas.generales;

/**
 * Esrta clase representa el tipo de orden que se va a utilizar para un grafo.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public enum TipoOrdenGrafo {
    INC("INCREMENTAL",1), DEC("DECREMENTAL",2);

    private String nombre;
    private int valor;

    private TipoOrdenGrafo(String nombre, int valor) {
        this.nombre = nombre;
        this.valor = valor;
    }

    public String getNombre() {
        return nombre;
    }

    public int getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return nombre + " " + valor;
    }
}
