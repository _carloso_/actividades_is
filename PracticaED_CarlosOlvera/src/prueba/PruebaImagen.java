package prueba;

import estructurasNoLineales.Imagen;

public class PruebaImagen {
    public static void main(String[] args) {
        Imagen imagen = new Imagen("src/Yoda Cholo.jpg");
        imagen.imagenGris();
        imagen.vueltaHorizontal();
        imagen.vueltaVertical();
        imagen.modificarBrillo(100);
    }
}
