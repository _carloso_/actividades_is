﻿using System.ComponentModel.DataAnnotations;

namespace ExamenParcial3.Models
{
    public class Entradas
    {
        [Key]
        public int Id 
        { 
            get; 
            set; 
        }
        [Required]
        [Display(Name = "Nombre del Platillo")]
        public string Nombre 
        {
            get;
            set;
        }


    }
}
