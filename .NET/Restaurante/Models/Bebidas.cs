using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restaurante.Models
{
    public class Bebidas 
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Nombre de la Bebida")]
        public string Nombre { get; set; }
        [Required]
        [Display(Name = "% de Alcohol")]
        public double Alcohol { get; set; }
        public double? Precio { get; set; }
        [Display(Name = "Imagen de la Bebida")]
        public string? UrlImagen { get; set; }
    }
}