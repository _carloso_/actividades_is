package proyectoFinal;

/**
 * Esta clase representa una observacion de una actividad de una persona segun su estado de animo.
 * @author Cristian Omar Alvarado Rodriguez y Carlos Eduardo Olvera Mayorga.
 * @version 1.0
 */
public class Observacion {

    protected String nombre;
    protected double probabilidad;

    public Observacion(String nombre, double probabilidad){
        this.nombre = nombre;
        this.probabilidad = probabilidad;
    }

    /**
     * Este método retorna el nombre de la observación.
     * @return Regresa el nombre de la observación.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Este método asigna un nombre a la observación.
     * @param nombre Es el nombre de la observación.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Este método retorna la probabilidad de emisión de la observación.
     * @return Regresa la probabilidad de emisión de la observación.
     */
    public double getProbabilidad() {
        return probabilidad;
    }

    /**
     * Este método asigna una probabilidad de emisión a la observación.
     * @param probabilidad Es la probabilidad de emisión de la observación.
     */
    public void setProbabilidad(double probabilidad) {
        this.probabilidad = probabilidad;
    }

    @Override
    public String toString() {
        return nombre + " " + probabilidad;
    }
}
