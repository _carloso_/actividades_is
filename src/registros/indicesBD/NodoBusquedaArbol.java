package registros.indicesBD;

/**
 * Esta clase representa un nodo con un valor de índice y una dirección de registro.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class NodoBusquedaArbol {

    protected int indice;
    protected long direccion;

    public NodoBusquedaArbol(int indice, long direccion) {
        this.indice = indice;
        this.direccion = direccion;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public long getDireccion() {
        return direccion;
    }

    public void setDireccion(long direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return indice + "";
    }
}
