package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import registros.escuela.Aplicaciones;
import registros.escuela.Computadoras;
import registros.escuela.Usuarios;

public class PruebaCC {
    public static void main(String[] args) {
        //APLICACIONES
        Aplicaciones app1 = new Aplicaciones("Chrome", "1.92", "Google", 256 , "600 MB");
        Aplicaciones app2 = new Aplicaciones("Dia","2.1","Alexander Larsson",10,"50 MB");
        Aplicaciones app3 = new Aplicaciones("Office","3.1", "Microsoft", 2," 16 GB");
        Aplicaciones app4 = new Aplicaciones("Intellij","2021.3","JetBrains",8,"2.5 GB");
        //COMPUTADORA 1 DATOS
        ListaDinamica aplicacionesCompu1 = new ListaDinamica();
        aplicacionesCompu1.agregar(app1);
        aplicacionesCompu1.agregar(app2);
        aplicacionesCompu1.agregar(app3);
        aplicacionesCompu1.agregar(app4);
        //usuario1
        ListaDinamica historialUsuario1 = new ListaDinamica();
        historialUsuario1.agregar(app1);
        historialUsuario1.agregar(app3);
        Usuarios usuario1 = new Usuarios("_carloso_","11/02","13:00","14:30",historialUsuario1);
        //usuario2
        ListaDinamica historialUsuario2 = new ListaDinamica();
        historialUsuario2.agregar(app1);
        Usuarios usuario2 = new Usuarios("nepepino","11/02","16:00","20:30",historialUsuario2);
        //usuario3
        ListaDinamica historialUsuario3 = new ListaDinamica();
        historialUsuario3.agregar(app3);
        Usuarios usuario3 = new Usuarios("1carloso1","12/02","22:00","00:30",historialUsuario3);
        //lista de usuarios
        ListaDinamica usuariosCompu1 = new ListaDinamica();
        usuariosCompu1.agregar(usuario1);
        usuariosCompu1.agregar(usuario2);
        usuariosCompu1.agregar(usuario3);
        //datos de la computadora
        Computadoras computadora1 = new Computadoras("123", 1, 64, "1TB", "ryzen 5", "Lenovo", aplicacionesCompu1, usuariosCompu1);

        //COMPUTADORA 2 DATOS
        ListaDinamica aplicacionesCompu2 = new ListaDinamica();
        aplicacionesCompu2.agregar(app1);
        aplicacionesCompu2.agregar(app3);
        //usuario4
        ListaDinamica historialUsuario4 = new ListaDinamica();
        historialUsuario4.agregar(app1);
        historialUsuario4.agregar(app3);
        Usuarios usuario4 = new Usuarios("JhonyBravo69","21/03","13:00","14:30",historialUsuario4);
        //usuario5
        ListaDinamica historialUsuario5 = new ListaDinamica();
        historialUsuario5.agregar(app3);
        Usuarios usuario5 = new Usuarios("battinson","21/03","16:00","20:30",historialUsuario5);
        //lista de usuarios
        ListaDinamica usuariosCompu2 = new ListaDinamica();
        usuariosCompu2.agregar(usuario4);
        usuariosCompu2.agregar(usuario5);
        //datos de la computadora
        Computadoras computadora2 = new Computadoras("456", 2, 128, "1TB", "ryzen 7", "HP", aplicacionesCompu2, usuariosCompu2);

        //COMPUTADORA 3 DATOS
        ListaDinamica aplicacionesCompu3 = new ListaDinamica();
        aplicacionesCompu3.agregar(app4);
        aplicacionesCompu3.agregar(app2);
        //usuario4
        ListaDinamica historialUsuario6 = new ListaDinamica();
        historialUsuario6.agregar(app4);
        historialUsuario6.agregar(app2);
        Usuarios usuario6 = new Usuarios("olverita","21/02","13:00","14:30",historialUsuario6);
        //usuario5
        ListaDinamica historialUsuario7 = new ListaDinamica();
        historialUsuario7.agregar(app2);
        Usuarios usuario7 = new Usuarios("batfleck","11/04","16:00","20:30",historialUsuario7);

        //lista de usuarios
        ListaDinamica usuariosCompu3 = new ListaDinamica();
        usuariosCompu3.agregar(usuario6);
        usuariosCompu3.agregar(usuario7);
        //datos de la computadora
        Computadoras computadora3 = new Computadoras("789", 1, 8, "256TB", "ryzen 3", "Huawei", aplicacionesCompu3, usuariosCompu3);


        //LISTA DE COMPUTADORAS
        ListaDinamica listaDeComputadoras = new ListaDinamica();
        listaDeComputadoras.agregar(computadora1.datos());
        listaDeComputadoras.agregar(computadora2.datos());
        listaDeComputadoras.agregar(computadora3.datos());

        //ACTIVIDADES
        SalidaPorDefecto.consola("-----IMPRIMIR COMPUTADORAS-----\n");
        listaDeComputadoras.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----INFO. DE UNA COMPUTADORA ESPECIFICA-----\n");
        SalidaPorDefecto.consola("Buscando la computadora: 123 ->" + listaDeComputadoras.buscar(computadora1.datos()));
        if(listaDeComputadoras.buscar(computadora1.datos()) != null){
            SalidaPorDefecto.consola(computadora1.getAplicaciones()+"\n");
        }
        SalidaPorDefecto.consola("Buscando la computadora: 456 ->" + listaDeComputadoras.buscar(computadora2.datos()));
        if(listaDeComputadoras.buscar(computadora2.datos()) != null){
            SalidaPorDefecto.consola(computadora2.getAplicaciones()+"\n");
        }
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----AGREGAR APLICACIONES A UNA COMPUTADORA-----\n");
        SalidaPorDefecto.consola("Buscando la computadora: 789 ->" + listaDeComputadoras.buscar(computadora3.datos()));
        if(listaDeComputadoras.buscar(computadora3.datos()) != null){
            Aplicaciones appNueva = new Aplicaciones("Edge", "2.91", "Microsoft", 256, "600 MB");
            aplicacionesCompu3.agregar(appNueva);
            SalidaPorDefecto.consola("Se agrego la aplicacion " + appNueva.datos()+"\n");
            SalidaPorDefecto.consola(aplicacionesCompu3+"\n");
        }
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----ELIMINAR APLICACIONES A UNA COMPUTADORA-----\n");
        SalidaPorDefecto.consola("Buscando la computadora: 123 ->" + listaDeComputadoras.buscar(computadora1.datos()));
        if(listaDeComputadoras.buscar(computadora1.datos()) != null){
            SalidaPorDefecto.consola("Buscando la app: Office ->" + aplicacionesCompu1.buscar(app3)+"\n"); //se busca la app antes de eliminarla
            if (aplicacionesCompu1.buscar(app3) != null){
                aplicacionesCompu1.eliminar(app3);
            }
            SalidaPorDefecto.consola(aplicacionesCompu1.toString());
        }
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----DAR DE ALTA UNA COMPUTADORA-----\n");
        ListaDinamica aplicacionesCompuNueva = new ListaDinamica();
        aplicacionesCompuNueva.agregar(app1);
        aplicacionesCompuNueva.agregar(app2);
        aplicacionesCompuNueva.agregar(app3);
        aplicacionesCompuNueva.agregar(app4);
        Aplicaciones app5 = new Aplicaciones("Warzone","2021.3","Blizzard",8,"128 GB");
        aplicacionesCompuNueva.agregar(app5);
        ListaDinamica usuariosCompuNueva = new ListaDinamica();
        Computadoras computadoraNueva = new Computadoras("019", 2, 32, "1 TB", "ryzen 7", "MSI", aplicacionesCompuNueva , usuariosCompuNueva);
        listaDeComputadoras.agregar(computadoraNueva.datos());
        SalidaPorDefecto.consola("Se dio de alta la computadora: "+listaDeComputadoras.buscar(computadoraNueva.datos()));
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----DAR DE BAJA UNA COMPUTADORA-----\n");
        SalidaPorDefecto.consola("Buscando la computadora: 456 ->" + listaDeComputadoras.buscar(computadora2.datos())); //se busca la compu antes de eliminarla
        if(listaDeComputadoras.buscar(computadora1.datos()) != null){
            listaDeComputadoras.eliminar(computadora2.datos());
            SalidaPorDefecto.consola(listaDeComputadoras.toString());
        }
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----COMPUTADORAS CON CHROME-----\n");
        SalidaPorDefecto.consola("Buscar Chrome en Computadora No. 123: " + aplicacionesCompu1.buscar(app1)+"\n");
        if (aplicacionesCompu1.buscar(app1) != null){
            SalidaPorDefecto.consola("La computadora tiene Chrome\n");
        }
        else{
            SalidaPorDefecto.consola("La computadora no tiene Chrome\n");
        }
        SalidaPorDefecto.consola("Buscar Chrome en Computadora No. 789: " + aplicacionesCompu3.buscar(app1)+"\n");
        if (aplicacionesCompu3.buscar(app1) != null){
            SalidaPorDefecto.consola("La computadora tiene Chrome\n");
        }
        else{
            SalidaPorDefecto.consola("La computadora no tiene Chrome\n");
        }
        SalidaPorDefecto.consola("Buscar Chrome en Computadora No. 019: " + aplicacionesCompuNueva.buscar(app1)+"\n");
        if (aplicacionesCompuNueva.buscar(app1) != null){
            SalidaPorDefecto.consola("La computadora tiene Chrome\n");
        }
        else{
            SalidaPorDefecto.consola("La computadora no tiene Chrome\n");
        }
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----COMPUTADORAS CON SUFICIENTE RAM PARA CORRER UNA APLICACION-----\n");
        SalidaPorDefecto.consola(app4.datos()+"\n");
        aplicacionesCompu1.buscar(app4);
        if (aplicacionesCompu1.buscar(app4) != null){
            SalidaPorDefecto.consola("La computadora "+computadora1.toString() +" tiene la app instalada\n");
            if(app4.getRamMinima() < computadora1.getRam()){
                SalidaPorDefecto.consola("La computadora tiene la memoria RAM suficiente para correr la aplicacion\n");
            }
            else{
                SalidaPorDefecto.consola("La computadora no tiene la suficiente memoria para correr la aplicacion\n");
            }
        }
        aplicacionesCompu3.buscar(app4);
        if (aplicacionesCompu3.buscar(app4) != null){
            SalidaPorDefecto.consola("La computadora "+computadora3.toString() +" tiene la app instalada\n");
            if(app4.getRamMinima() < computadora3.getRam()){
                SalidaPorDefecto.consola("La computadora tiene la memoria RAM suficiente para correr la aplicacion\n");
            }
            else{
                SalidaPorDefecto.consola("La computadora no tiene la suficiente memoria para correr la aplicacion\n");
            }
        }
        aplicacionesCompuNueva.buscar(app4);
        if (aplicacionesCompuNueva.buscar(app4) != null){
            SalidaPorDefecto.consola("La computadora "+computadoraNueva.toString() +" tiene la app instalada\n");
            if(app4.getRamMinima() < computadoraNueva.getRam()){
                SalidaPorDefecto.consola("La computadora tiene la memoria RAM suficiente para correr la aplicacion\n");
            }
            else{
                SalidaPorDefecto.consola("La computadora no tiene la suficiente memoria para correr la aplicacion\n");
            }
        }
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----USUARIOS QUE HAN USADO UNA COMPUTADORA ESPECIFICA-----\n");
        SalidaPorDefecto.consola("Computadora " +computadora3.toString()+"\n");
        SalidaPorDefecto.consola("Los usuarios que han usado la computadora son: \n" +computadora3.getUsuarios()+"\n");
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----INFORMACION DEL USUARIO-----\n");
        SalidaPorDefecto.consola(usuario6.toString()+"\n");
        if (usuariosCompu1.buscar(usuario6) != null){
            SalidaPorDefecto.consola("EL usuario " + usuario6.getUsuario() + " uso la computadora el dia " + usuario6.getDia() + "  de " +
                    usuario6.getHoraInicio() + " a " + usuario6.getHoraFin() + " en el centro de computo numero " + computadora1.getNoCC()+"\n");
        }
        if (usuariosCompu3.buscar(usuario6) != null){
            SalidaPorDefecto.consola("EL usuario " + usuario6.getUsuario() + " uso la computadora el dia " + usuario6.getDia() + "  de " +
                    usuario6.getHoraInicio() + " a " + usuario6.getHoraFin() + " en el centro de computo numero " + computadora3.getNoCC()+"\n");
        }
        if (usuariosCompuNueva.buscar(usuario6) != null){
            SalidaPorDefecto.consola("EL usuario " + usuario6.getUsuario() + " uso la computadora el dia " + usuario6.getDia() + "  de " +
                    usuario6.getHoraInicio() + " a " + usuario6.getHoraFin() + " en el centro de computo numero " + computadoraNueva.getNoCC()+"\n");
        }
    }
}



