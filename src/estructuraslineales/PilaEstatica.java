package estructurasLineales;

/**
 * Este clase representa un TDA de una pila estática.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class PilaEstatica implements LoteAlmacenamiento{

    protected ListaEstatica pila;

    public PilaEstatica(int maximo) {
        pila = new ListaEstatica(maximo);
    }

    @Override
    public boolean vacio() {
        return pila.vacia();
    }

    @Override
    public boolean lleno() {
        return pila.llena();
    }

    public int numeroElementos(){
        return pila.numeroElementos();
    }

    @Override
    public boolean poner(Object valor) {
        int valorRetorno = pila.agregar(valor);
        if (valorRetorno >= 0) {
            return true;
        } else { // Retorna -1
            return false;
        }
    }

    @Override
    public Object quitar() {
        return pila.eliminar();
    }

    @Override
    public void imprimir() {
        pila.imprimir();
    }

    @Override
    public Object verUltimo() {
        return pila.verUltimo();
    }
}