package registros.ProyectoFinal;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;

/**
 * Clase donde re sealizaran las pruebas de el metodo de ordenamiento Radix
 */
public class PruebaRadix {
    public static void main(String[] args) {

        ListaEstatica lista = new ListaEstatica(6);
        lista.agregar("53");
        lista.agregar("3");
        lista.agregar("542");
        lista.agregar("748");
        lista.agregar("14");
        lista.agregar("214");

        SalidaPorDefecto.consola("El arreglo desordenado es el siguiente:\n");
        lista.imprimirOI();

        SalidaPorDefecto.consola("El arreglo ordenado con el metodo Radix es:\n");
        RadixSort.radixSort(lista);
        lista.imprimirOI();
    }
}
