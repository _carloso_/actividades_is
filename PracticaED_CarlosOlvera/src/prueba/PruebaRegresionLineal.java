package prueba;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import herramientas.matematicas.RegresionLineal;

public class PruebaRegresionLineal {
    public static void main(String[] args) {
        RegresionLineal calculo = new RegresionLineal("src/x.txt", "src/y.txt");
        SalidaPorDefecto.consola("-Actividad 2-\n");
        SalidaPorDefecto.consola("ECUACION FORMULA REGRESION LINEAL CON LOS VALORES DE LAS TABLAS X y Y\n");
        calculo.ecuacion();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Ingrese el numero de habitaciones de la casa que desea cotizar: ");
        double habitaciones = EntradaPorDefecto.consolaDouble();
        SalidaPorDefecto.consola("La aproximacion de costo para la casa que desea es $" + calculo.aproximacion(habitaciones)+"\n");

    }
}
