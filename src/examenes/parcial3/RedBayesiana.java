package examenes.parcial3;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasnolineales.Matriz2;

/**
 * Esta clase representa una red bayesiana de síntomas y enfermedades.
 * @author Cristian Omar Alvarado Rodriguez y Carlos Eduardo Olvera Mayorga.
 * @version 1.0
 */
public class RedBayesiana {
    protected GrafoRedBayesiana redBayesiana;

    public RedBayesiana(int cantidadNodos){
        redBayesiana = new GrafoRedBayesiana(cantidadNodos);
    }

    /**
     * Este método agrega un nuevo nodo al grafo de la red bayesiana.
     * @param nodo Es el nuevo nodo que se agregará al grafo.
     * @return Regresa <b>true</b> si el nodo se agregó correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregarNodo(NodoRedBayesiana nodo) {
        return redBayesiana.agregarNodo(nodo);
    }

    /**
     * Este método agrega una nueva arista al grafo de la red bayesiana.
     * @param origen Es el nodo origen de la arista.
     * @param destino Es el nodo destino de la arista.
     * @return Regresa <b>true</b> si la arista se agregó correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregarArista(Object origen, Object destino) {
        return redBayesiana.agregarArista(origen, destino);
    }

    /**
     * Este método muestra un menú de opciones para el usuario.
     */
    public void menuOpciones() {
        int opcion = 0;
        String respuesta = "";
        do {
            SalidaPorDefecto.consola("------ Menu de opciones ------\n");
            SalidaPorDefecto.consola("1. Probabilidades Condicionales.\n");
            SalidaPorDefecto.consola("2. Probabilidades Conjuntas.\n");
            SalidaPorDefecto.consola("3. Salir.\n");
            SalidaPorDefecto.consola(">Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    menuProbCondicionales();
                    break;
                case 2:
                    menuProbaConjuntas();
                    break;
                case 3:
                    SalidaPorDefecto.consola("Saliendo...");
                    break;
                default:
                    SalidaPorDefecto.consola("Opción inválida.\n");
                    break;
            }
        } while (opcion != 3);
    }

    /**
     * Este método muestra las variables de la red bayesiana para calcular las probabilidades conjuntas.
     */
    private void menuProbaConjuntas() {
        SalidaPorDefecto.consola("--- Probabilidades Conjuntas (Variables) ---\n");
        SalidaPorDefecto.consola("(V) Exposicion al virus AH1N1.\n");
        SalidaPorDefecto.consola("(F) Fiebre.\n");
        SalidaPorDefecto.consola("(E) Estornudos.\n");
        SalidaPorDefecto.consola("(R) Resfriado.\n");
        SalidaPorDefecto.consola("(I) Influenza.\n");
        SalidaPorDefecto.consola("(T) Tos.\n");
        SalidaPorDefecto.consola("(D) Dolor muscular.\n");
        SalidaPorDefecto.consola(">Ingresa las variables separadas por comas o espacios: ");
        String respuesta = EntradaPorDefecto.consolaCadenas();
        double proba = probabilidadConjunta(respuesta);
        respuesta = respuesta.toUpperCase();
        SalidaPorDefecto.consola("**** Probabilidad conjunta de P(" + respuesta + ") = "
                + proba + " ****\n");
    }

    /**
     * Este método muestra un menu de opciones para calcular las probabilidades condicionales de la red bayesiana.
     */
    private void menuProbCondicionales() {
        int opcion = 0;
        String respuesta = "";
        do {
            SalidaPorDefecto.consola("----- Menu de probabilidades condicionales -----\n");
            SalidaPorDefecto.consola("1. Probabilidad De Exposición al Virus AH1N1 P(V).\n");
            SalidaPorDefecto.consola("2. Probabilidad De Fiebre P(F|V).\n");
            SalidaPorDefecto.consola("3. Probabilidad De Estornudos P(E|V).\n");
            SalidaPorDefecto.consola("4. Probabilidad De Resfriado P(R|F,E).\n");
            SalidaPorDefecto.consola("5. Probabilidad De Influenza P(I|F,E).\n");
            SalidaPorDefecto.consola("6. Probabilidad De Tos P(T|R).\n");
            SalidaPorDefecto.consola("7. Probabilidad De Dolor Muscular P(D|I).\n");
            SalidaPorDefecto.consola("8. Regresar...\n");
            SalidaPorDefecto.consola(">Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    subMenuExposicionVirus();
                    break;
                case 2:
                    subMenuFiebre();
                    break;
                case 3:
                    subMenuEstornudos();
                    break;
                case 4:
                    subMenuResfriado();
                    break;
                case 5:
                    subMenuInfluenza();
                    break;
                case 6:
                    subMenuTos();
                    break;
                case 7:
                    subMenuDolorMuscular();
                    break;
                default:
                    SalidaPorDefecto.consola("Opción inválida.\n");
                    break;
            }

        } while (opcion != 8);
    }

    /**
     * Este método muestra un submenu de opciones para calcular la probabilidad de exposición al virus AH1N1 P(V).
     */
    private void subMenuExposicionVirus() {
        int opcion = 0;
        String respuesta = "";
        double probabilidad = 0;
        do {
            SalidaPorDefecto.consola("1. Probabilidad de 'si' exposición al virus AH1N1.\n");
            SalidaPorDefecto.consola("2. Probabilidad de 'no' exposición al virus AH1N1.\n");
            SalidaPorDefecto.consola("3. Regresar...\n");
            SalidaPorDefecto.consola(">Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    probabilidad = probabilidadExposicionVirus("si");
                    SalidaPorDefecto.consola("*** La probabilidad de que si te hayas expuesto al virus es: "
                            + probabilidad + " % ***\n");
                    break;
                case 2:
                    probabilidad = probabilidadExposicionVirus("no");
                    SalidaPorDefecto.consola("*** La probabilidad de que no te hayas expuesto al virus es: "
                            + probabilidad + " % ***\n");
                    break;
                default:
                    SalidaPorDefecto.consola("Opción inválida.\n");
                    break;
            }
        } while (opcion != 3);
    }

    /**
     * Este método muestra un submenu de opciones para calcular la probabilidad de dolor muscular P(D|I).
     */
    private void subMenuDolorMuscular() {
        int opcion = 0;
        String respuesta = "";
        do {
            SalidaPorDefecto.consola("1. Si Dolor Muscular.\n");
            SalidaPorDefecto.consola("2. No Dolor Muscular.\n");
            SalidaPorDefecto.consola("3. Regresar...\n");
            SalidaPorDefecto.consola(">Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    SalidaPorDefecto.consola("¿Tienes Influenza? (Si/No): ");
                    respuesta = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta)) {
                        double probabilidad = probabilidadDolorMuscular(respuesta,opcion);
                        SalidaPorDefecto.consola("La probabilidad de que si tengas dolor muscular es de: " +
                                probabilidad + " %\n");
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                case 2:
                    SalidaPorDefecto.consola("¿Tienes Influenza? (Si/No): ");
                    respuesta = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta)) {
                        double probabilidad = probabilidadDolorMuscular(respuesta,opcion);
                        SalidaPorDefecto.consola("La probabilidad de que no tengas dolor muscular es de: " +
                                probabilidad + " %\n");
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                default:
                    SalidaPorDefecto.consola("Opción inválida.\n");
                    break;
            }
        } while (opcion != 3);
    }

    /**
     * Este método muestra un submenu de opciones para calcular la probabilidad de tos P(T|R).
     */
    private void subMenuTos() {
        int opcion = 0;
        String respuesta = "";
        do {
            SalidaPorDefecto.consola("1. Si Tos.\n");
            SalidaPorDefecto.consola("2. No Tos.\n");
            SalidaPorDefecto.consola("3. Regresar...\n");
            SalidaPorDefecto.consola(">Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    SalidaPorDefecto.consola("¿Tienes Resfriado? (Si/No): ");
                    respuesta = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta)) {
                        double probabilidad = probabilidadTos(respuesta,opcion);
                        SalidaPorDefecto.consola("La probabilidad de que si tengas tos es de: " +
                                probabilidad + " %\n");
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                case 2:
                    SalidaPorDefecto.consola("¿Tienes Resfriado? (Si/No): ");
                    respuesta = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta)) {
                        double probabilidad = probabilidadTos(respuesta,opcion);
                        SalidaPorDefecto.consola("La probabilidad de que no tengas tos es de: " +
                                probabilidad + " %\n");
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                default:
                    SalidaPorDefecto.consola("Opción inválida.\n");
                    break;
            }
        } while (opcion != 3);
    }

    /**
     * Este método muestra un submenu de opciones para calcular la probabilidad de influenza P(I|F,E).
     */
    private void subMenuInfluenza() {
        int opcion = 0;
        String respuesta1 = "";
        String respuesta2 = "";
        do {
            SalidaPorDefecto.consola("1. Si Influenza\n");
            SalidaPorDefecto.consola("2. No Influenza.\n");
            SalidaPorDefecto.consola("3. Regresar...\n");
            SalidaPorDefecto.consola(">Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    SalidaPorDefecto.consola("¿Tienes Fiebre? (Si/No): ");
                    respuesta1 = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta1)) {
                        SalidaPorDefecto.consola("¿Tienes Estornudos? (Si/No): ");
                        respuesta2 = EntradaPorDefecto.consolaCadenas();
                        if (respuestaValida(respuesta2)) {
                            double probabilidad = probabilidadInfluenza(respuesta1, respuesta2, opcion);
                            SalidaPorDefecto.consola("La probabilidad de que si tengas influenza es de: "
                                    + probabilidad + " %\n");
                        } else {
                            SalidaPorDefecto.consola("Respuesta inválida.\n");
                        }
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                case 2:
                    SalidaPorDefecto.consola("¿Tienes Fiebre? (Si/No): ");
                    respuesta1 = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta1)) {
                        SalidaPorDefecto.consola("¿Tienes Estornudos? (Si/No): ");
                        respuesta2 = EntradaPorDefecto.consolaCadenas();
                        if (respuestaValida(respuesta2)) {
                            double probabilidad = probabilidadInfluenza(respuesta1, respuesta2, opcion);
                            SalidaPorDefecto.consola("La probabilidad de que no tengas influenza es de: "
                                    + probabilidad + " %\n");
                        } else {
                            SalidaPorDefecto.consola("Respuesta inválida.\n");
                        }
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                default:
                    SalidaPorDefecto.consola("Opción inválida.\n");
                    break;
            }
        } while (opcion != 3);
    }

    /**
     * Este método muestra un submenu de opciones para calcular la probabilidad de resfriado P(R|F,E).
     */
    private void subMenuResfriado() {
        int opcion = 0;
        String respuesta1 = "";
        String respuesta2 = "";
        do {
            SalidaPorDefecto.consola("1. Si Resfriado.\n");
            SalidaPorDefecto.consola("2. No Resfriado.\n");
            SalidaPorDefecto.consola("3. Regresar...\n");
            SalidaPorDefecto.consola(">Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    SalidaPorDefecto.consola("¿Tienes Fiebre? (Si/No): ");
                    respuesta1 = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta1)) {
                        SalidaPorDefecto.consola("¿Tienes Estornudos? (Si/No): ");
                        respuesta2 = EntradaPorDefecto.consolaCadenas();
                        if (respuestaValida(respuesta2)) {
                            double probabilidad = probabilidadResfriado(respuesta1, respuesta2, opcion);
                            SalidaPorDefecto.consola("La probabilidad de que si tengas resfriado es de: "
                                    + probabilidad + " %\n");
                        } else {
                            SalidaPorDefecto.consola("Respuesta inválida.\n");
                        }
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                case 2:
                    SalidaPorDefecto.consola("¿Tienes Fiebre? (Si/No): ");
                    respuesta1 = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta1)) {
                        SalidaPorDefecto.consola("¿Tienes Estornudos? (Si/No): ");
                        respuesta2 = EntradaPorDefecto.consolaCadenas();
                        if (respuestaValida(respuesta2)) {
                            double probabilidad = probabilidadResfriado(respuesta1, respuesta2, opcion);
                            SalidaPorDefecto.consola("La probabilidad de que no tengas resfriado es de: "
                                    + probabilidad + " %\n");
                        } else {
                            SalidaPorDefecto.consola("Respuesta inválida.\n");
                        }
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                default:
                    SalidaPorDefecto.consola("Opción inválida.\n");
                    break;
            }
        } while (opcion != 3);
    }

    /**
     * Este método muestra un submenu de opciones para calcular la probabilidad de estornudos P(E|V).
     */
    private void subMenuEstornudos() {
        int opcion = 0;
        String respuesta = "";
        do {
            SalidaPorDefecto.consola("1. Si Estornudos.\n");
            SalidaPorDefecto.consola("2. No Estornudos.\n");
            SalidaPorDefecto.consola("3. Regresar...\n");
            SalidaPorDefecto.consola(">Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    SalidaPorDefecto.consola("¿Te expusiste al virus AH1N1? (Si/No): ");
                    respuesta = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta)) {
                        double probabilidad = probabilidadEstornudos(respuesta, opcion);
                        SalidaPorDefecto.consola("La probabilidad de que si tengas estornudos es de: "
                                + probabilidad + " %\n");
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                case 2:
                    SalidaPorDefecto.consola("¿Te expusiste al virus AH1N1? (Si/No): ");
                    respuesta = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta)) {
                        double probabilidad = probabilidadEstornudos(respuesta, opcion);
                        SalidaPorDefecto.consola("La probabilidad de que no tengas estornudos es de: "
                                + probabilidad + " %\n");
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                default:
                    SalidaPorDefecto.consola("Opción inválida.\n");
                    break;
            }
        } while (opcion != 3);
    }

    /**
     * Este método muestra un submenu de opciones para calcular la probabilidad de fiebre P(F|V).
     */
    private void subMenuFiebre() {
        int opcion = 0;
        String respuesta = "";
        do {
            SalidaPorDefecto.consola("1. Si fiebre.\n");
            SalidaPorDefecto.consola("2. No fiebre.\n");
            SalidaPorDefecto.consola("3. Regresar...\n");
            SalidaPorDefecto.consola(">Ingrese una opción: ");
            opcion = EntradaPorDefecto.consolaInteger();
            switch (opcion) {
                case 1:
                    SalidaPorDefecto.consola("¿Te expusiste al virus AH1N1? (Si/No): ");
                    respuesta = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta)) {
                        double probabilidad = probabilidadFiebre(respuesta, opcion);
                        SalidaPorDefecto.consola("La probabilidad de que si tengas fiebre es de: "
                                + probabilidad + " %\n");
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                case 2:
                    SalidaPorDefecto.consola("¿Te expusiste al virus AH1N1? (Si/No): ");
                    respuesta = EntradaPorDefecto.consolaCadenas();
                    if (respuestaValida(respuesta)) {
                        double probabilidad = probabilidadFiebre(respuesta, opcion);
                        SalidaPorDefecto.consola("La probabilidad de que no tengas fiebre es de: "
                                + probabilidad + " %\n");
                    } else {
                        SalidaPorDefecto.consola("Respuesta inválida.\n");
                    }
                    break;
                default:
                    SalidaPorDefecto.consola("Opción inválida.\n");
                    break;
            }
        } while (opcion != 3);
    }

    /**
     * Este método nos permite obtener la probabilidad de exposición al virus AH1N1.
     * @param respuesta Es la respuesta de la pregunta de si se expuso al virus AH1N1 o no.
     * @return Regresa la probabilidad de exposición al virus AH1N1.
     */
    private double probabilidadExposicionVirus(String respuesta) {
        NodoRedBayesiana nodoComida = redBayesiana.obtenerNodo("virus");
        if (nodoComida != null) {
            Matriz2 tablaProbabilidad = nodoComida.getTablaProbabilidad();
            if (tablaProbabilidad != null) {
                respuesta = respuesta.toLowerCase();
                switch (respuesta) {
                    case "si":
                        return (double) tablaProbabilidad.obtenerValor(0, 0);
                    case "no":
                        return (double) tablaProbabilidad.obtenerValor(0, 1);
                    default:
                        return 0.0;
                }
            } else {
                return 0.0;
            }
        } else {
            return 0.0;
        }
    }

    /**
     * Este método nos permite obtener la probabilidad de tener fiebre P(F|V).
     * @param respuesta Es la respuesta de la pregunta de si exposición al virus AH1N1 o no.
     * @param siNoFiebre Opción para saber la probabilidad de sí tener fiebre o no.
     * @return Regresa la probabilidad de fiebre P(F|V).
     */
    private double probabilidadFiebre(String respuesta, int siNoFiebre) {
        NodoRedBayesiana nodoFiebre = redBayesiana.obtenerNodo("fiebre");
        if (nodoFiebre != null) {
            Matriz2 tablaProbabilidad = nodoFiebre.getTablaProbabilidad();
            if (tablaProbabilidad != null) {
                respuesta = respuesta.toLowerCase();
                switch (respuesta) {
                    case "si":
                        if (siNoFiebre == 1) {
                            return (double) tablaProbabilidad.obtenerValor(0, 0);
                        } else {
                            return (double) tablaProbabilidad.obtenerValor(1, 0);
                        }
                    case "no":
                        if (siNoFiebre == 1) {
                            return (double) tablaProbabilidad.obtenerValor(0, 1);
                        } else {
                            return (double) tablaProbabilidad.obtenerValor(1, 1);
                        }
                    default:
                        return 0.0;
                }
            } else {
                return 0.0;
            }
        } else {
            return 0.0;
        }
    }

    /**
     * Este método nos permite obtener la probabilidad de tener estornudos P(E|V).
     * @param respuesta Es la respuesta de la pregunta de si exposición al virus AH1N1 o no.
     * @param siNoEstornudos Opción para saber la probabilidad de sí tener estornudos o no.
     * @return Regresa la probabilidad de estornudos P(E|V).
     */
    private double probabilidadEstornudos(String respuesta, int siNoEstornudos) {
        NodoRedBayesiana nodoEstornudos = redBayesiana.obtenerNodo("estornudos");
        if (nodoEstornudos != null) {
            Matriz2 tablaProbabilidad = nodoEstornudos.getTablaProbabilidad();
            if (tablaProbabilidad != null) {
                respuesta = respuesta.toLowerCase();
                switch (respuesta) {
                    case "si":
                        if (siNoEstornudos == 1) {
                            return (double) tablaProbabilidad.obtenerValor(0, 0);
                        } else {
                            return (double) tablaProbabilidad.obtenerValor(1, 0);
                        }
                    case "no":
                        if (siNoEstornudos == 1) {
                            return (double) tablaProbabilidad.obtenerValor(0, 1);
                        } else {
                            return (double) tablaProbabilidad.obtenerValor(1, 1);
                        }
                    default:
                        return 0.0;
                }
            } else {
                return 0.0;
            }
        } else {
            return 0.0;
        }
    }

    /**
     * Este método nos permite obtener la probabilidad de tener resfriado P(R|F,E).
     * @param resFiebre Es la respuesta de la pregunta de si tener fiebre o no.
     * @param resEstornudos Es la respuesta de la pregunta de si tener estornudos o no.
     * @param siNoResfriado Opción para saber la probabilidad de sí tener resfriado o no.
     * @return Regresa la probabilidad de resfriado P(R|F,E).
     */
    private double probabilidadResfriado(String resFiebre, String resEstornudos, int siNoResfriado) {
        NodoRedBayesiana nodoResfriado = redBayesiana.obtenerNodo("resfriado");
        if (nodoResfriado != null) {
            Matriz2 tablaProbabilidad = nodoResfriado.getTablaProbabilidad();
            if (tablaProbabilidad != null) {
                resFiebre = resFiebre.toLowerCase();
                resEstornudos = resEstornudos.toLowerCase();
                switch (resFiebre) {
                    case "si":
                        switch (resEstornudos) {
                            case "si": // si, si (si resfriado)
                                if (siNoResfriado == 1) {
                                    return (double) tablaProbabilidad.obtenerValor(0, 0);
                                } else { // si, si (no resfriado)
                                    return (double) tablaProbabilidad.obtenerValor(1, 0);
                                }
                            case "no": // si, no (si resfriado)
                                if (siNoResfriado == 1) {
                                    return (double) tablaProbabilidad.obtenerValor(0, 1);
                                } else { // si, no (no resfriado)
                                    return (double) tablaProbabilidad.obtenerValor(1, 1);
                                }
                            default:
                                return 0.0;
                        }
                    case "no":
                        switch (resEstornudos) {
                            case "si": // no, si (si resfriado)
                                if (siNoResfriado == 1) {
                                    return (double) tablaProbabilidad.obtenerValor(0, 2);
                                } else { // no, si (no resfriado)
                                    return (double) tablaProbabilidad.obtenerValor(1, 2);
                                }
                            case "no": // no, no (si resfriado)
                                if (siNoResfriado == 1) {
                                    return (double) tablaProbabilidad.obtenerValor(0, 3);
                                } else { // no, no (no resfriado)
                                    return (double) tablaProbabilidad.obtenerValor(1, 3);
                                }
                            default:
                                return 0.0;
                        }
                    default:
                        return 0.0;
                }
            } else {
                return 0.0;
            }
        } else {
            return 0.0;
        }
    }

    /**
     * Este método nos permite obtener la probabilidad de tener influenza P(I|F,E).
     * @param resFiebre Es la respuesta de la pregunta de si tener fiebre o no.
     * @param resEstornudos Es la respuesta de la pregunta de si tener estornudos o no.
     * @param siNoInfluenza Opción para saber la probabilidad de sí tener influenza o no.
     * @return Regresa la probabilidad de influenza P(I|F,E).
     */
    private double probabilidadInfluenza(String resFiebre, String resEstornudos, int siNoInfluenza) {
        NodoRedBayesiana nodoInfluenza = redBayesiana.obtenerNodo("influenza");
        if (nodoInfluenza != null) {
            Matriz2 tablaProbabilidad = nodoInfluenza.getTablaProbabilidad();
            if (tablaProbabilidad != null) {
                resFiebre = resFiebre.toLowerCase();
                resEstornudos = resEstornudos.toLowerCase();
                switch (resFiebre) {
                    case "si":
                        switch (resEstornudos) {
                            case "si": // si, si (si influenza)
                                if (siNoInfluenza == 1) {
                                    return (double) tablaProbabilidad.obtenerValor(0, 0);
                                } else { // si, si (no influenza)
                                    return (double) tablaProbabilidad.obtenerValor(1, 0);
                                }
                            case "no": // si, no (si influenza)
                                if (siNoInfluenza == 1) {
                                    return (double) tablaProbabilidad.obtenerValor(0, 1);
                                } else { // si, no (no influenza)
                                    return (double) tablaProbabilidad.obtenerValor(1, 1);
                                }
                            default:
                                return 0.0;
                        }
                    case "no":
                        switch (resEstornudos) {
                            case "si": // no, si (si influenza)
                                if (siNoInfluenza == 1) {
                                    return (double) tablaProbabilidad.obtenerValor(0, 2);
                                } else { // no, si (no influenza)
                                    return (double) tablaProbabilidad.obtenerValor(1, 2);
                                }
                            case "no": // no, no (si influenza)
                                if (siNoInfluenza == 1) {
                                    return (double) tablaProbabilidad.obtenerValor(0, 3);
                                } else { // no, no (no influenza)
                                    return (double) tablaProbabilidad.obtenerValor(1, 3);
                                }
                            default:
                                return 0.0;
                        }
                    default:
                        return 0.0;
                }
            } else {
                return 0.0;
            }
        } else {
            return 0.0;
        }
    }

    /**
     * Este método nos permite obtener la probabilidad de tener tos P(T|R).
     * @param resResfriado Es la respuesta de la pregunta de si tener resfriado o no.
     * @param siNoTos Opción para saber la probabilidad de sí tener tos o no.
     * @return Regresa la probabilidad de tos P(T|R).
     */
    private double probabilidadTos(String resResfriado, int siNoTos) {
        NodoRedBayesiana nodoTos = redBayesiana.obtenerNodo("tos");
        if (nodoTos != null) {
            Matriz2 tablaProbabilidad = nodoTos.getTablaProbabilidad();
            if (tablaProbabilidad != null) {
                resResfriado = resResfriado.toLowerCase();
                switch (resResfriado) {
                    case "si":
                        if (siNoTos == 1) { // si, si (si tos)
                            return (double) tablaProbabilidad.obtenerValor(0, 0);
                        } else { // si, si (no tos)
                            return (double) tablaProbabilidad.obtenerValor(1, 0);
                        }
                    case "no":
                        if (siNoTos == 1) { // no, si (si tos)
                            return (double) tablaProbabilidad.obtenerValor(0, 1);
                        } else { // no, si (no tos)
                            return (double) tablaProbabilidad.obtenerValor(1, 1);
                        }
                    default:
                        return 0.0;
                }
            } else {
                return 0.0;
            }
        } else {
            return 0.0;
        }
    }

    /**
     * Este método nos permite obtener la probabilidad de tener dolor muscular P(D|I).
     * @param resInfluenza Es la respuesta de la pregunta de si tener influenza o no.
     * @param siNoDolorMuscular Opción para saber la probabilidad de sí tener dolor muscular o no.
     * @return Regresa la probabilidad de dolor muscular P(D|I).
     */
    private double probabilidadDolorMuscular(String resInfluenza, int siNoDolorMuscular) {
        NodoRedBayesiana nodoDolorMuscular = redBayesiana.obtenerNodo("dolor muscular");
        if (nodoDolorMuscular != null) {
            Matriz2 tablaProbabilidad = nodoDolorMuscular.getTablaProbabilidad();
            if (tablaProbabilidad != null) {
                resInfluenza = resInfluenza.toLowerCase();
                switch (resInfluenza) {
                    case "si":
                        if (siNoDolorMuscular == 1) { // si, si (si dolor muscular)
                            return (double) tablaProbabilidad.obtenerValor(0, 0);
                        } else { // si, si (no dolor muscular)
                            return (double) tablaProbabilidad.obtenerValor(1, 0);
                        }
                    case "no":
                        if (siNoDolorMuscular == 1) { // no, si (si dolor muscular)
                            return (double) tablaProbabilidad.obtenerValor(0, 1);
                        } else { // no, si (no dolor muscular)
                            return (double) tablaProbabilidad.obtenerValor(1, 1);
                        }
                    default:
                        return 0.0;
                }
            } else {
                return 0.0;
            }
        } else {
            return 0.0;
        }
    }

    /**
     * Este método nos permite calcular la probabilidad conjunta de varias variables.
     * @param variables Es la cadena de variables de las cuales se desea calcular la probabilidad conjunta.
     * @return Regresa la probabilidad conjunta de las variables.
     */
    // calcular la probabilidad conjunta, por ejemplo P(V,F,I) = P(V) * P(F|V) * P(I|F,E)
    private double probabilidadConjunta(String variables) {
        double probabilidad = 1.0;
        String variable = "";
        double probabilidadVariable = 0.0;
        for (int pos = 0; pos < variables.length(); pos++) {
            char caracter = variables.charAt(pos);
            variable = String.valueOf(caracter);
            variable = variable.toLowerCase();
            if (variable.equalsIgnoreCase("V")) {
                SalidaPorDefecto.consola("---> Probabilidad P(V):\n");
                String respuestaExpoVirius = preguntarSioNo("¿Tiene exposicion a virus AH1N1?");
                probabilidadVariable = probabilidadExposicionVirus(respuestaExpoVirius);
                probabilidad *= probabilidadVariable;
                SalidaPorDefecto.consola("P(V) = " + probabilidadVariable + "\n");
            } else if (variable.equalsIgnoreCase("F")) {
                SalidaPorDefecto.consola("---> Probabilidad P(F|V):\n");
                String respuestaExpoVirius = preguntarSioNo("¿Tiene exposicion a virus AH1N1?");
                String respuestaFiebre = preguntarSioNo("¿Probabilidad de si tener o no tener fiebre?");
                if (respuestaFiebre.equalsIgnoreCase("si")) {
                    probabilidadVariable = probabilidadFiebre(respuestaExpoVirius, 1);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(F|V) = " + probabilidadVariable + "\n");
                } else {
                    probabilidadVariable = probabilidadFiebre(respuestaExpoVirius, 0);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(F|V) = " + probabilidadVariable + "\n");
                }
            } else if (variable.equalsIgnoreCase("E")) {
                SalidaPorDefecto.consola("---> Probabilidad P(E|V):\n");
                String respuestaExpoVirius = preguntarSioNo("¿Tiene exposicion a virus AH1N1?");
                String respuestaEstornudos = preguntarSioNo("¿Probabilidad de si tener o no tener estornudos?");
                if (respuestaEstornudos.equalsIgnoreCase("si")) {
                    probabilidadVariable = probabilidadEstornudos(respuestaExpoVirius, 1);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(E|V) = " + probabilidadVariable + "\n");
                } else {
                    probabilidadVariable = probabilidadEstornudos(respuestaExpoVirius, 0);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(E|V) = " + probabilidadVariable + "\n");
                }
            } else if (variable.equalsIgnoreCase("I")) {
                SalidaPorDefecto.consola("---> Probabilidad P(I|F,E):\n");
                String respuestaFiebre = preguntarSioNo("¿Tienes Fiebre?");
                String respuestaEstornudos = preguntarSioNo("¿Tienes Estornudos?");
                String respuestaInfluenza = preguntarSioNo("¿Probabilidad de si tener o no Influenza?");
                if (respuestaInfluenza.equalsIgnoreCase("si")) {
                    probabilidadVariable = probabilidadInfluenza(respuestaFiebre, respuestaEstornudos, 1);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(I|F,E) = " + probabilidadVariable + "\n");
                } else {
                    probabilidadVariable = probabilidadInfluenza(respuestaFiebre, respuestaEstornudos, 0);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(I|F,E) = " + probabilidadVariable + "\n");
                }
            } else if (variable.equalsIgnoreCase("R")) {
                SalidaPorDefecto.consola("---> Probabilidad P(R|F,E):\n");
                String respuestaFiebre = preguntarSioNo("¿Tienes Fiebre?");
                String respuestaEstornudos = preguntarSioNo("¿Tienes Estornudos?");
                String respuestaResfriado = preguntarSioNo("¿Probabilidad de si tener o no Resfriado?");
                if (respuestaResfriado.equalsIgnoreCase("si")) {
                    probabilidadVariable = probabilidadResfriado(respuestaFiebre, respuestaEstornudos, 1);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(R|F,E) = " + probabilidadVariable + "\n");
                } else {
                    probabilidadVariable = probabilidadResfriado(respuestaFiebre, respuestaEstornudos, 0);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(R|F,E) = " + probabilidadVariable + "\n");
                }
            } else if (variable.equalsIgnoreCase("T")) {
                SalidaPorDefecto.consola("---> Probabilidad P(T|R):\n");
                String respuesaResfriado = preguntarSioNo("¿Tienes Resfriado?");
                String respuestaTos = preguntarSioNo("¿Probabilidad de si tener o no tener Tos?");
                if (respuestaTos.equalsIgnoreCase("si")) {
                    probabilidadVariable = probabilidadTos(respuesaResfriado, 1);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(T|R) = " + probabilidadVariable + "\n");
                } else {
                    probabilidadVariable = probabilidadTos(respuesaResfriado, 0);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(T|R) = " + probabilidadVariable + "\n");
                }
            } else if (variable.equalsIgnoreCase("D")) {
                SalidaPorDefecto.consola("---> Probabilidad P(D|I):\n");
                String respuestaInfluenza = preguntarSioNo("¿Tienes Influenza?");
                String respuestaDolorM = preguntarSioNo("¿Probabilidad de si tener o no Dolor muscular?");
                if (respuestaDolorM.equalsIgnoreCase("si")) {
                    probabilidadVariable = probabilidadDolorMuscular(respuestaInfluenza, 1);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(D|I) = " + probabilidadVariable + "\n");
                } else {
                    probabilidadVariable = probabilidadDolorMuscular(respuestaInfluenza, 0);
                    probabilidad *= probabilidadVariable;
                    SalidaPorDefecto.consola("P(D|I) = " + probabilidadVariable + "\n");
                }
            }
        }
        return probabilidad;
    }

    /**
     * Este método permite preguntar al usuario si o no ala pregunta que se le pasa por parámetro.
     * @param pregunta Es la pregunta que se le pide contestar al usuario.
     * @return Regresa si o no ala pregunta que se le pide contestar al usuario.
     */
    private String preguntarSioNo(String pregunta) {
        String respuesta = "";
        do {
            SalidaPorDefecto.consola(pregunta + " (si/no): ");
            respuesta = EntradaPorDefecto.consolaCadenas();
        } while (!respuestaValida(respuesta));
        return respuesta;
    }

    /**
     * Este método permite validar la respuesta ingresada por el usuario (Si/No).
     * @param respuesta Es la respuesta ingresada por el usuario.
     * @return Regresa <b>true</b> si la respuesta es válida, <b>false</b> si la respuesta es inválida.
     */
    private boolean respuestaValida(String respuesta) {
        return respuesta.equalsIgnoreCase("si") || respuesta.equalsIgnoreCase("no");
    }

    /**
     * Este método muestra la información de la red bayesiana.
     */
    public void imprimirRedBayesiana() {
        redBayesiana.imprimirGrafo();
    }
}