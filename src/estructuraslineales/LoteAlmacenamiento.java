package estructurasLineales;

/**
 * Este interfaz define los métodos que se deben implementar en una clase que represente un lote de almacenamiento.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public interface LoteAlmacenamiento {
    /**
     * Este método no indica si el lote de almacenamiento está vacío.
     * @return Regresa <b>true</b> si el lote está vacío o <b>false</b> en caso contrario.
     */
    public boolean vacio();

    /**
     * Este método no indica si el lote de almacenamiento está lleno.
     * @return Regresa <b>true</b> si el lote está lleno o <b>false</b> en caso contrario.
     */
    public boolean lleno();

    /**
     * Este método agrega un elemento al lote de almacenamiento.
     * @param valor El elemento a agregar.
     * @return Regresa <b>true</b> si el elemento se agregó correctamente o <b>false</b> si el lote de almacenamiento está lleno.
     */
    public boolean poner(Object valor);

    /**
     * Este método elimina el último elemento agregado al lote de almacenamiento.
     * @return Regresa el elemento eliminado o <b>null</b> si el lote de almacenamiento está vacío.
     */
    public Object quitar();

    /**
     * Este método imrimirá el contenido del lote de almacenamiento.
     */
    public void imprimir();

    /**
     * Este método nos indica cual fue es el ultimo elemento agregado al lote de almacenamiento.
     * @return Regresa el elemento agregado o <b>null</b> si el lote de almacenamiento está vacío.
     */
    public Object verUltimo();
}