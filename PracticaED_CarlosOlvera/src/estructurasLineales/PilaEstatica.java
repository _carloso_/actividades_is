package estructurasLineales;

public class PilaEstatica implements LoteAlmacenamiento {
    protected ListaEstatica pila;

    public PilaEstatica(int maximo) {
        pila = new ListaEstatica(maximo);
    }

    @Override
    public boolean vacio() {
        return pila.vacia();
    }

    @Override
    public boolean lleno() {
        return pila.llena();
    }

    @Override
    public boolean poner(Object valor) {
        int valorRetorno = pila.agregar(valor);
        if (valorRetorno >= 0) {
            return true;
        } else { //retorna -1
            return false;
        }
    }

    @Override
    public Object quitar() {
        return pila.eliminar();
    }

    @Override
    public void imprimir() {
        pila.imprimir();
    }

    @Override
    public Object verUltimo() {
        return pila.verUltimo();
    }

    @Override
    public int maximo() {
        return pila.maximo();
    }
}