package prueba;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import registros.arroz.Campesino;
import registros.arroz.Granja;

public class PruebaGranja2 {
    public static void main(String[] args) {
        int contadorMeses1 = 0;
        int contadorMeses2 = 0;
        Granja granja = new Granja ("Minshuku Zaigomon", 2);
        SalidaPorDefecto.consola("La granja "+ granja.getNombre() + " experta en producir arroz año tras año. \n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");

        ListaEstatica produccionAnualCampesino1 = new ListaEstatica(12);
        SalidaPorDefecto.consola("---Produccion del campesino 1---\n");
        for (int cont = 0 ;cont <12; cont++){   //se agregan las toneladas de los 12 meses del año del campsesino 1
            SalidaPorDefecto.consola("Ingrese las toneladas del mes " + (cont+1) + " " + "del año 1: ");
            double toneladas = EntradaPorDefecto.consolaDouble();
            produccionAnualCampesino1.agregar(toneladas);
        }
        // se le agregan los valores de produccion anual 1 a el campesino 1
        Campesino campesino1 = new Campesino("Carlos","Olvera", "8097059",produccionAnualCampesino1);
        granja.agregarCampesino(campesino1);
        SalidaPorDefecto.consola("\n");

        ListaEstatica produccionAnualCampesino2 = new ListaEstatica(12);
        SalidaPorDefecto.consola("---Produccion del campesino 2---\n");
        for (int cont = 0 ;cont <12; cont++){   //se agregan las toneladas de los 12 meses del año del campsesino 2
            SalidaPorDefecto.consola("Ingrese las toneladas del mes " + (cont+1) + " " + "del año 1: ");
            double toneladas = EntradaPorDefecto.consolaDouble();
            produccionAnualCampesino2.agregar(toneladas);
        }
        // se le agregan los valores de produccion anual 1 a el campesino 2
        Campesino campesino2 = new Campesino("Eduardo","Mayorga", "8097060",produccionAnualCampesino2);
        granja.agregarCampesino(campesino2);

        //PRESENTACION DE LOS CAMPESINOS
        SalidaPorDefecto.consola("La granja "+ granja.getNombre() + " emplea los siguientes campesinos: \n");
        granja.imprimirListadoCampesinos();
        SalidaPorDefecto.consola("\n");

        //A) PRODUCCION ANUAL DE LOS CAMPESINOS
        SalidaPorDefecto.consola("La produccion anual del Campesino " + campesino1.getNombre() +" "+ campesino1.getApellido() +
                " es "+ granja.calcularPromedioAnualCampesino("8097059")+ " toneladas\n");
        SalidaPorDefecto.consola("La produccion anual del Campesino " + campesino2.getNombre() +" "+ campesino2.getApellido() +
                " es "+ granja.calcularPromedioAnualCampesino("8097060")+ " toneladas\n");
        SalidaPorDefecto.consola("\n");

        //B)MESES MENORES AL PROMEDIO EN PRODUCCION
        for (int i = 0 ;  i <=12; i++) { //se crea un ciclo for para que recorra toda la lista e identifique cual valor es menor al promedio.
            if ((double) produccionAnualCampesino1.obtener(i) < granja.calcularPromedioAnualCampesino("8097059")) {
                contadorMeses1 = contadorMeses1 + 1;
            }
        }

        for (int i = 0 ;  i <=12; i++) { //se crea un ciclo for para que recorra toda la lista e identifique cual valor es menor al promedio.
            if ((double) produccionAnualCampesino2.obtener(i) < granja.calcularPromedioAnualCampesino("8097060")) {
                contadorMeses2 = contadorMeses2 + 1;
            }
        }

        SalidaPorDefecto.consola(contadorMeses1+ " meses tuvieron toneladas menores al promedio del Campesino " +
                campesino1.getNombre() +" "+ campesino1.getApellido());

        SalidaPorDefecto.consola(contadorMeses2+ " meses tuvieron toneladas menores al promedio del Campesino " +
                campesino2.getNombre() +" "+ campesino2.getApellido());

        //C)EL MES QUE TUVO MAS TONELADAS

        //D)TONELADA QUE SE OBTUVO EL ULTIMO MES DE CADA CAMPESINO
        SalidaPorDefecto.consola(produccionAnualCampesino1.obtener(12) + " son las toneladas del ultimo mes del año 1 del Campesino " +
                campesino1.getNombre() +" "+ campesino1.getApellido());

        SalidaPorDefecto.consola(produccionAnualCampesino2.obtener(12) + " son las toneladas del ultimo mes del año 1 del Campesino " +
                campesino2.getNombre() +" "+ campesino2.getApellido());

    }
}
