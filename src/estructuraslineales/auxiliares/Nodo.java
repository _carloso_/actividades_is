package estructurasLineales.auxiliares;

/**
 * Esta claes representa un nodo de una lista dinamica.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class Nodo {

    protected Object contenido;
    protected Nodo nodoDer;

    public Nodo(Object contenido) {
        this.contenido = contenido;
        nodoDer = null;
    }

    public Object getContenido() {
        return contenido;
    }

    public void setContenido(Object contenido) {
        this.contenido = contenido;
    }

    public Nodo getNodoDer() {
        return nodoDer;
    }

    public void setNodoDer(Nodo nodoDer) {
        this.nodoDer = nodoDer;
    }

    @Override
    public String toString() {
        return contenido.toString();
    }
}
