package pruebas;

import entradaSalida.SalidaPorDefecto;
import registros.habitacionesDeCasas.CorrelacionHabitacionesPrecios;

public class PruebaCorrelacionHabitacionesPrecios {
    public static void main(String[] args) {
        CorrelacionHabitacionesPrecios corr = new CorrelacionHabitacionesPrecios("C:\\Users\\Cristian\\Desktop\\x.txt", "C:\\Users\\Cristian\\Desktop\\y.txt");

        SalidaPorDefecto.consola("*Promedio de X: " + corr.promedioDeX() + "\n");
        SalidaPorDefecto.consola("*Promedio de Y: " + corr.promedioDeY() + "\n");
        SalidaPorDefecto.consola("*Sumatoria (xi - promedio(x))^2: " + corr.sumatoriaCuadradaDeXiMenosPromedioX() + "\n");
        SalidaPorDefecto.consola("*Sumatoria (yi - promedio(y))^2: " + corr.sumatoriaCuadradaDeYiMenosPromedioY() + "\n");
        SalidaPorDefecto.consola("*Desviación estandar de la población de x: " + corr.desviacionEstandarDePoblacionDeX() + "\n");
        SalidaPorDefecto.consola("*Desviación estandar de la población de y: " + corr.desviacionEstandarDePoblacionDeY() + "\n");
        SalidaPorDefecto.consola("*Desviación estandar de la muestra de x: " + corr.desviacionEstandarDeMuestraX() + "\n");
        SalidaPorDefecto.consola("*Desviación estandar de la muestra de y: " + corr.desviacionEstandarDeMuestraY() + "\n");
        SalidaPorDefecto.consola("*Sumatoria (xi - promedio(x)) * (yi - promedio(y)): " + corr.sumatoriaDeXiMenosPromedioXYiMenosPromedioY() + "\n");
        SalidaPorDefecto.consola("*Covarianza poblacional: " + corr.covarianzaPoblacional() + "\n");
        SalidaPorDefecto.consola("*Covarianza muestral: " + corr.covarianzaMuestral() + "\n");
        corr.coeficienteRDePearson();
        SalidaPorDefecto.consola("*Correlacion poblacional: " + corr.correlacionPoblacional() + "\n");
        SalidaPorDefecto.consola("*Correlacion Muestral: " + corr.correlacionMuestral() + "\n");

        SalidaPorDefecto.consola("\n--- Regresion Lineal ---\n");
        SalidaPorDefecto.consola("*Beta0: " + corr.beta0() + "\n");
        SalidaPorDefecto.consola("*Beta1: " + corr.beta1() + "\n");
        corr.calcularEcuacionDeRegresionLineal();

        corr.estimarValor(10);

        // grafica de los datos
        corr.graficarDatosXY();
    }
}
