package estructurasNoLineales;
import estructurasLineales.auxiliares.NodoDoble;
import herramientas.generales.Utilerias;

import static estructurasNoLineales.TipoOrdenRecorrido.*;

public class ArbolMonticulo extends ArbolBinario{
    TipoOrdenRecorrido orden;
    public ArbolMonticulo(TipoOrdenRecorrido ordenRecorrido){
        if (ordenRecorrido == ASC){
            orden = ASC;
        }
        else if (ordenRecorrido == DESC){
            orden = DESC;
        }
    }

    public boolean agregar(Object valor){
        if(raiz==null){ //el valor lo agregaremos como la nueva raíz
            NodoDoble nuevoNodo=new NodoDoble(valor);
            if(nuevoNodo!=null){ //si hay espacio
                raiz=nuevoNodo;
                return true;
            }else{ //no hay espacio o hay error
                return false;
            }
        }else{ //ya hay nodos
            return agregar(raiz,valor);
        }
    }
    private boolean agregar(NodoDoble subRaiz, Object valor){
        boolean agregado = false;
        if(orden == ASC){
            //Comparar si el valor es mayor o menor a la subraíz
            if(Utilerias.compararObjetos(valor,subRaiz.getContenido())<0){ //valor < subRaiz
                if(subRaiz.getNodoIzq()==null){ //llegamos a la posicion donde le toca ser insertado
                    NodoDoble nuevoNodo=new NodoDoble(valor);
                    if(nuevoNodo!=null){ // si hay espacio
                        subRaiz.setNodoIzq(nuevoNodo);
                        return true;
                    }else{ //no hay espacio
                        return false;
                    }
                }else{ //hacemos llamada recursiva hacia esa rama
                    return agregar(subRaiz.getNodoIzq(),valor);
                }
            }else if(Utilerias.compararObjetos(valor,subRaiz.getContenido())>0){ //valor > subRaiz
                if(subRaiz.getNodoDer()==null){ //llegamos a la posicion donde le toca ser insertado
                    NodoDoble nuevoNodo=new NodoDoble(valor);
                    NodoDoble nodoPadre = new NodoDoble(subRaiz.getContenido()) ;
                    if(nuevoNodo!=null){ // si hay espacio
                        subRaiz.setNodoDer(nodoPadre);
                        subRaiz.setContenido(nuevoNodo);
                        return true;
                    }else{ //no hay espacio
                        return false;
                    }
                }else{ //hacemos llamada recursiva hacia esa rama
                    return agregar(subRaiz.getNodoDer(),valor);
                }
            }else{ //valor = subRaiz
                agregado = false;
            }
            agregado = true;
        }
        else{
            //Comparar si el valor es mayor o menor a la subraíz
            if(Utilerias.compararObjetos(valor,subRaiz.getContenido())<0){ //valor < subRaiz
                if(subRaiz.getNodoIzq()==null){ //llegamos a la posicion donde le toca ser insertado
                    NodoDoble nuevoNodo=new NodoDoble(valor);
                    NodoDoble nodoPadre = new NodoDoble(subRaiz.getContenido()) ;
                    if(nuevoNodo!=null){ // si hay espacio
                        subRaiz.setNodoIzq(nodoPadre);
                        subRaiz.setContenido(nuevoNodo);
                        return true;
                    }else{ //no hay espacio
                        return false;
                    }
                }else{ //hacemos llamada recursiva hacia esa rama
                    return agregar(subRaiz.getNodoIzq(),valor);
                }
            }else if(Utilerias.compararObjetos(valor,subRaiz.getContenido())>0 ){ //valor > subRaiz o valor == subRaiz
                if(subRaiz.getNodoDer()==null){ //llegamos a la posicion donde le toca ser insertado
                    NodoDoble nuevoNodo=new NodoDoble(valor);
                    if(nuevoNodo!=null){ // si hay espacio
                        subRaiz.setNodoDer(nuevoNodo);
                        return true;
                    }else{ //no hay espacio
                        return false;
                    }
                }else{ //hacemos llamada recursiva hacia esa rama
                    return agregar(subRaiz.getNodoDer(),valor);
                }
            }
            else{ //valor = subRaiz
                agregado = false;
            }
            agregado = true;
        }
        return agregado;
    }

    public void eliminar(){

    }
}
