package registros.ProyectoFinal;
import estructurasLineales.ListaEstatica;
import estructurasNoLineales.Matriz2;

/**
 * Clase RadixSort que contiene el metodo de ordenamiento
 * Por Carlos Eduardo Olvera Mayorga
 */
public class RadixSort {

    /**
     * Metodo que ordena una lista con el metodo RadixSort
     * @param arr lista Estatica que se recibe como parametro para ordenarla
     */
    public static void radixSort(ListaEstatica arr) {
        Matriz2 bucket = new Matriz2(10,arr.numeroElementos(),"X");
        //El tamaño de esta lista es la cantidad de los digitos numericos
        ListaEstatica bucketOfElement = new ListaEstatica(10);
        //Se agregan estos valores, ya que no se puede utilizar la lista con valores nullos
        bucketOfElement.agregar(0);
        bucketOfElement.agregar(0);
        bucketOfElement.agregar(0);
        bucketOfElement.agregar(0);
        bucketOfElement.agregar(0);
        bucketOfElement.agregar(0);
        bucketOfElement.agregar(0);
        bucketOfElement.agregar(0);
        bucketOfElement.agregar(0);
        bucketOfElement.agregar(0);
        // Encuentra el elemento más grande en la matriz
        int max= arr.numeroMaximo();
        // Calcula el número de bits del elemento más grande
        int maxLength = (max+"").length();
        for(int m = 0,n=1;m<maxLength;m++,n*=10) {
            // Coloca los números en arr en los cubos correspondientes según sus unidades, decenas, centenas, etc.
            for(int i = 0 ; i < arr.numeroElementos();i++) {
                //esta variable guarda el resultado de la formula del modulo
                //Esta formula nos permite recuperar un digito en particular en funcion de una posicion determinada
                int digit = Integer.parseInt((String)arr.obtener(i))/n%10;
                // Asignar el valor de arr [i] a la matriz bidimensional en el depósito
                int columna = (int) bucketOfElement.obtener(digit);//se crea la variable columna, para no confundir las lineas con mucho texto
                bucket.cambiar(digit,columna,arr.obtener(i));
                // en un array seria: bucketOfElement[digit]++;
                int incremento = ((int)bucketOfElement.obtener(digit))+1;//se crea la variable incremento, para no confundir las lineas con mucho texto
                bucketOfElement.cambiar(digit,incremento);
            }
            //esta variable se usa como indice del arreglo principal
            int index = 0;
            // Leer los elementos en el depósito y reasignarlos a arr
            for(int j = 0;j<10;j++) {
                for(int k = 0 ; k<(int)bucketOfElement.obtener(j);k++) {
                    Object valorACambiar =bucket.obtener(j,k); //se crea la variable valorACambiar, para no confundir las lineas con mucho texto
                    arr.cambiar(index, valorACambiar);
                    index++;
                }
                // Borrar el número de elementos en cada uno
                bucketOfElement.cambiar(j,0);
            }
        }
    }
}
