package estructurasLineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.auxiliares.NodoClave;
import estructurasnolineales.Matriz2;

/**
 * Esta clase representa a un TDA de tipo lista enlazada con clave y valor.
 * @author Cristian Omar Alvarado Rodríguez.
 * @version 1.0.
 */
public class ListaDinamicaClave {

    protected NodoClave primero;
    protected NodoClave ultimo;

    public ListaDinamicaClave() {
        primero = null;
        ultimo = null;
    }

    /**
     * Este método nos indica si la lista está vacía o no.
     * @return Regresa <b>true</b> si la lista está vacía, <b>false</b> si no está vacía.
     */
    public boolean vacia() {
        if (primero == null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método agrega un nuevo nodo a la lista.
     * @param clave Es la clave del nuevo nodo que se va a agregar.
     * @param valor Es el valor del nuevo nodo que se va a agregar.
     * @return Regresa <b>true</b> si el nodo se agregó correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregar(Object clave, Object valor) {
        NodoClave nuevoNodoClave = new NodoClave(clave, valor);
        if (nuevoNodoClave != null) {
            if (!vacia()) { // Si la lista no está vacía
                if (!existeLaClave(clave)) { //Si no existe la clave en la lista
                    ultimo.setNodoDer(nuevoNodoClave);
                    ultimo = nuevoNodoClave;
                    return true;
                } else { //Si ya existe la clave en la lista
                    // reemplazar el contenido del nodo por el nuevo valor
                    boolean valorRetornoRemplazar = remplazarValor(clave, valor);
                    if (!valorRetornoRemplazar) {
                        return false;
                    } else {
                        return true;
                    }
                }
            } else { // Si la lista está vacía
                primero = nuevoNodoClave;
                ultimo = nuevoNodoClave;
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite saber si una clave ya existe en la lista.
     * @param clave Es la clave que se va a buscar en la lista.
     * @return Regresa <b>true</b> si la clave ya existe en la lista, <b>false</b> si no existe.
     */
    private boolean existeLaClave(Object clave) {
        NodoClave iterador = primero;
        while(iterador != null && !iterador.getClave().toString().equalsIgnoreCase(clave.toString())) {
            iterador = iterador.getNodoDer();
        }
        if (iterador != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite remplazar el valor de un nodo de la lista por un nuevo valor, en caso de que la clave del
     * nodo coincida con la clave que se le pasa como parámetro.
     * @param clave Es la clave del nodo que se va a remplazar su valor.
     * @param valor Es el nuevo valor que se le va a asignar al nodo.
     * @return Regresa <b>true</b> si el valor del nodo se remplazó correctamente o <b>false</b> si no se pudo remplazar.
     */
    private boolean remplazarValor(Object clave, Object valor) {
        if (!vacia()) { // Si la lista no está vacía
            NodoClave iterador = primero;
            while(iterador != null && !iterador.getClave().toString().equalsIgnoreCase(clave.toString())){
                iterador = iterador.getNodoDer();
            }
            if (iterador != null) {
                iterador.setContenido(valor);
                return true;
            } else {
                return false;
            }
        } else { // Si la lista está vacía
            return false;
        }
    }

    /**
     * Este método nos permite eliminar un nodo de la lista con base en su clave.
     * @param clave Es la clave del nodo que se va a eliminar.
     * @return Regresa el contenido del nodo eliminado o <b>null</b> si no se pudo eliminar.
     */
    public Object eliminar(Object clave) {
        if (!vacia()) {
            NodoClave nodoBuscado = primero;
            NodoClave nodoAnterior = null;
            while (nodoBuscado != null && !nodoBuscado.getClave().toString().equalsIgnoreCase(clave.toString())) {
                nodoAnterior = nodoBuscado;
                nodoBuscado = nodoBuscado.getNodoDer();
            }
            if (nodoBuscado != null) {
                Object contenidoEliminado = nodoBuscado.getContenido();
                if (primero == ultimo) {
                    primero = null;
                    ultimo = null;
                } else if (nodoBuscado == primero) {
                    primero = primero.getNodoDer();
                } else if (nodoBuscado == ultimo) {
                    ultimo = nodoAnterior;
                    ultimo.setNodoDer(null);
                } else {
                    nodoAnterior.setNodoDer(nodoBuscado.getNodoDer());
                }
                return contenidoEliminado;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Este método nos permite eliminar un nodo de la lista con base en su contenido.
     * @param valor Es el contenido del nodo que se va a eliminar.
     * @return Regresa el contenido del nodo eliminado o <b>null</b> si no se pudo eliminar.
     */
    public Object eliminarContenido(Object valor) {
        if (!vacia()) {
            NodoClave nodoBuscado = primero;
            NodoClave nodoAnterior = null;
            while (nodoBuscado != null && !nodoBuscado.getContenido().toString().equalsIgnoreCase(valor.toString())) {
                nodoAnterior = nodoBuscado;
                nodoBuscado = nodoBuscado.getNodoDer();
            }
            if (nodoBuscado != null) {
                Object contenidoEliminado = nodoBuscado.getContenido();
                if (primero == ultimo) {
                    primero = null;
                    ultimo = null;
                } else if (nodoBuscado == primero) {
                    primero = primero.getNodoDer();
                } else if (nodoBuscado == ultimo) {
                    ultimo = nodoAnterior;
                    ultimo.setNodoDer(null);
                } else {
                    nodoAnterior.setNodoDer(nodoBuscado.getNodoDer());
                }
                return contenidoEliminado;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Este método nos permite buscar un nodo de la lista con base en su clave.
     * @param clave Es la clave del nodo que se va a buscar.
     * @return Regresa el contenido del nodo buscado o <b>null</b> si no se encontró.
     */
    public Object buscar(Object clave) {
        if (!vacia()) {
            NodoClave nodoBuscado = primero;
            while (nodoBuscado != null && !nodoBuscado.getClave().toString().equalsIgnoreCase(clave.toString())) {
                nodoBuscado = nodoBuscado.getNodoDer();
            }
            if (nodoBuscado != null) {
                return nodoBuscado.getContenido();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Este método nos permite buscar un nodo de la lista con base en su contenido.
     * @param valor Es el contenido del nodo que se va a buscar.
     * @return Regresa el contenido del nodo buscado o <b>null</b> si no se encontró.
     */
    public Object buscarContenido(Object valor) {
        if (!vacia()) {
            NodoClave nodoBuscado = primero;
            while (nodoBuscado != null && !nodoBuscado.getContenido().toString().equalsIgnoreCase(valor.toString())) {
                nodoBuscado = nodoBuscado.getNodoDer();
            }
            if (nodoBuscado != null) {
                return nodoBuscado.getContenido();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Este método nos permite cambiar el contenido de un nodo de la lista con base en su clave.
     * @param clave Es la clave del nodo que se va a cambiar.
     * @param valor Es el nuevo contenido del nodo.
     * @return Regresa <b>true</b> si se pudo cambiar el contenido o <b>false</b> si no se pudo.
     */
    public boolean cambiar(Object clave, Object valor) {
        boolean valorRetornoRemplazo = remplazarValor(clave, valor);
        if (valorRetornoRemplazo == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite remplazar el contenido de un nodo de la lista.
     * @param valor Es el contenido del nodo que se va a remplazar.
     * @param nuevoValor Es el nuevo contenido.
     * @return Regresa <b>true</b> si se pudo remplazar el contenido o <b>false</b> si no se pudo.
     */
    public boolean cambiarContenido(Object valor, Object nuevoValor) {
        if (!vacia()) {
            NodoClave nodoBuscado = primero;
            while (nodoBuscado != null && !nodoBuscado.getContenido().toString().equalsIgnoreCase(valor.toString())) {
                nodoBuscado = nodoBuscado.getNodoDer();
            }
            if (nodoBuscado != null) {
                nodoBuscado.setContenido(nuevoValor);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método imprime la lista completa clave-valor.
     */
    public void imprimir() {
       NodoClave iterador = primero;
       while(iterador != null){
           Object clave = iterador.getClave();
           Object contenido = iterador.getContenido();
           SalidaPorDefecto.consola("[" + clave + ":" + contenido + "]" + "->");
           iterador = iterador.getNodoDer();
       }
       SalidaPorDefecto.consola("null");
    }

    /**
     * Este método imprime las claves de la lista.
     */
    public void imprimirClaves() {
        NodoClave iterador = primero;
        while(iterador != null){
            Object clave = iterador.getClave();
            SalidaPorDefecto.consola(clave + "->");
            iterador = iterador.getNodoDer();
        }
        SalidaPorDefecto.consola("null");
    }

    /**
     * Este método imprime los valores (contenidos) de la lista.
     */
    public void imprimirContenidos() {
        NodoClave iterador = primero;
        while(iterador != null){
            Object contenido = iterador.getContenido();
            SalidaPorDefecto.consola(contenido + "->");
            iterador = iterador.getNodoDer();
        }
        SalidaPorDefecto.consola("null");
    }

    /**
     * Este método nos permite saber el tamaño de la lista (cuantos nodos tiene actualmente).
     * @return Regresa el tamaño de la lista.
     */
    public int numeroElementos() {
        NodoClave iterador = primero;
        int contadorElementos = 0;
        while (iterador != null) {
            contadorElementos++;
            iterador = iterador.getNodoDer();
        }
        return contadorElementos;
    }

    /**
     * Este método nos permite obtener una lista estatica que contiene dos listas, una con las claves y otra con los valores
     * de la lista actual.
     * @return Regresa una lista estatica con dos listas, una con las claves y otra con los valores de la lista actual.
     */
    public ListaEstatica aListasEstaticas() {
        ListaEstatica listaEstaticaRetorno = new ListaEstatica(2);
        ListaEstatica listaEstaticaClaves = new ListaEstatica(numeroElementos());
        ListaEstatica listaEstaticaContenidos = new ListaEstatica(numeroElementos());
        NodoClave iterador = primero;
        while (iterador != null) {
            listaEstaticaClaves.agregar(iterador.getClave());
            listaEstaticaContenidos.agregar(iterador.getContenido());
            iterador = iterador.getNodoDer();
        }
        listaEstaticaRetorno.agregar(listaEstaticaClaves);
        listaEstaticaRetorno.agregar(listaEstaticaContenidos);
        return listaEstaticaRetorno;
    }

    /**
     * Este método nos permite obtener una lista dinámica simple que contiene dos listas, una con las claves y otra con los valores
     * de la lista actual.
     * @return Regresa una lista dinámica simple con dos listas, una con las claves y otra con los valores de la lista actual.
     */
    public ListaDinamica aListasDinamicas() {
        ListaDinamica listaDinamicaRetorno = new ListaDinamica();
        ListaDinamica listaDinamicaClaves = new ListaDinamica();
        ListaDinamica listaDinamicaContenidos = new ListaDinamica();
        NodoClave iterador = primero;
        while (iterador != null) {
            listaDinamicaClaves.agregar(iterador.getClave());
            listaDinamicaContenidos.agregar(iterador.getContenido());
            iterador = iterador.getNodoDer();
        }
        listaDinamicaRetorno.agregar(listaDinamicaClaves);
        listaDinamicaRetorno.agregar(listaDinamicaContenidos);
        return listaDinamicaRetorno;
    }

    /**
     * Este método nos permite obtener una matriz 2D que contiene las claves y los valores de la lista actual.
     * @return Regresa una matriz 2D que contiene las claves y los valores de la lista actual.
     */
    public Matriz2 aMatriz2() {
        Matriz2 matriz2 = new Matriz2(numeroElementos(), 2);
        NodoClave iterador = primero;
        int contadorFila = 0;
        while (iterador != null) {
            matriz2.cambiar(contadorFila, 0, iterador.getClave()); // Clave se agrega en la primera columna de la matriz
            matriz2.cambiar(contadorFila, 1, iterador.getContenido()); // Contenido se agrega en la segunda columna de la matriz
            iterador = iterador.getNodoDer();
            contadorFila++;
        }
        return matriz2;
    }

    /**
     * Este método vacía por completo a la lista.
     */
    public void vaciar() {
        primero = null;
        ultimo = null;
    }

    /**
     * Este método nos permite obtener el contenido de un nodo de la lista a partir de su clave.
     * @param clave Es la clave del nodo que se desea obtener.
     * @return Regresa el contenido del nodo buscado.
     */
    public Object obtener(Object clave){
        if (!vacia()) {
            NodoClave iterador = primero;
            while (iterador != null && !iterador.getClave().toString().equals(clave.toString())) {
                iterador = iterador.getNodoDer();
            }
            if (iterador != null) {
                return iterador.getContenido();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Este método agrega los elementos de una lista dinamica con claves y valores a la lista actual.
     * @param lista2 Es la lista que se desea agregar a la lista actual.
     * @return Regresa <b>true</b> si la lista fue agregada correctamente, de lo contrario regresa <b>false</b>.
     */
    public boolean agregarLista(ListaDinamicaClave lista2) {
       if (!lista2.vacia()) {
           NodoClave iterador = lista2.primero;
           while (iterador != null) {
              if (!existeLaClave(iterador.getClave())) {
                  agregar(iterador.getClave(), iterador.getContenido());
              }
              iterador = iterador.getNodoDer();
           }
           return true;
       } else {
           return false;
       }
    }

    /**
     * Este método agrega los elementos de una lista estática con claves y otra lista con valores a la lista actual.
     * @param arregloClaves Es la lista estática con claves que se desean agregar a la lista actual.
     * @param arregloValores Es la lista estática con valores que se desean agregar a la lista actual.
     * @return Regresa <b>true</b> si las listas fueron agregadas correctamente, o <b>false</b> si no se pudieron agregar.
     */
    public boolean agregarListasEstaticas(ListaEstatica arregloClaves, ListaEstatica arregloValores) {
        if (!arregloClaves.vacia() && !arregloValores.vacia()) {
           if (arregloClaves.numeroElementos() == arregloValores.numeroElementos()) {
               for (int pos = 0; pos < arregloClaves.numeroElementos(); pos++) {
                   Object clave = arregloClaves.obtener(pos);
                   Object valor = arregloValores.obtener(pos);
                   if (!existeLaClave(clave)) {
                      agregar(clave, valor);
                   }
               }
               return true;
           } else {
               return false;
           }
        } else {
            return false;
        }
    }

    /**
     * Este método agrega los elementos de una lista dinámica simple con claves y otra lista con valores a la lista actual.
     * @param listaClaves Es la lista dinámica simple con claves que se desean agregar a la lista actual.
     * @param listaValores Es la lista dinámica simple con valores que se desean agregar a la lista actual.
     * @return Regresa <b>true</b> si las listas fueron agregadas correctamente, o <b>false</b> si no se pudieron agregar.
     */
    public boolean agregarListasDinamicas(ListaDinamica listaClaves, ListaDinamica listaValores) {
        if (!listaClaves.vacia() && !listaValores.vacia()) {
            if (listaClaves.contarCantidadDeNodos() == listaValores.contarCantidadDeNodos()) {
                listaClaves.iniciarIterador();
                listaValores.iniciarIterador();
                while (listaClaves.hayNodos() == true && listaValores.hayNodos() == true) {
                    Object clave = listaClaves.obtenerNodo();
                    Object valor = listaValores.obtenerNodo();
                    if (!existeLaClave(clave)) {
                        agregar(clave, valor);
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método agrega los elementos de una matriz 2D, donde la primera columna tiene las claves y la segunda columna tiene los valores.
     * @param matriz Es la matriz que se desea agregar a la lista actual.
     * @return Regresa <b>true</b> si la matriz fue agregada correctamente, o <b>false</b> si no se pudo agregar.
     */
    public boolean agregarMatriz2(Matriz2 matriz) {
        if (matriz.obtenerColumnas() == 2) {
            for (int fila = 0; fila < matriz.obtenerRenglones(); fila++) {
                Object clave = matriz.obtenerValor(fila, 0);
                Object valor = matriz.obtenerValor(fila, 1);
                if (!existeLaClave(clave)) {
                    agregar(clave, valor);
                }
            }
            return true;
        } else {
            return false;
        }
    }
}