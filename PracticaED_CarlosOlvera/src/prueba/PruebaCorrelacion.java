package prueba;

import entradaSalida.SalidaPorDefecto;
import herramientas.matematicas.Correlacion;

public class PruebaCorrelacion {
    public static void main(String[] args) {
        Correlacion calculo = new Correlacion("src/x.txt", "src/y.txt");
        SalidaPorDefecto.consola("Promedio X: " + calculo.promedioX()+"\n");
        SalidaPorDefecto.consola("Promedio Y: " + calculo.promedioY()+"\n");
        SalidaPorDefecto.consola("Sumatoria X (xi-promedioX)^2: " + calculo.sumatoriaX()+"\n");
        SalidaPorDefecto.consola("Sumatoria Y (yi-promedioY)^2: " + calculo.sumatoriaY()+"\n");
        SalidaPorDefecto.consola("Sumatoria (xi-PromedioX)*(yi-PromedioY): " + calculo.sumatoriaNumerador()+"\n");
        SalidaPorDefecto.consola("Coovarianza: "+ calculo.covarianza()+"\n");
        SalidaPorDefecto.consola("-----POBLACIONAL-----\n");
        SalidaPorDefecto.consola("Desviacion Estandar X Poblacional: " + calculo.desviacionEstandarXPoblacional()+"\n");
        SalidaPorDefecto.consola("Desviacion Estandar Y Poblacional: " + calculo.desviacionEstandarYPoblacional()+"\n");
        SalidaPorDefecto.consola("Correlación Poblacional: " + calculo.correlacionPoblacional()+"\n");
        calculo.interpretacionVarianza(calculo.correlacionPoblacional());
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----MUESTRAL-----\n");
        SalidaPorDefecto.consola("Desviacion Estandar X Muestral: " + calculo.desviacionEstandarXMuestral()+"\n");
        SalidaPorDefecto.consola("Desviacion Estandar Y Muestral: " + calculo.desviacionEstandarYMuestral()+"\n");
        SalidaPorDefecto.consola("Correlación Muestral: " + calculo.correlacionMuestral()+"\n");
        calculo.interpretacionVarianza(calculo.correlacionMuestral());
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("La diferencia entre la Correlacion Poblacional y el muestral es de: "+(calculo.correlacionPoblacional()-calculo.correlacionMuestral()));
        calculo.graficarDatos();
    }
}
