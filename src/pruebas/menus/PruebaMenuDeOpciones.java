package pruebas.menus;

import entradaSalida.SalidaPorDefecto;
import registros.menudeopciones.MenuDeOpciones;

public class PruebaMenuDeOpciones {
    public static void main(String[] args) {
        MenuDeOpciones.mostrarMenu();

        SalidaPorDefecto.consola("**** Historial de Busquedas ****" + "\n");
        MenuDeOpciones.mostrarHistorialDeBusquedas();
    }
}
