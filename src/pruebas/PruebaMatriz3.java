package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasnolineales.Matriz2;
import estructurasnolineales.Matriz3;

public class PruebaMatriz3 {
    public static void main(String[] args) {
        Matriz3 matriz = new Matriz3(4, 3, 5, 0);
        matriz.imprimirXColumnas();

        matriz.cambiar(2, 1, 1, 100);
        matriz.cambiar(1, 0, 2, 200);
        matriz.imprimirXColumnas();

        SalidaPorDefecto.consola("Obteniendo valores: " + matriz.obtener(2, 1, 1) + "\n");
        SalidaPorDefecto.consola("Obteniendo valores: " + matriz.obtener(1, 0, 2) + "\n");
        SalidaPorDefecto.consola("Obteniendo valores: " + matriz.obtener(100, 0, 2) + "\n");

        // Probando el método que pasa de una matriz3 a una matriz2
        SalidaPorDefecto.consola("\n Las matrices 2 obtenidas de la matriz 3 son: \n");
        invocarMetodoParticion(matriz);

    }

    public static void invocarMetodoParticion(Matriz3 matrizAConvertir) {
        ListaEstatica matrices2Obtenidas = matrizAConvertir.aMatriz2XColumnas();
        // Por cada celda del arreglo unidimensional de matrices2Obtenidas
        // recorremos y imrimimos
        for (int indiceMatriz2 = 0; indiceMatriz2 < matrices2Obtenidas.numeroElementos(); indiceMatriz2++) {
            Matriz2 matriz2 = (Matriz2) matrices2Obtenidas.obtener(indiceMatriz2);
            matriz2.imprimirXReglones();
            SalidaPorDefecto.consola("\n");
        }
    }
}
