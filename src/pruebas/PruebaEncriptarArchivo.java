package pruebas;

import entradaSalida.ArchivoTexto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import registros.encriptarmensajespilas.EncriptarArchivo;

public class PruebaEncriptarArchivo {
    public static void main(String[] args) {

        ListaEstatica archivoEncriptado = EncriptarArchivo.encriptar("C:\\Users\\Cristian\\Desktop\\archivo.txt",
                        "C:\\Users\\Cristian\\Desktop\\");
        SalidaPorDefecto.consola("->Contenido del archivo original: " + "\n");
        ListaEstatica archivoOriginal = ArchivoTexto.leer("C:\\Users\\Cristian\\Desktop\\archivo.txt");
        EncriptarArchivo.imprimirArchivo(archivoOriginal);

        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("->Contenido del archivo encriptado: " + "\n");
        EncriptarArchivo.imprimirArchivo(archivoEncriptado);
    }
}