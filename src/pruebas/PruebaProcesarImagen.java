package pruebas;

import registros.procesamientoDeImagen.ProcesarImagen;

import java.awt.image.BufferedImage;

public class PruebaProcesarImagen {
    public static void main(String[] args) {
        ProcesarImagen procImagen = new ProcesarImagen();
        BufferedImage img1 = procImagen.abrirImagen();
        procImagen.mostrarImagen(img1, "Imagen original");


        BufferedImage img2 = procImagen.escalaDeGrises();
        procImagen.mostrarImagen(img2, "Imagen en escala de grises");

        /*
        BufferedImage img3 = procImagen.aumentarBrillo(20);
        procImagen.mostrarImagen(img3, "Imagen con 20 de brillo");

        BufferedImage img4 = procImagen.disminuirBrillo(20);
        procImagen.mostrarImagen(img4, "Imagen con -20 de brillo");
         */


        /*
        BufferedImage img6 = procImagen.girarImagenEn180Grados();
        procImagen.mostrarImagen(img6, "Imagen girada en 180 grados");

        BufferedImage img5 = procImagen.girarImagenEn90Grados();
        procImagen.mostrarImagen(img5, "Imagen girada en 90 grados");

         */

        //BufferedImage img7 = procImagen.girarImagenEn270Grados();
        //procImagen.mostrarImagen(img7, "Imagen girada en 270 grados");

        //BufferedImage img8 = procImagen.redimensionarImagen(TipoTamainio.DI_MITAD);
        //procImagen.mostrarImagen(img8, "Imagen redimensionada a la mitad de tamaño");

        //BufferedImage img9 = procImagen.redimensionarImagen(600,700);
        //procImagen.mostrarImagen(img9, "Imagen redimensionada a 600x700");

        //BufferedImage img10 = procImagen.aplicarMarcoImagen(15, Color.GREEN);
        //procImagen.mostrarImagen(img10, "Imagen con marco de 15 pixeles y color verde");

        //BufferedImage img11 = procImagen.imagenNegativa();
        //procImagen.mostrarImagen(img11, "Imagen negativa");

    }
}
