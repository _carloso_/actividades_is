package registros.parcial3;

import estructurasLineales.ListaEstatica;
import estructurasNoLineales.Matriz2;

/**
 * Esta clase representa un grafo para una red bayesiana.
 * @author Cristian Omar Alvarado Rodriguez y Carlos Eduardo Olvera Mayorga.
 * @version 1.0
 */
public class GrafoRedBayesiana {

    protected ListaEstatica vertices; // lista de vertices del grafo
    protected Matriz2 aristas;

    public GrafoRedBayesiana(int cantidadNodos) {
        vertices = new ListaEstatica(cantidadNodos);
        aristas = new Matriz2(cantidadNodos, cantidadNodos, 0.0);
    }

    /**
     * Este método agrega un nuevo nodo al grafo.
     * @param nodo Es el nodo que se agregará al grafo.
     * @return Regresa <b>true</b> si el nodo se agregó correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregarNodo(NodoRedBayesiana nodo) {
        Integer numVertice = (Integer) vertices.buscar(nodo);
        if (numVertice == null) {
            int retornoOperacion = vertices.agregar(nodo);
            if (retornoOperacion >= 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método agrega una nueva arista al grafo.
     * @param origen Es el nodo origen de la arista.
     * @param destino Es el nodo destino de la arista.
     * @return Regresa <b>true</b> si la arista se agregó correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregarArista(Object origen, Object destino) {
        return agregarArista(origen, destino, 1.0);
    }

    /**
     * Este método agrega una nueva arista al grafo.
     * @param origen Es el nodo origen de la arista.
     * @param destino Es el nodo destino de la arista.
     * @param peso Es el peso de la arista.
     * @return Regresa <b>true</b> si la arista se agregó correctamente, <b>false</b> si no se pudo agregar.
     */
    public boolean agregarArista(Object origen, Object destino, double peso) {
        //Verificar que existan
        Integer indiceOrigen=(Integer)vertices.buscar(origen);
        Integer indiceDestino=(Integer)vertices.buscar(destino);

        if(indiceOrigen!=null && indiceDestino!=null){ //si existen ambos
            //creamos la arista
            return aristas.cambiar(indiceOrigen,indiceDestino,peso);
        }else{ //uno u otro no existe
            return false;
        }
    }

    /**
     * Este método muestra la información del grafo.
     */
    public void imprimirGrafo() {
        System.out.println("Vertices: ");
        vertices.imprimir();
        System.out.println("Aristas: ");
        aristas.imprimirXRenglones();
    }

    /**
     * Este método nos permite obtener un nodo del grafo.
     * @param nodo Es el nodo que se desea obtener.
     * @return Regresa el nodo que se desea obtener.
     */
    public NodoRedBayesiana obtenerNodo(Object nodo) {
        Integer indiceNodoBuscado = (Integer) vertices.buscar(nodo);
        NodoRedBayesiana nodoRetorno = (NodoRedBayesiana) vertices.obtener(indiceNodoBuscado);
        if (nodoRetorno != null) {
            return nodoRetorno;
        } else {
            return null;
        }
    }
}