package prueba;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;

public class PruebaListaEstatica {
    public static void main(String argumentos[]) {
        ListaEstatica lista = new ListaEstatica(6);

        lista.agregar("F");
        lista.agregar("D");
        lista.agregar("A");
        lista.agregar("S");
        lista.agregar("G");

        lista.imprimir();

        SalidaPorDefecto.consola("\n");

        lista.imprimirOI();

        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("Buscando el valor: D ->" + lista.buscar("D") + "\n");
        SalidaPorDefecto.consola("Buscando el valor: H ->" + lista.buscar("H") + "\n");

        SalidaPorDefecto.consola("Eliminando A: " + lista.eliminar("A") + "\n");
        SalidaPorDefecto.consola("Eliminando Z: " + lista.eliminar("Z") + "\n");

        lista.imprimir();

        SalidaPorDefecto.consola("Prueba de entrada: ");
        String cadenaEntrada = EntradaPorDefecto.consolaCadenas();
        SalidaPorDefecto.consola("Tecleaste: " + cadenaEntrada + "\n");

        SalidaPorDefecto.consola("---SUBLISTA---" + "\n");
        ListaEstatica sublista = (ListaEstatica) lista.subLista(-1,4);
        if (sublista != null) {
            sublista.imprimir();
        } else {
            SalidaPorDefecto.consola("ERROR" + "\n");
        }

        SalidaPorDefecto.consola("---OBTENER---" + "\n");
        if (lista.obtener(1) != null) {
            SalidaPorDefecto.consola("Obtener indice: 1 ->" + lista.obtener(1) + "\n");
        } else {
            SalidaPorDefecto.consola("ERROR" + "\n");
        }

        SalidaPorDefecto.consola("---CAMBIAR 1---" + "\n");
        SalidaPorDefecto.consola("Cambiar indice 5 por la letra Z -> "+"\n");
        if (lista.cambiar(3, "Z") != false) {
            lista.imprimir();
        }
        else{
            SalidaPorDefecto.consola("ERROR"+ "\n");
        }

        SalidaPorDefecto.consola("---CAMBIAR 2---" + "\n");
        SalidaPorDefecto.consola("Cambiar letra Z por la letra H -> "+"\n");
        if (lista.cambiar("Z", "H", 3) != false) {
            lista.imprimir();
        }
        else{
            SalidaPorDefecto.consola("ERROR"+ "\n");
        }

        SalidaPorDefecto.consola("--ELIMINAR---" + "\n");
        SalidaPorDefecto.consola("Eliminar indice 2-> "+"\n");
        lista.eliminar(2);
        lista.imprimir();

        SalidaPorDefecto.consola("--INVERTIR---" + "\n");
        lista.invertir();
        lista.imprimir();

        SalidaPorDefecto.consola("--CLONAR---" + "\n");
        lista.clonar();
        lista.imprimir();

        SalidaPorDefecto.consola("--BUSCAR VALOR---" + "\n");
        SalidaPorDefecto.consola("Buscar valor H ->" + lista.buscarValores("H") + "\n");

        SalidaPorDefecto.consola("---VACIAR---" + "\n");
        lista.vaciar();
        lista.imprimir();

        SalidaPorDefecto.consola("---REDIMENSIONAR---" + "\n");
        lista.redimensionar(3);
        lista.imprimir();



    }




}
