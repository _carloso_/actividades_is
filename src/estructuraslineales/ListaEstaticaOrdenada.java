package estructurasLineales;

import entradaSalida.SalidaPorDefecto;
import herramientas.comunes.TipoOrden;

/**
 * Esta clase representa un TDA de una lista ordenada de elementos.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class ListaEstaticaOrdenada extends ListaEstatica {

    protected int MAXIMO;
    protected int ultimo;
    protected Object[] datos;
    protected TipoOrden tipoOrden;

    public ListaEstaticaOrdenada(int maximo, TipoOrden tipoOrden) {
        super(maximo);
        MAXIMO = maximo;
        datos = new Object[MAXIMO];
        ultimo = -1;
        this.tipoOrden = tipoOrden;
    }


    /**
     * Este metodo agrega un elemento a la lista ordenada de forma ascendente o descendente segun el tipo de orden.
     *
     * @param valor Es el valor que se desea agregar a la lista ordenada.
     * @return Regresa el indice en donde se agrego el valor.
     */
    @Override
    public int agregar(Object valor) {
        if (!llena()) {
            if (tipoOrden == TipoOrden.ASC) {
                return agregarAscendente(valor);
            } else {
                return agregarDesendente(valor);
            }
        } else { // Esta llena
            return -1;
        }
    }

    /**
     * Este metodo agrega un elemento a la lista ordenada de forma ascendente.
     *
     * @param valor Es el valor que se desea agregar a la lista ordenada.
     * @return Regresa el indice en donde se agrego el valor.
     */
    private int agregarAscendente(Object valor) {
        Integer posicion = 0;
        while (posicion <= ultimo && (valor.toString().compareTo(datos[posicion].toString())) > 0) {
            posicion++;
        }
        if (posicion > ultimo || (valor.toString().compareTo(datos[posicion].toString())) < 0) {
            ultimo = ultimo + 1;
            for (int moviminto = ultimo; moviminto > posicion; moviminto--) {
                this.datos[moviminto] = this.datos[moviminto - 1];
            }
            this.datos[posicion] = valor;
            return posicion;
        } else { // Si lo encontro
            return -1;
        }
    }

    /**
     * Este metodo agrega un elemento a la lista ordenada de forma descendente.
     *
     * @param valor Es el valor que se desea agregar a la lista ordenada.
     * @return Regresa el indice en donde se agrego el valor.
     */
    private int agregarDesendente(Object valor) {
        Integer posicion = 0;
        while (posicion <= ultimo && (valor.toString().compareTo(datos[posicion].toString())) < 0) {
            posicion++;
        }
        if (posicion > ultimo || (valor.toString().compareTo(datos[posicion].toString())) > 0) {
            ultimo = ultimo + 1;
            for (int moviminto = ultimo; moviminto > posicion; moviminto--) {
                this.datos[moviminto] = this.datos[moviminto - 1];
            }
            this.datos[posicion] = valor;
            return posicion;
        } else { // Si lo encontro
            return -1;
        }
    }

    /**
     * Este metodo busca un elemento en la lista ordenada de forma ascendente o descendente segun el tipo de orden.
     * @param valor Es el valor que se desea buscar en la lista ordenada.
     * @return Regresa el indice <b>mapeado</b> del valor buscado.
     */
    @Override
    public Object buscar(Object valor) {
        if (tipoOrden == TipoOrden.DESC) {
            Integer posicion = 0;
            while (posicion <= ultimo && (valor.toString().compareTo(datos[posicion].toString())) < 0) {
                posicion++;
            }
            if (posicion > ultimo || (valor.toString().compareTo(datos[posicion].toString())) > 0) {
                return (-1) * (posicion + 1);
            } else {
                return posicion + 1;
            }
        } else {
            return buscarAscendente(valor);
        }
    }

    /**
     * Este metodo busca un elemento en la lista ordenada de forma ascendente.
     * @param valor Es el valor que se desea buscar en la lista ordenada.
     * @return Regresa el indice <b>mapeado</b> del valor buscado.
     */
    // no terminado
    private Object buscarAscendente(Object valor) {
        Integer posicion = ultimo;
        while (posicion >= 0 && (valor.toString().compareTo(datos[posicion].toString())) > 0) {
            posicion--;
        }
        if (posicion < 0 || (valor.toString().compareTo(datos[posicion].toString())) < 0) {
            return (-1) * (posicion + 1);
        } else {
            return posicion + 1;
        }
    }

    @Override
    public boolean llena() {
        if (ultimo == MAXIMO - 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object obtener(int indice) {
        if (enRango(indice)) {
            return null;
        } else {
            return this.datos[indice];
        }
    }


    @Override
    public void imprimir() {
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            SalidaPorDefecto.consola(posicion + "-> " + datos[posicion] + "\n");
        }
    }

    @Override
    public int numeroElementos() {
        return ultimo + 1;
    }


    @Override
    public boolean cambiar(Object valor, Object nuevoValor, int numOcurrencias) {
        if (numOcurrencias == 1) {
            if (tipoOrden == TipoOrden.DESC) {
                return cambiarDesendente(valor, nuevoValor);
            } else {
                return cambiarAscendente(valor, nuevoValor);
            }
        } else {
            return false;
        }
    }

    /**
     * Este metodo cambia un elemento de la lista ordenada de forma ascendente.
     * @param valor Es el valor que se desea cambiar.
     * @param nuevoValor Es el nuevo valor que se desea asignar al valor buscado.
     * @return Regresa <b>true</b> si se cambio el valor, <b>false</b> en caso contrario.
     */
    private boolean cambiarAscendente(Object valor, Object nuevoValor) {
        int posicion = 0;
        while (posicion <= ultimo && (valor.toString().compareTo(datos[posicion].toString())) > 0) {
            posicion++;
        }
        if (posicion > ultimo || (valor.toString().compareTo(datos[posicion].toString())) < 0) {
            return false;
        } else {
            eliminar(datos[posicion]);
            agregar(nuevoValor);
            return true;
        }
    }

    /**
     * Este metodo cambia un elemento de la lista ordenada de forma descendente.
     * @param valor Es el valor que se desea buscar en la lista ordenada y cambiar.
     * @param nuevoValor Es el nuevo valor que se desea poner en la lista ordenada.
     * @return Regresa <b>true</b> si se pudo cambiar el valor, <b>false</b> en caso contrario.
     */
    private boolean cambiarDesendente(Object valor, Object nuevoValor) {
        int posicion = 0;
        while (posicion <= ultimo && (valor.toString().compareTo(datos[posicion].toString())) < 0) {
            posicion++;
        }
        if (posicion > ultimo || (valor.toString().compareTo(datos[posicion].toString())) > 0) {
            return false;
        } else {
            eliminar(datos[posicion]);
            agregar(nuevoValor);
            return true;
        }
    }

    /**
     * Este metodo elimina un elemento de la lista ordenada de forma ascendente o descendente segun el tipo de orden.
     *
     * @param valor Es el valor que se desea eliminar de la lista ordenada.
     * @return Regresa el indice en donde se elimino el valor.
     */
    @Override
    public Object eliminar(Object valor) {
        if (tipoOrden == TipoOrden.ASC) {
            return eliminarAscendente(valor);
        } else {
            return eliminarDesendente(valor);
        }
    }

    /**
     * Este metodo elimina un elemento de la lista ordenada de forma descendente y reordena la lista.
     *
     * @param valor Es el valor que se desea eliminar de la lista ordenada.
     * @return Regresa el valor que se elimino de la lista.
     */
    private Object eliminarDesendente(Object valor) {
        int posicion = 0;
        while (posicion <= ultimo && (valor.toString().compareTo(datos[posicion].toString())) < 0) {
            posicion++;
        }
        if (posicion > ultimo || (valor.toString().compareTo(datos[posicion].toString())) > 0) {
            return null;
        } else { // Si lo encontro
            Object valorEliminado = datos[posicion];
            for (int moviminto = posicion; moviminto < ultimo; moviminto++) {
                this.datos[moviminto] = this.datos[moviminto + 1];
            }
            ultimo = ultimo - 1;
            return valorEliminado;
        }
    }

    /**
     * Este metodo elimina un elemento de la lista ordenada de forma ascendente y reordena la lista.
     *
     * @param valor Es el valor que se desea eliminar de la lista ordenada.
     * @return Regresa el valor que se elimino de la lista.
     */
    private Object eliminarAscendente(Object valor) {
        int posicion = 0;
        while (posicion <= ultimo && (valor.toString().compareTo(datos[posicion].toString())) > 0) {
            posicion++;
        }
        if (posicion > ultimo || (valor.toString().compareTo(datos[posicion].toString())) < 0) {
            return null;
        } else { // Si lo encontro
            Object valorEliminado = datos[posicion];
            for (int moviminto = posicion; moviminto < ultimo; moviminto++) {
                this.datos[moviminto] = this.datos[moviminto + 1];
            }
            ultimo = ultimo - 1;
            return valorEliminado;
        }
    }

    @Override
    protected boolean enRango(int indice){
        if(indice >=0 && indice <= ultimo){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean cambiar(int indice, Object valor) {
        if (enRango(indice)) {
            eliminar(datos[indice]);
            agregar(valor);
            return true;
        } else {
            return false;
        }
    }

    // TODO: Checar porque los valores de lista2 son null.
    @Override
    public boolean agregarLista(ListaAlmacenamiento lista2) {
        if (lista2 instanceof ListaEstaticaOrdenada) {
            ListaEstaticaOrdenada listaTemp = (ListaEstaticaOrdenada) lista2;
            for (int pos = 0; pos <= listaTemp.ultimo; pos++) {
                Object valor =  listaTemp.obtener(pos);
                //SalidaPorDefecto.consola( valor + " ");
                agregar(valor);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void invertir() {
        for (int i = 0; i < ultimo / 2; i++) {
            Object temp = datos[i];
            datos[i] = datos[ultimo - i];
            datos[ultimo - i] = temp;
        }
        if (tipoOrden == TipoOrden.ASC) {
            tipoOrden = TipoOrden.DESC;
        } else {
            tipoOrden = TipoOrden.ASC;
        }
    }

    @Override
    public void rellenar(Object valor){
        if (valor instanceof Integer) { // Si es un numero entero
            int nuevoValor = (Integer) valor;
            if (nuevoValor > 0) { // Si es positivo
                if (tipoOrden == TipoOrden.ASC) {
                    // Rellenar la lista con valores de 0 hasta nuevoValor
                    for (int pos = 1; pos <= nuevoValor; pos++) {
                        agregar(pos);
                    }
                } else {
                    // rellenar de nuevoValor hasta 1
                    for (int pos = nuevoValor; pos > 0; pos--) {
                        agregar(pos);
                    }
                }
            } else { // Si es negativo
                if (tipoOrden == TipoOrden.ASC) {
                    // rellenar desde nuevoValor hasta -1 (invertir)
                    for (int pos = nuevoValor; pos < 0; pos++) {
                        agregar(pos);
                    }
                    invertir();
                } else {
                    // rellenar desde -1 hasta nuevoValor
                    for (int pos = nuevoValor; pos < 0; pos++) {
                        agregar(pos);
                    }
                    invertir();
                }
            }
        } else { // es una cadena
            if (valor.toString().length() == 1) { // Si es una letra
                char letra = (char) valor.toString().charAt(0);
                int nuevoValor = (int) letra;
                if (tipoOrden == TipoOrden.ASC) {
                    // rellenar desde A hasta nuevoValor
                    for (int pos = 65; pos <= nuevoValor; pos++) {
                        // Convertir a letra
                        char letraTemp = (char) pos;
                        agregar(letraTemp);
                    }
                } else {
                    // rellenar desde nuevoValor hasta A
                    for (int pos = nuevoValor; pos >= 65; pos--) {
                        // Convertir a letra
                        char letraTemp = (char) pos;
                        agregar(letraTemp);
                    }
                }
            } else { // Si es una cadena de mas de una letra
                SalidaPorDefecto.consola("No se puede rellenar con" +
                        " una cadena de mas de una letra");
            }
        }
    }

    public ListaAlmacenamiento arregloDesordenado(){
        ListaAlmacenamiento listaDesordenada = new ListaEstatica(numeroElementos());
        // rellenar la lista con los valores aleatorios
        for (int pos = 0; pos <= ultimo; pos++) {
            // generar letras mayusculas aleatorias de la A a la Z
            char letraAleatoria = (char) (Math.random() * (90 - 65) + 65);
            listaDesordenada.agregar(letraAleatoria);
        }
        return listaDesordenada;
    }
}