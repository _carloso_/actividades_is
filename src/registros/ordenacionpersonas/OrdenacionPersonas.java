package registros.ordenacionpersonas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import herramientas.generales.Utilerias;

/**
 * Esta clase nos permite ordenar una lista de personas por nombre o por edad.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class OrdenacionPersonas {

    protected ListaEstatica personas; // para guardar a las personas

    public OrdenacionPersonas(int numPersonas) {
        personas = new ListaEstatica(numPersonas);
    }

    // agregar persona
    public boolean agregarPersona(Persona persona) {
        int valorRetornoAgregar = personas.agregar(persona);
        if (valorRetornoAgregar != -1) {
            return true;
        } else {
            return false;
        }
    }

    // Ordenar por edad mayor a menor (Criterio de ordenación: Edad)
    // Desventaja: Complejidad O(N^2)
    // Se podría mejorar la complejidad con algún otro algoritmo de ordenación
    // por ejemplo: QuickSort, MergeSort, HeapSort, etc, que tienen una complejidad O(N log N) complejidad logarítmica.
    private void ordenarPorEdad() {
        if (!personas.vacia()) {
            for (int pos1 = 0; pos1 < personas.numeroElementos(); pos1++) {
                for (int pos2 = pos1 + 1; pos2 < personas.numeroElementos(); pos2++) {
                    Persona persona1 = (Persona) personas.obtener(pos1);
                    Persona persona2 = (Persona) personas.obtener(pos2);
                    if (persona1.getEdad() > persona2.getEdad()) {
                        personas.intercambiar(pos1, pos2);
                    } else if (persona1.getEdad() == persona2.getEdad()) {
                        if (persona1.getNombre().compareToIgnoreCase(persona2.getNombre()) > 0) {
                            personas.intercambiar(pos1, pos2);
                        }
                    }
                }
            }
        } else {
            SalidaPorDefecto.consola("No hay personas\n");
        }
    }

    // orden natural o por nombre: Criterio de ordenación: Nombre
    // Complejidad O(N^2)
    private void ordenarPorNombre() {
        if (!personas.vacia()) {
            for (int pos1 = 0; pos1 < personas.numeroElementos(); pos1++) {
                for (int pos2 = pos1 + 1; pos2 < personas.numeroElementos(); pos2++) {
                    Persona persona1 = (Persona) personas.obtener(pos1);
                    Persona persona2 = (Persona) personas.obtener(pos2);
                    if (persona1.getNombre().compareToIgnoreCase(persona2.getNombre()) > 0) {
                        personas.intercambiar(pos1, pos2);
                    } else if (persona1.getNombre().compareToIgnoreCase(persona2.getNombre()) == 0) {
                        if (persona1.getEdad() > persona2.getEdad()) {
                            personas.intercambiar(pos1, pos2);
                        }
                    }
                }
            }
        } else {
            SalidaPorDefecto.consola("No hay personas\n");
        }
    }

    // ordenación por nombre y edad, a través de su método ToString
    // Complejidad O(N^2)
    public void ordenacionToString() {
        if (!personas.vacia()) {
            for (int pos1 = 0; pos1 < personas.numeroElementos(); pos1++) {
                for (int pos2 = pos1 + 1; pos2 < personas.numeroElementos(); pos2++) {
                    Persona persona1 = (Persona) personas.obtener(pos1);
                    Persona persona2 = (Persona) personas.obtener(pos2);
                    if (Utilerias.compararObjetos(persona1, persona2) > 0) {
                        personas.intercambiar(pos1, pos2);
                    } else if (Utilerias.compararObjetos(persona1, persona2) == 0) {
                        if (persona1.getEdad() > persona2.getEdad()) {
                            personas.intercambiar(pos1, pos2);
                        }
                    }
                }
            }
        } else {
            SalidaPorDefecto.consola("No hay personas\n");
        }
    }

    // ordenar por edad o por nombre
    // criterios: 1. Edad 2. Nombre
    public void ordenar(TipoCriterio criterio) {
        if (criterio == TipoCriterio.EDAD) {
            ordenarPorEdad();
        } else {
            ordenarPorNombre();
        }
    }

    // Mostrar Personas
    public void mostrarPersonas() {
        personas.imprimir();
    }
}