package estructurasNoLineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaEstatica;
import estructurasLineales.PilaDinamica;
import estructurasLineales.PilaEstatica;
import estructurasNoLineales.auxiliares.Vertice;

public class GrafoDinamico {
    protected ListaDinamica listaAdyacencia; //guarda las listas secundarias (vértices orígenes hacia losdestinos).

    public GrafoDinamico(){
        listaAdyacencia=new ListaDinamica();
    }

    private ListaDinamica buscarVerticeListaAdyacencia(Object contenidoVertice){
        //Recorrer la lista de Adycacencia para buscarlo solo en la primera posicion de cada sublista

        listaAdyacencia.inicializarIterador();

        while(listaAdyacencia.hayNodos()==true){ //recorremos la lista
            //sacamos un elemento de la lista, es decir una sublista
            ListaDinamica subListaCadaVertice=(ListaDinamica) listaAdyacencia.obtenerNodo();
            //checar si el primer elemento de cada sublista es igual al que nos dan como argumento
            Vertice primerVerticeCadasublista=(Vertice)subListaCadaVertice.verPrimero();
            //checo si este primero es igual al que me dan como argumento
            if(primerVerticeCadasublista.toString().equalsIgnoreCase(contenidoVertice.toString())==true){ //sí está
                return subListaCadaVertice;
            }
        }
        //en caso que nunca entre el ciclo o al if, quiere decir que no lo encontramos
        return null;
    }

    public boolean agregarVertice(Object contenido){
        // Checar si existe o no ese vértice
        // Para esto debemos buscar en la primera posicion de cada sublista
        ListaDinamica sublistaVerticeAEncontrar=buscarVerticeListaAdyacencia(contenido);

        if(sublistaVerticeAEncontrar==null){ //no existe, podemos agregarlo
            ListaDinamica sublistaVerticeNuevo=new ListaDinamica();  //creamos la sublista del vértice nuevo
            Vertice verticeNuevo=new Vertice();  //creamos un nuevo vértice
            verticeNuevo.setContenido(contenido);
            sublistaVerticeNuevo.agregar(verticeNuevo); //agregamos el vértice nuevo a su propia sublista

            //agregamos la sublista a la lista de adycacencia
            if(listaAdyacencia.agregar(sublistaVerticeNuevo)==0){ //si se pudo agregar
                return true;
            }else{ //no se pudo agregar
                return false;
            }
        }else{ //ya existe
            return false;
        }
    }

    public boolean agregarArista(Object origen, Object destino) {
        //Se ocupa saber si el origen y el destino existen
        ListaDinamica subListaOrigen = buscarVerticeListaAdyacencia(origen);
        ListaDinamica subListaDestino = buscarVerticeListaAdyacencia(destino);

        //checar si son existentes
        if (subListaOrigen != null && subListaDestino != null) { //existen
            //sacamos el primer vértice de la sublista del destino
            Vertice verticeDestino = (Vertice) subListaDestino.verPrimero();
            //agregar este vértice al final de la sublista del origen
            if (subListaOrigen.agregar(verticeDestino) == 0) { //si se pudo agregar
                return true;
            } else { //no se pudo agregar
                return false;
            }
        } else { //no existen
            return false;
        }
    }

    public void imprimir(){
        //Recorrer la lista de adyacencia y sacar cada una de las sublistas
        listaAdyacencia.inicializarIterador();

        while(listaAdyacencia.hayNodos()==true){
            ListaDinamica sublistaCadaVertice=(ListaDinamica) listaAdyacencia.obtenerNodo();
            //imprimir cada sublista
            sublistaCadaVertice.imprimir();
            SalidaPorDefecto.consola("\n");
        }
    }

    //paso 3 de recorrido en profundidad
    private void enpilarYMarcarVerticesAdyacentes(ListaDinamica sublistaVerticeActual, PilaDinamica pila, ListaDinamica marcados){

        sublistaVerticeActual.inicializarIterador();
        sublistaVerticeActual.obtenerNodo(); //nos brincamos el primero, que es el vértice actual

        //empezaremos a partir del segundo, que son los vecinos del primero (vértice actual)
        while(sublistaVerticeActual.hayNodos()==true){
            //obtener elemento de la sublista (vecino)
            Vertice verticeVecino=(Vertice)sublistaVerticeActual.obtenerNodo();
            //checar si está marcado, porque vecino si es
            if(marcados.buscar(verticeVecino.getContenido())==null){ //no está, por lo tanto no está marcado
                //enpilamos
                pila.poner(verticeVecino);
                //marcamos
                marcados.agregar(verticeVecino);
            }
        }
    }

    //Recorrido en profundidad
    public ListaDinamica recorridoProfunidad(Object origen){
        ListaDinamica recorridoP=new ListaDinamica();
        PilaDinamica pila=new PilaDinamica();
        ListaDinamica marcados=new ListaDinamica();

        //Pasos:

        //0. Validar la existencia del origen.
        ListaDinamica sublistaOrigen=buscarVerticeListaAdyacencia(origen);
        if(sublistaOrigen!=null){ //existe
            //1.- Partir de un vértice origen. Este vértice se marca y se mete en una pila.
            //sacar el vértice Oirgen de la sublista
            Vertice verticeOrigen=(Vertice)sublistaOrigen.verPrimero();
            //marcamos este vértice origen
            marcados.agregar(verticeOrigen);
            //meter el origen en la pila
            pila.poner(verticeOrigen);

            while(pila.vacio()==false) {
                //2.- Mientras existan vértices en la pila, se van a extraer (de uno por uno) y se procesarán (imprimir).
                Vertice verticeActual=(Vertice)pila.quitar(); //sacamos de pila
                recorridoP.agregar(verticeActual.getContenido());//agregamos en la salida el contenido del vértice

                //3.- Los vértices adyacentes (vecinos directos) no marcados y que están enlazados al nodo que actualmente
                // se procesa (el paso 2) se marcan y se meten en la pila.
                //Obtener la sublista del vértice actual
                ListaDinamica subListaVerticeActual=buscarVerticeListaAdyacencia(verticeActual.getContenido());
                enpilarYMarcarVerticesAdyacentes(subListaVerticeActual, pila, marcados);
            }
        }else{ //no existe
            return null;
        }
        return recorridoP;
    }

    /*public double algoritmoKruska(){
        double costoTotal = 0.0;
        listaAdyacencia.inicializarIterador();
        while(listaAdyacencia.hayNodos()==true){
            ListaDinamica sublistaCadaVertice=(ListaDinamica) listaAdyacencia.obtenerNodo();
            Vertice primero = (Vertice) sublistaCadaVertice.verPrimero();
            int posicionOrigen = primero.getNumVertice()-1;
            sublistaCadaVertice.inicializarIterador();
            sublistaCadaVertice.obtenerNodo();
            while (sublistaCadaVertice.hayNodos() == true){
                Vertice verticeCadaSublista = (Vertice) sublistaCadaVertice.obtenerNodo();
                int posicionDestino = verticeCadaSublista.getNumVertice()-1;
                costoTotal += matrizAdyacencia.obtenerValor(posicionOrigen,posicionDestino);
            }
        }
        return costoTotal;
    }*/

    public void algoritmoKruska2(){
        listaAdyacencia.inicializarIterador();
        int cont = 0;
        while(listaAdyacencia.hayNodos()==true){
            ListaDinamica sublistaCadaVertice=(ListaDinamica) listaAdyacencia.obtenerNodo();
            Vertice primero = (Vertice) sublistaCadaVertice.verPrimero();
            sublistaCadaVertice.inicializarIterador();
            while (sublistaCadaVertice.hayNodos() == true){
                Vertice verticeCadaSublista = (Vertice) sublistaCadaVertice.obtenerNodo();
                int posicionDestino = verticeCadaSublista.getNumVertice()-1;
                if (primero == verticeCadaSublista){
                    int arista = 1;
                    SalidaPorDefecto.consola(primero+"-"+verticeCadaSublista+": " + arista +"\n");
                }
                else{
                    while (cont <= sublistaCadaVertice.contarCantidadNodos()) {
                        cont++;
                        ListaDinamica sublistaposicionSiguiente=(ListaDinamica) listaAdyacencia.obtener(cont);
                        Vertice posicionSiguiente = (Vertice) sublistaCadaVertice.obtener(cont);
                        if (posicionSiguiente == verticeCadaSublista){
                            int camino = cont;
                            int camino2 = verticeCadaSublista.getNumVertice()-1;
                            int distancia = camino+camino2;
                            SalidaPorDefecto.consola(primero+"-"+verticeCadaSublista+": " + distancia +"\n");
                            break;
                        }
                    }
                }

            }
        }
    }
}
