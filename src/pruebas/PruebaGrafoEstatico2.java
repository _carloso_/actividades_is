package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasnolineales.GrafoEstatico;

public class PruebaGrafoEstatico2 {
    public static void main(String[] args) {
        GrafoEstatico grafo = new GrafoEstatico(5);

        // grafo no conexo: vertices
        grafo.agregarVertice("A");
        grafo.agregarVertice("B");
        grafo.agregarVertice("C");
        grafo.agregarVertice("D");
        grafo.agregarVertice("E");

        // grafo no conexo: aristas
        grafo.agregarArista("A", "B");
        grafo.agregarArista("B", "A");
        grafo.agregarArista("B", "C");
        grafo.agregarArista("C", "B");
        grafo.agregarArista("D", "E");
        grafo.agregarArista("E", "D");

        SalidaPorDefecto.consola("Grafo no conexo y no dirigido: \n" );
        grafo.imprimir();

        SalidaPorDefecto.consola("\nRecorrido en profundidad origen 'A': \n" );
        grafo.recorridoProfunidad("A").imprimir();

        SalidaPorDefecto.consola("\n\nComponentes conexas del grafo: " + "\n");
        grafo.mostrarComponentesConexasNoDirigido("A");

        SalidaPorDefecto.consola("\n\n*Hay ruta entre 'A' y 'E'?: " + grafo.hayRuta("A", "E"));
        SalidaPorDefecto.consola("\n*Hay ruta entre 'C' y 'A'?: " + grafo.hayRuta("C", "A"));
    }
}
