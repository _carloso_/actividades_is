package estructurasLineales;

import herramientas.comunes.TipoPrioridad;
/**
 * Clase que representa un TDA de una cola de prioridad.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class ColaEstaticaDePrioridad extends ColaEstatica {

    private int[] prioridades;
    private TipoPrioridad tipoPrioridad;

    public ColaEstaticaDePrioridad(int maximo, TipoPrioridad tipoPrioridad) {
       super(maximo);
       prioridades = new int[maximo];
       this.tipoPrioridad = tipoPrioridad;
    }

    /**
     * Este método inserta un elemento en la cola de prioridad con la prioridad indicada.
     * @param valor Es el valor que se insertará en la cola de prioridad.
     * @param prioridad Es la prioridad que tiene el valor que se insertará en la cola de prioridad.
     * @return Retorna true si se pudo insertar el valor en la cola de prioridad, false sino se pudo insertar.
     */
    public boolean insertar(Object valor, int prioridad) {
        boolean agregado = poner(valor);
        if (agregado) {
            prioridades[ultimo] = prioridad;
            if (tipoPrioridad == TipoPrioridad.MENOR_A_MAYOR) {
                ordenarDeMenorAMayorPriorida();
            } else {
                ordenarDeMayorAMenorPrioridad();
            }
        }
        return agregado;
    }

    /**
     * Este método ordena los elementos de la colaPrioridad por su prioridad (De menor a mayor).
     */
    private void ordenarDeMenorAMayorPriorida() {
        for (int pos = 0; pos < ultimo; pos++) { // recorre la cola
            for (int posSig = pos + 1; posSig <= ultimo; posSig++) { // segundo for para recorrer la cola desde la posición siguiente
                if (prioridades[pos] > prioridades[posSig]) { // si la prioridad de la posición es mayor que la de la posición siguiente
                    int prioActual = prioridades[pos]; // se guarda la prioridad de la posición
                    prioridades[pos] = prioridades[posSig]; // se cambia la prioridad de la posición por la de la posición siguiente
                    prioridades[posSig] = prioActual; // se cambia la prioridad de la posición siguiente por la de la posición actual
                    Object valorActual = datos[pos]; // se guarda el valor de la posición actual
                    datos[pos] = datos[posSig]; // se cambia el valor de la posición actual por el de la posición siguiente
                    datos[posSig] = valorActual; // se cambia el valor de la posición siguiente por el valor actual.
                }
            }
        }
    }

    /**
     * Este método ordena los elementos de la colaPrioridad por su prioridad (De mayor a menor).
     */
    private void ordenarDeMayorAMenorPrioridad() {
        for (int pos = 0; pos < ultimo; pos++) { // recorre la cola
            for (int posSig = pos + 1; posSig <= ultimo; posSig++) { // segundo for para recorrer la cola desde la posición siguiente
                if (prioridades[pos] < prioridades[posSig]) { // si la prioridad de la posición es menor que la de la posición siguiente
                    int prioActual = prioridades[pos]; // se guarda la prioridad de la posición
                    prioridades[pos] = prioridades[posSig]; // se cambia la prioridad de la posición por la de la posición siguiente
                    prioridades[posSig] = prioActual; // se cambia la prioridad de la posición siguiente por la de la posición actual
                    Object valorActual = datos[pos]; // se guarda el valor de la posición actual
                    datos[pos] = datos[posSig]; // se cambia el valor de la posición actual por el de la posición siguiente
                    datos[posSig] = valorActual; // se cambia el valor de la posición siguiente por el valor actual.
                }
            }
        }
    }
}