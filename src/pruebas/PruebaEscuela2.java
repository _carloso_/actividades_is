package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import registros.escuelas.Alumno2;
import registros.escuelas.Escuela2;

public class PruebaEscuela2 {
    public static void main(String[] args) {
        Escuela2 escuela = new Escuela2("Escuela Profesional de Datos");

        ListaDinamica calificaciones1 = new ListaDinamica();
        calificaciones1.agregar(8.9);
        calificaciones1.agregar(5.2);
        calificaciones1.agregar(9.1);
        calificaciones1.agregar(3.1);
        Alumno2 alumno1 = new Alumno2("90945654","Pedro", "Lopez",23, calificaciones1);

        ListaDinamica calificaciones2 = new ListaDinamica();
        calificaciones2.agregar(9.2);
        calificaciones2.agregar(5.1);
        calificaciones2.agregar(8.0);
        Alumno2 alumno2 = new Alumno2("90945651","Maria", "Mendez",21, calificaciones2);

        ListaDinamica calificaciones3 = new ListaDinamica();
        calificaciones3.agregar(9.2);
        calificaciones3.agregar(6.5);
        calificaciones3.agregar(8.5);
        calificaciones3.agregar(3.2);
        calificaciones3.agregar(9.4);
        Alumno2 alumno3 = new Alumno2("90945656","Rosa", "Lira",20,calificaciones3);

        escuela.agregarAlumno(alumno1);
        escuela.agregarAlumno(alumno2);
        escuela.agregarAlumno(alumno3);

        SalidaPorDefecto.consola("La escuela "+ escuela.getNombre() + " tiene los siguientes alumnos: \n");
        escuela.imprimirListadoAlumnos();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("El promedio del alumno 90945656 es "+
                escuela.calcularPromedioAlumno("90945656")+ "\n");


        SalidaPorDefecto.consola("La escuela "+ escuela.getNombre() + " tiene los siguientes alumnos: \n");
        escuela.imprimirListadoDatosAlumnos();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("El promedio del alumno 90945656 es "+
                escuela.calcularPromedioAlumno("90945656")+ "\n");

    }
}
