package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaEstatica;

public class PruebaColaEstatica {
    public static void main(String[] args) {
        ColaEstatica cola = new ColaEstatica(5);
        cola.poner("F");
        cola.poner("J");
        cola.poner("A");
        cola.poner("K");

        cola.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("Atendiendo al siguiente: " + cola.quitar() + "\n");
        SalidaPorDefecto.consola("Atendiendo al siguiente: " + cola.quitar() + "\n");
        SalidaPorDefecto.consola("Atendiendo al siguiente: " + cola.quitar() + "\n");

        cola.imprimir();

        /*
        cola.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("Buscar K: " + cola.buscar("K") + "\n");
        SalidaPorDefecto.consola("Buscar: " + cola.buscar("fjd") + "\n");

        SalidaPorDefecto.consola("\n");
        cola.imprimir();
         */
    }
}
