package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import registros.competenciasciclismo.Ciclista;
import registros.competenciasciclismo.ControlCompetenciasCiclismo;
import registros.competenciasciclismo.EventoCiclismo;

public class PruebaCiclistas {
    public  static void  main(String []argumentos){
        ControlCompetenciasCiclismo competencias=new ControlCompetenciasCiclismo(5,3,4);

        //datos vacíos
        competencias.imprimirDatosCompetencias();

        SalidaPorDefecto.consola("\n");

        EventoCiclismo evento1=new EventoCiclismo("Carrera Centenario","Las Palmas");
        competencias.agregarEvento(evento1);
        EventoCiclismo evento2=new EventoCiclismo("Carrera Nuevo Milenio","Las Cumbres");
        competencias.agregarEvento(evento2);

        Ciclista ciclista1=new Ciclista(1,"Pedro Perez",30,'M');
        competencias.agregarCiclista(ciclista1);
        Ciclista ciclista2=new Ciclista(9,"Rosa Lopez",29,'F');
        competencias.agregarCiclista(ciclista2);

        competencias.agregarAnio(2005);
        competencias.agregarAnio(2009);
        competencias.agregarAnio(2010);
        competencias.agregarAnio(2020);

        competencias.imprimirDatosCompetencias();

        SalidaPorDefecto.consola("\n");

        competencias.agregarKilometrosRecorridos("Carrera Centenario",9,2005,90);
        competencias.agregarKilometrosRecorridos("Carrera Centenario",9,2009,85);
        competencias.agregarKilometrosRecorridos("Carrera Centenario",9,2020,80);
        competencias.agregarKilometrosRecorridos("Carrera Nuevo Milenio",9,2009,81);


        competencias.agregarKilometrosRecorridos("Carrera Centenario",1,2005,70);
        competencias.agregarKilometrosRecorridos("Carrera Centenario",1,2009,75);
        competencias.agregarKilometrosRecorridos("Carrera Centenario",1,2020,70);
        competencias.agregarKilometrosRecorridos("Carrera Nuevo Milenio",1,2009,71);

        competencias.imprimirDatosCompetencias();

        SalidaPorDefecto.consola("\n");

        ListaEstatica aniosPedro= new ListaEstatica(3);
        aniosPedro.agregar(2005);
        aniosPedro.agregar(2009);
        aniosPedro.agregar(2010);
        SalidaPorDefecto.consola("Los kilómetros recorridos de Pedro en Carrera Centenario en los años 2005, 2009, 2010 son: "
                + competencias.kilometrosXEventoXCiclista("Carrera Centenario",1,aniosPedro));
    }
}
