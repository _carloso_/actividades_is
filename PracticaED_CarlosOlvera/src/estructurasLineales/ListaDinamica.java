package estructurasLineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.auxiliares.*;
import estructurasNoLineales.Matriz2;

import static estructurasLineales.TipoTabla.*;

public class ListaDinamica implements ListaAlmacenamiento{
    protected Nodo primero;
    protected Nodo ultimo;
    protected Nodo iterador;


    public ListaDinamica(){
        primero=null;
        ultimo=null;
        iterador=null;
    }

    @Override
    public boolean vacia(){
        if(primero==null){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int agregar(Object valor){
        Nodo nuevoNodo=new Nodo(valor); //paso 1
        if(nuevoNodo!=null){ //hay espacio
            if(vacia()==true){  //a)
                primero=nuevoNodo; //paso 2
                ultimo=nuevoNodo;
            }else{ //b) y c)
                ultimo.setNodoDer(nuevoNodo); //paso 2
                ultimo=nuevoNodo; //paso 3
            }
            return 0;
        }else{ //no hay espacio
            return -1;
        }
    }

    public int agregarInicio(Object valor){
        Nodo nuevoNodo=new Nodo(valor); //paso 1
        if(nuevoNodo!=null){ //hay esapcio
            if(vacia()==true){ //a)
                primero=nuevoNodo; //paso 2
                ultimo=nuevoNodo;
            }else{  //b)
                nuevoNodo.setNodoDer(primero); //paso 2
                primero=nuevoNodo; //paso 3
            }
            return 0;
        }else{ //no hay espacio
            return -1;
        }
    }

    @Override
    public void imprimir(){
        Nodo iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            SalidaPorDefecto.consola(contenido+" -> ");//paso 2
            iterador=iterador.getNodoDer();//paso 3
        }
        SalidaPorDefecto.consola("null");
    }

    @Override
    public void imprimirOI(){
        invertir();
        Nodo iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            SalidaPorDefecto.consola(contenido+" -> ");//paso 2
            iterador=iterador.getNodoDer();//paso 3
        }
        SalidaPorDefecto.consola("null");
        invertir();
    }

    @Override
    public Object buscar(Object valor){
        Nodo nodoBuscado = primero;
        while (nodoBuscado != null && !nodoBuscado.getContenido().equals(valor)){ //buscamos mientras haya donde buscar o mientras no lo encontremos
            nodoBuscado = nodoBuscado.getNodoDer();//avanza hacia el siguiente nodo
        }
        if (nodoBuscado == null){ //no se encontro el valor
            return null;
        }
        else{
            return  nodoBuscado.getContenido();
        }
    }

    public Object eliminarInicio(){
        if(vacia()==false){ //hay algo
            Object contenidoEliminado=primero.getContenido(); //paso 1
            if(primero==ultimo){ //b
                primero=null;//paso 2
                ultimo=null;
            }else{ //c
                primero=primero.getNodoDer();//paso 2
            }
            return contenidoEliminado;
        }else{ //vacía a)
            return null;
        }
    }
    public Nodo obtenerPrimero(){
        Nodo iterador = primero;
        return iterador;
    }
    @Override
    public Object eliminar(Object valor) {
        if (vacia() == false) { // hay algo
            Nodo nodoAnterior = primero;
            Nodo nodoBuscado = primero;
            while (nodoBuscado != null && !valor.toString().equalsIgnoreCase(nodoBuscado.toString())) { // buscamos mientras haya nodos y no lo encuentre
                nodoAnterior = nodoBuscado;
                nodoBuscado = nodoBuscado.getNodoDer(); // avanzo al siguiente nodo
            }
            if (nodoBuscado == null) { // no se encontro el valor b)
                return null;
            } else { // si se encontro
                if (primero == ultimo) { // c)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    primero = null;
                    ultimo = null;
                    return contenidoEliminado;
                } else if (nodoBuscado == primero) { // d)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    primero = primero.getNodoDer();
                    return contenidoEliminado;
                } else if (nodoBuscado == ultimo) { // e)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    ultimo = nodoAnterior;
                    ultimo.setNodoDer(null);
                    return contenidoEliminado;
                } else { // f)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    nodoAnterior.setNodoDer(nodoBuscado.getNodoDer());
                    return contenidoEliminado;
                }
            }
        } else { // no hay nada a)
            return null;
        }
    }

    @Override
    public Object eliminar(){
        if(vacia()==false){ //hay datos
            Object contenidoEliminado=ultimo.getContenido();//paso 1
            if(primero==ultimo){  //b)
                //contenidoEliminado=ultimo.getContenido();//paso 1
                primero=null; //paso 2
                ultimo=null;
            }else{ //c)
                //contenidoEliminado=ultimo.getContenido();//paso 1
                //buscar a penultimo
                Nodo penultimo=primero;
                while(penultimo.getNodoDer()!=ultimo){
                    penultimo=penultimo.getNodoDer(); //i=i+1
                }
                ultimo=penultimo; //paso 2
                ultimo.setNodoDer(null);//paso 3
            }
            return contenidoEliminado;
        }else { //a)
            return null;
        }
    }

    @Override
    public Object verUltimo(){
        Nodo iterador=ultimo; //paso inicial
        Object contenido=iterador.getContenido(); //paso 1
        return contenido;
    }

    public ListaEstatica aListaEstatica(){
        ListaEstatica listaE = new ListaEstatica(contarCantidadNodos());
        Nodo iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            listaE.agregar(contenido);
            iterador=iterador.getNodoDer();//paso 3
        }
        return listaE;
    }

    public ListaEstatica aListaEstatica(ListaEstatica elementosADescartar){
        ListaEstatica listaE = new ListaEstatica(contarCantidadNodos());
        Nodo iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            if ( elementosADescartar.buscar(contenido) == null){ //se busca que en en el argumento no ste el valor, si no lo esta, se agrega a la lista estatica
                listaE.agregar(contenido);
            }
            iterador=iterador.getNodoDer();//paso 3
        }
        return listaE;
    }
    public Matriz2 aMatriz2D(int filas, int columnas){
        ListaEstatica listaE = new ListaEstatica(contarCantidadNodos());
        Matriz2 matriz2D = new Matriz2(filas, columnas,null);
        Nodo iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            listaE.agregar(contenido);
            iterador=iterador.getNodoDer();//paso 3
        }
        matriz2D.agregarColumna(listaE,0);

        return matriz2D;
    }

    public int contarCantidadNodos(){
        Nodo iterador = primero;
        int cantidad = 0;
        while (iterador != null){
            cantidad++;
            iterador = iterador.getNodoDer(); //se recorre el nodo al siguiente
        }
        return cantidad;
    }

    public boolean agregarLista(ListaAlmacenamiento listaDatos2){
        if (listaDatos2 instanceof ListaDinamica){
            ListaDinamica listaDinamica = (ListaDinamica) listaDatos2;
            Nodo iterador=primero; //paso inicial
            while(iterador!=null){
                agregar(iterador.getContenido());
                iterador=iterador.getNodoDer();
            }
            return true;
        }
        else if (listaDatos2 instanceof ListaEstatica){
            ListaEstatica listaEstatica= (ListaEstatica) listaDatos2;
            for (int cont = 0; cont<listaEstatica.numeroElementos();cont++){
                agregar(listaEstatica.obtener(cont));
            }
            return true;
        }
        else{
            return  false;
        }
    }

    public Object clonar(){
        ListaDinamica listaClonada = new ListaDinamica();
        Nodo iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            listaClonada.agregar(contenido);
            iterador=iterador.getNodoDer();//paso 3
        }
        return listaClonada;
    }

    public boolean agregarMatriz2D(Matriz2 tabla, TipoTabla enumTipoTabla){
        if (enumTipoTabla == FILA){
            for (int cont = 0; cont<tabla.obtenerRenglones();cont++) {
                for (int cont1 = 0; cont1<tabla.obtenerColumnas();cont1++) {
                    agregar(tabla.obtener(cont, cont1));
                }
            }
            return true;
        }
        else if (enumTipoTabla == COL){
            for (int cont = 0; cont<tabla.obtenerColumnas();cont++) {
                for (int cont1 = 0; cont1<tabla.obtenerRenglones();cont1++) {
                    agregar(tabla.obtener(cont1, cont));
                }
            }
            return true;
        }
        return false;
    }

    public void vaciar(){
        primero=null;
        ultimo=null;
    }

    public void rellenar(Object valor, int cantidad){
        for (int i = 0; i < cantidad; i++){
            agregar(valor);
        }
    }

    public int contar(Object elemento){
        int contador = 0;
        Nodo iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            if (elemento == contenido){
                contador++;
            }
            iterador=iterador.getNodoDer();//paso 3
        }
        return contador;
    }
    public void invertir(){
        ListaEstatica listaE = new ListaEstatica(contarCantidadNodos());
        Nodo iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            listaE.agregar(contenido);
            iterador=iterador.getNodoDer();//paso 3
        }
        vaciar();
        for(int i = listaE.numeroElementos()-1; i >= 0; i-- ){
            agregar(listaE.obtener(i));
        }
    }
    public boolean cambiar(Object valorViejo, Object valorNuevo, int numVeces){
        int viejoVeces = contar(valorViejo);
        Object viejo = valorViejo;
        int contador = 0;
        Nodo iterador=primero; //paso inicial
        if(viejoVeces< numVeces){
            return false;
        }
        else{
            while(iterador!=null && contador<numVeces){
                Object contenido=iterador.getContenido(); //paso 1
                if (viejo == contenido){
                    iterador.setContenido(valorNuevo);
                    contador++;
                }
                iterador=iterador.getNodoDer();//paso 3
            }
            return true;
        }
    }
    public boolean cambiar(int indice, Object valor){
        int contador = -1;
        Nodo iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            contador++;
            if (contador == indice){
                iterador.setContenido(valor);
                return true;
            }
            iterador=iterador.getNodoDer();//paso 3
        }
        return false;
    }

    public Object obtener(int indice){
        int contador = -1;
        Object valor = null;
        Nodo iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            contador++;
            if (contador == indice){
                valor = contenido;
            }
            iterador=iterador.getNodoDer();//paso 3
        }
        return valor;
    }

    public boolean esIgual(ListaAlmacenamiento listaDatos2){
        if (listaDatos2 instanceof ListaDinamica){
            ListaDinamica listaDinamica = (ListaDinamica) listaDatos2;
            if (contarCantidadNodos() == listaDinamica.contarCantidadNodos()){
                Nodo iterador=primero; //paso inicial
                Nodo iterador2 = listaDinamica.primero;
                while(iterador!=null && iterador2 != null){
                    Object contenido=iterador.getContenido(); //paso 1
                    Object contenido2=iterador2.getContenido();
                    if (contenido != contenido2){
                        return false;
                    }
                    iterador=iterador.getNodoDer();//paso 3
                    iterador=iterador.getNodoDer();//paso 3
                }
                return true;
            }
            else{
                return false;
            }

        }
        else if (listaDatos2 instanceof ListaEstatica){
            ListaEstatica listaEstatica= (ListaEstatica) listaDatos2;
            if (contarCantidadNodos() == listaEstatica.numeroElementos()){
                Nodo iterador=primero; //paso inicial
                int pos = 0;
                while(iterador!=null && pos < listaEstatica.numeroElementos() ){
                    Object contenido=iterador.getContenido(); //paso 1
                    if (contenido != listaEstatica.obtener(pos)){
                        return false;
                    }
                    iterador=iterador.getNodoDer();//paso 3
                    pos++;
                }
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return  false;
        }
    }
    public Object eliminarIndice(int indice){
        int cantidadNodos = contarCantidadNodos();
        if (indice == 0){
            return eliminarInicio();
        }
        else if(indice >0 && indice< cantidadNodos){
            Nodo nodoBuscado = primero;
            int posicionActual = 0;
            while (posicionActual < indice-1){
                nodoBuscado = nodoBuscado.getNodoDer();
                posicionActual++;
            }
            Nodo nodoEliminado = nodoBuscado.getNodoDer();
            nodoBuscado.setNodoDer(nodoEliminado.getNodoDer());
            return nodoEliminado.getContenido();
        }
        else {
            return null;
        }
    }

    @Override
    public String toString() {
        String lista = "";
        Nodo iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            lista += contenido+" -> ";
            iterador=iterador.getNodoDer();//paso 3
        }
        lista += "null";
        return  lista;
    }

    public Object verPrimero(){
        if(vacia()==false){ //hay algo
            return primero.getContenido();
        }else{
            return null;
        }
    }

    public void inicializarIterador(){
        iterador=primero;
    }

    public boolean hayNodos(){
        if(iterador==null){
            return false;
        }else{
            return true;
        }
    }

    public Object obtenerNodo(){
        if(hayNodos()==true){
            Object contenidoNodo=iterador.getContenido();
            iterador=iterador.getNodoDer();
            return  contenidoNodo;
        }else{ //no hay elementos
            return null;
        }
    }
}
