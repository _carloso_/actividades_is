package recursividad;

/**
 * Esta clase contiene diversos métodos para resolver problemas de recursividad.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class EjerciciosDeRecursividad {

    /**
     * Este método calcula la multiplicación entera de dos números, usando recursión.
     * @param num1 Es el primer número a multiplicar.
     * @param num2 Es el segundo número a multiplicar.
     * @return Regresa el resultado de la multiplicación.
     */
    // multiplicación entera, a*b = a + a + a +(b veces a)..+ a
    public static int multiplicar(int num1, int num2) {
        if (num2 == 0) { // caso base
            return 0;
        } else { // caso recursivo
            return num1 + multiplicar(num1, num2 - 1);
        }
    }

    /**
     * Este método determina si un número es primo.
     * @param num Es el número a evaluar.
     * @return Regresa <b>true</b> si el número es primo, <b>false</b> si no es primo.
     */
    public static boolean esPrimo(int num) {
       if (num == 1) { // caso base, 1 no es primo
            return false;
       } else { // caso recursivo
            return esPrimo(num, 2);
       }
    }

    /**
     * Este método determina si un número es primo, usando recursión.
     * @param num Es el número a evaluar.
     * @param divisor Es el divisor que se usará para evaluar si el número es primo.
     * @return Regresa <b>true</b> si el número es primo, <b>false</b> si no es primo.
     */
    private static boolean esPrimo(int num, int divisor) {
        if (divisor > num / 2) {
            return true;
        } else if (num % divisor == 0) {
            return false;
        } else {
            return esPrimo(num, divisor + 1);
        }
    }

    /**
     * Este método calcula el máximo común divisor de dos números.
     * @param num1 Es el primer número.
     * @param num2 Es el segundo número.
     * @return Regresa el máximo común divisor de los dos números.
     */
    public static int mcd(int num1, int num2) {
        if (num2 == 0) {
            return num1;
        } else {
            return mcd(num2, num1 % num2);
        }
    }

    /**
     * Este método convierte un número decimal a binario de forma recursiva.
     * @param num Es el número decimal a convertir.
     * @return Regresa el número binario correspondiente al número decimal dado.
     */
    public static long aBinario(int num) {
        if (num < 2) { // caso base
            return num;
        } else { // caso recursivo
            return num % 2 + aBinario(num / 2) * 10;
        }
    }

    /**
     * Este método convierte un número decimal a hexadecimal de forma recursiva.
     * @param num Es el número decimal a convertir.
     * @return Regresa el número hexadecimal correspondiente al número decimal dado.
     */
    public static String aHexadecimal(int num) {
        String[] digitosHex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
        if (num < 16) {
            return digitosHex[num];
        } else {
            return aHexadecimal(num / 16) + digitosHex[num % 16];
        }
    }
}