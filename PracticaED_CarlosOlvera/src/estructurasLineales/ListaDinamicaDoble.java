package estructurasLineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.auxiliares.Nodo;
import estructurasLineales.auxiliares.NodoDoble;

public class ListaDinamicaDoble implements ListaAlmacenamiento{
    protected NodoDoble primero;
    protected NodoDoble ultimo;
    protected NodoDoble iterador;

    public ListaDinamicaDoble(){
        primero=null;
        ultimo=null;
    }

    @Override
    public boolean vacia(){
        if(primero==null){
            return true;
        }else{
            return false;
        }
    }

    public boolean agregarLista(ListaAlmacenamiento listaDatos2){
        if (listaDatos2 instanceof ListaDinamica){
            ListaDinamica listaDinamica = (ListaDinamica) listaDatos2;
            NodoDoble iterador=primero; //paso inicial
            while(iterador!=null){
                agregar(iterador.getContenido());
                iterador=iterador.getNodoDer();
            }
            return true;
        }
        else if (listaDatos2 instanceof ListaEstatica){
            ListaEstatica listaEstatica= (ListaEstatica) listaDatos2;
            for (int cont = 0; cont<listaEstatica.numeroElementos();cont++){
                agregar(listaEstatica.obtener(cont));
            }
            return true;
        }
        else{
            return  false;
        }
    }

    public Object obtener(int indice){
        int contador = -1;
        Object valor = null;
        NodoDoble iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            contador++;
            if (contador == indice){
                valor = contenido;
            }
            iterador=iterador.getNodoDer();//paso 3
        }
        return valor;
    }

    @Override
    public int agregar(Object valor){
        NodoDoble nuevoNodo=new NodoDoble(valor); //paso 1
        if(nuevoNodo!=null){ //hay espacio
            if(vacia()==true){ //a)
                primero=nuevoNodo;//paso 2
                ultimo=nuevoNodo;
            }else{  //b)
                ultimo.setNodoDer(nuevoNodo);//paso 2
                nuevoNodo.setNodoIzq(ultimo);//paso 3
                ultimo=nuevoNodo;//paso 4
            }
            return 0;
        }else{  //no hay espacio
            return -1;
        }
    }

    @Override
    public void imprimir(){
        if(vacia()==true){
            SalidaPorDefecto.consola("null");
        }else{
            iniciarIteradorInicio(); //el nodo temporal empieza del inicio
            SalidaPorDefecto.consola("null <- ");
            while (iterador!=ultimo){ //MIENTRAS EL ITERADOR NO SEA EL ULTIMO
                SalidaPorDefecto.consola(iterador.getContenido()+ " <-> ");
                iterador=iterador.getNodoDer();
            }
            SalidaPorDefecto.consola(iterador.getContenido()+ " -> null");
        }
    }

    @Override
    public void imprimirOI(){
        if(vacia()==true){
            SalidaPorDefecto.consola("null");
        }else{
            iniciarIteradorFinal(); //el nodo temporal empieza del Final
            SalidaPorDefecto.consola("null <- ");
            while (iterador!=primero){//MIENTRAS EL ITERADOR NO SEA EL PRIMERO
                SalidaPorDefecto.consola(iterador.getContenido()+ " <-> ");
                iterador=iterador.getNodoIzq();
            }
            SalidaPorDefecto.consola(iterador.getContenido()+ " -> null");
        }
    }

    @Override
    public Object buscar(Object valor){
        iniciarIteradorInicio();
        while (iterador != null && !iterador.getContenido().equals(valor)){ //buscamos mientras haya donde buscar o mientras no lo encontremos
            iterador = iterador.getNodoDer();//avanza hacia el siguiente nodo
        }
        if (iterador == null){ //no se encontro el valor
            return null;
        }
        else{
            return  iterador.getContenido();
        }
    }

    @Override
    public Object eliminar(Object valor){
        if (vacia() == false) { // hay algo
            NodoDoble nodoAnterior = primero;
            NodoDoble nodoBuscado = primero;
            while (nodoBuscado != null && !valor.toString().equalsIgnoreCase(nodoBuscado.toString())) { // buscamos mientras haya nodos y no lo encuentre
                nodoAnterior = nodoBuscado;
                nodoBuscado = nodoBuscado.getNodoDer(); // avanzo al siguiente nodo
            }
            if (nodoBuscado == null) { // no se encontro el valor b)
                return null;
            } else { // si se encontro
                if (primero == ultimo) { // c)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    primero = null;
                    ultimo = null;
                    return contenidoEliminado;
                } else if (nodoBuscado == primero) { // d)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    primero = primero.getNodoDer();
                    return contenidoEliminado;
                } else if (nodoBuscado == ultimo) { // e)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    ultimo = nodoBuscado.getNodoIzq();
                    ultimo.setNodoDer(null);
                    return contenidoEliminado;
                } else { // f)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    nodoAnterior.setNodoDer(nodoBuscado.getNodoDer());
                    return contenidoEliminado;
                }
            }
        } else { // no hay nada a)
            return null;
        }
    }

    @Override
    public Object eliminar(){
        if(vacia()==false){ //hay elementos
            Object contenidoEliminado=ultimo.getContenido();//paso 1
            if(primero==ultimo){   //b) único
                //Object contenidoEliminado=ultimo.getContenido();//paso 1
                primero=null;
                ultimo=null;//paso 2
            }else{    //c)  varios
                //Object contenidoEliminado=ultimo.getContenido();//paso 1
                ultimo=ultimo.getNodoIzq(); //paso 2
                ultimo.setNodoDer(null);//paso 3
            }
            return contenidoEliminado;
        }else {   //a)
            return null;
        }
    }

    @Override
    public Object verUltimo(){
        iniciarIteradorFinal(); //paso inicial
        Object contenido=iterador.getContenido(); //paso 1
        return contenido;
    }

    public int agregarInicio(Object valor){
        NodoDoble nuevoNodo=new NodoDoble(valor); //paso 1
        if(nuevoNodo!=null){ //hay esapcio
            if(vacia()==true){ //a)
                primero=nuevoNodo; //paso 2
                ultimo=nuevoNodo;
            }else{  //b)
                nuevoNodo.setNodoDer(primero); //paso 2
                primero=nuevoNodo; //paso 3
            }
            return 0;
        }else{ //no hay espacio
            return -1;
        }
    }

    public Object eliminarInicio(){
        if(vacia()==false){ //hay algo
            Object contenidoEliminado=primero.getContenido(); //paso 1
            if(primero==ultimo){ //b
                primero=null;//paso 2
                ultimo=null;
            }else{ //c
                primero=primero.getNodoDer();//paso 2
            }
            return contenidoEliminado;
        }else{ //vacía a)
            return null;
        }
    }

    public void iniciarIteradorInicio(){
        iterador = primero;
    }

    public void iniciarIteradorFinal(){
        iterador = ultimo;
    }

    public Object buscarFinalInicio(Object valor){
        iniciarIteradorFinal();
        while (iterador != null && !iterador.getContenido().equals(valor)){ //buscamos mientras haya donde buscar o mientras no lo encontremos
            iterador = iterador.getNodoIzq();//avanza hacia el siguiente nodo
        }
        if (iterador == null){ //no se encontro el valor
            return null;
        }
        else{
            return  iterador.getContenido();
        }
    }

    public boolean hayNodos(){
        if (iterador != null){
            return true;
        }else{
            return false;
        }
    }

    public Object obtenerNodoDer(){
        if (hayNodos() == true){
            Object contenido = iterador.getContenido();
            iterador = iterador.getNodoDer();
            return contenido;
        }
        else{
            return null;
        }
    }

    public Object obtenerNodoIzq(){
        if (hayNodos() == true){
            Object contenido = iterador.getContenido();
            iterador = iterador.getNodoIzq();
            return contenido;
        }
        else{
            return null;
        }
    }

    public int contarCantidadNodos(){
        iniciarIteradorInicio();
        int cantidad = 0;
        while (iterador != null){
            cantidad++;
            iterador = iterador.getNodoDer(); //se recorre el nodo al siguiente
        }
        return cantidad;
    }
    /////////METODOS DE LA INTERFAZ

    public ListaDinamicaDoble clonar(){
        ListaDinamicaDoble listaClonada = new ListaDinamicaDoble();
        iniciarIteradorInicio(); //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            listaClonada.agregar(contenido);
            iterador=iterador.getNodoDer();//paso 3
        }
        return listaClonada;
    }

    public void rellenar(Object valor, int cantidad){
        for (int i = 0; i < cantidad; i++){
            agregar(valor);
        }
    }

    public void vaciar(){
        primero=null;
        ultimo=null;
    }

    public void invertir(){
        ListaEstatica listaE = new ListaEstatica(contarCantidadNodos());
        iniciarIteradorFinal(); //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            listaE.agregar(contenido);
            iterador=iterador.getNodoIzq();//paso 3
        }
        vaciar();
        for(int i = 0; i < listaE.numeroElementos(); i++ ){
            agregar(listaE.obtener(i));
        }
    }

    public int contar(Object elemento){
        int contador = 0;
        iniciarIteradorInicio(); //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            if (elemento == contenido){
                contador++;
            }
            iterador=iterador.getNodoDer();//paso 3
        }
        return contador;
    }

    public boolean cambiar(Object valorViejo, Object valorNuevo, int numVeces){
        int viejoVeces = contar(valorViejo);
        Object viejo = valorViejo;
        int contador = 0;
        iniciarIteradorInicio(); //paso inicial
        if(viejoVeces< numVeces){
            return false;
        }
        else{
            while(iterador!=null && contador<numVeces){
                Object contenido=iterador.getContenido(); //paso 1
                if (viejo == contenido){
                    iterador.setContenido(valorNuevo);
                    contador++;
                }
                iterador=iterador.getNodoDer();//paso 3
            }
            return true;
        }
    }

    public boolean insertar(Object valor, int indice) {
        // incertar en una posicion en especifico
        int cantidadDeNodos = contarCantidadNodos();
        if (indice == 0) { // agregar al inicio
            int valorRetorno = agregarInicio(valor);
            if (valorRetorno == 0) {
                return true;
            } else {
                return false;
            }
        } else if (indice > 0 && indice <= cantidadDeNodos) { // posicion valida, se puede agregar
            NodoDoble iterador = primero;
            int posicionActual = 0;
            while (posicionActual < indice - 1) {
                iterador = iterador.getNodoDer();
                posicionActual++;
            }
            NodoDoble nuevoNodo = new NodoDoble(valor);
            nuevoNodo.setNodoIzq(iterador);
            nuevoNodo.setNodoDer(iterador.getNodoDer());
            if (iterador.getNodoDer() != null) {
                iterador.getNodoDer().setNodoIzq(nuevoNodo);
            }
            iterador.setNodoDer(nuevoNodo);
            return true;
        } else { // la posicion es mayor que la cantidad de nodos o es negativa, se agrega al final
            int valorRetorno = agregar(valor);
            if (valorRetorno == 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean esSublista(ListaEstatica lista) {

            iniciarIteradorInicio(); //paso inicial
            int contadorLista = lista.numeroElementos();
            int contador = 0;
            int pos = 0;
            boolean verificador = false;
            while(iterador!=null && pos < lista.numeroElementos() ){
                Object contenido=iterador.getContenido(); //paso 1
                if (contenido == lista.obtener(pos)){
                    contador++;
                }
                iterador=iterador.getNodoDer();//paso 3
                pos++;
            }
            if (contador == contadorLista){
                verificador = true;
            }
            return verificador;
    }
}
