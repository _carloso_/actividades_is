package estructurasnolineales;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaEstatica;
import estructurasLineales.PilaEstatica;
import estructurasLineales.auxiliares.NodoDoble;

/**
 * Esta clase representa a un arbol binario.
 * @author Cristian Omar Alavarado Rodriguez.
 * @version 1.0.
 */
public class ArbolBinario {

    protected NodoDoble raiz;

    public ArbolBinario(){
        raiz = null;
    }

    /**
     * Este método se encarga de generar el arbol de forma manual por parte del usuario.
     * @return Regresea true si el arbol se genero correctamente, false si nos se pudo generar el arbol.
     */
    public boolean generarArbol() {
        SalidaPorDefecto.consola("Indica la raiz del arbol: ");
        String contenidoNodo = EntradaPorDefecto.consolaCadenas();
        NodoDoble nuevoNodo = new NodoDoble(contenidoNodo);
        if (nuevoNodo != null) { // hay espacio de memoria
            raiz = nuevoNodo;
            generarArbol(raiz);
            return true;
        } else { // no hay espacio de memoria
            return false;
        }
    }

    /**
     * Este Este método se encarga de generar el arbol de forma manual por parte del usuario (método recursivo).
     * @param subRaiz Es una subRaiz del arbol que se va a crear.
     */
    private void generarArbol(NodoDoble subRaiz) { // por cada subraiz
        // agregar hijo izquierdo
        SalidaPorDefecto.consola("¿El nodo " + subRaiz.getContenido() + " tiene hijo izquierdo? (s/n): ");
        String respuesta = EntradaPorDefecto.consolaCadenas();
        if (respuesta.equalsIgnoreCase("s")) {
            SalidaPorDefecto.consola("Indica el contenido del hijo izquierdo de " + subRaiz.getContenido() + ": ");
            String contenidoNodoIzquierdo = EntradaPorDefecto.consolaCadenas();
            NodoDoble nuevoNodoIzquierdo = new NodoDoble(contenidoNodoIzquierdo);
            if (nuevoNodoIzquierdo != null) { // hay espacio de memoria
                subRaiz.setNodoIzq(nuevoNodoIzquierdo); // ligamos la subraiz a este hijo nuevo
                // llamada recursiva para ver si a este nodo izquierdo le corresponden tener hijos también
                generarArbol(subRaiz.getNodoIzq());
            }
        } // si no quiere tener hijo izquierdo, no hacemos nada, es decir, caso base.

        // agregar hijo derecho
        SalidaPorDefecto.consola("¿El nodo " + subRaiz.getContenido() + " tiene hijo derecho? (s/n): ");
        String respuesta2 = EntradaPorDefecto.consolaCadenas();
        if (respuesta2.equalsIgnoreCase("s")) {
            SalidaPorDefecto.consola("Indica el contenido del hijo derecho de " + subRaiz.getContenido() + ": ");
            String contenidoNodoDerecho = EntradaPorDefecto.consolaCadenas();
            NodoDoble nuevoNodoDerecho = new NodoDoble(contenidoNodoDerecho);
            if (nuevoNodoDerecho != null) { // hay espacio de memoria
                subRaiz.setNodoDer(nuevoNodoDerecho); // ligamos la subraiz a este hijo nuevo
                // llamada recursiva para ver si a este nodo derecho le corresponden tener hijos también
                generarArbol(subRaiz.getNodoDer());
            }
        } // si no quiere tener hijo derecho, no hacemos nada, es decir, caso base.
    }

    /**
     * Este método muestra el contenido del arbol mediante un recorrido en inOrden.
     */
    public void inOrden() {
        inOrden(raiz);
    }

    /**
     * Este método muestra el contenido del arbol mediante un recorrido en inOrden.
     * @param subRaiz Es la raiz de un sub arbol.
     */
    private void inOrden(NodoDoble subRaiz) {
        if (subRaiz != null) {
            inOrden(subRaiz.getNodoIzq());
            SalidaPorDefecto.consola(subRaiz.getContenido() + " ");
            inOrden(subRaiz.getNodoDer());
        } // caso base donde no se hace nada
    }

    /**
     * Este método muestra el contenido del arbol mediante un recorrido en preOrden.
     */
    public void preOrden() {
        preOrden(raiz);
    }

    /**
     * Este método muestra el contenido del arbol mediante un recorrido en preOrden.
     * @param subRaiz Es la raiz de un sub arbol.
     */
    private void preOrden(NodoDoble subRaiz) {
        if (subRaiz != null) {
            SalidaPorDefecto.consola(subRaiz.getContenido() + " ");
            preOrden(subRaiz.getNodoIzq());
            preOrden(subRaiz.getNodoDer());
        } // caso base donde no se hace nada
    }

    /**
     * Este método muestra el contenido del arbol mediante un recorrido en postOrden.
     */
    public void postOrden() {
        postOrden(raiz);
    }

    /**
     * Este método muestra el contenido del arbol mediante un recorrido en postOrden.
     * @param subRaiz Es la raiz de un sub arbol.
     */
    private void postOrden(NodoDoble subRaiz) {
        if (subRaiz != null) {
            postOrden(subRaiz.getNodoIzq());
            postOrden(subRaiz.getNodoDer());
            SalidaPorDefecto.consola(subRaiz.getContenido() + " ");
        } // caso base donde no se hace nada
    }

    /**
     * Este método nos permite obtener la altura del arbol.
     * @return Regresa un numero entero que indica la altura del arbol.
     */
        public int obtenerAltura() {
        return obtenerAltura(raiz);
    }

    /**
     * Este método recorre el árbol para obtener la altura del mismo.
     * @param subRaiz Es la subraiz del árbol que se está recorriendo.
     * @return Regresa la altura del árbol.
     */
    private int obtenerAltura(NodoDoble subRaiz) {
        if (subRaiz == null) {
            return 0;
        }
        int alturaIzquierda = obtenerAltura(subRaiz.getNodoIzq());
        int alturaDerecha = obtenerAltura(subRaiz.getNodoDer());
        if (alturaIzquierda > alturaDerecha) {
            return alturaIzquierda + 1;
        } else {
            return alturaDerecha + 1;
        }
    }

    /**
     * Este método nos permite obtener el nivel en el que se encuentra algún nodo proporcionado por el usuario.
     * @param contenido Es el contenido del nodo del cual se desea saber en qué nivel del arbol se encuentra.
     * @return Regresa el numero de nivel en donde se encunatra el nodo, o regresa -1 si nos se encontra al nodo.
     */
    public int obtenerNivel(String contenido) {
        return obtenerNivel(raiz, contenido, 1);
    }

    /**
     * Este método recorre el árbol para obtener el nivel en el que se encuentra algún nodo proporcionado por el usuario.
     * @param subRaiz Es la subraiz del árbol que se está recorriendo.
     * @param contenido Es el contenido del nodo que se está buscando.
     * @param nivel Es el nivel en el que se encuentra el nodo.
     * @return Regresa el nivel en el que se encuentra el nodo.
     */
    private int obtenerNivel(NodoDoble subRaiz, String contenido, int nivel) {
        if (subRaiz != null) {
            if (subRaiz.getContenido().equals(contenido)) {
                return nivel;
            }
            int nivelIzquierdo = obtenerNivel(subRaiz.getNodoIzq(), contenido, nivel + 1);
            if (nivelIzquierdo != -1) {
                return nivelIzquierdo;
            }
            int nivelDerecho = obtenerNivel(subRaiz.getNodoDer(), contenido, nivel + 1);
            if (nivelDerecho != -1) {
                return nivelDerecho;
            }
        }
        return -1;
    }

    /**
     * Este método nos permite saber si un determinado nodo es un nodo raiz, un nodo hoja o un nodo intermedio, asi como
     * saber cual es su padre en caso de que tuviera.
     * @param contenido Es el contenido del nodo del cual se desea obtener su informacion.
     */
    public void obtenerInfoNodo(String contenido) {
        obtenerInfoNodo(raiz, contenido);
    }

    /**
     * Este método recorre el árbol para obtener información del nodo que se está buscando.
     * @param subRaiz Es la subraiz del árbol que se está recorriendo.
     * @param contenido Es el contenido del nodo que se está buscando.
     */
    private void obtenerInfoNodo(NodoDoble subRaiz, String contenido) {
        NodoDoble nodoPadre = null;
        if (subRaiz != null) {
            if (subRaiz.getContenido().equals(contenido)) {
                if (subRaiz == raiz) {
                    SalidaPorDefecto.consola("Nodo raíz, no tiene padre");
                } else if (subRaiz.getNodoIzq() == null && subRaiz.getNodoDer() == null) {
                    SalidaPorDefecto.consola("Nodo hoja, ");
                    nodoPadre = obtenerNodoPadre(raiz, subRaiz);
                    if (nodoPadre != null) {
                        SalidaPorDefecto.consola("Su padre es " + nodoPadre.getContenido());
                    } else {
                        SalidaPorDefecto.consola("No tiene padre");
                    }
                } else {
                    SalidaPorDefecto.consola("Nodo intermedio, ");
                    nodoPadre = obtenerNodoPadre(raiz, subRaiz);
                    if (nodoPadre != null) {
                        SalidaPorDefecto.consola("Su padre es " + nodoPadre.getContenido());
                    } else {
                        SalidaPorDefecto.consola("No tiene padre.");
                    }
                }
            }
            obtenerInfoNodo(subRaiz.getNodoIzq(), contenido);
            obtenerInfoNodo(subRaiz.getNodoDer(), contenido);
        }
    }

    /**
     * Este método nos permite saber cual es el nodo padre de un determinado nodo.
     * @param raiz Es la raiz del arbol.
     * @param subRaiz Es la subraiz del árbol que se está recorriendo.
     * @return Regresa el nodo padre de un determinado nodo.
     */
    private NodoDoble obtenerNodoPadre(NodoDoble raiz, NodoDoble subRaiz) {
        if (raiz != null) {
            if (raiz.getNodoIzq() != null && raiz.getNodoIzq().getContenido().equals(subRaiz.getContenido())) {
                return raiz;
            }
            if (raiz.getNodoDer() != null && raiz.getNodoDer().getContenido().equals(subRaiz.getContenido())) {
                return raiz;
            }
            NodoDoble nodoPadre = obtenerNodoPadre(raiz.getNodoIzq(), subRaiz);
            if (nodoPadre != null) {
                return nodoPadre;
            }
            nodoPadre = obtenerNodoPadre(raiz.getNodoDer(), subRaiz);
            if (nodoPadre != null) {
                return nodoPadre;
            }
        }
        return null;
    }

    /**
     * Este método se encarga de mostrar la cantidad de nodos que hay en cada nivel del arbol.
     */
    public void contarNodosPorNivel() {
        SalidaPorDefecto.consola("Nodos por nivel: " + "\n");
        int numNiveles = obtenerAltura();
        for (int nivel = 1; nivel <= numNiveles; nivel++) {
            SalidaPorDefecto.consola("El Nivel " + nivel + " Tiene " + contarNodosPorNivel(raiz, nivel) + " nodos"  + "\n");
        }
    }

    /**
     * Este método cuenta el número de nodos que hay en un determinado nivel.
     * @param subRaiz Es la subraiz del árbol que se está recorriendo.
     * @param nivel Es el nivel del cual se van a contar la cantidad de nodos.
     * @return Regresa la cantidad de nodos que hay en un determinado nivel del arbol.
     */
    private int contarNodosPorNivel(NodoDoble subRaiz, int nivel) {
        int contador = 0;
        if (subRaiz != null) {
            if (nivel == 1) {
                contador++;
            } else {
                contador += contarNodosPorNivel(subRaiz.getNodoIzq(), nivel - 1);
                contador += contarNodosPorNivel(subRaiz.getNodoDer(), nivel - 1);
            }
        }
        return contador;
    }

    /**
     * Este método nos permite saber si un determinado nodo tiene hermanos.
     * @param contenido Es el contenido del nodo del cual se desea saber si tine hermanos.
     * @return Regresa true si el nodo tine hermanos, false si no tiene.
     */
    public boolean tieneHermano(String contenido) {
        return tieneHermano(raiz, contenido);
    }

    /**
     * Este método recorre el árbol para saber si un nodo tine hermanos.
     * @param subRaiz Es la subraiz del árbol que se está recorriendo.
     * @param contenido Es el contenido del nodo que se está buscando.
     */
    private boolean tieneHermano(NodoDoble subRaiz, String contenido) {
        boolean tieneHermano = false;
        if (subRaiz != null) {
            if (subRaiz.getContenido().equals(contenido)) {
                if (subRaiz.getNodoIzq() != null && subRaiz.getNodoDer() != null) {
                    //SalidaPorDefecto.consola("Tiene hermano " + subRaiz.getNodoIzq().getContenido() + " y " + subRaiz.getNodoDer().getContenido());
                    tieneHermano = true;
                }
            }
            tieneHermano(subRaiz.getNodoIzq(), contenido);
            tieneHermano(subRaiz.getNodoDer(), contenido);
        }
        return tieneHermano;
    }

    /**
     * Este método nos permite obtener la cantidad de nodos que hay en el árbol.
     * @return Regresa la cantidad de nodos que hay en el árbol.
     */
    public int cantidadNodos() {
        return cantidadNodos(raiz);
    }

    /**
     * Este método recorre el árbol para saber la cantidad de nodos que hay en el árbol.
     * @param subRaiz Es la subraiz del árbol que se está recorriendo.
     * @return Regresa la cantidad de nodos que hay en el árbol.
     */
    private int cantidadNodos(NodoDoble subRaiz) {
        int cantidad = 0;
        if (subRaiz != null) {
            cantidad = 1 + cantidadNodos(subRaiz.getNodoIzq()) + cantidadNodos(subRaiz.getNodoDer());
        }
        return cantidad;
    }

    /**
     * Este método recorre el árbol en anchura o amplitud.
     */
    public void recorridoAmplitudRecursivo() {
        int numNiveles = obtenerAltura(); // Obtenemos la altura del árbol.
        for (int nivel = 1; nivel <= numNiveles; nivel++) { // Recorremos los niveles del árbol.
            recorridoAmplitudRecursivo(raiz, nivel);
        }
    }

    /**
     * Este método recorre el árbol en anchura o amplitud recursivamente.
     * @param subRaiz Es la subraiz del subárbol que se está recorriendo.
     * @param nivel Es el nivel de arbol.
     */
    private void recorridoAmplitudRecursivo(NodoDoble subRaiz, int nivel) {
        if (subRaiz != null) {
            if (nivel == 1) {
                SalidaPorDefecto.consola(subRaiz.getContenido() + " ");
            } else {
                recorridoAmplitudRecursivo(subRaiz.getNodoIzq(), nivel - 1);
                recorridoAmplitudRecursivo(subRaiz.getNodoDer(), nivel - 1);
            }
        }
    }

    /**
     * Este método recorre el árbol en amplitud de forma iterativa.
     */
    public void recorridoAmplitudIterativoCola() {
        ColaEstatica cola;
        if (raiz != null) { // Hay nodos en el árbol
            cola = new ColaEstatica(cantidadNodos());
            cola.poner(raiz);
            while (!cola.vacio()) {
                NodoDoble nodo = (NodoDoble) cola.quitar();
                SalidaPorDefecto.consola(nodo.getContenido() + " ");
                if (nodo.getNodoIzq() != null && nodo.getNodoDer() != null) {
                    cola.poner(nodo.getNodoIzq());
                    cola.poner(nodo.getNodoDer());
                } else if (nodo.getNodoIzq() != null) {
                    cola.poner(nodo.getNodoIzq());
                } else if (nodo.getNodoDer() != null) {
                    cola.poner(nodo.getNodoDer());
                }
            }
        } else { // No hay nodos en el árbol
            SalidaPorDefecto.consola("El árbol está vacío.");
        }
    }

    /**
     * Este método recorre el árbol en profundidad.
     */
    public void recorridoAmplitudIterativoPila() {
        PilaEstatica pila;
        if (raiz != null) { // Hay nodos en el árbol
            pila = new PilaEstatica(cantidadNodos());
            pila.poner(raiz);
            while (!pila.vacio()) {
                NodoDoble nodo = (NodoDoble) pila.quitar();
                SalidaPorDefecto.consola(nodo.getContenido() + " ");
                if (nodo.getNodoIzq() != null && nodo.getNodoDer() != null) {
                    pila.poner(nodo.getNodoIzq());
                    pila.poner(nodo.getNodoDer());
                } else if (nodo.getNodoIzq() != null) {
                    pila.poner(nodo.getNodoIzq());
                } else if (nodo.getNodoDer() != null) {
                    pila.poner(nodo.getNodoDer());
                }
            }
        } else { // No hay nodos en el árbol
            SalidaPorDefecto.consola("El árbol está vacío.");
        }
    }

    /**
     * Este método recorre el árbol en postorden de forma iterativa.
     */
    public void postordenIterativo() {
        // recorrido postorden: izq, der, raiz
        PilaEstatica pila;
        NodoDoble aux1;
        NodoDoble aux2;
        if (raiz != null) { // Hay nodos en el árbol
            pila = new PilaEstatica(obtenerAltura());
            aux2 = raiz;
            pila.poner(raiz);
            while (!pila.vacio()) { // Mientras la pila no este vacia
                aux1 = (NodoDoble) pila.verUltimo(); // Obtener el ultimo nodo que se encuentra en la pila
                if (aux1 == aux2 || aux1 == aux2.getNodoIzq() || aux1 == aux2.getNodoDer()) {
                    if (aux1.getNodoIzq() != null) {
                        pila.poner(aux1.getNodoIzq());
                    } else if (aux1.getNodoDer() != null) {
                        pila.poner(aux1.getNodoDer());
                    }
                    if (aux1.getNodoIzq() == null && aux1.getNodoDer() == null) { // Si el nodo no tiene hijos
                        SalidaPorDefecto.consola(pila.quitar() + " ");
                    }
                } else if (aux2 == aux1.getNodoIzq()) {
                    if (aux1.getNodoDer() != null) {
                        pila.poner(aux1.getNodoDer());
                    } else if (aux1.getNodoDer() == null) {
                        SalidaPorDefecto.consola(pila.quitar() + " ");
                    }
                } else if (aux2 == aux1.getNodoDer()) {
                    SalidaPorDefecto.consola(pila.quitar() + " ");
                }
                aux2 = aux1;
            }
        } else { // No hay nodos en el árbol
            SalidaPorDefecto.consola("El árbol está vacío.");
        }
    }
}