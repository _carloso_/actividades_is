package estructurasNoLineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaEstatica;
import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaEstatica;
import estructurasLineales.PilaEstatica;
import estructurasNoLineales.auxiliares.Vertice;
import herramientas.generales.EtiquetaGrafo;
import herramientas.generales.TipoOrden;

public class GrafoEstatico {
    //Dónde guardar los vértices
    protected ListaEstatica vertices;
    //Dónde guardar las aristas
    protected Matriz2 aristas;
    //Definimos el orden para trabjar con grafos ponderados
    protected TipoOrden orden;
    //Se guarda la cantidad de vertices
    protected int cantidadVertices;
    //lista estatica que se utilizara para el metodo grafoNoDirigido
    protected ListaEstatica verticesManipulables;
    //variable Object que se utilizara para el metodo Conexo
    protected Object componenteMarcado;


    public GrafoEstatico(int cantidadVertices){
        vertices=new ListaEstatica(cantidadVertices);
        aristas=new Matriz2(cantidadVertices,cantidadVertices,0.0);
        this.cantidadVertices = cantidadVertices;
        verticesManipulables = vertices; //esta variable toma los valores de la lista de vertices
    }

    public GrafoEstatico(int cantidadVertices, TipoOrden orden){
        vertices=new ListaEstatica(cantidadVertices);
        this.orden=orden;
        //asignar por defecto los valores infinitos
        if(this.orden==TipoOrden.DEC){ //decremental
            aristas=new Matriz2(cantidadVertices,cantidadVertices,Double.MAX_VALUE); //+inifnito
        }else{  //incremental
            aristas=new Matriz2(cantidadVertices,cantidadVertices,Double.MIN_VALUE); //-infinito
        }
        aristas.matrizDiagonal(0.0);//diagonal con ceros
    }


    public boolean agregarVertice(Object contenido){
        //Al momento de agregar vértices es oportuno checar que no se registren contenidos duplicados.
        //Buscar la existencia del vértice
        Integer indiceVertice=(Integer)vertices.buscar(contenido);

        if(indiceVertice==null){ //no existe
            //Crear un vértice
            Vertice nuevoVertice=new Vertice();

            //Le colocamos su atributos
            nuevoVertice.setContenido(contenido);
            nuevoVertice.setNumVertice(vertices.numeroElementos()); // el número de vértice lo obtenemos de la cantidad de vértices

            int retornoOperacion=vertices.agregar(nuevoVertice);
            if(retornoOperacion>=0){ //si se pudo agregar
                return true;
            }else{ //no se pudo agregar
                return false;
            }
        }else{ //si existe
            return false;
        }
    }

    public Object eliminarVertice(Object vertice){
        //Buscar la existencia del vértice
        Integer indiceVertice=(Integer)vertices.buscar(vertice);
        if(indiceVertice!=null) { //existe
            //se elimina el vertice y se guarda el valor para regresarlo
            Object verticeEliminado = vertices.eliminar(vertice);
            //se elimina el renglon de el vertice
            aristas.eliminarRenglon(indiceVertice);
            //se elimina la columna de el vertice
            aristas.eliminarColumna(indiceVertice);
            return verticeEliminado;
        }
        else{//no existe
            return null;
        }
    }

    public boolean esAdyacente(Object origen, Object destino){
        Integer indiceVerticeOrigen=(Integer)vertices.buscar(origen);
        Integer indiceVerticeDestino=(Integer)vertices.buscar(destino);
        if(indiceVerticeOrigen!=null || indiceVerticeDestino != null) { //existen
            //se verifica que los vertices formen una arista
            if ((Double)aristas.obtener(indiceVerticeOrigen,indiceVerticeDestino) != 0.0){
                return true;
            }
            else{
                return false;
            }
        }
        else {
            return false;
        }
    }

    public Object eliminarArista(Object origen, Object destino){
        Integer indiceVerticeOrigen=(Integer)vertices.buscar(origen);
        Integer indiceVerticeDestino=(Integer)vertices.buscar(destino);
        if(indiceVerticeOrigen!=null || indiceVerticeDestino != null) { //existen
            Object verticeEliminado = aristas.obtener(indiceVerticeOrigen,indiceVerticeDestino);
            //se elimina el renglon de el vertice
            aristas.eliminarRenglon(indiceVerticeOrigen);
            //se elimina la columna de el vertice
            aristas.eliminarColumna(indiceVerticeDestino);
            return verticeEliminado;
        }
        else{
            return null;
        }
    }

    public void buscarVertice(Object vertice){
        if (vertices.buscar(vertice) != null) {//existe
            Object verticeInfo;
            SalidaPorDefecto.consola("El vertice "+ vertice + " es el numero " + ((Integer)vertices.buscar(vertice)+1) + " en el grafo.");
        }
        else{
            SalidaPorDefecto.consola("No existe el vertice");
        }
    }

    public boolean esPseudografo(){
        for(int fila=0;fila<aristas.obtenerRenglones();fila++) {   //reng x reng
            for (int col = 0; col < aristas.obtenerColumnas(); col++) { //extrae col x col
                if ((Double)aristas.obtener(fila,col) != 0.0 && fila == col){
                    return true;
                }
            }
        }
        return false;
    }


    public boolean esMultiografo(){
        int cont = 0;
        for(int fila=0;fila<aristas.obtenerRenglones();fila++) {   //reng x reng
            for (int col = 0; col < aristas.obtenerColumnas(); col++) { //extrae col x col
                if ((Double)aristas.obtener(fila,col) != 0.0 && fila == col){
                    cont++;
                }
            }
        }
        if (cont >= 2){
            return true;
        }
        return false;
    }

    public int gradoVertice(Object vertice){
        int cont =0 ;
        Integer indice=(Integer)vertices.buscar(vertice);
        if (indice != null){
            for (int destino = 0; destino < aristas.obtenerColumnas(); destino++) { //extrae col x col
                Double arista = (Double) aristas.obtener(indice, destino);
                if (arista != 0.0){
                    cont++;
                }
            }
        }
        return cont;
    }

    public boolean hayRuta(Object origen, Object destino){
        boolean hayRuta = false;
        Integer indiceVerticeOrigen=(Integer)vertices.buscar(origen);
        Integer indiceVerticeDestino=(Integer)vertices.buscar(destino);
        if(indiceVerticeOrigen!=null || indiceVerticeDestino != null) { //existen
            for (int cont = 0;cont < vertices.numeroElementos();cont++){
                if(indiceVerticeOrigen != cont){
                    if (esAdyacente(indiceVerticeOrigen,cont) == true){
                        indiceVerticeOrigen = cont;
                        break;
                    }
                }

            }
        }
        if (indiceVerticeOrigen == indiceVerticeDestino){
            return true;
        }
        else {
            return false;
        }
    }

    public boolean esConexo() {
        boolean esConexo = false;
        for (int cont = 0; cont < vertices.numeroElementos(); cont++) {
            Object verticeOrigen = vertices.obtener(cont);
            for (int cont2 = 0; cont2 < vertices.numeroElementos(); cont2++) {
                Object verticeDestino = vertices.obtener(cont2);
                if (verticeOrigen != verticeDestino) {
                    if (esAdyacente(verticeOrigen, verticeDestino) == true) {
                        esConexo = true;
                    } else {
                        esConexo = false;
                        componenteMarcado = vertices.obtener(cont);
                    }
                }
            }

        }
        return esConexo;
    }
    //Metodo para la clase GrafoNoDirigido
    public boolean esConexo(ListaEstatica vertices) {
        boolean esConexo = false;
        for (int cont = 0; cont < vertices.numeroElementos(); cont++) {
            Object verticeOrigen = vertices.obtener(cont);
            for (int cont2 = 0; cont2 < vertices.numeroElementos(); cont2++) {
                Object verticeDestino = vertices.obtener(cont2);
                if (verticeOrigen != verticeDestino) {
                    if (esAdyacente(verticeOrigen, verticeDestino) == true) {
                        esConexo = true;
                    } else {
                        esConexo = false;
                        //variable global que guardara el vertice que no es conexo
                        componenteMarcado = vertices.obtener(cont);
                    }
                }
            }

        }
        return esConexo;
    }

    public void grafoNoDirigido() {
        //se utiliza la variable "VerticesManipulables" para manipular los vertices que si son conexos

        //variable que guardara el indice del primer vertice en la lista
        int cont = 0;
        //se verifica que el grafo sea conexo
        if(verticesManipulables.numeroElementos() == 0){
            SalidaPorDefecto.consola("No es un grafo Conexo");
        }
        if (esConexo(verticesManipulables) == true){
            SalidaPorDefecto.consola("Componentes Conexos: \n");
            verticesManipulables.imprimir();
            if (verticesManipulables.numeroElementos() == vertices.numeroElementos()){
                SalidaPorDefecto.consola("Es un grafo Conexo");
            }
            else{
                SalidaPorDefecto.consola("No es un grafo Conexo");
            }

        }
        else{//si no es, se repite el metodo con un valor menos, el valor que no es conexo
            verticesManipulables.eliminar(componenteMarcado);
            grafoNoDirigido();//se vuelve a repetir el ciclo
        }
    }

    public void listarAristas(){

        SalidaPorDefecto.consola("[");
        for (int origen = 0; origen< aristas.obtenerRenglones(); origen++) {
            for (int destino = 0; destino < aristas.obtenerColumnas(); destino++) { //extrae col x col
                Double peso = (Double) aristas.obtener(origen, destino);
                if ( peso != 0.0) {
                    SalidaPorDefecto.consola("(" + vertices.obtener(origen) + ", " + vertices.obtener(destino) +", "+ peso + ")");
                }
            }
        }
        SalidaPorDefecto.consola("]");

    }
    public void listarAristas(Object vertice){
        Integer indice=(Integer)vertices.buscar(vertice);
        if (indice != null){
            SalidaPorDefecto.consola("[");
                for (int destino = 0; destino < aristas.obtenerColumnas(); destino++) { //extrae col x col
                    Double peso = (Double) aristas.obtener(indice, destino);
                    if ( peso != 0.0) {
                        SalidaPorDefecto.consola("(" + vertices.obtener(indice) + ", " + vertices.obtener(destino) +", "+ peso + ")");
                    }
            }
            SalidaPorDefecto.consola("]");
        }
        else {
            SalidaPorDefecto.consola("No existe el vertice");
        }
    }

    public void listarVertices(){
        vertices.imprimir();
    }

    public boolean agregarArista(Object origen, Object destino){ // V1 -> V2
        return agregarArista(origen,destino,1.0);
    }

    public boolean agregarArista(Object origen, Object destino, double peso){ // V1 -> V2
        //Verificar que existan
        Integer indiceOrigen=(Integer)vertices.buscar(origen);
        Integer indiceDestino=(Integer)vertices.buscar(destino);

        if(indiceOrigen!=null && indiceDestino!=null){ //si existen ambos
            //creamos la arista
            return aristas.cambiar(indiceOrigen,indiceDestino,peso);
        }else{ //uno u otro no existe
            return false;
        }
    }



    public void imprimir(){
        SalidaPorDefecto.consola("Vértices:\n");
        vertices.imprimir();

        SalidaPorDefecto.consola("Aristas:\n");
        aristas.imprimirXRenglones();
    }

    //método axuliar de paso 1 de OT
    private int gradoDeEntradaXVertice(int cadaDestino){
        int gradodeEntradaVertice=0;
        //recorrer todos los renglones (origenes) hacia el vertice destino
        for(int cadaOrigen=0; cadaOrigen< aristas.obtenerRenglones(); cadaOrigen++){
            //usando la matriz obtenemos esa flecha
            Double flecha=(Double)aristas.obtener(cadaOrigen,cadaDestino);
            if(flecha!=null && flecha>0){ //hay una flecha del orgien a ese destino
                gradodeEntradaVertice++;
            }
        }
        return gradodeEntradaVertice;
    }

    //paso 1 de OT
    private void inicializarGradosEntrada(ListaEstatica gradosEntrada){
        //recorrer todos los posibles vèrtices o procesos (destinos en la matriz), para calcular en cada uno de ellos
        //la cantidad de flechas o aristas que les llega (grados de entrada).
        for(int cadaDestino=0; cadaDestino< aristas.obtenerColumnas(); cadaDestino++){
            //para cada uno de estos destinos posibles, calculemos los grados de entrada o flechas que le llegan a èl.
            //es decir los renglones (origenes) que llegan a este destino
            int gradosEntradaXVerticeDestino=gradoDeEntradaXVertice(cadaDestino);
            gradosEntrada.agregar(gradosEntradaXVerticeDestino); // el grado de entrada se guarda en  la misma posición
            //que el vertice destino
        }
    }

    //paso 2 y 5 de OT
    private void encolarYMarcarVerticesGrado0(ListaEstatica gradosEntrada, ListaEstatica marcados, ColaEstatica colaProcesamiento){
        //recorrer todos los vèrtices para determinar lo que se requiere
        for(int cadaVertice=0;cadaVertice<gradosEntrada.numeroElementos();cadaVertice++){
            //si no esta marcado y tiene grado de E 0, encolamos y marcamos
            if((int)gradosEntrada.obtener(cadaVertice)==0 && (boolean)marcados.obtener(cadaVertice)==false){
                colaProcesamiento.poner(cadaVertice); //encolamos
                marcados.cambiar(cadaVertice,true); //marcamos
            }
        }
    }

    //paso 4 de OT
    private void recalcularGradosEntradaVertices(ListaEstatica gradosEntrada, ListaEstatica marcados, int indiceVerticeActual){
        for(int cadaDestino=0; cadaDestino< aristas.obtenerColumnas(); cadaDestino++){
            //recorremos los destinos posibles provinientes del verticeActual y si tiene flecha, le afectaba
            //y que no estuviera marcado.

            //saber si desde origen a ese destino hay flecha
            Double flecha=(Double)aristas.obtener(indiceVerticeActual,cadaDestino);
            if(flecha!=null && flecha>0 && (boolean)marcados.obtener(cadaDestino)==false){ //hay flecha hacia ese destino
                //actualizamos la incidencia o grado de entrada, es decir, se resta en 1
                int gradoEntradaVerticeDestino=(int)gradosEntrada.obtener(cadaDestino);
                gradosEntrada.cambiar(cadaDestino,gradoEntradaVerticeDestino - 1);
            }
        }
    }

    //mètodo principal de OT
    public ListaDinamica ordenacionTopologica(){
        ListaDinamica ordenProcesos=new ListaDinamica(); // es el resultado de la ordenación topológica
        ColaEstatica colaProcesamiento=new ColaEstatica(vertices.numeroElementos());
        ListaEstatica gradosEntrada=new ListaEstatica(vertices.numeroElementos());
        ListaEstatica marcados=new ListaEstatica(vertices.numeroElementos());

        //0.-En otro módulo o función deberá llevarse a cabo una verificación de no existencia de ciclos.

        //1.- Inicializar las incidencias (grados de entrada de los vértices).
        inicializarGradosEntrada(gradosEntrada);

        //2.- Los procesos (vértices) con grados de entrada en 0 (no marcados)
        // se colocan en una cola de procesamiento y se marcan como ya analizados.

        //inicializar los mrcados como false
        marcados.rellenar(false,vertices.numeroElementos());
        //invocar al mètodo que determina el paso 2 como tal
        encolarYMarcarVerticesGrado0(gradosEntrada, marcados, colaProcesamiento);

        while(colaProcesamiento.vacio()==false) { //mientras no esté vacía
            //3.- Sacar un proceso (vértice) de la cola de procesamiento y
            // lo ejecutamos (mientras haya datos en la cola).
            int indiceVerticeActual=(int)colaProcesamiento.quitar();
            Vertice verticeActual=(Vertice)vertices.obtener(indiceVerticeActual);
            ordenProcesos.agregar(verticeActual.getContenido());

            //4.- Recalcular grados de entrada dado el paso 3.
            recalcularGradosEntradaVertices(gradosEntrada, marcados, indiceVerticeActual);

            //5.- Los procesos (vértices) con grado de entrada 0 (no marcados) se colocan
            // en la cola de procesamiento y se marcan como ya analizados.
            encolarYMarcarVerticesGrado0(gradosEntrada, marcados, colaProcesamiento);
        }
        return ordenProcesos;
    }

    //paso 3 de recorrido en profundidad
    private void enpilarYMarcarVerticesAdyacentes(int indiceVerticeActual, PilaEstatica pila, ListaEstatica marcados){
        for(int cadaDestino=0;cadaDestino<aristas.obtenerColumnas(); cadaDestino++){
            //recorremos a todos los destinos posibles a partir de lvértice actual (origen)
            Double flecha=(Double)aristas.obtener(indiceVerticeActual,cadaDestino);
            //hay flecha si hay adyacencia y no están marcados
            if(flecha!=null && flecha>0 && (boolean)marcados.obtener(cadaDestino)==false){
                //enpilamos
                pila.poner(cadaDestino);
                //marcamos
                marcados.cambiar(cadaDestino,true);
            }
        }
    }

    //Recorrido en profundidad
    public ListaDinamica recorridoProfunidad(Object origen){
        ListaDinamica recorridoP=new ListaDinamica();
        PilaEstatica pila=new PilaEstatica(vertices.numeroElementos());
        ListaEstatica marcados=new ListaEstatica(vertices.numeroElementos());

        //Pasos:
        //0. Validar la existencia del origen.
        Integer indiceOrigen=(Integer)vertices.buscar(origen);
        if(indiceOrigen!=null){ //existe
            //1.- Partir de un vértice origen. Este vértice se marca y se mete en una pila.
            //Llenar el arreglo de marcados con falsos.
            marcados.rellenar(false,vertices.numeroElementos());
            //marcamos este vértice origen
            marcados.cambiar(indiceOrigen,true);
            //meter el origen en la pila
            pila.poner(indiceOrigen);

            while(pila.vacio()==false) {
                //2.- Mientras existan vértices en la pila, se van a extraer (de uno por uno) y se procesarán (imprimir).
                int indiceVerticeActual=(int)pila.quitar(); //sacamos de pila
                Vertice verticeActual=(Vertice)vertices.obtener(indiceVerticeActual); //obtenemos el objeto vértice
                recorridoP.agregar(verticeActual.getContenido());//agregamos en la salida el contenido del vértice

                //3.- Los vértices adyacentes (vecinos directos) no marcados y que están enlazados al nodo que actualmente
                // se procesa (el paso 2) se marcan y se meten en la pila.
                enpilarYMarcarVerticesAdyacentes(indiceVerticeActual, pila, marcados);
            }
        }else{ //no existe
            return null;
        }

        return recorridoP;
    }

    ///////////////////////////////////////////////////DIJKSTRA

    // Paso 1
    private void inicializarEtiquetasGrafo(ListaEstatica etiquetasOptimas, int indiceVerticeOrigen, double metricaIndiceOrigen,
                                           double metricaVertices, int verticeAnterior){
        for(int cadaVertice=0; cadaVertice<vertices.numeroElementos(); cadaVertice++){
            EtiquetaGrafo etiqueta=new EtiquetaGrafo();
            etiqueta.setMetricaAcumulada(metricaVertices); // en este caso en nuestro ejemplo era infinito....
            etiqueta.setVerticeAnterior(verticeAnterior); // por ejemplo en nuestro caso - (-1)
            etiqueta.setInteracion(0);
            etiquetasOptimas.agregar(etiqueta);//agregarla a nuestro arreglo de de etiquetas
        }
        //en particular falta cambiar el valor e la métrica en el vértice origen -> 0
        EtiquetaGrafo etiquetaVerticeOrigen=(EtiquetaGrafo) etiquetasOptimas.obtener(indiceVerticeOrigen);
        etiquetaVerticeOrigen.setMetricaAcumulada(metricaIndiceOrigen); // por ejemplo con 0.0
    }

    // Paso 2
    private void actualizarMetricaAcumuladaEtiquetas(int verticeActual, ListaEstatica etiquetasOptimas, ListaEstatica permanentes, int iteracion,
                                                     double infinito){

        //recorrer todos los vértices
        for(int cadaPosibleVecino=0; cadaPosibleVecino<aristas.obtenerColumnas();cadaPosibleVecino++){
            //checar cuáles son vecinos no marcados
            Double flechaMetricaOrigenActualDestino=(Double)aristas.obtener(verticeActual, cadaPosibleVecino);
            if(flechaMetricaOrigenActualDestino!=null && flechaMetricaOrigenActualDestino!=0 && flechaMetricaOrigenActualDestino!=infinito &&
                    (boolean)permanentes.obtener(cadaPosibleVecino)==false){
                //calcularemos las métricas acumuladas desde el vértice actual a este cada vecino adyacente y si resulta mejor la métrica
                //se actualizará en la etiqueta

                //sacar la métrica acumulada del vértice actual
                EtiquetaGrafo etiquetaVerticeActual=(EtiquetaGrafo) etiquetasOptimas.obtener(verticeActual);
                double metricaAcumuladaVerticeActual=etiquetaVerticeActual.getMetricaAcumulada();
                //sumar la métrica acumulada del vértice actual +  la métrica del vértice actual hacia el vecino
                double metricaAcumuladaVerticeActualDestino= metricaAcumuladaVerticeActual + flechaMetricaOrigenActualDestino;

                //sacar la métrica acumulada del vecino
                EtiquetaGrafo etiquetaVerticeDestino=(EtiquetaGrafo) etiquetasOptimas.obtener(cadaPosibleVecino);
                double metricaVerticeDestino=etiquetaVerticeDestino.getMetricaAcumulada();

                //comparar si es mejor la métrica acumulada hacia un vecino desde el origen actual
                //no olvidad si es DEC y INC
                boolean banderaActualizarEtiqueta=false;
                if(orden==TipoOrden.DEC){ //más chico es mejor
                    if(metricaAcumuladaVerticeActualDestino<metricaVerticeDestino){ //si es mejor, actualizar
                        banderaActualizarEtiqueta=true;
                    }
                }else{ //INC, más grande es mejor
                    if(metricaAcumuladaVerticeActualDestino>metricaVerticeDestino){ //si es mejor, actualizar
                        banderaActualizarEtiqueta=true;
                    }
                } //if
                if(banderaActualizarEtiqueta==true){ //cambiar los valores de la etiqueta
                    etiquetaVerticeDestino.setInteracion(iteracion);
                    etiquetaVerticeDestino.setMetricaAcumulada(metricaAcumuladaVerticeActualDestino);
                    etiquetaVerticeDestino.setVerticeAnterior(verticeActual);
                }
            }//if vecino
        } //for
    }

    // Etiquetas de Dijkstra

    public ListaEstatica etiquetasOptimasDijkstra(Object origen){
        ListaEstatica etiquetasOptimas=new ListaEstatica(vertices.numeroElementos()); //arreglo paralelo para las etiquetas
        ListaEstatica permanentes=new ListaEstatica(vertices.numeroElementos()); //arreglo paralelo para los marcados como permanentes
        //definimos el infinito
        double infinito=0;
        if(orden==TipoOrden.DEC){ //decremental, más chico es mejor,  +infinito
            infinito=Double.MAX_VALUE;
        }else{ //incremental, más grande es mejor, -infinito
            infinito=Double.MIN_VALUE;
        }

        //Pasos:

        //0.- Validar que el oirgen exista
        Integer indiceVerticeOrigen=(Integer)vertices.buscar(origen);
        if(indiceVerticeOrigen!=null) { //si existe el origen

            //1.- Inicializar etiquetas partiendo de un nodo origen, marcándolo como permanente.
            inicializarEtiquetasGrafo(etiquetasOptimas, indiceVerticeOrigen, 0.0, infinito, -1);
            //marcar el vértice origen como permanente
            permanentes.rellenar(false, vertices.numeroElementos());//rellenado con falsos
            permanentes.cambiar(indiceVerticeOrigen,true); //marcamos el origen

            //indicar que el vértice origen es el vértice Actual, para la primera itreración
            int verticeActual=indiceVerticeOrigen;
            for(int iteracion=1; iteracion< vertices.numeroElementos(); iteracion++) { // el último vértice no se necesita procesar en el paso 2 y 3
                //2.- Calcular los nuevos valores de las etiquetas de los vértices a partir de las métricas acumuladas hacia
                //    los vecinos (adyacentes) no marcados como permanentes; todo esto partiendo del vértice acrtual. si se mejora
                //    la métrica, se actualiza la etiqueta de ese vértice.
                actualizarMetricaAcumuladaEtiquetas(verticeActual, etiquetasOptimas, permanentes,iteracion, infinito);

                //3.- Elegir el vértice con la mejor métrica acumulada (óptima), tomando en cuenta vértices no marcados como permanentes.
                //    entonces ese vértice elegido se marca y se convierte en el vértice actual.
            }
            return etiquetasOptimas;
        }else{ //no existe el origen
            return null;
        }
    }

    // Métria Origen -> Destino
    public double obtenerMetricaOptimaDijkstra(Object origen, Object destino){
        return 0.0;
    }

    // Ruta de Origen -> Destino
    public ListaDinamica obtenerRutaOptimaDijkstra(Object origen, Object destino){
        return null;
    }

    public Object obtenerVertice(int i) {
        return vertices.obtener(i);
    }
}

