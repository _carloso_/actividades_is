package estructurasLineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.auxiliares.Nodo;
import estructurasnolineales.Matriz2;
import herramientas.comunes.TipoTabla;

/**
 * Esta clase representa un TDA de tipo Lista Dinamica.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class ListaDinamica implements ListaAlmacenamiento {

    protected Nodo primero;
    protected Nodo ultimo;
    protected Nodo iterador;

    public ListaDinamica() {
        primero = null;
        ultimo = null;
        iterador = null;
    }

    @Override
    public boolean vacia() {
        if (primero == null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int agregar(Object valor) {
        Nodo nuevoNodo = new Nodo(valor);
        if (nuevoNodo != null) { // hay espacio en memoria
            if (!vacia()) {
                ultimo.setNodoDer(nuevoNodo);
                ultimo = nuevoNodo;
            } else {
                primero = nuevoNodo;
                ultimo = nuevoNodo;
            }
            return 0;
        } else { // no hay espacio en memoria
            return -1;
       }
    }

    /**
     * Este método permite agregar un nuevo nodo al inicio de la lista.
     * @param valor Es el valor del nuevo nodo a agregar.
     * @return Retorna 0 si se pudo agregar el nuevo nodo, -1 si no se pudo.
     */
    public int agregarInicio(Object valor) {
        Nodo nuevoNodo = new Nodo(valor);
        if (nuevoNodo != null) { // hay espacio en memoria
            if (!vacia()) {
                nuevoNodo.setNodoDer(primero);
                primero = nuevoNodo;
            } else {
                primero = nuevoNodo;
                ultimo = nuevoNodo;
            }
            return 0;
        } else { // no hay espacio en memoria
            return -1;
        }
    }

    /**
     * Este método permite eliminar el primer nodo de la lista.
     * @return Regresa el contenido del nodo eliminado.
     */
    public Object eliminarInicio() {
        if (vacia() == false) { // hay algo
            Object contenidoEliminado = primero.getContenido(); // paso 1
            if (primero == ultimo) { // solo hay un nodo b)
                // paso 2
                primero = null;
                ultimo = null;
            } else { // hay mas de un nodo c)
                // paso 2
                primero = primero.getNodoDer();
            }
            return contenidoEliminado;
        } else { // no hay nada
            return null;
        }
    }

    @Override
    public void imprimir() {
       Nodo iterador = primero;
       while (iterador != null) {
           Object contenido = iterador.getContenido();
           SalidaPorDefecto.consola(contenido + " -> ");
           iterador = iterador.getNodoDer();
       }
       SalidaPorDefecto.consola("null");
    }

    @Override
    public void imprimirOI() {
        // Imprimir en orden inverso los elementos de la lista.
        // A -> B -> C -> null, forma normal de imprimir.
        // C -> B -> A -> null, forma inversa de imprimir.
        invertir(); // se invierte el orden de la lista.
        imprimir(); // se imprime la lista invertida.
        invertir(); // se regresa el orden de la lista a su forma original.
    }

    @Override
    public Object buscar(Object valor) {
        Nodo nodoBuscado = primero;
        while (nodoBuscado != null && !valor.toString().equalsIgnoreCase(nodoBuscado.toString())) { // buscamos mientras haya nodos y no lo encuentre
            nodoBuscado = nodoBuscado.getNodoDer(); // avanzo al siguiente nodo
        }
        if (nodoBuscado == null) { // no se encontro el valor
            return null;
        } else { // se encontro el valor
            return nodoBuscado.getContenido();
        }
    }

    /**
     * Este método permite buscar un nodo en la lista dinámica y regresar su posicion.
     * @param valor Es el valor del nodo a buscar.
     * @return Regresa la posicion del nodo buscado si se encuentra, -1 si no se encuentra.
     */
    public int buscarPos(Object valor) {
        Nodo nodoBuscado = primero;
        int posicion = 0;
        while (nodoBuscado != null && !valor.toString().equalsIgnoreCase(nodoBuscado.toString())) { // buscamos mientras haya nodos y no lo encuentre
            nodoBuscado = nodoBuscado.getNodoDer(); // avanzo al siguiente nodo
            posicion++;
        }
        if (nodoBuscado == null) { // no se encontró el valor (elemento)
            return -1;
        } else { // se encontró el valor (elemento)
            return posicion;
        }
    }

    /**
     * Este método permite eliminar un nodo en la lista dinamica en la posicion indicada.
     * @param indice Es la posicion del nodo a eliminar.
     * @return Regresa el contenido del nodo eliminado.
     */
    public Object eliminarPos(int indice) {
        int cantidadDeNodos = contarCantidadDeNodos();
        if (indice == 0) {
            return eliminarInicio();
        } else if (indice > 0 && indice < cantidadDeNodos) {
            Nodo nodoBuscado = primero;
            int posicionDelNodoActual = 0;
            while (posicionDelNodoActual < indice - 1) {
                nodoBuscado = nodoBuscado.getNodoDer();
                posicionDelNodoActual++;
            }
            Nodo nodoEliminado = nodoBuscado.getNodoDer();
            nodoBuscado.setNodoDer(nodoEliminado.getNodoDer());
            return nodoEliminado.getContenido();
        } else {
            return null;
        }
    }

    @Override
    public Object eliminar(Object valor) {
        if (vacia() == false) { // hay algo
            Nodo nodoAnterior = primero;
            Nodo nodoBuscado = primero;
            while (nodoBuscado != null && !valor.toString().equalsIgnoreCase(nodoBuscado.toString())) { // buscamos mientras haya nodos y no lo encuentre
                nodoAnterior = nodoBuscado;
                nodoBuscado = nodoBuscado.getNodoDer(); // avanzo al siguiente nodo
            }
            if (nodoBuscado == null) { // no se encontro el valor b)
                return null;
            } else { // si se encontro
                Object contenidoEliminado = nodoBuscado.getContenido();
                if (primero == ultimo) { // c)
                    primero = null;
                    ultimo = null;
                } else if (nodoBuscado == primero) { // d)
                    primero = primero.getNodoDer();
                } else if (nodoBuscado == ultimo) { // e)
                    ultimo = nodoAnterior;
                    ultimo.setNodoDer(null);
                } else { // f)
                    nodoAnterior.setNodoDer(nodoBuscado.getNodoDer());
                }
                return contenidoEliminado;
            }
        } else { // no hay nada a)
            return null;
        }
    }

    @Override
    public boolean esIgual(ListaAlmacenamiento lista2) {
        if (lista2 instanceof ListaDinamica) { // si la lista de almacenamiento es una lista dinámica
            ListaDinamica listaDinamica = (ListaDinamica) lista2;
            if (listaDinamica.contarCantidadDeNodos() == contarCantidadDeNodos()) {
                Nodo iterador = primero;
                Nodo iterador2 = listaDinamica.primero;
                while (iterador != null && iterador2 != null) {
                    if (iterador.getContenido() != iterador2.getContenido()) {
                        return false;
                    }
                    iterador = iterador.getNodoDer();
                    iterador2 = iterador2.getNodoDer();
                }
                return true;
            } else {
                return false;
            }
        } else if (lista2 instanceof ListaEstatica) { // si la lista de almacenamiento es una lista estática
            ListaEstatica listaEstatica = (ListaEstatica) lista2;
            if (listaEstatica.numeroElementos() == contarCantidadDeNodos()) {
                Nodo iterador = primero;
                int pos = 0;
                while (iterador != null && pos < listaEstatica.numeroElementos()) {
                    if (iterador.getContenido() != listaEstatica.obtener(pos)) {
                        return false;
                    }
                    iterador = iterador.getNodoDer();
                    pos++;
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean cambiar(Object valorViejo, Object valorNuevo, int numVeces) {
        // Cambiar el valor de un nodo de la lista por otro valor dado.
        // Si el valor no existe, no hacer nada.
        // Si el valor existe, cambiarlo por el nuevo valor numVeces veces.
        // Si el valor existe mas de numVeces veces, cambiarlo por el nuevo valor una numVeces.
        if (vacia() == false) { // hay algo
            Object nodoBuscado = buscar(valorViejo);
            if (nodoBuscado != null && numVeces > 0) { // se encontro el valor, por lo menos una vez
                int vecesValorBuscado = contar(valorViejo);
                if (vecesValorBuscado >= numVeces) { // hay mas veces que las que se quieren cambiar
                    Nodo iterador = primero;
                    while (iterador != null && numVeces > 0) {
                        if (iterador.getContenido().equals(valorViejo)) { // se encontro el valor
                            int posicion = buscarPos(valorViejo);
                            eliminarPos(posicion);
                            insertar(valorNuevo, posicion);
                            numVeces--;
                        }
                        iterador = iterador.getNodoDer();
                    }
                    return true;
                } else { // no hay mas veces que las que se quieren cambiar
                    return false;
                }
            } else { // no se encontro el valor o numVeces es menor igual p menor que cero
                return false;
            }
        } else { // no hay nada
            return false;
        }
    }

    @Override
    public ListaEstatica buscarValores(Object valor) {
        ListaEstatica listaValores = new ListaEstatica(contarCantidadDeNodos());
       // Buscar todos los valores de la lista que coincidan con el valor dado y gurdar sus posiciones en una lista estática.
        if (vacia() == false) { // hay algo
            Nodo iterador = primero;
            while (iterador != null) {
                if (iterador.getContenido().equals(valor)) { // se encontro el valor
                    int posicionBuscada = buscarPos(valor);
                    listaValores.agregar(posicionBuscada);
                }
                iterador = iterador.getNodoDer();
            }
            return listaValores;
        } else { // no hay nada
            return null;
        }
    }

    @Override
    public Object eliminar() { // elimina el ultimo nodo
        if (vacia() == false) { // hay algo
            Object contenidoEliminado = ultimo.getContenido(); // paso 1
            if (primero == ultimo) { // solo hay un nodo b)
                // paso 2
                primero = null;
                ultimo = null;
            } else { // hay mas de un nodo c)
                // bucara el nodo penultimo
                Nodo penultimo = primero;
                while (penultimo.getNodoDer() != ultimo) {
                    penultimo = penultimo.getNodoDer(); // i = i + 1
                }
                ultimo = penultimo; // paso 2
                penultimo.setNodoDer(null); // paso 3
            }
            return contenidoEliminado;
        } else { // no hay nada a)
            return null;
        }
    }

    @Override
    public boolean agregarLista(ListaAlmacenamiento lista2) {
        if (lista2 instanceof ListaDinamica) {
            // agregar cada elemento de la lista dinámica a la lista actual
            ListaDinamica listaDinamica = (ListaDinamica) lista2;
            Nodo iterador = listaDinamica.primero;
            while (iterador != null) {
                agregar(iterador.getContenido());
                iterador = iterador.getNodoDer();
            }
            return true;
        } else if (lista2 instanceof ListaEstatica) {
            // agregar cada elemento de la lista estática a la lista actual
            ListaEstatica listaEstatica = (ListaEstatica) lista2;
            for (int pos = 0; pos < listaEstatica.numeroElementos(); pos++) {
                agregar(listaEstatica.obtener(pos));
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void invertir() {
        Nodo iterador = primero;
        Nodo anterior = null;
        Nodo siguiente = null;
        while (iterador != null) {
            siguiente = iterador.getNodoDer(); // guardo el siguiente nodo
            iterador.setNodoDer(anterior); // cambio el siguiente nodo
            anterior = iterador; // avanzo al siguiente nodo
            iterador = siguiente; // avanzo al siguiente nodo
        }
        ultimo = primero; // el ultimo es el primero
        primero = anterior; // el primero es el ultimo
    }

    @Override
    public int contar(Object valor) {
        Nodo iterador = primero;
        int contador = 0;
        while (iterador != null) {
            if (valor.toString().equalsIgnoreCase(iterador.toString())) {
                contador++;
            }
            iterador = iterador.getNodoDer();
        }
        return contador;
    }

    @Override
    public boolean eliminarLista(ListaAlmacenamiento lista2) {
        if (lista2 instanceof ListaDinamica) {
            ListaDinamica listaDinamica = (ListaDinamica) lista2;
            Nodo iterador = listaDinamica.primero;
            while (iterador != null) {
                eliminar(iterador.getContenido());
                iterador = iterador.getNodoDer();
            }
            return true;
        } else if (lista2 instanceof ListaEstatica) {
            ListaEstatica listaEstatica = (ListaEstatica) lista2;
            for (int pos = 0; pos < listaEstatica.numeroElementos(); pos++) {
                eliminar(listaEstatica.obtener(pos));
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void rellenar(Object valor, int cantidad) {
        for (int iterador = 0; iterador < cantidad; iterador++) {
            agregar(valor);
        }
    }

    @Override
    public Object clonar() {
        ListaDinamica listaClonada = new ListaDinamica();
        Nodo iterador = primero;
        while (iterador != null) {
            listaClonada.agregar(iterador.getContenido());
            iterador = iterador.getNodoDer();
        }
        return listaClonada;
    }

    @Override
    public Object subLista(int indiceInicial, int indiceFinal) {
        return null;
    }

    @Override
    public boolean esSublista(ListaAlmacenamiento lista2) {
        return false;
    }

    @Override
    public boolean insertar(Object valor, int indice) {
        int cantidadDeNodos = contarCantidadDeNodos();
        if (indice == 0) { // agregar al inicio
             int valorRetorno = agregarInicio(valor);
             if (valorRetorno == 0) {
                 return true;
             } else {
                 return false;
             }
        } else if (indice >= 0 && indice <= cantidadDeNodos) { // posicion valida, se puede agregar
            Nodo iterador = primero;
            int posicionActual = 0;
            while (posicionActual < indice - 1) {
                iterador = iterador.getNodoDer();
                posicionActual++;
            }
            Nodo nuevoNodo = new Nodo(valor);
            nuevoNodo.setNodoDer(iterador.getNodoDer());
            iterador.setNodoDer(nuevoNodo);
            return true;
        } else { // la posicion es mayor que la cantidad de nodos o es negativa, se agrega al final
            int valorRetorno = agregar(valor);
            if (valorRetorno == 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public void rellenar(Object valor) {

    }

    @Override
    public Object verUltimo(){
        if (vacia() == false) {
            return ultimo.getContenido();
        } else {
            return null;
        }
    }

    /**
     * Este método cuenta la cantidad de elementos (nodos) que tiene la lista.
     * @return Regresa la cantidad de elementos que tiene la lista dinámica.
     */
    public int contarCantidadDeNodos() {
        Nodo iterador = primero;
        int cantidadElementos = 0;
        while (iterador != null) {
            cantidadElementos++;
            iterador = iterador.getNodoDer();
        }
        return cantidadElementos;
    }

    /**
     * Este método agrega cada elemento de la lista dinámica a una lista estática.
     * @return Regresa la lista estática con los elementos de la lista dinámica.
     */
    public ListaEstatica aListaEstatica() {
        int cantidadElementos = contarCantidadDeNodos();
        ListaEstatica listaEstatica = new ListaEstatica(cantidadElementos);
        Nodo iterador = primero;
        while (iterador != null) {
            listaEstatica.agregar(iterador.getContenido());
            iterador = iterador.getNodoDer();
        }
        return listaEstatica;
    }

    /**
     * Este método guarda los elementos de la lista dinámica actual en una lista estática, excepto los elementos de
     * la lista estática pasada como parámetro.
     * @param elementosADescartar Es la lista estática que contiene los elementos que no se deben guardar.
     * @return Regresa la lista estática con los elementos de la lista dinámica actual excepto los elementos de la lista
     * estática pasada como parámetro.
     */
    public ListaEstatica aListaEstatica(ListaEstatica elementosADescartar) {
       int cantidadElementos = contarCantidadDeNodos();
       ListaEstatica listaEstatica = new ListaEstatica(cantidadElementos);
       Nodo iterador = primero;
       while (iterador != null) {
           Object contenido = iterador.getContenido();
           if (elementosADescartar.buscar(contenido) == null) {
               listaEstatica.agregar(contenido);
           }
           iterador = iterador.getNodoDer();
       }
       return listaEstatica;
    }

    /**
     * Este método guarda los elementos de la lista dinámica actual en una matriz2D.
     * @param renglones Es la cantidad de renglones que tendrá la matriz2D.
     * @param columnas Es la cantidad de columnas que tendrá la matriz2D.
     * @return Regresa la matriz2D con los elementos de la lista dinámica actual.
     */
    public Matriz2 aMatriz2D(int renglones, int columnas) {
        Matriz2 matriz2D = new Matriz2(renglones, columnas, null);
        Nodo iterador = primero;
        int renglon = 0;
        int columna = 0;
        while (iterador != null) {
            matriz2D.cambiar(renglon, columna, iterador.getContenido());
            iterador = iterador.getNodoDer();
            columna++;
            if (columna == columnas) {
                columna = 0;
                renglon++;
            }
        }
        return matriz2D;
    }

    /**
     * Este método agrega los elementos de una matriz2D al final de la lista dinámica actual.
     * @param tabla Es la matriz2D que contiene los elementos que se van a agregar a la lista dinámica actual.
     * @param enumTipoTabla Es la forma en que se van a agregar los elementos de la matriz2D a la lista dinámica actual.
     * @return Regresa true si los elementos de la matriz2D fueron agregados a la lista dinámica actual. De lo contrario
     * regresa false.
     */
    public boolean agregarMatriz2D(Matriz2 tabla, TipoTabla enumTipoTabla){
        if (enumTipoTabla == TipoTabla.COLUMNA) {
            for (int col = 0; col < tabla.obtenerColumnas(); col++) {
                for (int fila = 0; fila < tabla.obtenerRenglones(); fila++) {
                    agregar(tabla.obtenerValor(fila, col));
                }
            }
            return true;
        } else if (enumTipoTabla == TipoTabla.RENGLON) {
            for (int fila = 0; fila < tabla.obtenerRenglones(); fila++) {
                for (int col = 0; col < tabla.obtenerColumnas(); col++) {
                    agregar(tabla.obtenerValor(fila, col));
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método vacía la lista dinámica actual.
     */
    public void vaciar(){
        primero = ultimo = null;
    }

    /**
     * Este método cambia el valor de un elemento de la lista dinámica actual por otro, en una determinada posición.
     * @param indice Es la posición del elemento que se va a cambiar.
     * @param contenido Es el nuevo valor del elemento que se va a cambiar.
     * @return Regresa true si el elemento fue cambiado. De lo contrario regresa false.
     */
    public boolean cambiar(int indice, Object contenido) {
        int cantidadDeElementos = contarCantidadDeNodos();
        if (indice >= 0 && indice < cantidadDeElementos) {
            Object elemento = obtener(indice);
            if (elemento != null) {
                eliminarPos(indice);
                insertar(contenido, indice);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite obtener el elemento de la lista de una determinada posición.
     * @param indice Es la posición del elemento que se va a obtener.
     * @return Regresa el elemento de la lista de la posición indicada.
     */
    public Object obtener(int indice) {
        int cantidadDeNodos = contarCantidadDeNodos();
        if (indice >= 0 && indice < cantidadDeNodos) { // validar que el índice sea válido, que esté dentro del rango de la lista
            Nodo iterador = primero; // inicializar el iterador
            for (int pos = 0; pos < indice; pos++) { // se recorre la lista hasta llegar a la posición del elemento que se desea obtener
                iterador = iterador.getNodoDer();
            }
            return iterador.getContenido();
        } else {
            return null;
        }
    }

    /**
     * Este método nos permite redimensionar la lista dinámica actual.
     * @param maximo Es el nuevo tamaño máximo de la lista dinámica actual.
     * @return Regresa true si la lista dinámica actual fue redimensionada. De lo contrario regresa null.
     */
    public Object redimensionar(int maximo) {
        int cantidadDeElementos = contarCantidadDeNodos();
        // redimensiona el tamaño de la lista al nuevo tamaño indicado por máximo.
        // Si el tamaño es menor, los elementos sobrantes deben ser eliminados.
        // Si el tamaño es mayor, los datos anteriores deben conservarse, y los nuevos espacios deben llenarse con null.
        if (maximo >= 0) {
            if (maximo < cantidadDeElementos) {
                Nodo iterador = primero;
                for (int pos = 0; pos < maximo; pos++) {
                    iterador = iterador.getNodoDer();
                }
                while (iterador != null) {
                    eliminar(iterador.getContenido());
                    iterador = iterador.getNodoDer();
                }
                return true;
            } else if (maximo > cantidadDeElementos) {
                for (int pos = cantidadDeElementos; pos < maximo; pos++) {
                    insertar(null, pos);
                }
                return true;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Este método inicializa un iterador para recorrer la lista dinámica actual.
     */
    public void iniciarIterador(){
        iterador = primero;
    }

    /**
     * Este método nos permite saber si aún existen elementos por recorrer en la lista dinámica actual.
     * @return Regresa true si aún existen elementos por recorrer en la lista dinámica actual. De lo contrario regresa false.
     */
    public boolean hayNodos(){
        if (iterador != null){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite obtener el elemento actual de la lista dinámica.
     * @return Regresa el elemento actual de la lista dinámica.
     */
    public Object obtenerNodo(){
        if (hayNodos() == true){
            Object contenidoDelNodo = iterador.getContenido();
            iterador = iterador.getNodoDer();
            return contenidoDelNodo;
        } else { // no hay nodos
            return null;
        }
    }

    @Override
    public Object verPrimero() {
        if (primero != null) {
            return primero.getContenido();
        } else {
            return null;
        }
    }

    /**
     * Este método nos permite obtener el primer elemento de la lista dinámica actual.
     * @return Regresa el primer elemento de la lista dinámica actual.
     */
    public Nodo obtenerPrimero() {
        Nodo nodo = primero;
        return nodo;
    }
}