package registros.procesamientoDeImagen;

import entradaSalida.SalidaPorDefecto;
import estructurasnolineales.Matriz2Numerica;
import herramientas.comunes.TipoTamainio;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Esta clase se encarga de procesar y modificar una imagen.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class ProcesarImagen {

    private BufferedImage imagenActual;
    private Matriz2Numerica matrizImagen;

    /**
     * Este método abre una ventana para seleccionar una imagen y la muestra en una ventana.
     * @return Regresa la imagen seleccionada.
     */
    public BufferedImage abrirImagen(){
        //Creamos la variable que será devuelta (la creamos como null)
        BufferedImage bmp = null;
        //Creamos un nuevo cuadro de diálogo para seleccionar imagen
        JFileChooser selector=new JFileChooser();
        //Le damos un título
        selector.setDialogTitle("Seleccione una imagen");
        //Filtramos los tipos de archivos
        FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG, PNG", "jpg", "png");
        selector.setFileFilter(filtroImagen);
        //Abrimos el cuadro de diálog
        int opcion = selector.showOpenDialog(null);
        //Comprobamos que pulse en aceptar
        if(opcion == JFileChooser.APPROVE_OPTION){
            try {
                // Devuelvo la imagen seleccionado
                File imagenSeleccionada = selector.getSelectedFile();
                //Asignamos a la variable bmp la imagen leida
                bmp = ImageIO.read(imagenSeleccionada);
            } catch (Exception e) {
                SalidaPorDefecto.jOptionPaneMensaje("Error: " + e.getMessage());
            }
        }
        //Asignamos la imagen cargada a la propiedad imageActual
        imagenActual = bmp;
        // gurdamos la imagen en nuestra matriz
        guardarMatrizImagen(imagenActual);
        //Retornamos el valor
        return bmp;
    }

    /**
     * Este método muestra a la imagen actual en una ventana.
     * @param imagen Es la imagen que se va a mostrar en la ventana.
     */
    public void mostrarImagen(BufferedImage imagen, String titulo){
        //Creamos una ventana
        JFrame frame = new JFrame();
        //Le damos un título
        frame.setTitle(titulo);
        //Le damos un tamaño
        frame.setSize(imagen.getWidth(),imagen.getHeight());
        //Damos la imagen al frame
        frame.getContentPane().add(new JLabel(new ImageIcon(imagen)));
        // creo un panel para un boton de guardar que mande llamar al método guardarImagen
        JPanel panel = new JPanel();
        JButton boton = new JButton("Guardar");
        boton.addActionListener(e -> guardarImagen());
        panel.add(boton);
        //Añadimos el panel al frame
        frame.getContentPane().add(panel, BorderLayout.SOUTH);
        //Hacemos visible la ventana
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * Este método guarda los pixeles de la imagen en una matriz 2D numerica.
     * @param imagen Es la imagen que contiene los pixeles que se van a guardar en la matriz.
     */
    private boolean guardarMatrizImagen(BufferedImage imagen){
        boolean seGuardado = false;
        if (imagen != null) {
            //Creamos una matriz de numeros para guardar los pixles de la imagen
            matrizImagen = new Matriz2Numerica(imagen.getWidth(), imagen.getHeight());
            //Recorremos la matriz para guardar los pixles
            for(int fila = 0; fila < imagen.getWidth(); fila++){
                for(int col = 0; col < imagen.getHeight(); col++){
                    //Asignamos el valor del pixel a la matriz
                    matrizImagen.cambiar(fila, col, imagen.getRGB(fila,col));
                }
            }
            seGuardado = true;
        }
        return seGuardado;
    }

    /**
     * Este método nos permite guardar la imagen actual con ayuda de un JFileChooser.
     */
    private void guardarImagen(){
        // gurdar la imgaen actual ya modificada en una ruta especificada
        try {
            //Creamos una ventana para guardar la imagen
            JFileChooser selector = new JFileChooser();
            //Le damos un título
            selector.setDialogTitle("Guardar imagen");
            //Filtramos los tipos de archivos
            FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG", "jpg");
            selector.setFileFilter(filtroImagen);
            imagenActual.flush();
            //Abrimos el cuadro de diálog
            int flag=selector.showSaveDialog(null);
            //Comprobamos que pulse en aceptar
            if(flag == JFileChooser.APPROVE_OPTION){
                //Devuelve el fichero seleccionado
                File imagenSeleccionada = selector.getSelectedFile();
                //Asignamos a la variable bmp la imagen leida
                ImageIO.write(imagenActual, "jpg", imagenSeleccionada);
                SalidaPorDefecto.jOptionPaneMensaje("Se guardo correctamente la imagen");
            } else {
                SalidaPorDefecto.consola("No se ha guardado la imagen");
            }

        }  catch (Exception e) {
            SalidaPorDefecto.jOptionPaneMensaje("Error: " + e.getMessage());
        }
    }

    /**
     * Este método convierte la imagen a una escala de grises.
     * @return Regresa una nueva imagen modificada con la escala de grises.
     */
    public BufferedImage escalaDeGrises(){
        //Variables que almacenarán los píxeles
        int red, green, blue;
        int promedioPixel, colorSRGB;
        // Variables para el tamaño de la imagen
        int ancho = imagenActual.getWidth();
        int alto = imagenActual.getHeight();
        // Variable para el color de la imagen
        Color colorAux;
        // Validamos que la imagen no sea nula (se valida que la matriz no sea nula)
        if (matrizImagen.obtenerRenglones() != 0 && matrizImagen.obtenerColumnas() != 0) {
            // Creamos una nueva imagen con el mismo tamaño que la original
            BufferedImage nuevaImagen = new BufferedImage(ancho, alto, imagenActual.getType());
            // Recorremos la matriz pixel a pixel
            for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++) {
                for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++) {
                    // Almacenamos el color del píxel
                    colorAux = new Color(imagenActual.getRGB(filaPixel, colPixel));
                    // Guardamos los valores de los colores del píxel en las variables correspondientes (R,G,B)
                    red = colorAux.getRed();
                    green = colorAux.getGreen();
                    blue = colorAux.getBlue();
                    // Calculamos el promedio de los colores del píxel
                    promedioPixel = ((red + green + blue) / 3);
                    // Cambiamos a formato a sRGB
                    colorSRGB = (promedioPixel << 16) | (promedioPixel << 8) | promedioPixel;
                    // Asignamos el nuevo valor a la nueva imagen
                    nuevaImagen.setRGB(filaPixel, colPixel, colorSRGB);
                }
            }
            imagenActual = nuevaImagen;
            return nuevaImagen;
        } else {
            return null;
        }
    }

    /**
     * Este método aumenta el brillo de la imagen en un valor especificado.
     * @param valor Es el valor que se le sumará a cada píxel de la imagen para aumentar el brillo.
     * @return Regresa una nueva imagen con el brillo aumentado.
     */
   public BufferedImage aumentarBrillo(int valor){
        // se le suma el valor a cada pixel de la imagen
       BufferedImage nuevaImagen = new BufferedImage(imagenActual.getWidth(), imagenActual.getHeight(), BufferedImage.TYPE_INT_RGB);
       for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++) {
           for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++) {
               // sumara a cada pixel el valor
               nuevaImagen.setRGB(filaPixel, colPixel, imagenActual.getRGB(filaPixel, colPixel) + valor);
           }
       }
       imagenActual = nuevaImagen;
       return nuevaImagen;
   }

    /**
     * Este método disminuye el brillo de la imagen en un valor especificado.
     * @param valor Es el valor que se le restara a cada pixel de la imagen para disminuir el brillo.
     * @return Regresa una nueva imagen con el brillo disminuido.
     */
   public BufferedImage disminuirBrillo(int valor){
       // se le resta el valor a cada pixel de la imagen
       BufferedImage nuevaImagen = new BufferedImage(imagenActual.getWidth(), imagenActual.getHeight(), imagenActual.getType());
       for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++) {
           for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++) {
               // restara a cada pixel el valor
               nuevaImagen.setRGB(filaPixel, colPixel, imagenActual.getRGB(filaPixel, colPixel) - valor);
           }
       }
       imagenActual = nuevaImagen;
       return nuevaImagen;
   }

    /**
     * Este método gira a la imagen actual de forma horizontal.
     * @return Regresa una nueva imagen rotada horizontalmente.
     */
    public BufferedImage guirarImagenEnHorizontal(){
        //Creamos una nueva imagen
        BufferedImage nuevaImagen = new BufferedImage(imagenActual.getWidth(), imagenActual.getHeight(), imagenActual.getType());
        //Recorremos la imagen píxel a píxel
        for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++ ){
            for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++ ){
                //Cambiamos el valor de la imagen
                nuevaImagen.setRGB(imagenActual.getWidth() - filaPixel - 1, colPixel, imagenActual.getRGB(filaPixel, colPixel));
            }
        }
        // Asignamos la imagen a la propiedad imagenActual
        imagenActual = nuevaImagen;
        // Retornamos la imagen nueva ya modificada
        return nuevaImagen;
    }

    /**
     * Este método girará la imagen en forma vertical.
     * @return Regresa una nueva imagen girada verticalmente.
     */
    public BufferedImage girarImagenEnVertical(){
        int ancho = imagenActual.getWidth();
        int alto = imagenActual.getHeight();
        int aux = alto - 1;
        //Creamos una nueva imagen
        BufferedImage nuevaImagen = new BufferedImage(ancho, alto, imagenActual.getType());
        //Recorremos la imagen píxel a píxel
        for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++ ){
            for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++ ){
                int valorPixel = imagenActual.getRGB(filaPixel, colPixel);
                nuevaImagen.setRGB(filaPixel, aux - colPixel, valorPixel);
            }
        }
        // Asignamos la imagen a la propiedad imagenActual
        imagenActual = nuevaImagen;
        // Retornamos la imagen nueva ya modificada
        return nuevaImagen;
    }

    /**
     * Este método gira la imagen 90 grados.
     * @return Regresa una nueva imagen girada en 90 grados.
     */
    public BufferedImage girarImagenEn90Grados(){
        int ancho = matrizImagen.obtenerColumnas();
        int alto = matrizImagen.obtenerRenglones();
        // crear una nueva imagen con el tamaño de la matriz y con los valores de la matriz
        BufferedImage nuevaImagen = new BufferedImage(ancho, alto, imagenActual.getType());
        //Recorremos la imagen píxel a píxel
        for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++ ){
            for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++ ){
                int valorPixel = imagenActual.getRGB(filaPixel, colPixel);
                //Cambiamos el valor de la imagen
                nuevaImagen.setRGB(colPixel, alto - filaPixel - 1, valorPixel);
            }
        }
        //Asignamos la imagen nueva a la propiedad de la imagen actual
        imagenActual = nuevaImagen;
        //Retornamos la nueva imagen
        return nuevaImagen;
    }


    /**
     * Este método gira la imagen 180 grados.
     * @return Regresa una nueva imagen girada en 180 grados.
     */
    public BufferedImage girarImagenEn180Grados(){
        int ancho = imagenActual.getWidth();
        int alto = imagenActual.getHeight();
        int aux = alto - 1;
        //Creamos una nueva imagen
        BufferedImage nuevaImagen = new BufferedImage(ancho, alto, imagenActual.getType());
        //Recorremos la imagen píxel a píxel
        for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++ ){
            for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++ ){
                int valorPixel = imagenActual.getRGB(filaPixel, colPixel);
                nuevaImagen.setRGB(filaPixel, aux - colPixel, valorPixel);
            }
        }
        // Asignamos la imagen a la propiedad imagenActual
        imagenActual = nuevaImagen;
        // Retornamos la imagen nueva ya modificada
        return nuevaImagen;
    }

    /**
     * Este método se encarga de hacer girar a la imagen en 270 grados.
     * @return Regresa una nueva imagen girada en 270 grados.
     */
    public BufferedImage girarImagenEn270Grados(){
        return aplicarTranspuesta();
    }

    /**
     * Este método aplica la transpuesta a la matriz que almacena los pixeles de la imagen actual, y retorna una
     * nueva imagen con los pixeles de la matriz transpuestos.
     * @return Regresa una nueva imagen con los pixeles transpuestos.
     */
    // Cuando se aplica la transpuesta a la matriz que almacena los pixeles de la imagen actual,
    // se optiene una nueva imagen girada en 270 grados.
    public BufferedImage aplicarTranspuesta(){
        matrizImagen.transpuesta();
        int ancho = matrizImagen.obtenerColumnas();
        int alto = matrizImagen.obtenerRenglones();
        // crear una nueva imagen con el tamaño de la matriz y con los valores de la matriz
        BufferedImage nuevaImagen = new BufferedImage(ancho, alto, imagenActual.getType());
        // recorrer la matriz y asignar los valores a la nueva imagen
        for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++){
            for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++){
                int valorPixel = imagenActual.getRGB(filaPixel, colPixel);
                nuevaImagen.setRGB(colPixel, filaPixel, valorPixel);
            }
        }
        //Asignamos la imagen a la propiedad imageActual
        imagenActual = nuevaImagen;
        //Retornamos la imagen
        return nuevaImagen;
    }

    /**
     * Este método permite redimensionar la imagen actual, ya sea al aumentar o disminuir su tamaño.
     * @param tamanio Es el tamaño que se desea aplicar a la imagen.
     * @return Regresa una nueva imagen con el tamaño deseado.
     */
    public BufferedImage redimensionarImagen(TipoTamainio tamanio){
        if (tamanio == TipoTamainio.AU_DOBELE){
            return aumentarTamanioAlDobele();
        } else if (tamanio == TipoTamainio.AU_TRIPLE){
            return aumentarTamanioAlTriple();
        } else if (tamanio == TipoTamainio.DI_MITAD){
            return disminuirTamanioAlaMitad();
        } else {
            return null;
        }
    }

    /**
     * Este método aumenta el tamaño de la imagen actual al doble de su tamaño original.
     * @return Regresa una nueva imagen con el doble de tamaño de la imagen original.
     */
    private BufferedImage aumentarTamanioAlDobele(){
        //Creamos una nueva imagen
        BufferedImage nuevaImagen = new BufferedImage(imagenActual.getWidth() * 2, imagenActual.getHeight() * 2, imagenActual.getType());
        //Recorremos la imagen píxel a píxel
        for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++ ){
            for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++ ){
                //Cambiamos el valor de la imagen
                nuevaImagen.setRGB(filaPixel * 2, colPixel * 2, imagenActual.getRGB(filaPixel, colPixel));
                nuevaImagen.setRGB(filaPixel * 2, colPixel * 2 + 1, imagenActual.getRGB(filaPixel, colPixel));
                nuevaImagen.setRGB(filaPixel * 2 + 1, colPixel * 2, imagenActual.getRGB(filaPixel, colPixel));
                nuevaImagen.setRGB(filaPixel * 2 + 1, colPixel * 2 + 1, imagenActual.getRGB(filaPixel, colPixel));
            }
        }
        //Asignamos la imagen a la propiedad imageActual
        imagenActual = nuevaImagen;
        //Retornamos la imagen
        return nuevaImagen;
    }

    /**
     * Este método aumenta el tamaño de la imagen actual al triple de su tamaño original.
     * @return Regresa una nueva imagen con el tamaño al triple.
     */
    private BufferedImage aumentarTamanioAlTriple(){
        //Creamos una nueva imagen
        BufferedImage nuevaImagen = new BufferedImage(imagenActual.getWidth() * 3, imagenActual.getHeight() * 3, imagenActual.getType());
        //Recorremos la imagen píxel a píxel
        for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++ ){
            for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++ ){
                // Obtenemos el valor del pixel
                int valorPixel = imagenActual.getRGB(filaPixel, colPixel);
                //Cambiamos el valor de la imagen
                nuevaImagen.setRGB(filaPixel * 3, colPixel * 3, valorPixel);
                nuevaImagen.setRGB(filaPixel * 3, colPixel * 3 + 1, valorPixel);
                nuevaImagen.setRGB(filaPixel * 3, colPixel * 3 + 2, valorPixel);
                nuevaImagen.setRGB(filaPixel * 3 + 1, colPixel * 3, valorPixel);
                nuevaImagen.setRGB(filaPixel * 3 + 1, colPixel * 3 + 1, valorPixel);
                nuevaImagen.setRGB(filaPixel * 3 + 1, colPixel * 3 + 2, valorPixel);
                nuevaImagen.setRGB(filaPixel * 3 + 2, colPixel * 3, valorPixel);
                nuevaImagen.setRGB(filaPixel * 3 + 2, colPixel * 3 + 1, valorPixel);
                nuevaImagen.setRGB(filaPixel * 3 + 2, colPixel * 3 + 2, valorPixel);
            }
        }
        //Asignamos la imagen a la propiedad imageActual
        imagenActual = nuevaImagen;
        // Retornamos la imagen nueva con el triple de tamaño
        return nuevaImagen;
    }

    /**
     * Este método se encarga de disminuir el tamaño de la imagen a la mitad.
     * @return Regresa una nueva imagen con el tamaño reducido a la mitad.
     */
    private BufferedImage disminuirTamanioAlaMitad(){
        int anchoImagen = imagenActual.getWidth();
        int altoImagen = imagenActual.getHeight();
        //Creamos una nueva imagen
        BufferedImage nuevaImagen = new BufferedImage(anchoImagen / 2, altoImagen / 2, imagenActual.getType());
        //Recorremos la imagen píxel a píxel
        for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++ ){
            for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++ ){
                //Cambiamos el valor de la imagen
                int valorPixel = imagenActual.getRGB(filaPixel, colPixel);
                nuevaImagen.setRGB(filaPixel / 2, colPixel / 2, valorPixel);
            }
        }
        //Asignamos la imagen a la propiedad imageActual
        imagenActual = nuevaImagen;
        // Retornamos la imagen nueva ya modificada
        return nuevaImagen;
    }

    /**
     * Este método se encarga de cambiar el tamaño de la imagen a un ancho y a un alto en específico.
     * @param ancho Es el ancho que contendrá la nueva imagen.
     * @param alto Es el alto que contendrá la nueva imagen.
     * @return Regresa una nueva imagen con el nuevo tamaño.
     */
    // redimesinar la imagen en los valores espcificados por los parámetros, el valor de los parámetros debe ser mayor a 0
    public BufferedImage redimensionarImagen(int ancho, int alto){
       int anchoImagen = imagenActual.getWidth();
       int altoImagen = imagenActual.getHeight();
       if (ancho > 0 && alto > 0){
           //Creamos una nueva imagen
           BufferedImage nuevaImagen = new BufferedImage(ancho, alto, imagenActual.getType());
           //Recorremos la imagen píxel a píxel
           for (int filaPixel = 0; filaPixel < alto; filaPixel++ ){
               for (int colPixel = 0; colPixel < ancho; colPixel++ ){
                   //Cambiamos el valor de la imagen
                   int valorPixel = imagenActual.getRGB(colPixel * anchoImagen / ancho, filaPixel * altoImagen / alto);
                   nuevaImagen.setRGB(colPixel, filaPixel, valorPixel);
               }
           }
           //Asignamos la imagen a la propiedad imageActual
           imagenActual = nuevaImagen;
           //Retornamos la imagen
           return nuevaImagen;
       } else {
           return null;
       }
    }

    /**
     * Este método le coloca una marca(borde) a la imagen sin modificar su tamaño actual.
     * @param grosor Es el grosor del marco(borde).
     * @param color Es el color del marco(borde).
     * @return Regresa una nueva imagen con el marco(borde) añadido.
     */
    public BufferedImage aplicarMarcoImagen(int grosor, Color color){
        // Creamos una nueva imagen con un tamaño un poco mayor que la original para poder añadir el marco
        BufferedImage nuevaImagen = new BufferedImage(imagenActual.getWidth() + grosor * 2,
                imagenActual.getHeight() + grosor * 2, imagenActual.getType());
        //Recorremos la imagen nueva píxel a píxel, y le colocamos el color del marco
        for (int filaPixel = 0; filaPixel < nuevaImagen.getHeight(); filaPixel++ ){
            for (int colPixel = 0; colPixel < nuevaImagen.getWidth(); colPixel++ ){
                // Le colocamos el color del marco
                nuevaImagen.setRGB(colPixel, filaPixel, color.getRGB());
            }
        }
        // Sobre la nueva imagen se coloca la imagen original con el tamaño original
        // Recorremos la imagen actual píxel a píxel
        for (int filaPixel = 0; filaPixel < imagenActual.getHeight(); filaPixel++ ){
            for (int colPixel = 0; colPixel < imagenActual.getWidth(); colPixel++ ){
                //Cambiamos el valor de la imagen
                nuevaImagen.setRGB(colPixel + grosor, filaPixel + grosor, imagenActual.getRGB(colPixel, filaPixel));
            }
        }
        imagenActual = nuevaImagen;
        // Retornamos la imagen
        return nuevaImagen;


    }

    /**
     * Este método convierte a la imagen actual en un color negativo.
     * @return Regresa la imagen negativa.
     */
    public BufferedImage imagenNegativa(){
      // se crea una nueva imagen
      int ancho = imagenActual.getWidth();
      int alto = imagenActual.getHeight();
      BufferedImage nuevaImagen = new BufferedImage(ancho, alto, imagenActual.getType());
      // se recorre la imagen pixel a pixel
      for (int filaPixel = 0; filaPixel < matrizImagen.obtenerRenglones(); filaPixel++ ){
        for (int colPixel = 0; colPixel < matrizImagen.obtenerColumnas(); colPixel++ ){
          // se obtiene el color de la imagen original
          int valorPixel = imagenActual.getRGB(filaPixel, colPixel);
          // se obtiene el negativo del pixel original restando a 255 el valor del pixel
          int negativo = 255 - valorPixel;
          // se coloca el negativo en la imagen nueva
          nuevaImagen.setRGB(filaPixel, colPixel, negativo);
        }
      }
      imagenActual = nuevaImagen;
      // se retorna la nueva imagen
      return nuevaImagen;
    }
}