package entradaSalida;

import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaEstatica;

import java.io.*;

/**
 * Esta clase no permite la lectura y escritura de archivos de texto.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class ArchivoTexto {

    /**
     * Este método lee un archivo de texto y lo guarda en una lista estática.
     * @param archivo Es el archivo que se va a leer.
     * @return Regresa una lista estática con los datos del archivo.
     */
    public static ListaEstatica leer(String archivo){
        FileReader input=null;
        int registro=0;
        ListaEstatica datos=null;
        BufferedReader buffer = null;

        try {
            String cadena=null;
            input = new FileReader(archivo);
            buffer = new BufferedReader(input);
            datos=new ListaEstatica((int)buffer.lines().count());
            buffer.close();
            input.close();
            input = new FileReader(archivo);
            buffer = new BufferedReader(input);
            while((cadena = buffer.readLine())!=null) {
                datos.agregar(cadena);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try{
                input.close();
                buffer.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return datos;
    }

    /**
     * Este método escribe un archivo de texto con los datos de una lista estática.
     * @param arreglo Es la lista estática con los datos que se van a escribir en el archivo.
     * @param archivo Es la ruta del archivo que se va a escribir.
     */
    public static void escribir(ListaEstatica arreglo, String archivo){
        FileWriter output=null;
        try {
            output = new FileWriter(archivo);
            for(int posicion=0;posicion<arreglo.numeroElementos() -1 ;posicion++) {
                output.write(arreglo.obtener(posicion)+"\n");
            }
            double ultimo= (double) arreglo.obtener(arreglo.numeroElementos()-1);
            output.write(String.valueOf(ultimo));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try{
                output.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Este método lee un archivo de texto y lo guarda en una lista dinámica.
     * @param archivo Es la ruta del archivo que se va a leer.
     * @return Regresa una lista dinámica con los datos del archivo.
     */
    public static ListaDinamica leerRecursivo(String archivo) {
        FileReader input=null;
        ListaDinamica datos = new ListaDinamica();
        BufferedReader buffer = null;

        try {
            input = new FileReader(archivo);
            buffer = new BufferedReader(input);
            buffer.close();
            input.close();
            input = new FileReader(archivo);
            buffer = new BufferedReader(input);
            leerRecursivo(buffer,datos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try{
                input.close();
                buffer.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return datos;
    }

    /**
     * Este método lee un archivo de texto de forma recursiva y lo guarda en una lista dinámica.
     * @param buffer Es el buffer que se va a leer.
     * @param datos Es la lista dinámica donde se van a guardar los datos.
     */
    private static void leerRecursivo(BufferedReader buffer, ListaDinamica datos) {
        String cadena = null;
        try {
            if((cadena = buffer.readLine())!=null) { // caso base
                datos.agregar(cadena);
                leerRecursivo(buffer,datos);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}