package prueba;

import com.sun.tools.javac.Main;
import entradaSalida.SalidaPorDefecto;
import estructurasNoLineales.ArbolBinarioBusqueda;

public class PruebaABB {
    public static void main(String[] args) {
        ArbolBinarioBusqueda arbol = new ArbolBinarioBusqueda();
        arbol.agregar("8");
        arbol.agregar("3");
        arbol.agregar("10");
        arbol.agregar("1");
        arbol.agregar("6");
        arbol.agregar("14");
        arbol.agregar("4");
        arbol.agregar("7");
        arbol.agregar("13");

        SalidaPorDefecto.consola("Anchura con COLA: ");
        arbol.anchuraCOLA();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Anchura con PILA: ");
        arbol.anchuraPILA();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("InOrden: ");
        arbol.inorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PreOrden: ");
        arbol.preorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PostOrden recursivo: ");
        arbol.postorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PostOrden iterativo: ");
        arbol.postorden();
        SalidaPorDefecto.consola("\n");


    }
}
