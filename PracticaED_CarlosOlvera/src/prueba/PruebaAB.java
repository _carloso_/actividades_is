package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasNoLineales.ArbolBinario;

public class PruebaAB {
    public  static  void  main(String argumentos[]){
        ArbolBinario arbol=new ArbolBinario();
        //ACTUVIDAD 1
        arbol.generarArbol();
        SalidaPorDefecto.consola("InOrden: ");
        arbol.inorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PreOrden: ");
        arbol.preorden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("PostOrden: ");
        arbol.postorden();
        //arbol.imprimirListas();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----CAMBIO DE VALORES-----\n");
        arbol.cambiarValores("a","x");
        arbol.cambiarValores("b","z");
        arbol.imprimirCambios();
        arbol.imprimirListas();
        //ACTIVIDAD 2
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("la altura del arbol es: "+ arbol.obtenerAltura()+"\n");
        SalidaPorDefecto.consola("El nodo a esta en el nivel: " + arbol.obtenerNivel("a")+"\n");
        arbol.contarNodosXnivel();
    }


}
