package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasNoLineales.Matriz2;
import estructurasNoLineales.TipoColumna;
import estructurasNoLineales.TipoRenglon;

public class PruebaMatriz2 {
    public static void main(String[] args) {
        Matriz2 matriz = new Matriz2(4,4,"0");
        Matriz2 matriz2 = new Matriz2(4,4,"X");
        Matriz2 matriz3 = new Matriz2(5,5,"0");
        ListaEstatica lista = new ListaEstatica(4);
        lista.agregar("2");
        lista.agregar("2");
        lista.agregar("2");
        lista.agregar("2");

        SalidaPorDefecto.consola("----SALIDA POR COLUMNAS------\n");
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("----CAMBIAR VALORES-----\n");
        matriz.cambiar(0,0,"1");
        matriz.cambiar(0,1,"1");
        matriz.cambiar(0,2,"1");
        matriz.cambiar(0,3,"1");
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("----OBTENER VALOR------\n");
        SalidaPorDefecto.consola("fila 0 columna 1: " + matriz.obtener(0,1)+ "\n");
        SalidaPorDefecto.consola("----TRANSPUESTA-----\n");
        matriz.transpuesta();
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("-----INFORMACION DE LA MATRIZ-----\n");
        SalidaPorDefecto.consola("La matriz tiene " + matriz.obtenerRenglones() + " renglones\n");
        SalidaPorDefecto.consola("La matriz tiene " + matriz.obtenerColumnas() + " columnas\n");
        SalidaPorDefecto.consola("-----CLONAR EL ARREGLO-----\n");
        Matriz2 matriz2Copia = (Matriz2) matriz.clonar();
        matriz2Copia.imprimirXRenglones();
        SalidaPorDefecto.consola("----VERIFICAR SI matriz1 ES IGUAL A matriz2-----\n");
        if (matriz.esIgual(matriz2) == true){
            SalidaPorDefecto.consola("Las listas son iguales.");
        }
        else{
            SalidaPorDefecto.consola("Las listas no son iguales.\n");
        }
        SalidaPorDefecto.consola("-----VECTOR COLUMNA-----\n");
        Matriz2 vectorColumna = (Matriz2) matriz.vectorColumna(5, "X");
        vectorColumna.imprimirXRenglones();
        SalidaPorDefecto.consola("-----VECTOR RENGLON-----\n");
        Matriz2 vectorRenglon = (Matriz2) matriz.vectorRenglon(5, "Y");
        vectorRenglon.imprimirXRenglones();
        SalidaPorDefecto.consola("-----AGREGAR RENGLON LISTA ESTETICA-----\n");
        matriz.agregarRenglon(lista,3);
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("-----AGREGAR COLUMNA LISTA ESTETICA-----\n");
        matriz.agregarColumna(lista,3);
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("-----MATRIZ VECTOR COLUMNA-----\n");
        Matriz2 matriz2C = matriz.matrizVectorColumna();
        matriz2C.imprimirXRenglones();
        SalidaPorDefecto.consola("-----MATRIZ VECTOR RENGLON-----\n");
        Matriz2 matriz2R = matriz.matrizVectorRenglon();
        matriz2R.imprimirXRenglones();
        SalidaPorDefecto.consola("-----REDIMENSIONAR-----\n");
        matriz.redefinir(matriz3);
        SalidaPorDefecto.consola("(se redimensiono a matriz 5x5 y se le agregaron valores nuevos en la fila 0)\n");
        matriz.cambiar(0,0,"2");
        matriz.cambiar(0,1,"1");
        matriz.cambiar(0,2,"1");
        matriz.cambiar(0,3,"1");
        matriz.cambiar(0,4,"1");
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("----QUITAR COLUMNAS IZQ-----\n");
        matriz.quitarColumna(TipoColumna.IZQ);
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("----QUITAR COLUMNAS DER-----\n");
        matriz.quitarColumna(TipoColumna.DER);
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("----QUITAR RENGLONES SUP-----\n");
        matriz.quitarRenglon(TipoRenglon.SUP);
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("----QUITAR RENGLONES INF-----\n");
        matriz.quitarRenglon(TipoRenglon.INF);
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("----ELIMINAR COLUMNAS-----\n");
        matriz.eliminarColumna(2);
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("----ELIMINAR RENGLON-----\n");
        matriz.eliminarRenglon(2);
        matriz.imprimirXRenglones();
        SalidaPorDefecto.consola("----MULTIPLICACION-----\n");
        Matriz2 prueba = Matriz2.multiplicacion(matriz,matriz2R);
        prueba.imprimirXRenglones();
    }
}
