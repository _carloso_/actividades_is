package actividadesparciales;

import entradaSalida.SalidaPorDefecto;

import java.util.Objects;

/**
 * Esta clase representa un TDA Fecha.
 * @author Cristina Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class Fecha {

    private int dia;
    private int mes;
    private int anio;

    public Fecha(int dia, int mes, int anio){
       if (validarFecha(dia, mes, anio)) {
           this.dia = dia;
           this.mes = mes;
           this.anio = anio;
       } else {
           SalidaPorDefecto.consola("Fecha incorrecta");
       }
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        if (dia > 0 && dia < 32) {
            this.dia = dia;
        } else {
            SalidaPorDefecto.consola("Dia incorrecto");
        }
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        if (mes > 0 && mes < 13) {
            this.mes = mes;
        } else {
            SalidaPorDefecto.consola("Mes incorrecto");
        }
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        if (anio > 0) {
            this.anio = anio;
        } else {
            SalidaPorDefecto.consola("Año incorrecto");
        }
    }

    /**
     * Este método comprueba si la fecha es válida.
     * @param dia Es el día de la fecha.
     * @param mes Es el mes de la fecha.
     * @param anio Es el año de la fecha.
     * @return Retorna true si la fecha es válida, false en caso contrario.
     */
    private boolean validarFecha(int dia, int mes, int anio){
        // validar si es anio bisiesto y si es fecha valida
        boolean esValida = false;
        if (anio % 4 == 0 && anio % 100 != 0 || anio % 400 == 0) {
            if (mes == 2) {
                if (dia > 0 && dia < 30) {
                    esValida = true;
                }
            } else if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
                if (dia > 0 && dia < 32) {
                    esValida = true;
                }
            } else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
                if (dia > 0 && dia < 31) {
                    esValida = true;
                }
            } else {
                esValida = false;
            }
        } else {
            if (mes == 2) {
                if (dia > 0 && dia < 29) {
                    esValida = true;
                }
            } else if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
                if (dia > 0 && dia < 32) {
                    esValida = true;
                }
            } else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
                if (dia > 0 && dia < 31) {
                    esValida = true;
                }
            } else {
                esValida = false;
            }
        }
        return esValida;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Fecha)) return false;
        Fecha fecha = (Fecha) obj;
        return getDia() == fecha.getDia() && getMes() == fecha.getMes() && getAnio() == fecha.getAnio();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDia(), getMes(), getAnio());
    }

    @Override
    public String toString() {
        return dia + "/" + mes + "/" + anio;
    }
}