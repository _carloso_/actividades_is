package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasnolineales.ArbolBinarioBusqueda;

public class PruebaABB2 {
    public static void main(String[] args) {
        ArbolBinarioBusqueda arbol = new ArbolBinarioBusqueda();

        // agregar elementos
        arbol.agregar(8);
        arbol.agregar(5);
        arbol.agregar(10);
        arbol.agregar(6);
        arbol.agregar(9);
        arbol.agregar(15);
        arbol.agregar(4);
        arbol.agregar(17);
        arbol.agregar(13);
        arbol.agregar(7);
        arbol.agregar(12);

        // imprimir inorden, preorden y postorden
        SalidaPorDefecto.consola("Recorridos en profundidad del arbol:" + "\n");
        SalidaPorDefecto.consola("Inorden: ");
        arbol.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: ");
        arbol.preOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: ");
        arbol.postOrden();

        // Recorrido en anchura o amplitud
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorrido en amplitud (Recursivo): ");
        arbol.recorridoAmplitudRecursivo();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorrido en amplitud (Iterativo con cola): ");
        arbol.recorridoAmplitudIterativoCola();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorrido en amplitud (Iterativo con pila): ");
        arbol.recorridoAmplitudIterativoPila();

        // Recorrido postOrden Iterativo
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorrido en postorden iterativo: ");
        arbol.postordenIterativo();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Cantidad de nodos: " + arbol.cantidadNodos());
        SalidaPorDefecto.consola("\n");

        /*
        ArbolBinario arbol2 = new ArbolBinario();
        arbol2.generarArbol();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorridos del árbol 2: " +"\n");
        SalidaPorDefecto.consola("Inorden: \n");
        arbol2.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: \n");
        arbol2.preOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: \n");
        arbol2.postOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorrido en amplitud (Recursivo): \n");
        arbol2.recorridoAmplitudRecursivo();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorrido en amplitud (Iterativo con cola): \n");
        arbol2.recorridoAmplitudIterativo();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorrido en amplitud (Iterativo con pila): \n");
        arbol2.recorridoAmplitudIterativoPila();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorrido en postorden iterativo: \n");
        arbol2.postordenIterativo();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("\nCantidad de nodos: " + arbol2.cantidadNodos());

         */
    }
}
