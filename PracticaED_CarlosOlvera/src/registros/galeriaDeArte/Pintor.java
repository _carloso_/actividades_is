package registros.galeriaDeArte;

public class Pintor {
    public String nombre;
    public String sexo;
    public String domicilo;
    public String RFC;
    public String fechaCumpleanos;
    public String gradoEstudios;
    public int noPintor;
    public int edad;
    public Pintor(int noPintor,String nombre, int edad, String fechaCumpleanos,String sexo, String RFC,
                  String gradoEstudios, String domicilo){
        this.nombre = nombre;
        this.noPintor = noPintor;
        this.fechaCumpleanos = fechaCumpleanos;
        this.sexo = sexo;
        this.RFC = RFC;
        this.gradoEstudios = gradoEstudios;
        this.domicilo = domicilo;
        this.edad = edad;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public void setNoPintor(int noPintor) {
        this.noPintor = noPintor;
    }

    public void setRFC(String RFC) {
        this.RFC = RFC;
    }

    public void setFechaCumpleanos(String fechaCumpleanos) {
        this.fechaCumpleanos = fechaCumpleanos;
    }

    public void setDomicilo(String domicilo) {
        this.domicilo = domicilo;
    }

    public void setGradoEstudios(String gradoEstudios) {
        this.gradoEstudios = gradoEstudios;
    }

    public String getNombre() {
        return nombre;
    }

    public int getNoPintor() {
        return noPintor;
    }

    public int getEdad() {
        return edad;
    }

    public String getDomicilo() {
        return domicilo;
    }

    public String getFechaCumpleanos() {
        return fechaCumpleanos;
    }

    public String getGradoEstudios() {
        return gradoEstudios;
    }

    public String getRFC() {
        return RFC;
    }

    public String getSexo() {
        return sexo;
    }

    @Override
    public String toString() {
        return ""+noPintor;
    }

    public String obtenerDatos(){
        return "No. de pintor: "+ ", Nombre: " + nombre + ", Sexo: " + sexo + ", Domicilo: " + domicilo + ", RFC: " + RFC +
                ", Fecha de Cumpleaños: " + fechaCumpleanos + ", Grado de Estudios: " + gradoEstudios + "\n";
    }
}
