package proyectoFinal;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasnolineales.Matriz2;

public class GrafoMOM {

    protected ListaEstatica vertices; // lista de vertices del grafo
    protected Matriz2 aristas; // gurda las probabilidades de transición entre nodos (matriz de adyacencia)
    protected Matriz2 probaEmisiones; // guarda las probabilidades de emisión de cada observación en cada nodo

    public GrafoMOM(int cantidadNodos, int cantidadObservaciones) {
        vertices = new ListaEstatica(cantidadNodos);
        aristas = new Matriz2(cantidadNodos, cantidadNodos, 0.0);
        probaEmisiones = new Matriz2(cantidadNodos, cantidadObservaciones, 0.0);
    }

    public boolean agregarNodo(NodoMOM nodo) {
        Integer numVertice = (Integer) vertices.buscar(nodo);
        if (numVertice == null) {
            int retornoOperacion = vertices.agregar(nodo);
            if (retornoOperacion >= 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean agregarArista(Object origen, Object destino) {
        return agregarArista(origen, destino, 1.0);
    }

    public boolean agregarArista(Object origen, Object destino, double peso) {
        //Verificar que existan
        Integer indiceOrigen=(Integer)vertices.buscar(origen);
        Integer indiceDestino=(Integer)vertices.buscar(destino);

        if(indiceOrigen!=null && indiceDestino!=null){ //si existen ambos
            //creamos la arista
            return aristas.cambiar(indiceOrigen,indiceDestino,peso);
        }else{ //uno u otro no existe
            return false;
        }
    }

    private void generarMatrizProbaEmisiones() {
        //generamos la matriz de probabilidades de emisiones
        for (int i = 0; i < vertices.numeroElementos(); i++) {
            NodoMOM nodo = (NodoMOM) vertices.obtener(i);
            ListaEstatica observacionesNodo = nodo.getObservaciones();
            for (int j = 0; j < observacionesNodo.numeroElementos(); j++) {
                Integer indiceEstado = (Integer) vertices.buscar(nodo);
                if (indiceEstado != null) {
                    Integer indiceObservacion = (Integer) observacionesNodo.buscar(observacionesNodo.obtener(j));
                    if (indiceObservacion != null) {
                        Observacion observacion = (Observacion) observacionesNodo.obtener(j);
                        probaEmisiones.cambiar(indiceEstado, j, observacion.getProbabilidad());
                    }
                }
            }
        }
    }

    public void imprimirGrafo() {
        if (!vertices.vacia()) {
            for (int pos1 = 0; pos1 < vertices.numeroElementos(); pos1++) {
                NodoMOM nodoTemp = (NodoMOM) vertices.obtener(pos1);
                SalidaPorDefecto.consola("Estado: " + nodoTemp.getNombre() + "\n");
                SalidaPorDefecto.consola("Probabilidad inicial: " + nodoTemp.getProbaInicial() + "\n");
                SalidaPorDefecto.consola("Probabilidad observaciones: \n");
               ListaEstatica observaciones = nodoTemp.getObservaciones();
                for (int pos2 = 0; pos2 < observaciones.numeroElementos(); pos2++) {
                    SalidaPorDefecto.consola(observaciones.obtener(pos2) + "\n");
                }
                SalidaPorDefecto.consola("\n");
            }
            SalidaPorDefecto.consola("Probabilidades de emision:\n");
            generarMatrizProbaEmisiones();
            probaEmisiones.imprimirXReglones();
            SalidaPorDefecto.consola("Probabilidades de transicion:\n");
            aristas.imprimirXReglones();
        } else {
            SalidaPorDefecto.consola("No hay nodos en el grafo.\n");
        }
    }

    public NodoMOM obtenerNodo(Object nodo) {
        Integer indiceNodoBuscado = (Integer) vertices.buscar(nodo);
        NodoMOM nodoRetorno = (NodoMOM) vertices.obtener(indiceNodoBuscado);
        if (nodoRetorno != null) {
            return nodoRetorno;
        } else {
            return null;
        }
    }

    public double obtenerProbabilidadTransicion(Object origen, Object destino) {
        Integer indiceOrigen = (Integer) vertices.buscar(origen);
        Integer indiceDestino = (Integer) vertices.buscar(destino);
        if (indiceOrigen != null && indiceDestino != null) {
            return (double) aristas.obtenerValor(indiceOrigen, indiceDestino);
        } else {
            return -1;
        }
    }

    public Matriz2 obtenerMatrizProbaTransicion() {
        Matriz2 matrizRetorno = new Matriz2(vertices.numeroElementos(), vertices.numeroElementos(), 0.0);
        for (int i = 0; i < vertices.numeroElementos(); i++) {
            for (int j = 0; j < vertices.numeroElementos(); j++) {
                matrizRetorno.cambiar(i, j, aristas.obtenerValor(i, j));
            }
        }
        return matrizRetorno;
    }

    public int getNumeroNodos() {
        return vertices.numeroElementos();
    }

    public double probabilidadActividad(Object estadoAnimoActual, int indiceActividad) {
        double probabilidad = 0.0;
        Integer indiceEstado = (Integer) vertices.buscar(estadoAnimoActual);
        if (indiceEstado != null) {
           NodoMOM nodo = (NodoMOM) vertices.obtener(indiceEstado);
            ListaEstatica observaciones = nodo.getObservaciones();
            Observacion observacion = (Observacion) observaciones.obtener(indiceActividad);
            probabilidad = observacion.getProbabilidad();
        }
        return probabilidad;
    }
}
