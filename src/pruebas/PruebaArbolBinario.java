package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasnolineales.ArbolBinario;

public class PruebaArbolBinario {
    public static void main(String[] args) {
        ArbolBinario arbol = new ArbolBinario();
        arbol.generarArbol();

        SalidaPorDefecto.consola("\nImprimiendo el árbol: \n");
        SalidaPorDefecto.consola("Inorden: ");
        arbol.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: ");
        arbol.postOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: ");
        arbol.preOrden();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Altura del arbol: " + arbol.obtenerAltura());

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Nivel del nodo B: " + arbol.obtenerNivel("B") + "\n");

        SalidaPorDefecto.consola("\n");
        arbol.contarNodosPorNivel();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("EL nodo B es un: ");
        arbol.obtenerInfoNodo("B");

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("¿El nodo B tine hermano?: " + arbol.tieneHermano("B") + "\n");
        SalidaPorDefecto.consola("\n");
    }
}
