package estructurasNoLineales;

public enum TipoLogaritmo {
    NATURAL("base e",1), BASE10("base 10",2), BASE2("base 2",3) ;

    private String nombre;
    private int valor;

    private TipoLogaritmo (String nombre, int valor){
        this.nombre = nombre;
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }

    public String getNombre() {
        return nombre;
    }
}
