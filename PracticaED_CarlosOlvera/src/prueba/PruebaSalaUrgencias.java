package prueba;

import entradaSalida.SalidaPorDefecto;
import registros.prioridad.Paciente;
import registros.prioridad.SalaDeUrgencias;

public class PruebaSalaUrgencias {
    public static void main(String[] args) {
        SalaDeUrgencias urgencias = new SalaDeUrgencias(4);
        Paciente paciente1 = new Paciente(112233,"Carlos Olvera",19,"Quemaduras", 3);
        Paciente paciente2 = new Paciente(112234,"Pedro Maromas", 34, "Balazo en las piernas", 2);
        Paciente paciente3 = new Paciente(112235,"Hanna Montana", 21, "Sobredosis", 5);
        Paciente paciente4 = new Paciente(112236,"Mike Wasowski", 69, "Paro cardiaco", 4);
        urgencias.agregarPaciente(paciente1.getPrioridad());
        urgencias.agregarPaciente(paciente2.getPrioridad());
        urgencias.agregarPaciente(paciente3.getPrioridad());
        urgencias.agregarPaciente(paciente4.getPrioridad());

        SalidaPorDefecto.consola("-----LISTA DE PACIENTES-----\n");
        SalidaPorDefecto.consola(urgencias.infoPaciente(paciente1)+"\n");
        SalidaPorDefecto.consola(urgencias.infoPaciente(paciente2)+"\n");
        SalidaPorDefecto.consola(urgencias.infoPaciente(paciente3)+"\n");
        SalidaPorDefecto.consola(urgencias.infoPaciente(paciente4)+"\n");
        SalidaPorDefecto.consola("prioridades sin orden: ");
        urgencias.imprimirListaPacientes();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----LISTA DE PRIORIDADES----\n");
        urgencias.ordenarPrioridadUrgencia();
        urgencias.imprimirListaPacientes();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----ATENDER PACIENTES----\n");
        SalidaPorDefecto.consola("El primer paciente que se atendera es el de prioridad: ");
        SalidaPorDefecto.consola(""+urgencias.verPrimerPaciente());
        SalidaPorDefecto.consola("\n");
        urgencias.atenderPaciente();
        SalidaPorDefecto.consola("-----LISTA DE PRIORIDADES ACTUALIZADA----\n");
        urgencias.imprimirListaPacientes();


    }
}

