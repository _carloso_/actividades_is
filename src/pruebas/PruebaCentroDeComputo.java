package pruebas;

import entradaSalida.SalidaPorDefecto;
import registros.escuelaCentrosDeComputo.*;

public class PruebaCentroDeComputo {
    public static void main(String[] args) {
        ControlCentroDeComputo control = new ControlCentroDeComputo("Escuela de Informática");

        // Creo un centro de computo
        CentroDeComputo centro1 = new CentroDeComputo(1, "Centro de Computo Principal");

        // Crear computadoras
        Computadora comp1 = new Computadora(1, "HP", "GJH7897", "8Gb", "512GB", "i5", "Windows 10");
        Computadora comp2 = new Computadora(2, "Lenovo", "JKL5678", "16Gb", "1TB", "i3", "Windows 10");
        Computadora comp3 = new Computadora(3, "Dell", "MNO9012", "2Gb", "512GB", "Celeron", "Windows 7");


        // creamos aplicaciones para las computadoras
        Aplicacion aplicacion1 = new Aplicacion(1, "Word", "Microsoft", "2.3", "500Mb");
        Aplicacion aplicacion2 = new Aplicacion(2, "Photoshop", "Adobe", "5.2", "700Mb");
        Aplicacion aplicacion3 = new Aplicacion(3, "Excel", "Microsoft", "2.3", "500Mb");
        Aplicacion aplicacion4 = new Aplicacion(4, "AutoCAD", "Autodesk", "3.7.3", "2Gb");
        Aplicacion aplicacion5 = new Aplicacion(5, "IntelliJ", "JetBrains", "2019.1", "1Gb");
        Aplicacion aplicacion6 = new Aplicacion(6, "Chrome", "Google", "80.0", "1Gb");

        // creamos los usuarios de las computadoras
        Usuario usuario1 = new Usuario(1, "Juan", "Perez", "juan@gmail.com", true, "2:00 PM", "8:00 PM", "22/03/2022");
        Usuario usuario2 = new Usuario(2, "Pedro", "Gonzalez", "pedro@gmail.com", true, "10:00 AM", "3:00 PM", "20/03/2022");
        Usuario usuario3 = new Usuario(3, "Maria", "Lopez", "maria@hotmail.com", false, "No se uso", "No se uso", "No se uso");
        Usuario usuario4 = new Usuario(4, "Cristian", "Alvarado", "cris@gmail.com", true, "12:00 PM", "4:00 PM", "21/03/2022");

        // agregamos aplicaciones usadas por los usuarios
        usuario1.agregarAplicacionUsada(aplicacion1);
        usuario1.agregarAplicacionUsada(aplicacion3);
        usuario2.agregarAplicacionUsada(aplicacion2);
        usuario2.agregarAplicacionUsada(aplicacion4);
        usuario4.agregarAplicacionUsada(aplicacion1);
        usuario4.agregarAplicacionUsada(aplicacion3);

        // agregamos las aplicaciones a las computadoras
        comp1.agregarAplicacion(aplicacion1);
        comp1.agregarAplicacion(aplicacion2);
        comp2.agregarAplicacion(aplicacion3);
        comp1.agregarAplicacion(aplicacion5);
        comp1.agregarAplicacion(aplicacion6);
        comp3.agregarAplicacion(aplicacion4);
        comp3.agregarAplicacion(aplicacion6);
        comp3.agregarAplicacion(aplicacion5);

        // agregamos los usuarios a las computadoras
        comp1.agregarUsuario(usuario1);
        comp1.agregarUsuario(usuario2);
        comp2.agregarUsuario(usuario3);
        comp3.agregarUsuario(usuario4);

        // agregamos las computadoras al centro de computo
        centro1.agregarComputadora(comp1);
        centro1.agregarComputadora(comp2);
        centro1.agregarComputadora(comp3);

        // agregar un centro de computo a la escuela
        control.agregarCentroDeComputo(centro1);

        // imprimir los datos de la escuela
        control.imprimirCentrosDeComputo();

        // imprimir los datos aplicacion buscada
        SalidaPorDefecto.consola("\n--- Computadora Numero 2 buscada ---" + "\n");
        centro1.buscarComputadora(2).mostrarDatos();

        SalidaPorDefecto.consola("\n--- Computadora Que Usan Chrome ---" + "\n");
        control.computadorasDeUnCentroDeComputoConChromeInstalado(1);

        SalidaPorDefecto.consola("\n--- Usuarios que Usaron la Computadora Numero 3 ---" + "\n");
        control.usuariosQueUsaronUnaComputadora(1,3);

        SalidaPorDefecto.consola("\nEliminar la aplicacion 4 de la computadora 3: " +
                control.eliminarAplicacionDeUnaComputadora(1,3,aplicacion4));

        SalidaPorDefecto.consola("\n--- Computadora Numero 3 ---" + "\n");
        control.buscarComputadoraDeUnCentroDeComuto(1,3);

        Computadora comp4 = new Computadora(4, "HP", "GJH7897", "8Gb", "512GB", "i7", "Windows 10");
        control.agregarComputadoraAUnCentroDeComputo(1,comp4);

        SalidaPorDefecto.consola("\n--- Computadora Numero 4 ---" + "\n");
        control.buscarComputadoraDeUnCentroDeComuto(1,4);

        control.agregarAplicacionAUnaComputadora(1,4,aplicacion6);

        SalidaPorDefecto.consola("\n--- Computadora Numero 4 ---" + "\n");
        control.buscarComputadoraDeUnCentroDeComuto(1,4);


        SalidaPorDefecto.consola("\n--- Computadora Numero 4 ---" + "\n");
        control.agregarUnUsuarioAUnaComputadora(1,4,usuario2);
        control.buscarComputadoraDeUnCentroDeComuto(1,4);


        SalidaPorDefecto.consola("\n--- Computadora Numero 4 ---" + "\n");
        control.eliminarComputadoraDeUnCentroDeComputo(1,4);
        control.buscarComputadoraDeUnCentroDeComuto(1,4);

        SalidaPorDefecto.consola("\n--- Usuarios que no usan el centro de computo 1 ---" + "\n");
        control.usuariosQueNoUsanLaComputadorasDelCentroDeComputo(1);
    }
}
