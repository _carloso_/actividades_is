package herramientas.comunes;

/**
 * Este enumerado representa los tipos de tablas que se pueden utilizar en la aplicación.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public enum TipoTabla {
    COLUMNA("columna",1), RENGLON("renglon",2);

    private String nombre;
    private int valor;

    private TipoTabla(String nombre, int tipo) {
        this.nombre = nombre;
        this.valor = tipo;
    }

    /**
     * Este método retorna el nombre del tipo de tabla.
     * @return Regresa el nombre del tipo de tabla.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Este método retorna el tipo de tabla
     * @return Regresa el valor del tipo de tabla.
     */
    public int getValor() {
        return valor;
    }
}