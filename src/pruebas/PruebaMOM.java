package pruebas;

import estructurasLineales.ListaEstatica;
import proyectoFinal.ModeloOcultoMarkov;
import proyectoFinal.NodoMOM;
import proyectoFinal.Observacion;

public class PruebaMOM {
    public static void main(String[] args) {
        ModeloOcultoMarkov modelo = new ModeloOcultoMarkov(4,3);

        // observaciones con sus respectivas probabilidades (para las emisiones)
        Observacion bailar1 = new Observacion("bailar", 0.5);
        Observacion bailar2 = new Observacion("bailar", 0.2);
        Observacion bailar3 = new Observacion("bailar", 0.2);
        Observacion bailar4 = new Observacion("bailar", 0.3);

        Observacion leer1 = new Observacion("leer", 0.2);
        Observacion leer2 = new Observacion("leer", 0.2);
        Observacion leer3 = new Observacion("leer", 0.1);
        Observacion leer4 = new Observacion("leer", 0.3);

        Observacion escucharMusica1 = new Observacion("escucharMusica", 0.3);
        Observacion escucharMusica2 = new Observacion("escucharMusica", 0.6);
        Observacion escucharMusica3 = new Observacion("escucharMusica", 0.7);
        Observacion escucharMusica4 = new Observacion("escucharMusica", 0.4);

        // crear listas de las observaciones
        ListaEstatica observaciones1 = new ListaEstatica(3);
        observaciones1.agregar(bailar1);
        observaciones1.agregar(leer1);
        observaciones1.agregar(escucharMusica1);

        ListaEstatica observaciones2 = new ListaEstatica(3);
        observaciones2.agregar(bailar2);
        observaciones2.agregar(leer2);
        observaciones2.agregar(escucharMusica2);

        ListaEstatica observaciones3 = new ListaEstatica(3);
        observaciones3.agregar(bailar3);
        observaciones3.agregar(leer3);
        observaciones3.agregar(escucharMusica3);

        ListaEstatica observaciones4 = new ListaEstatica(3);
        observaciones4.agregar(bailar4);
        observaciones4.agregar(leer4);
        observaciones4.agregar(escucharMusica4);

        // crear estados (nodos)
        NodoMOM feliz = new NodoMOM("feliz", 0.4, observaciones1);
        NodoMOM enojado = new NodoMOM("enojado", 0.1, observaciones2);
        NodoMOM triste = new NodoMOM("triste", 0.3, observaciones3);
        NodoMOM aburrido = new NodoMOM("aburrido", 0.2, observaciones4);

        // agregar estados al grafo
        modelo.agregarNodoEstado(feliz);
        modelo.agregarNodoEstado(enojado);
        modelo.agregarNodoEstado(triste);
        modelo.agregarNodoEstado(aburrido);

        // agregar probabilidades de transiciones (aristas)
        modelo.agregarArista(feliz, feliz, 0.3);
        modelo.agregarArista(feliz, enojado, 0.4);
        modelo.agregarArista(feliz, triste, 0.2);
        modelo.agregarArista(feliz, aburrido, 0.1);

        modelo.agregarArista(enojado, feliz, 0.2);
        modelo.agregarArista(enojado, enojado, 0.5);
        modelo.agregarArista(enojado, triste, 0.1);
        modelo.agregarArista(enojado, aburrido, 0.2);

        modelo.agregarArista(triste, feliz, 0.1);
        modelo.agregarArista(triste, enojado, 0.3);
        modelo.agregarArista(triste, triste, 0.4);
        modelo.agregarArista(triste, aburrido, 0.2);

        modelo.agregarArista(aburrido, feliz, 0.4);
        modelo.agregarArista(aburrido, enojado, 0.2);
        modelo.agregarArista(aburrido, triste, 0.1);
        modelo.agregarArista(aburrido, aburrido, 0.3);


        // mostrar el menu de opciones para el usuario
        modelo.mostrarMenu();

        //Matriz2 matriz = modelo.calcularMatrizTransiciones(2);
        //matriz.imprimirXReglones();

    }
}
