package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasnolineales.ArbolBinarioBusqueda;

public class PruebaABB {
    public static void main(String[] args) {
        ArbolBinarioBusqueda arbol = new ArbolBinarioBusqueda();

        // agregar elementos
        arbol.agregar(8);
        arbol.agregar(5);
        arbol.agregar(10);
        arbol.agregar(6);
        arbol.agregar(9);
        arbol.agregar(15);
        arbol.agregar(4);
        arbol.agregar(17);
        arbol.agregar(13);
        arbol.agregar(7);
        arbol.agregar(12);

        // imprimir inorden, preorden y postorden
        SalidaPorDefecto.consola("Recorridos del árbol: " +"\n");
        SalidaPorDefecto.consola("Inorden: \n");
        arbol.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: \n");
        arbol.preOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: \n");
        arbol.postOrden();

        // buscar elementos
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("El elemento 5 está en el árbol: " + arbol.buscar(5) + "\n"); // si está
        SalidaPorDefecto.consola("El elemento 20 está en el árbol: " + arbol.buscar(20) + "\n"); // null

        // eliminar el elemento 12
        arbol.eliminar(12);

        // imprimir inorden, preorden y postorden
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorridos del árbol (Se eliminó el elemento 12): " +"\n");
        SalidaPorDefecto.consola("Inorden: \n");
        arbol.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: \n");
        arbol.preOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: \n");
        arbol.postOrden();
        SalidaPorDefecto.consola("\n");


        // eliminar el elemento 6
        arbol.eliminar(6);

        // imprimir inorden, preorden y postorden
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorridos del árbol (Se eliminó el elemento 6): " +"\n");
        SalidaPorDefecto.consola("Inorden: \n");
        arbol.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: \n");
        arbol.preOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: \n");
        arbol.postOrden();

        // eliminar el elemento 10
        arbol.eliminar(10);

        // imprimir inorden, preorden y postorden
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Recorridos del árbol (Se eliminó el elemento 10): " +"\n");
        SalidaPorDefecto.consola("Inorden: \n");
        arbol.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: \n");
        arbol.preOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: \n");
        arbol.postOrden();
    }
}