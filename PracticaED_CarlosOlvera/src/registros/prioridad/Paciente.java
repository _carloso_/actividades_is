package registros.prioridad;

public class Paciente {
    int nss;
    String nombre;
    int edad;
    String problema;
    int prioridad;
    public Paciente(int nss, String nombre, int edad, String problema, int prioridad ){
        this.nss = nss;
        this.nombre = nombre;
        this.edad = edad;
        this.problema = problema;
        this.prioridad = prioridad;
    }

    public int getEdad() {
        return edad;
    }

    public String getNombre() {
        return nombre;
    }

    public int getNss() {
        return nss;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public String getProblema() {
        return problema;
    }
    public String DatosGenerales(){
        return"Nombre: "+nombre +", Edad: "+edad + ", NSS: "+nss + ", Problema: "+problema + ", Prioridad: "+prioridad;
    }

    @Override
    public String toString() {
        return ""+prioridad;
    }
}
