package herramientas.matematicas;

import entradaSalida.archivos.ArchivoTexto;
import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaEstatica;
import estructurasLineales.auxiliares.Nodo;

/*
Actividad 2 Examen 2
 */
public class DistribucionNormal {
    public ListaDinamica tabX;
    protected ListaEstatica tabXEstatica;
    public ListaEstatica valoresFx;

    /*
Se inicializan las tablas donde guardaremos los datos
 */
    public DistribucionNormal(String tablaX){
        tabX = new ListaDinamica();
        tabXEstatica = ArchivoTexto.leerRecursivo(tablaX); //variable tipo ListaEstatica donde se guardara el contenido de la tabla1
        tabX.agregarLista(tabXEstatica);
        valoresFx = new ListaEstatica(tabXEstatica.numeroElementos());
    }

    /*
    Promedio recursivo
     */
    public double promedio(Nodo nodo, double suma, int contador){
        if (nodo == null){
            return  suma/contador;
        }
        return promedio(nodo.getNodoDer(),suma+Double.parseDouble(nodo.getContenido().toString()),contador+1);
    }

    /*
    utilizar metodo recursivo promedio
     */
    public double promedio(){
        return promedio(tabX.obtenerPrimero(),0,0);
    }

    /*
    Desviacion estandar recursiva formula   √((xi-promedioX) ^/ (n-1))
     */
    public double desviacionEstandar(Nodo nodo, double suma, int contador){
        if (nodo == null){
            return Math.sqrt(suma/(contador-1));
        }
        else{
            return desviacionEstandar(nodo.getNodoDer(),suma + Math.pow(Double.parseDouble(nodo.getContenido().toString())-promedio(),2),contador+1);
        }
    }

    /*
    utilizar metodo recursivo desviacion estandar
     */
    public double desviacionEstandar(){
            return desviacionEstandar(tabX.obtenerPrimero(),0,0);
    }

    public void fx(){
        fx(tabXEstatica,tabXEstatica.numeroElementos()-1,0);
    }
    /*
    Metodo para obtener la distribucion normal de forma recursiva
     */
    public void fx(ListaEstatica tabXEstatica, int ultimo, int posicion){
        if (posicion == ultimo){
            double desviacionEstandar = desviacionEstandar();
            double x = Double.parseDouble(tabXEstatica.obtener(ultimo).toString());
            double fx = 1 / (desviacionEstandar * Math.sqrt(2 * Math.PI)) * (Math.exp(-(1/ (2*Math.pow(desviacionEstandar, 2)) * Math.pow(x - promedio(), 2))));
            valoresFx.agregar(fx);

        }
        else{
            double desviacionEstandar = desviacionEstandar();
            double x = Double.parseDouble(tabXEstatica.obtener(posicion).toString());
            double fx = 1 / (desviacionEstandar * Math.sqrt(2 * Math.PI)) * (Math.exp(-(1/ (2*Math.pow(desviacionEstandar, 2)) * Math.pow(x - promedio(), 2))));
            valoresFx.agregar(fx);
            fx(tabXEstatica,ultimo, posicion+1);
        }
    }


}
