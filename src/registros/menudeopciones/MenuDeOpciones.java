package registros.menudeopciones;

import entradaSalida.ArchivoTexto;
import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasLineales.PilaEstatica;

public class MenuDeOpciones {

    private static PilaEstatica pila = new PilaEstatica(20);
    private static ListaEstatica menuDeportes;
    private static int opcion = 0;

    /**
     * Este método imprime el menu de opciones.
     */
    public static void mostrarMenu() {
       do {
           menuDeportes = ArchivoTexto.leer("C:\\Users\\Cristian\\Desktop\\Deportes.txt");
           menuDeportes.imprimirOI();
           SalidaPorDefecto.consola("Elige Una Opcion: ");
           opcion = EntradaPorDefecto.consolaInteger();

           switch (opcion) {
               case 1:
                   pila.poner(menuDeportes.obtener(1));
                   do {
                       menuDeportes = ArchivoTexto.leer("C:\\Users\\Cristian\\Desktop\\Futbol.txt");
                       menuDeportes.imprimirOI();
                       SalidaPorDefecto.consola("Elige Una Opcion: ");
                       opcion = EntradaPorDefecto.consolaInteger();

                       switch (opcion) {
                           case 1:
                               pila.poner(menuDeportes.obtener(1));
                               do {
                                   menuDeportes = ArchivoTexto.leer("C:\\Users\\Cristian\\Desktop\\JugadoresFutbol.txt");
                                   menuDeportes.imprimirOI();
                                   SalidaPorDefecto.consola("Elige Una Opcion: ");
                                   opcion = EntradaPorDefecto.consolaInteger();

                                   switch (opcion) {
                                       case 1:
                                           pila.poner(menuDeportes.obtener(1));
                                           do {
                                               menuDeportes = ArchivoTexto.leer("C:\\Users\\Cristian\\Desktop\\Messi.txt");
                                               menuDeportes.imprimirOI();
                                               SalidaPorDefecto.consola("Elige Una Opcion: ");
                                               opcion = EntradaPorDefecto.consolaInteger();

                                               switch (opcion) {
                                                   case 1:
                                                       // Regresar al menu anterior
                                                       pila.poner(menuDeportes.obtener(0));
                                                       break;
                                                   default:
                                                       SalidaPorDefecto.consola("Opcion Invalida" +
                                                               "\n");
                                                       break;
                                               }
                                           } while (opcion != 1);
                                           break;
                                       case 2:
                                           pila.poner(menuDeportes.obtener(2));
                                       case 3:
                                           pila.poner(menuDeportes.obtener(3));
                                           break;
                                       default:
                                           SalidaPorDefecto.consola("Opcion Invalida" + "\n");
                                           break;
                                   } // Fin del switch
                               } while (opcion != 3);

                               break;
                           case 2:
                               pila.poner(menuDeportes.obtener(1));
                               break;
                           case 3:
                               pila.poner(menuDeportes.obtener(2));
                               break;
                           case 4:
                               break;
                           default:
                               SalidaPorDefecto.consola("Opcion " + opcion + " no valida" + "\n");
                               break;
                       }

                   } while (opcion != 4);
                   break;
               case 2:
                   pila.poner(menuDeportes.obtener(2));
                   do {
                       menuDeportes = ArchivoTexto.leer("C:\\Users\\Cristian\\Desktop\\Voleibol.txt");
                       menuDeportes.imprimirOI();
                       SalidaPorDefecto.consola("Elige Una Opcion: ");
                       opcion = EntradaPorDefecto.consolaInteger();

                       switch (opcion) {
                           case 1:
                               pila.poner(menuDeportes.obtener(1));
                               do {
                                   menuDeportes = ArchivoTexto.leer("C:\\Users\\Cristian\\Desktop\\DefinicionVoleibol.txt");
                                   menuDeportes.imprimirOI();
                                   SalidaPorDefecto.consola("Elige Una Opcion: ");
                                   opcion = EntradaPorDefecto.consolaInteger();

                                   switch (opcion) {
                                       case 1:
                                           pila.poner(menuDeportes.obtener(0));
                                           break;
                                       default:
                                           SalidaPorDefecto.consola("Opcion Invalida" + "\n");
                                           break;
                                   }
                               } while (opcion != 1);
                           case 2:
                               pila.poner(menuDeportes.obtener(2));
                               break;
                           default:
                               SalidaPorDefecto.consola("Opcion " + opcion + " no valida" + "\n");
                               break;
                       }
                   } while (opcion != 2);
                   break;
               case 3:
                   pila.poner(menuDeportes.obtener(3));
               case 4:
                   pila.poner(menuDeportes.obtener(4));
                   break;
               case 5:
                   break;
               default:
                   SalidaPorDefecto.consola("Opcion " + opcion + " no valida" + "\n");
           }
       } while (opcion != 5);
   }

    /**
     * Este método imprime el historial de navegacion del usuario.
     */
   public static void mostrarHistorialDeBusquedas() {
       // Mostrar historial de busquedas
       pila.imprimir();
   }
}