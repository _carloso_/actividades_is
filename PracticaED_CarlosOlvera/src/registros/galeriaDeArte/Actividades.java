package registros.galeriaDeArte;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;

public class Actividades {
    public String actividad;
    public String nombreExp;
    public String lugarExp;
    public String fechaExp;
    public String aforoExp;
    public String lugarAuto;
    public String fechaAuto;
    public String lugarViajar;
    public String act;
    public Actividades(String actividad) {
        this.actividad = actividad;
        if (actividad == "exponer") {
            SalidaPorDefecto.consola("Ingrese el nombre del evento: ");
            nombreExp = EntradaPorDefecto.consolaCadenas();
            SalidaPorDefecto.consola("Ingrese el lugar del evento: ");
            lugarExp = EntradaPorDefecto.consolaCadenas();
            SalidaPorDefecto.consola("Ingrese la fecha del evento: ");
            fechaExp = EntradaPorDefecto.consolaCadenas();
            SalidaPorDefecto.consola("Ingrese el aforo de personas del evento: ");
            aforoExp = EntradaPorDefecto.consolaCadenas();
        }
        if (actividad == "autografos") {
            SalidaPorDefecto.consola("Ingrese el lugar del la firma de autografos: ");
            lugarAuto = EntradaPorDefecto.consolaCadenas();
            SalidaPorDefecto.consola("Ingrese la fecha de la firma de autografos : ");
            fechaAuto = EntradaPorDefecto.consolaCadenas();
        }
        if (actividad == "viajar") {
            SalidaPorDefecto.consola("Ingrese el lugar a donde se viaja: ");
            lugarViajar = EntradaPorDefecto.consolaCadenas();
        }
    }

    public String pintar(){
        return "pintar"+ "\n";
    }

    public String exponer( String nombreEvento,String lugar, String fecha, String aforoPersonas){
        return "Nombre del evento: "+nombreEvento+", Lugar: "+lugar+", Fecha: "+fecha+", Aforo de personas: "+aforoPersonas+ "\n";
    }

    public String autografos(String lugar, String fecha){
        return "Lugar de la firma: "+lugar+", Fecha: "+fecha+ "\n";
    }
    public String viajar(String lugar){
        return "Lugar a donde se viaja: "+lugar+ "\n";
    }
    public String descansar(){
        return "descansar"+ "\n";
    }

    @Override
    public String toString() {
        return actividad;
    }

    public String obtenerDatos(){
        if (actividad == "pintar") {
            act = pintar();
        }
        if (actividad == "exponer") {
            act =exponer(nombreExp, lugarExp, fechaExp, aforoExp);

        }
        if (actividad == "autografos") {
            act = autografos(lugarAuto,fechaAuto);
        }
        if (actividad == "viajar") {
            act = viajar(lugarViajar);
        }
        if (actividad == "descansar") {
            act =descansar();
        }
        return actividad +": "+act;
    }
}
