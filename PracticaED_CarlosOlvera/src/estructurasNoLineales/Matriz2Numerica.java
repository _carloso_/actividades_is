package estructurasNoLineales;

import estructurasLineales.ListaEstaticaNumerica;

import static estructurasNoLineales.TipoLogaritmo.*;

public class Matriz2Numerica extends Matriz2 {

    public Matriz2Numerica(int renglones, int columnas) {
        super(renglones, columnas);
    }

    public Matriz2Numerica(int renglones, int columnas, Object valor) {
        super(renglones, columnas, valor);
    }

    @Override
    public void rellenar(Object valor) {
        if (valor instanceof Number) { //Comprobamos que valor sea de la clase number
            double valorDouble = ((Number) valor).doubleValue(); //utilizamos una nueva variable para convertir valor en tipo double
            super.rellenar(valorDouble);
        }
    }

    @Override
    public void imprimirXColumnas() {
        super.imprimirXColumnas();
    }

    @Override
    public void imprimirXRenglones() {
        super.imprimirXRenglones();
    }

    @Override
    public Object obtener(int fila, int col) {
        return super.obtener(fila, col);
    }

    @Override
    public boolean cambiar(int fila, int col, Object valor) {
        if (valor instanceof Number) { //Comprobamos que valor sea de la clase number
            double valorDouble = ((Number) valor).doubleValue(); //utilizamos una nueva variable para convertir valor en tipo double
            return super.cambiar(fila, col, valorDouble);
        } else {
            return false;
        }

    }

    public boolean porEscalar(Number escalar) {
        boolean multiplicado = false;
        if (escalar != null) {
            double escalarD = escalar.doubleValue();
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    double valorNuevo = ((Number)datos[fila][col]).doubleValue();
                    valorNuevo *= escalarD;
                    cambiar(fila, col, valorNuevo);
                }
            }
            multiplicado = true;
        }
        return multiplicado;
    }

    public boolean porEscalares(ListaEstaticaNumerica escalares) {
        boolean verificacion = false;
        int numElelementosArreglo = escalares.numeroElementos();
        if (numElelementosArreglo == renglones*columnas) {
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    double valorNuevo = ((Number)datos[fila][col]).doubleValue();
                    double escalar = ((Number)escalares.obtener(col)).doubleValue();
                    valorNuevo *= escalar;
                    cambiar(fila, col, valorNuevo);
                }
            }
            verificacion = true;
        }
        return verificacion;
    }

    public boolean sumarEscalar(Number escalar) {
        boolean multiplicado = false;
        if (escalar != null) {
            double escalarD = escalar.doubleValue();
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    double valorNuevo = ((Number)datos[fila][col]).doubleValue();
                    valorNuevo += escalarD;
                    cambiar(fila, col, valorNuevo);
                }
            }
            multiplicado = true;
        }
        return multiplicado;
    }

    public boolean sumarEscalares(ListaEstaticaNumerica escalares) {
        boolean verificacion = false;
        int numElelementosArreglo = escalares.numeroElementos(); //se le otorga una variable al numero de elementos de la lista
        if (numElelementosArreglo == renglones*columnas) {
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    double valorNuevo = ((Number)datos[fila][col]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    double escalar = ((Number)escalares.obtener(col)).doubleValue(); //el valor de esta posicion en la lista  se castea a una variable double
                    valorNuevo += escalar; //se multiplica el valor del arreglo por el escalar en la misma posicion
                    cambiar(fila, col, valorNuevo);
                }
            }
            verificacion = true;
        }
        return verificacion;
    }

    public boolean multiplicar(Matriz2Numerica matriz2){
        boolean cambiar = false;
        if (renglones == matriz2.obtenerRenglones() && columnas == matriz2.obtenerColumnas()) { //se verifica que las dos matrices tengan el mismo tamaño
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    double valorActual = ((Number) datos[fila][col]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    double valorMatriz2 = ((Number) matriz2.datos[fila][col]).doubleValue(); //el valor de esta posicion en la matriz2 se castea a una variable double
                    double producto = valorActual * valorMatriz2; //se multiplican los valores de cada matriz en la misma posicion
                    cambiar(fila, col, producto); //el producto se guarda en la posicion del valor en la matriz actual
                    cambiar = true; //tienen el mismo tamaño y se hizo la multiplicacion
                }
            }
        }
        return cambiar;
    }

    public boolean sumar(Matriz2Numerica matriz2){
        boolean cambiar = false;
        if (renglones == matriz2.obtenerRenglones() && columnas == matriz2.obtenerColumnas()) { //se verifica que las dos matrices tengan el mismo tamaño
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    double valorActual = ((Number) datos[fila][col]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    double valorMatriz2 = ((Number) matriz2.datos[fila][col]).doubleValue(); //el valor de esta posicion en la matriz2 se castea a una variable double
                    double suma = valorActual + valorMatriz2; //se suman los valores de cada matriz en la misma posicion
                    cambiar(fila, col, suma); //la suma se guarda en la posicion del valor en la matriz actual
                    cambiar = true; //tienen el mismo tamaño y se hizo la suma
                }
            }
        }
        return cambiar;
    }

    public boolean aplicarPotencia(Number escalar){
        boolean elevado = false;
        if (escalar != null) {
            double escalarD = escalar.doubleValue();
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    double valorNuevo = ((Number)datos[fila][col]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo = Math.pow(valorNuevo,escalarD); //con el metodo math se saca la potencia de cada posicion
                    cambiar(fila, col, valorNuevo);//la potencia se guarda en la posicion del valor en la matriz actual
                }
            }
            elevado = true;
        }
        return elevado;
    }

    public boolean aplicarLogaritmo(TipoLogaritmo tipoLogaritmo){
        boolean logaritmo = false;
        if (tipoLogaritmo == NATURAL) {
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    double valorNuevo = ((Number)datos[fila][col]).doubleValue();//el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo = Math.log(valorNuevo); //con la libreria math se saca el algoritmo natural de cada posicion
                    cambiar(fila, col, valorNuevo); //el algoritmo se guarda en la posicion del valor en la matriz actual
                }
            }
            logaritmo = true;
        }
        if (tipoLogaritmo == BASE10) {
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    double valorNuevo = ((Number)datos[fila][col]).doubleValue();//el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo = Math.log10(valorNuevo);//con la libreria math se saca el algoritmo base 10 de cada posicion
                    cambiar(fila, col, valorNuevo); //el algoritmo se guarda en la posicion del valor en la matriz actual
                }
            }
            logaritmo = true;
        }
        if (tipoLogaritmo == BASE2) {
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    double valorNuevo = ((Number)datos[fila][col]).doubleValue();//el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo = (Math.log(valorNuevo)/Math.log(2));//con la libreria math se saca el algoritmo base 10 de cada posicion
                    cambiar(fila, col, valorNuevo); //el algoritmo se guarda en la posicion del valor en la matriz actual
                }
            }
            logaritmo = true;
        }
        return logaritmo;
    }

    public boolean matrizDiagonal(Number contenido){
        int col = 0;
        boolean diagonal = false;
        if (contenido != null){
            rellenar(0); //todos los valores de la matriz se vuelven 0
            double contenidoD = contenido.doubleValue();
            for (int fila = 0; fila < renglones; fila++) {   //solo recorre los renglones por que solo se necesita modificar la columna
                cambiar(fila, col, contenidoD); //el contenfio se guarda en la posicion
                col++;//la columna se guarda de esta manera para darle forma de diagonal
                }
            diagonal = true;
            }
        return  diagonal;
        }

    public boolean potencia(int exponente){
        boolean elevado = false;
        if (exponente >= 0) {
            for (int cont = 1; cont <= exponente; cont++){
                for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                    for (int col = 0; col < columnas; col++) { //extrae col x col
                        double valorNuevo = ((Number)datos[fila][col]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                        valorNuevo *= valorNuevo; //valorNuevo se multiplica por si mismo
                        cambiar(fila, col, valorNuevo);//la potencia se guarda en la posicion del valor en la matriz actual
                    }
                }
            }
            elevado = true;
        }
        return elevado;
    }
    public boolean esDiagonalSuperior(){
        boolean esDiagonalSuperior = false;
        if (renglones == columnas){
            esDiagonalSuperior = true;
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    if (fila > col){
                        if(((Number)datos[fila][col]).doubleValue() != 0){
                            esDiagonalSuperior = false;
                            break;
                        }
                    }
                }
            }
        }
        return esDiagonalSuperior;
    }

    public boolean esDiagonalInferior(){
        boolean esDiagonalInferior = false;
        if (renglones == columnas){
            esDiagonalInferior = true;
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    if (fila < col){
                        if(((Number)datos[fila][col]).doubleValue() != 0){
                            esDiagonalInferior = false;
                            break;
                        }
                    }
                }
            }
        }
        return esDiagonalInferior;
    }

    public boolean doblarColumnas() {
        boolean doblado = false;
        if (columnas%2==0) {
            int mitad = columnas / 2;
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < mitad - 1; col++) { //recorre la primera mitad del arreglo
                    double valorNuevo = ((Number)datos[fila][col+1]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo += ((Number)datos[fila][col]).doubleValue();
                    cambiar(fila, col+1, valorNuevo);//la suma se guarda en la posicion del valor en la matriz actual
                }
                for (int colu = mitad ; colu < columnas -1; colu++) { //recorre la segunda mitad del arreglo
                    double valorNuevo = ((Number)datos[fila][colu]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo += ((Number)datos[fila][colu+1]).doubleValue();
                    cambiar(fila, colu, valorNuevo);//la suma se guarda en la posicion del valor en la matriz actual
                }
            }
            for(int col = 0; col < mitad - 1; col++){ //se eliminan de las columnas de la primera mitad del arreglo
                quitarColumna(TipoColumna.IZQ);
            }
            for(int colu = mitad ; colu < columnas; colu++) {//se eliminan de las columnas de la segunda mitad del arreglo
                quitarColumna(TipoColumna.DER);
            }
            doblado = true;
        } else {
            int mitad = (columnas - 1) / 2;
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < mitad - 1; col++) { //extrae col x col
                    double valorNuevo = ((Number)datos[fila][col+1]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo += ((Number)datos[fila][col]).doubleValue();
                    cambiar(fila, col+1, valorNuevo);//la suma se guarda en la posicion del valor en la matriz actual
                }
                for (int colu = mitad ; colu < columnas -1; colu++) { //extrae col x col
                    double valorNuevo = ((Number)datos[fila][colu]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo += ((Number)datos[fila][colu+1]).doubleValue();
                    cambiar(fila, colu, valorNuevo);//la suma se guarda en la posicion del valor en la matriz actual
                }
            }
            for(int col = 0; col < mitad - 1; col++){ //se eliminan de las columnas de la primera mitad del arreglo
                quitarColumna(TipoColumna.IZQ);
            }
            for(int colu = mitad ; colu < columnas; colu++) {//se eliminan de las columnas de la segunda mitad del arreglo
                quitarColumna(TipoColumna.DER);
            }
            doblado = true;
        }
        return doblado;
    }

    public boolean doblarRenglones() {
        boolean doblado = false;
        if (renglones%2==0) {
            int mitad = renglones / 2;
            for (int col = 0; col < columnas; col++) {  //col x col
                for (int fila = 0; fila < mitad - 1; fila++) { //recorre la primera mitad del arreglo
                    double valorNuevo = ((Number)datos[fila+1][col]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo += ((Number)datos[fila][col]).doubleValue();
                    cambiar(fila+1, col, valorNuevo);//la suma se guarda en la posicion del valor en la matriz actual
                }
                for (int filas = mitad ; filas < renglones - 1; filas++) { //recorre la segunda mitad del arreglo
                    double valorNuevo = ((Number)datos[filas][col]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo += ((Number)datos[filas+1][col]).doubleValue();
                    cambiar(filas, col, valorNuevo);//la suma se guarda en la posicion del valor en la matriz actual
                }
            }
            for(int fila = 0; fila < mitad - 1; fila++){ //se eliminan de las columnas de la primera mitad del arreglo
                quitarRenglon(TipoRenglon.SUP);
            }
            for(int filas = mitad ; filas < renglones ; filas++) {//se eliminan de las columnas de la segunda mitad del arreglo
                quitarRenglon(TipoRenglon.INF);
            }
            doblado = true;
        }
        else{
            int mitad = (renglones - 1) / 2;
            for (int col = 0; col < columnas; col++) {  //col x col
                for (int fila = 0; fila < mitad - 1; fila++) { //recorre la primera mitad del arreglo
                    double valorNuevo = ((Number)datos[fila+1][col]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo += ((Number)datos[fila][col]).doubleValue();
                    cambiar(fila+1, col, valorNuevo);//la suma se guarda en la posicion del valor en la matriz actual
                }
                for (int filas = mitad ; filas < renglones - 1; filas++) { //recorre la segunda mitad del arreglo
                    double valorNuevo = ((Number)datos[filas][col]).doubleValue(); //el valor de esta posicion en la matriz se castea a una variable double
                    valorNuevo += ((Number)datos[filas+1][col]).doubleValue();
                    cambiar(filas, col, valorNuevo);//la suma se guarda en la posicion del valor en la matriz actual
                }
            }
            for(int fila = 0; fila < mitad - 1; fila++){ //se eliminan de los renglones de la primera mitad del arreglo
                quitarRenglon(TipoRenglon.SUP);
            }
            for(int filas = mitad ; filas < renglones ; filas++) {//se eliminan de los renglones de la segunda mitad del arreglo
                quitarRenglon(TipoRenglon.INF);
            }
            doblado = true;
        }
        return doblado;
    }


}

