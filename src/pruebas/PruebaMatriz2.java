package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import estructurasnolineales.Matriz2;
import estructurasnolineales.Matriz2Numerica;
import herramientas.comunes.TipoColumna;
import herramientas.comunes.TipoRenglon;

public class PruebaMatriz2 {
    public static void main(String[] args) {
       Matriz2 matriz1 = new Matriz2(3, 3);
       matriz1.cambiar(0, 0, 5);
       matriz1.cambiar(0, 1, 2);
       matriz1.cambiar(0, 2, 1);
       matriz1.cambiar(1, 0, 7);
       matriz1.cambiar(1, 1, 4);
       matriz1.cambiar(1, 2, 3);
       matriz1.cambiar(2, 0, 6);
       matriz1.cambiar(2, 1, 8);
       matriz1.cambiar(2, 2, 9);

       SalidaPorDefecto.consola("------ Matriz 1 ------" + "\n");
       matriz1.imprimirXReglones();
       matriz1.transpuesta();
       SalidaPorDefecto.consola("------ Matriz 1 transpuesta ------" + "\n");
       matriz1.imprimirXReglones();

       Matriz2 matriz2 = new Matriz2(3, 3);
       matriz2.cambiar(0, 0, 8);
       matriz2.cambiar(0, 1, 2);
       matriz2.cambiar(0, 2, 1);
       matriz2.cambiar(1, 0, 5);
       matriz2.cambiar(1, 1, 9);
       matriz2.cambiar(1, 2, 1);
       matriz2.cambiar(2, 0, 6);
       matriz2.cambiar(2, 1, 4);
       matriz2.cambiar(2, 2, 3);

       SalidaPorDefecto.consola("------ Matriz 2 ------" + "\n");
       matriz2.imprimirXReglones();

       SalidaPorDefecto.consola("------ Matriz Copia De La Matriz 1 ------" + "\n");
       Matriz2 matrizCopia1 = matriz1.clonar();
       matrizCopia1.imprimirXReglones();
       SalidaPorDefecto.consola("\n->Matriz 1 y Matriz Copia 1 son iguales: " +
               matriz1.esIgual(matrizCopia1) + "\n");

       Matriz2 matriz3 = new Matriz2(2, 2);
       matriz3.cambiar(0, 0, 5);
       matriz3.cambiar(0, 1, 2);
       matriz3.cambiar(1, 0, 7);
       matriz3.cambiar(1, 1, 4);

       SalidaPorDefecto.consola("------ Matriz 3 ------" + "\n");
       matriz3.imprimirXReglones();
       SalidaPorDefecto.consola("\n");

       SalidaPorDefecto.consola("->Redimensionar Matriz 3 a 3x3: " + matriz3.redefinir(matriz2) + "\n");
       SalidaPorDefecto.consola("------ Matriz 3 Redimensionada/Cambiada ------" + "\n");
       matriz3.imprimirXReglones();

       ListaEstatica reglon = new ListaEstatica(3);
       reglon.agregar(5);
       reglon.agregar(2);
       reglon.agregar(1);

       ListaEstatica columna = new ListaEstatica(3);
       columna.agregar(8);
       columna.agregar(3);
       columna.agregar(9);

       SalidaPorDefecto.consola("\n->Agregar renglon 2 a la matriz 1: " +
               matriz1.agregarRenglon(reglon, 2) + "\n");
       matriz1.imprimirXReglones();

       SalidaPorDefecto.consola("\n->Agregar columna 2 a la matriz 1: " +
               matriz1.agregarColumna(columna, 2) + "\n");
       matriz1.imprimirXReglones();

       SalidaPorDefecto.consola("\n------ Vector Columna De La Matriz 2 ------" + "\n");
       Matriz2 vectorColumna = matriz2.aVectorColumna();
       vectorColumna.imprimirXReglones();

       SalidaPorDefecto.consola("\n------ Vector Renglon De La Matriz 1 ------" + "\n");
       Matriz2 vectorRenglon = matriz1.aVectorRenglon();
       vectorRenglon.imprimirXReglones();

       SalidaPorDefecto.consola("\n------ Matriz 4 ------" + "\n");
       Matriz2 matriz4 = new Matriz2(3, 3);
       matriz4.cambiar(0, 0, 5);
       matriz4.cambiar(0, 1, 2);
       matriz4.cambiar(0, 2, 1);
       matriz4.cambiar(1, 0, 9);
       matriz4.cambiar(1, 1, 1);
       matriz4.cambiar(1, 2, 0);
       matriz4.cambiar(2, 0, 3);
       matriz4.cambiar(2, 1, 4);
       matriz4.cambiar(2, 2, 8);
       matriz4.imprimirXReglones();

       SalidaPorDefecto.consola("\n->Quitar Columna Derecha De La Matriz 4: " +
               matriz4.quitarReglon(TipoColumna.DER) + "\n");
       matriz4.imprimirXReglones();

       SalidaPorDefecto.consola("\n->Quitar Renglon Superior De La Matriz 4: " +
               matriz4.eliminarRenglon(TipoRenglon.SUPE) + "\n");
       matriz4.imprimirXReglones();

       SalidaPorDefecto.consola("\n------ Matriz 3 ------" + "\n");
       matriz3.imprimirXReglones();
       SalidaPorDefecto.consola("\n->Eliminar Renglon 1 De La Matriz 3: " +
               matriz3.eliminarRenglon(1) + "\n");
       matriz3.imprimirXReglones();

        SalidaPorDefecto.consola("\n------ Matriz 1 ------" + "\n");
        matriz1.imprimirXReglones();
        SalidaPorDefecto.consola("\n->Eliminar Renglon 2 De La Matriz 1: " +
                matriz1.eliminarRenglon(2) + "\n");
        matriz1.imprimirXReglones();

        SalidaPorDefecto.consola("\n------ Matriz 1 ------" + "\n");
        matriz1.imprimirXReglones();
        SalidaPorDefecto.consola("\n->Eliminar Columna 0 De La Matriz 1: " +
                matriz1.eliminarColumna(0) + "\n");
        matriz1.imprimirXReglones();

        Matriz2 matriz5 = new Matriz2(2, 2);
        matriz5.cambiar(0, 0, 3);
        matriz5.cambiar(0, 1, 4);
        matriz5.cambiar(1, 0, 8);
        matriz5.cambiar(1, 1, 1);

        SalidaPorDefecto.consola("\n------ Matriz 5 ------" + "\n");
        matriz5.imprimirXReglones();

        Matriz2 matriz6 = new Matriz2(2, 2);
        matriz6.cambiar(0, 0, 5);
        matriz6.cambiar(0, 1, 2);
        matriz6.cambiar(1, 0, 7);
        matriz6.cambiar(1, 1, 4);
        SalidaPorDefecto.consola("\n->Agregar Matriz 6 a la Matriz 5: " +
                matriz6.agregarMatrizXRenglon(matriz5) + "\n");
        matriz6.imprimirXReglones();

    }
}
