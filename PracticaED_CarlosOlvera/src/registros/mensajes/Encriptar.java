package registros.mensajes;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import entradaSalida.archivos.ArchivoTexto;
import estructurasLineales.ListaEstatica;
import estructurasLineales.PilaEstatica;

import java.awt.image.BufferedImage;

public class Encriptar {
    protected static ListaEstatica texto; //almacena el archivo original
    protected static PilaEstatica pilaTexto; //pila donde se almacenan los caracteres
    protected static PilaEstatica pila; //pila donde se almacenan la busqueda de la simulacion de pagina
    protected static ListaEstatica pagina1; //almacena el archivo de la pagina1
    protected static ListaEstatica pagina2; //almacena el archivo de la pagina2
    protected static ListaEstatica pagina3; //almacena el archivo de la pagina3
    protected static ListaEstatica pagina4; //almacena el archivo de la pagina4
    protected static ListaEstatica pagina5; //almacena el archivo de la pagina5
    protected static ListaEstatica pagina6; //almacena el archivo de la pagina6
    protected static ListaEstatica pagina7; //almacena el archivo de la pagina7
    protected static ListaEstatica pagina8; //almacena el archivo de la pagina8
    protected static ListaEstatica pagina9; //almacena el archivo de la pagina9
    protected static ListaEstatica pagina10; //almacena el archivo de la pagina10
    protected static ListaEstatica pagina11; //almacena el archivo de la pagina11
    protected static ListaEstatica pagina12; //almacena el archivo de la pagina12
    protected static ListaEstatica pagina13; //almacena el archivo de la pagina13
    protected static ListaEstatica pagina14; //almacena el archivo de la pagina14
    protected static ListaEstatica pagina15; //almacena el archivo de la pagina15
    protected static ListaEstatica pagina16; //almacena el archivo de la pagina16
    protected static ListaEstatica pagina17; //almacena el archivo de la pagina17
    protected static ListaEstatica pagina18; //almacena el archivo de la pagina18
    protected static ListaEstatica pagina19; //almacena el archivo de la pagina17
    protected static ListaEstatica pagina20; //almacena el archivo de la pagina18

    /**public Encriptar(String archivo) {
        texto = ArchivoTexto.leer(archivo);
        pilaTexto = new PilaEstatica(archivo.length());

    }*/

    public static PilaEstatica codificar(String archivo) {
        pilaTexto = new PilaEstatica(archivo.length()+100);
        int ancho = (int) (Math.random()*11);
        int tmp = 1;
        String mensaje = "";
        String cadena ="";
        texto = ArchivoTexto.leer(archivo);
        for (int cont = 0; cont <= texto.numeroElementos() ; cont++){
            cadena+=texto.obtener(cont);
        }
        for (int cont = cadena.length()-1; cont > 0 ; cont--){
            char token = cadena.charAt(cont);
            int inicial = (int) (Math.random()*4);
            if (inicial == 3 && tmp == 1){
                pilaTexto.poner("Æ");
                mensaje += "Æ";
                tmp = 0;
            }
            if (tmp == 0){
                ancho = ancho-1;
            }
            if (ancho == -1){
                pilaTexto.poner("Æ");
                mensaje += "Æ";
                ancho = (int) (Math.random()*7);
                tmp = 1;
            }
            pilaTexto.poner(token);
            mensaje += token;

        }
        pilaTexto.poner(mensaje);
        return pilaTexto;
    }

    public static String imprimirMensajeCodificado() {
        String ultimo = (String) pilaTexto.verUltimo();
        return ultimo;
    }

    public static void imprimirMensajeDecodificado(String archivo){
        texto = ArchivoTexto.leer(archivo);
        texto.imprimir();
    };


    public static void hipervinculos(String archivo1, String archivo2, String archivo3, String archivo4, String archivo5, String archivo6, String archivo7, String archivo8, String archivo9, String archivo10,
                                     String archivo11, String archivo12, String archivo13, String archivo14, String archivo15, String archivo16, String archivo17, String archivo18, String archivo19, String archivo20) {
        pila = new PilaEstatica(50);
        int opcion = 1;
        pagina1 = ArchivoTexto.leer(archivo1);
        pagina2 = ArchivoTexto.leer(archivo2);
        pagina3 = ArchivoTexto.leer(archivo3);
        pagina4 = ArchivoTexto.leer(archivo4);
        pagina5 = ArchivoTexto.leer(archivo5);
        pagina6 = ArchivoTexto.leer(archivo6);
        pagina7 = ArchivoTexto.leer(archivo7);
        pagina8 = ArchivoTexto.leer(archivo8);
        pagina9 = ArchivoTexto.leer(archivo9);
        pagina10 = ArchivoTexto.leer(archivo10);
        pagina11 = ArchivoTexto.leer(archivo11);
        pagina12 = ArchivoTexto.leer(archivo12);
        pagina13 = ArchivoTexto.leer(archivo13);
        pagina14 = ArchivoTexto.leer(archivo14);
        pagina15 = ArchivoTexto.leer(archivo15);
        pagina16 = ArchivoTexto.leer(archivo16);
        pagina17 = ArchivoTexto.leer(archivo17);
        pagina18 = ArchivoTexto.leer(archivo18);
        pagina19 = ArchivoTexto.leer(archivo19);
        pagina20 = ArchivoTexto.leer(archivo20);

        do{
            SalidaPorDefecto.consola("Bienvenido a la wiki de Star Wars\n");
            pagina1.imprimirOI();
            SalidaPorDefecto.consola("Elige una opcion: ");
            double opcionD = EntradaPorDefecto.consolaDouble();
            opcion = (int) opcionD;
            switch (opcion){
                case 1:
                    pila.poner(pagina1.obtener(0));
                    do{
                        SalidaPorDefecto.consola("----------\n");
                        pagina2.imprimirOI();
                        SalidaPorDefecto.consola("Elige una opcion: ");
                        double opcionD1 = EntradaPorDefecto.consolaDouble();
                        opcion = (int) opcionD1;
                        switch (opcion){
                            case 1:
                                pila.poner(pagina2.obtener(0));
                                do{
                                    SalidaPorDefecto.consola("----------\n");
                                    pagina3.imprimirOI();
                                    SalidaPorDefecto.consola("Elige una opcion: ");
                                    double opcionD2 = EntradaPorDefecto.consolaDouble();
                                    opcion = (int) opcionD2;
                                    switch (opcion){
                                        case 1:
                                            pila.poner(pagina3.obtener(0));
                                            do{
                                                SalidaPorDefecto.consola("----------\n");
                                                pagina8.imprimirOI();
                                                SalidaPorDefecto.consola("Elige una opcion: ");
                                                double opcionD5 = EntradaPorDefecto.consolaDouble();
                                                opcion = (int) opcionD5;
                                                switch (opcion){
                                                    case 1:
                                                        pila.poner(pagina8.obtener(0));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina9.imprimirOI();
                                                        break;
                                                    case 2:
                                                        pila.poner(pagina8.obtener(1));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina10.imprimirOI();
                                                        break;
                                                }
                                            }
                                            while (opcion != 8);
                                            break;
                                        case 2:
                                            pila.poner(pagina3.obtener(1));
                                            do{
                                                SalidaPorDefecto.consola("----------\n");
                                                pagina8.imprimirOI();
                                                SalidaPorDefecto.consola("Elige una opcion: ");
                                                double opcionD5 = EntradaPorDefecto.consolaDouble();
                                                opcion = (int) opcionD5;
                                                switch (opcion){
                                                    case 1:
                                                        pila.poner(pagina8.obtener(0));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina11.imprimirOI();
                                                        break;
                                                    case 2:
                                                        pila.poner(pagina8.obtener(1));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina12.imprimirOI();
                                                        break;
                                                }
                                            }
                                            while (opcion != 8);
                                            break;
                                    }
                                }
                                while (opcion != 7);
                                break;
                            case 2:
                                pila.poner(pagina2.obtener(1));
                                do{
                                    SalidaPorDefecto.consola("----------\n");
                                    pagina4.imprimirOI();
                                    SalidaPorDefecto.consola("Elige una opcion: ");
                                    double opcionD3 = EntradaPorDefecto.consolaDouble();
                                    opcion = (int) opcionD3;
                                    switch (opcion){
                                        case 1:
                                            pila.poner(pagina4.obtener(0));
                                            do{
                                                SalidaPorDefecto.consola("----------\n");
                                                pagina8.imprimirOI();
                                                SalidaPorDefecto.consola("Elige una opcion: ");
                                                double opcionD5 = EntradaPorDefecto.consolaDouble();
                                                opcion = (int) opcionD5;
                                                switch (opcion){
                                                    case 1:
                                                        pila.poner(pagina8.obtener(0));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina13.imprimirOI();
                                                        break;

                                                    case 2:
                                                        pila.poner(pagina8.obtener(1));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina14.imprimirOI();
                                                        break;
                                                }
                                            }
                                            while (opcion != 8);
                                            break;
                                        case 2:
                                            pila.poner(pagina4.obtener(1));
                                            do{
                                                SalidaPorDefecto.consola("----------\n");
                                                pagina8.imprimirOI();
                                                SalidaPorDefecto.consola("Elige una opcion: ");
                                                double opcionD5 = EntradaPorDefecto.consolaDouble();
                                                opcion = (int) opcionD5;
                                                switch (opcion){
                                                    case 1:
                                                        pila.poner(pagina8.obtener(0));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina15.imprimirOI();
                                                        break;
                                                    case 2:
                                                        pila.poner(pagina8.obtener(1));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina16.imprimirOI();
                                                }
                                            }
                                            while (opcion != 8);
                                            break;
                                    }
                                }
                                while (opcion != 7);
                                break;
                        }
                    }
                    while (opcion != 6);
                    break;
                case 2:
                    pila.poner(pagina1.obtener(0));
                    do{
                        SalidaPorDefecto.consola("----------\n");
                        pagina5.imprimirOI();
                        SalidaPorDefecto.consola("Elige una opcion: ");
                        double opcionD1 = EntradaPorDefecto.consolaDouble();
                        opcion = (int) opcionD1;
                        switch (opcion){
                            case 1:
                                pila.poner(pagina5.obtener(0));
                                do{
                                    SalidaPorDefecto.consola("----------\n");
                                    pagina6.imprimirOI();
                                    SalidaPorDefecto.consola("Elige una opcion: ");
                                    double opcionD2 = EntradaPorDefecto.consolaDouble();
                                    opcion = (int) opcionD2;
                                    switch (opcion){
                                        case 1:
                                            pila.poner(pagina6.obtener(0));
                                            do{
                                                SalidaPorDefecto.consola("----------\n");
                                                pagina8.imprimirOI();
                                                SalidaPorDefecto.consola("Elige una opcion: ");
                                                double opcionD5 = EntradaPorDefecto.consolaDouble();
                                                opcion = (int) opcionD5;
                                                switch (opcion){
                                                    case 1:
                                                        pila.poner(pagina8.obtener(0));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina17.imprimirOI();
                                                        break;

                                                    case 2:
                                                        pila.poner(pagina8.obtener(1));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina18.imprimirOI();
                                                        break;
                                                }
                                            }
                                            while (opcion != 8);
                                            break;
                                    }
                                }
                                while (opcion != 7);
                                break;
                            case 2:
                                pila.poner(pagina5.obtener(1));
                                do{
                                    SalidaPorDefecto.consola("----------\n");
                                    pagina7.imprimirOI();
                                    SalidaPorDefecto.consola("Elige una opcion: ");
                                    double opcionD2 = EntradaPorDefecto.consolaDouble();
                                    opcion = (int) opcionD2;
                                    switch (opcion){
                                        case 1:
                                            pila.poner(pagina7.obtener(0));
                                            do{
                                                SalidaPorDefecto.consola("----------\n");
                                                pagina8.imprimirOI();
                                                SalidaPorDefecto.consola("Elige una opcion: ");
                                                double opcionD5 = EntradaPorDefecto.consolaDouble();
                                                opcion = (int) opcionD5;
                                                switch (opcion){
                                                    case 1:
                                                        pila.poner(pagina8.obtener(0));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina13.imprimirOI();
                                                        break;

                                                    case 2:
                                                        pila.poner(pagina8.obtener(1));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina14.imprimirOI();
                                                        break;
                                                }
                                            }
                                            while (opcion != 8);
                                            break;
                                        case 2:
                                            pila.poner(pagina7.obtener(1));
                                            do{
                                                SalidaPorDefecto.consola("----------\n");
                                                pagina8.imprimirOI();
                                                SalidaPorDefecto.consola("Elige una opcion: ");
                                                double opcionD5 = EntradaPorDefecto.consolaDouble();
                                                opcion = (int) opcionD5;
                                                switch (opcion){
                                                    case 1:
                                                        pila.poner(pagina8.obtener(0));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina19.imprimirOI();
                                                        break;

                                                    case 2:
                                                        pila.poner(pagina8.obtener(1));
                                                        SalidaPorDefecto.consola("----------\n");
                                                        pagina20.imprimirOI();
                                                        break;
                                                }
                                            }
                                            while (opcion != 8);
                                            break;
                                    }
                                }
                                while (opcion != 7);
                                break;
                        }
                    }
                    while (opcion != 6);
                    break;
            }
        }
        while (opcion !=0);
    }

    public static void historial(){
        pila.imprimir();
    }
}
