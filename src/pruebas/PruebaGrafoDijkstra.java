package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import estructurasnolineales.GrafoEstatico;
import herramientas.generales.TipoOrdenGrafo;

public class PruebaGrafoDijkstra {
    public static void main(String[] args) {
        GrafoEstatico grafo = new GrafoEstatico(8, TipoOrdenGrafo.DEC);

        // agregar vertices
        grafo.agregarVertice("A");
        grafo.agregarVertice("B");
        grafo.agregarVertice("C");
        grafo.agregarVertice("D");
        grafo.agregarVertice("E");
        grafo.agregarVertice("F");
        grafo.agregarVertice("G");
        grafo.agregarVertice("H");

        // agregar aristas
        grafo.agregarArista("A", "B", 5);
        grafo.agregarArista("B", "A", 5);

        grafo.agregarArista("A", "C", 3);
        grafo.agregarArista("C", "A", 3);

        grafo.agregarArista("A", "D", 8);
        grafo.agregarArista("D", "A", 8);

        grafo.agregarArista("B", "E", 4);
        grafo.agregarArista("E", "B", 4);

        grafo.agregarArista("D", "F", 7);
        grafo.agregarArista("F", "D", 7);

        grafo.agregarArista("C", "G", 6);
        grafo.agregarArista("G", "C", 6);

        grafo.agregarArista("D", "E", 3);
        grafo.agregarArista("E", "D", 3);

        grafo.agregarArista("B", "D", 1);
        grafo.agregarArista("D", "B", 1);

        grafo.agregarArista("C", "D", 2);
        grafo.agregarArista("D", "C", 2);

        grafo.agregarArista("F", "E", 4);
        grafo.agregarArista("E", "F", 4);

        grafo.agregarArista("F", "G", 9);
        grafo.agregarArista("G", "F", 9);

        grafo.agregarArista("E", "H", 7);
        grafo.agregarArista("H", "E", 7);

        grafo.agregarArista("G", "H", 5);
        grafo.agregarArista("H", "G", 5);

        SalidaPorDefecto.consola("Grafo: \n");
        grafo.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("Etiquetas Dijkstra: \n");
        grafo.etiquetasOptimasDijkstra("A").imprimir();

        SalidaPorDefecto.consola("\nMetrica de A -> H: ");
        Double metrica = grafo.obtenerMetricaOptimaDijkstra("A", "H");
        if (metrica != null) {
            SalidaPorDefecto.consola(metrica + "\n");
        } else {
            SalidaPorDefecto.consola("El origen o destino no existe\n");
        }

        SalidaPorDefecto.consola("\nLa ruta de A -> H es: ");
        ListaDinamica ruta = grafo.obtenerRutaOptimaDijkstra("A", "H");
        if (ruta != null) {
            ruta.imprimir();
        } else {
            SalidaPorDefecto.consola("El origen o destino no existe\n");
        }
    }
}