package proyectoFinal;

import estructurasLineales.ListaEstatica;

/**
 * Esta clase representa un nodo de un grafo de un modelo oculto de Markov.
 * @author Cristian Omar Alvarado Rodriguez y Carlos Eduardo Olvera Mayorga.
 * @version 1.0
 */
public class NodoMOM {

    protected String nombre;
    protected double probaInicial;
    protected ListaEstatica observaciones;

    public NodoMOM(String nombre, double probaInicial, ListaEstatica observaciones){
        this.nombre = nombre;
        this.probaInicial = probaInicial;
        this.observaciones = observaciones;
    }

    /**
     * Este método retorna el nombre del nodo (estado).
     * @return Regresa el nombre del nodo (estado).
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Este método asigna un nombre al nodo (estado).
     * @param nombre Es el nombre del nodo (estado).
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Este método retorna la probabilidad inicial del nodo (estado).
     * @return Regresa la probabilidad inicial del nodo (estado).
     */
    public double getProbaInicial() {
        return probaInicial;
    }

    /**
     * Este método asigna una probabilidad inicial al nodo (estado).
     * @param probaInicial Es la probabilidad inicial del nodo (estado).
     */
    public void setProbaInicial(double probaInicial) {
        this.probaInicial = probaInicial;
    }

    /**
     * Este método retorna la lista de observaciones del nodo (estado).
     * @return Regresa la lista de observaciones del nodo (estado).
     */
    public ListaEstatica getObservaciones() {
        return observaciones;
    }

    /**
     * Este método asigna una lista de observaciones al nodo (estado).
     * @param observaciones Es la lista de observaciones del nodo (estado).
     */
    public void setObservaciones(ListaEstatica observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public String toString() {
        return nombre;
    }
}