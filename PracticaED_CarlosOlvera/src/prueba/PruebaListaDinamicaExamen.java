package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamicaExamen;

public class PruebaListaDinamicaExamen {
    public static void main(String[] args) {
        ListaDinamicaExamen lista = new ListaDinamicaExamen();
        lista.agregar("A");//1
        lista.agregar("G");//2
        lista.agregar("B");//3
        lista.agregar("R");//4
        lista.agregar("C");//5
        lista.agregar("Z");//6
        SalidaPorDefecto.consola("-----ESTRUCTURA ORIGINAL-----\n");
        lista.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----ELIMINAR-----\n");
        lista.eliminar();
        lista.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----ELIMINAR INICIO-----\n");
        lista.eliminarInicio();
        lista.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----AGREGAR AL INICIO-----\n");
        lista.agregarInicio("X");//Inicio
        lista.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----BUSCAR UN VALOR-----\n");
        SalidaPorDefecto.consola("Buscar el valor X: "+ lista.buscar("X")+"\n");
        SalidaPorDefecto.consola("Buscar el valor A: "+ lista.buscar("A")+"\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----ELIMINAR UN VALOR ESPECIFICO-----\n");
        SalidaPorDefecto.consola("ELIMINAR R:\n");
        lista.eliminar("R");
        lista.imprimir();
        SalidaPorDefecto.consola("\n");


    }
}
