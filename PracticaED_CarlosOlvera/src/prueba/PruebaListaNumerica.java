package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstaticaNumerica;

public class PruebaListaNumerica {
    public static void main(String[] args) {
        ListaEstaticaNumerica lista1 = new ListaEstaticaNumerica(5);
        ListaEstaticaNumerica lista2 = new ListaEstaticaNumerica(5);
        ListaEstaticaNumerica exponentes = new ListaEstaticaNumerica(5);
        double totalListas = 0;
        SalidaPorDefecto.consola("---AGREGAR VALORES---\n");
        SalidaPorDefecto.consola("--lista 1--\n");
        lista1.agregar(10);
        lista1.agregar(9);
        lista1.agregar(8);
        lista1.agregar(7);
        lista1.imprimir();
        SalidaPorDefecto.consola("--lista 2--\n");
        lista2.agregar(6);
        lista2.agregar(5);
        lista2.agregar(4);
        lista2.agregar(3);
        lista2.imprimir();
        SalidaPorDefecto.consola("--lista Exponentes--\n");
        exponentes.agregar(2);
        exponentes.agregar(2);
        exponentes.agregar(2);
        exponentes.agregar(2);
        exponentes.imprimir();
        SalidaPorDefecto.consola("\n");


        SalidaPorDefecto.consola("---BUSCAR VALORES---\n");
        SalidaPorDefecto.consola("Buscar indice del valor 8.0: "+lista1.buscar(8)+"\n");
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---POTENCIAR LISTA CON VALOR---\n");
        SalidaPorDefecto.consola("Potenciar la lista 1 con valor 3: "+lista1.aplicarPotencia(3)+"\n");
        lista1.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---SUMAR LISTAS---\n");
        SalidaPorDefecto.consola("Suma de las listas 1 y 2 "+lista1.sumar(lista2)+"\n");
        lista1.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---MULTIPLICAR LISTAS---\n");
        SalidaPorDefecto.consola("Multiplicación de las listas 1 y 2 "+lista1.multiplicar(lista2)+"\n");
        lista1.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---POTENCIAR LISTA CON LISTA DE ESCALARES---\n");
        SalidaPorDefecto.consola("Potenciar la lista 1 con la lista de exponentes: "+lista1.aplicarPotencia(exponentes)+"\n");
        lista1.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---MULTIPLICAR ESCALAR---\n");
        SalidaPorDefecto.consola("Multiplicar valor por 2: "+lista1.porEscalar(2)+"\n");
        lista1.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---SUMAR ESCALAR---\n");
        SalidaPorDefecto.consola("Se le sumo al valor 100: "+lista1.sumarEscalar(100)+"\n");
        lista1.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---PRODUCTO ESCALAR---\n");
        SalidaPorDefecto.consola("El producto escalar de la lista 1 es: "+lista1.productoEscalar(lista2)+"\n");
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---NORMA---\n");
        SalidaPorDefecto.consola("La norma de la lista 1 es:"+lista1.norma()+"\n");
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---NORMA EUCLIDIANA---\n");
        SalidaPorDefecto.consola("La norma euclidiana de la lista 1 es: "+lista1.normaEuclidiana(lista2)+"\n");
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---SUMA LISTAS---\n");
        totalListas = lista1.sumarListaEstatica(lista1)+lista1.sumarListaEstatica(lista2)+lista1.sumarListaEstatica(exponentes);
        SalidaPorDefecto.consola("La suma de la lista 1, 2 y exponentes es: "+totalListas+"\n");
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---SUMA ESCALARES---\n");
        SalidaPorDefecto.consola("La suma la lista de escalares es: "+lista1.sumarEscalares(exponentes)+"\n");
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("---ELIMINAR VALORES---\n");
        SalidaPorDefecto.consola("---Eliminar el valor: " + lista1.eliminar(2) + "\n");
        lista1.imprimir();
        SalidaPorDefecto.consola("\n");
    }
}
