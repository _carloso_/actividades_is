package herramientas.comunes;

/**
 * Este enumerado representa los tipos de renglones que pueden existir en una tabla, superior o inferior.
 * @author Cristian Omar Alvarado Rodriguez.
 *  * @version 1.0
 */
public enum TipoRenglon {
    SUPE("SUPERIOR", 1), INFE("INFERIOR", 2);

    private String nombre;
    private int valor;

    /**
     * Constructor de la clase TipoRenglon
     * @param nombre Es el nombre del tipo de renglon.
     * @param valor Es el valor del tipo de renglon.
     */
    private TipoRenglon(String nombre, int valor) {
        this.nombre = nombre;
        this.valor = valor;
    }

    /**
     * Obtiene el nombre del tipo de renglon.
     * @return Retorna el nombre del tipo de renglon.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Obtiene el valor del tipo de renglon.
     * @return Retorna el valor del tipo de renglon.
     */
    public int getValor() {
        return valor;
    }

}
