package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaEstatica;

public class PruebaColaEstatica {
    public  static void main(String argus[]){
        ColaEstatica cola=new ColaEstatica(5);

        cola.poner("F");
        cola.poner("J");
        cola.poner("A");
        cola.poner("K");
        cola.poner("Y");
        cola.poner("Z");

        cola.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("Atendiendo al siguiente: " + cola.quitar()+ "\n");

        cola.imprimir();
        SalidaPorDefecto.consola("\n");

    }
}

