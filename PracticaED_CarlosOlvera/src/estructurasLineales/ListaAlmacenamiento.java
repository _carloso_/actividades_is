package estructurasLineales;

/**
 * Esta interfaz gestiona la funcionalidad de una lista de almacenamiento.
 * @author Clase ED.
 * @version 1.0
 */
public interface ListaAlmacenamiento {

    /**
     * Determina si una lista de almacenamiento esta vacia.
     * @return Regresa <b>true</b> si la lista esta vacia, <b>false</b> en caso contrario.
     */
    public boolean vacia();

    /**
     * Inserta al final de la lista el elemento especificados com oargumento.
     * @param valor Es el dato que se va a agregar en la lista.
     * @return Regresa la posicion en memoria(indice) en donde se agrega el valor, o -1 en caso que no se haya podido insertar.
     */
    public int agregar(Object valor);

    public void imprimir();

    public void imprimirOI();

    public Object buscar(Object valor);

    public Object eliminar(Object valor);

    public Object eliminar();

    public Object verUltimo();

}
