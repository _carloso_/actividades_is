﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Restaurante.Models;
using Restaurante.Data;
using Microsoft.EntityFrameworkCore;

namespace Restaurante.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly ApplicationDbContext _context;

    public HomeController(ILogger<HomeController> logger, ApplicationDbContext context)
    {
        _logger = logger;
        _context = context;
    }

    public async Task<IActionResult> Index()
        {
              return _context.Comidas != null ? 
                          View(await _context.Comidas.ToListAsync()) :
                          Problem("Entity set 'ApplicationDbContext.Comidas'  is null.");
        }

    public async Task<IActionResult> Menu()
        {
              return _context.Entradas != null ? 
                          View(await _context.Entradas.ToListAsync()) :
                          Problem("Entity set 'ApplicationDbContext.Comidas'  is null.");
        }

    

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
