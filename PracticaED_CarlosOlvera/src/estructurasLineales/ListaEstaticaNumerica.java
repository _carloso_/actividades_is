package estructurasLineales;

import entradaSalida.SalidaPorDefecto;

public class ListaEstaticaNumerica extends ListaEstatica{
    protected int MAXIMO;
    protected int ultimo;
    protected Number[] datos;
    public ListaEstaticaNumerica(int maximo) {
        super(maximo);
        this.MAXIMO = maximo;
        this.ultimo = -1;
        this.datos = new Number[MAXIMO];
    }

    @Override
    public void imprimir() {
        super.imprimir();
    }

    @Override
    public boolean vacia() {
        return super.vacia();
    }

    @Override
    public int agregar(Object valor) {
        if(valor instanceof Number) { //Comprobamos que valor sea de la clase number
            double valorDouble = ((Number) valor).doubleValue(); //utilizamos una nueva variable para convertir valor en tipo double
            return super.agregar(valorDouble);
        }
        else {
            return 0; //si no es de clase number, no se hace nada en el metodo
        }
    }

    @Override
    public Object eliminar(Object valor) {
        if(valor instanceof Number) {
            double valorDouble = ((Number) valor).doubleValue(); //utilizamos una nueva variable para convertir valor en tipo double
            return super.eliminar(valorDouble);

        }
        else{
            return null; //si no es de clase number, no se hace nada en el metodo
        }
    }

    @Override
    public Object buscar(Object valor) {
        if(valor instanceof Number) {
            double valorDouble = ((Number) valor).doubleValue(); //utilizamos una nueva variable para convertir valor en tipo double
            return super.buscar(valorDouble);
        }
        else{
            return null; //si no es de clase number, no se hace nada en el metodo
        }
    }

    public boolean porEscalar(Number escalar){
        if(!vacia()){
            double escalarDouble = escalar.doubleValue(); //el objeto valor se hace de tipo double
            for(int posicion = 0; posicion<=ultimo; posicion++){
                datos[posicion] = datos[posicion].doubleValue() * escalarDouble; //multiplicar cada posicion del arreglo
            }
            return true;
        }
        else{
            return false;
        }
    }

    public boolean sumarEscalar(Number escalar){
        if(!vacia()){
            double escalarDouble = escalar.doubleValue();
            for(int posicion = 0; posicion<=ultimo; posicion++){
                datos[posicion] =  escalarDouble + datos[posicion].doubleValue();//sumar cada posicion del arreglo
            }
            return true;
        }
        else{
            return false;
        }
    }

    public boolean sumar(ListaEstaticaNumerica lista2){
        if(numeroElementos() == lista2.numeroElementos() && !vacia()){
            for(int posicion=0; posicion<=ultimo; posicion++){
                datos[posicion] = datos[posicion].doubleValue() + lista2.datos[posicion].doubleValue(); //se suma el valor de la misma posicion de cada lista
            }
            return true;
        }
        else{
            return false;
        }
    }

    public boolean multiplicar(ListaEstaticaNumerica lista2){
        if(numeroElementos() == lista2.numeroElementos() && !vacia()){
            for(int posicion=0; posicion<=ultimo; posicion++){
                datos[posicion] = datos[posicion].doubleValue() * lista2.datos[posicion].doubleValue(); //se multiplica el valor de la misma posicion de cada lista
            }
            return true;
        }
        else{
            return false;
        }
    }

    public boolean aplicarPotencia(ListaEstaticaNumerica listaEcalares){
        if(!vacia()){
            for(int posicion=0; posicion<=ultimo; posicion++){
                for(int i=1; posicion<=listaEcalares.datos[posicion].doubleValue(); i++){
                    datos[posicion] = datos[posicion].doubleValue() * datos[posicion].doubleValue(); //la lista se multiplicara a si misma el numero de veces de cada dato de listaEscalares
                }
            }
            return true;
        }
        else{
            return false;
        }
    }

    public boolean aplicarPotencia(Number escalar){
        if(!vacia()){
            double escalarDouble = escalar.doubleValue();
            for(int posicion = 0; posicion<=ultimo; posicion++){
                for(int i=1; posicion<=escalarDouble; i++){     //la lista se multiplicara a si misma el numero de veces de escalar
                    datos[posicion] = datos[posicion].doubleValue() * datos[posicion].doubleValue();
                }
            }
            return true;
        }
        else{
            return false;
        }
    }

    public double productoEscalar(ListaEstaticaNumerica lista2) {
        double total = 0;
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            total = total + datos[posicion].doubleValue() * lista2.datos[posicion].doubleValue(); //se va a multiplicar el valor de posicion de lista 1 con lista 2
            //se van guardando los datos en la variable
        }
        return total;
    }

    public double norma(){
        double total = 0;
        double resultado;
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            total = total + datos[posicion].doubleValue() * datos[posicion].doubleValue(); //se van guardando los datos en la variable
        }
        resultado = Math.sqrt(total);
        return resultado;
    }

    public double normaEuclidiana(ListaEstaticaNumerica arreglo2){
        double total = 0;
        double resultado;
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            //se va a restar el valor de la lista dos con el de la uno, y despues multiplicandolo al cuadrado
            total = (arreglo2.datos[posicion].doubleValue() - datos[posicion].doubleValue())*(arreglo2.datos[posicion].doubleValue() - datos[posicion].doubleValue());
            total = total+total; //se van guardando los datos en la variable
        }
        resultado = Math.sqrt(total);  //la raiz cuadrada de la operacion
        return resultado;
    }

    public double sumarListaEstatica(ListaEstatica listas){
        double total = 0;
        ListaEstaticaNumerica listastmp = (ListaEstaticaNumerica) listas;
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            total = listastmp.datos[posicion].doubleValue() + datos[posicion].doubleValue();  //se suman todos los valores de la lista estatica, el dato se guarda en esta variable
            total = total+total; //se van guardando los datos en la variable
        }
        return total;
    }

    public double sumarEscalares(ListaEstaticaNumerica escalares){
        double total = 0;
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            for (int i = 0; posicion <= escalares.datos[posicion].doubleValue(); i++) //se va a multiplicar a si mismo las veces del escalar
            total = datos[posicion].doubleValue() * datos[posicion].doubleValue();
            total = total+total; //se van guardando los datos en la variable
        }
        return total;
    }
    public void agregarBuffer(double [] buffer) {
        if (vacia() == true) { //si la lista esta vacia, se agrega
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                agregar(buffer[posicion]);
            }
        }
        else {
            SalidaPorDefecto.consola("No se puede agregar el buffer a una lista vacia");
        }

    }


    public double[] leerArreglo() {
        double[] copia = new double[datos.length];
        if (!vacia()) {
            for (int posicion = 0; posicion <= ultimo; posicion++) {
                copia[posicion] = datos[posicion].doubleValue();
            }
        }
        return copia;
    }


}
