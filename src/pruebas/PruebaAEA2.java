package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasnolineales.ArbolExpAritm;

public class PruebaAEA2 {
    public static void main(String[] args) {
        ArbolExpAritm arbol = new ArbolExpAritm();

        arbol.generarArbolExpresiones("(((z^n)-(e/g))+(b*d))");

        SalidaPorDefecto.consola("Inorden: ");
        arbol.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: ");
        arbol.preOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: ");
        arbol.postOrden();
    }
}