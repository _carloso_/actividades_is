package actividadesparciales;

/**
 * Esta enumeración representa los tipos de actividades que pueden realizarse en la galería de arte.
 * @author Cristina Omar Alvarado Rodriguez.
 * @version 1.0
 */
public enum TipoActividad {
    PINTAR("Pintar", 1), EXPONER("Exposicion", 2), FIRMA_AUTOGRAFOS("Firma de autógrafos", 3),
    VIAJAR("Viajar", 4), DESCANSAR("Descansar", 5);

    private String nombre;
    private int valor;

    private TipoActividad(String nombre, int valor) {
        this.nombre = nombre;
        this.valor = valor;
    }

    public String getNombre() {
        return nombre;
    }

    public int getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
