package registros.habitacionesDeCasas;

import entradaSalida.ArchivoTexto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamicaDoble;
import estructurasLineales.ListaEstatica;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;

/**
 * Esta clase nos ayuda a calcular la correlación entre las habitaciones de las casas y los precios de las mismas, asi como
 * su regresion lineal para estimar el precio de las casas con base en el número de habitaciones.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class CorrelacionHabitacionesPrecios {

    private ListaDinamicaDoble x;
    private ListaDinamicaDoble y;

    // listas estáticas para obtener los datos de los archivos x e y.
    private ListaEstatica valoresX;
    private ListaEstatica valoresY;

    public CorrelacionHabitacionesPrecios(String rutaArchivoX, String rutaArchivoY) {
        x = new ListaDinamicaDoble();
        y = new ListaDinamicaDoble();
        valoresX = ArchivoTexto.leer(rutaArchivoX);
        valoresY = ArchivoTexto.leer(rutaArchivoY);
        aregarValores();
    }

    /**
     * Este método se encarga de agregar los valores de los archivos x e y a las listas enlazadas dobles.
     */
    private void aregarValores() {
        x.agregarLista(valoresX);
        y.agregarLista(valoresY);
    }

    /**
     * Este método se encarga de calcular el coeficiente r de pearson y muestra su interpretación.
     */
    public void coeficienteRDePearson() {
        double r = correlacionMuestral();
        if (r == 0) {
            SalidaPorDefecto.consola("*Ninguna correlación" + "\n");
        } else if (r == 1) {
            SalidaPorDefecto.consola("*Correlación positiva perfecta: r = " + r + "\n");
        } else if (r > 0 && r < 1) {
            SalidaPorDefecto.consola("*Correlación positiva: r = " + r + "\n");
        } else if (r < 0 && r > -1) {
            SalidaPorDefecto.consola("*Correlación negativa: r = " + r + "\n");
        } else if (r == -1) {
            SalidaPorDefecto.consola("*Correlación negativa perfecta: r = " + r + "\n");
        } else {
            SalidaPorDefecto.consola("*Correlación indefinida" + "\n");
        }
    }

    /**
     * Este método se encarga de calcular el promedio de los valores de x.
     * @return Regresa el promedio de los valores de x.
     */
    public double promedioDeX() {
        double suma = 0;
        int contador = 0;
        x.iniciarIteradorDelInicio();
        while (x.hayNodos() == true) {
            double valorX = Double.parseDouble(x.obtenerNodoDer().toString());
            suma += valorX;
            contador++;
        }
        return suma/contador;
    }

    /**
     * Este método se encarga de calcular el promedio de los valores de y.
     * @return Regresa el promedio de los valores de y.
     */
    public double promedioDeY() {
        double suma = 0;
        int contador = 0;
        y.iniciarIteradorDelInicio();
        while (y.hayNodos() == true) {
            double valorY = Double.parseDouble(y.obtenerNodoDer().toString());
            suma += valorY;
            contador++;
        }
        return suma/contador;
    }

    /**
     * Este método se encarga de calcular la sumatoria de (xi - Promedio(x))^2.
     * @return Regresa la sumatoria de (xi - Promedio(x))^2.
     */
    public double sumatoriaCuadradaDeXiMenosPromedioX() {
        double sumatoria = 0;
        double promedio = promedioDeX();
        x.iniciarIteradorDelInicio();
        while (x.hayNodos() == true) {
            double valorX = Double.parseDouble(x.obtenerNodoDer().toString());
            sumatoria += Math.pow(valorX - promedio, 2);
        }
        return sumatoria;
    }

    /**
     * Este método se encarga de calcular la sumatoria de (yi - Promedio(y))^2.
     * @return Regresa la sumatoria de (yi - Promedio(y))^2.
     */
    public double sumatoriaCuadradaDeYiMenosPromedioY() {
        double sumatoria = 0;
        double promedio = promedioDeY();
        y.iniciarIteradorDelInicio();
        while (y.hayNodos() == true) {
            double valorY = Double.parseDouble(y.obtenerNodoDer().toString());
            sumatoria += Math.pow(valorY - promedio, 2);
        }
        return sumatoria;
    }

    /**
     * Este método se encarga de calcular la desviación estandar de la población de x.
     * @return Regresa la desviación estandar de la población de x.
     */
    public double desviacionEstandarDePoblacionDeX() {
        double desviacionEst = Math.sqrt(sumatoriaCuadradaDeXiMenosPromedioX() / (x.contarCantidadDeNodosExistentes()));
        return desviacionEst;
    }

    /**
     * Este método se encarga de calcular la desviación estandar de la población de y.
     * @return Regresa la desviación estandar de la población de y.
     */
    public double desviacionEstandarDePoblacionDeY() {
        double desviacionEst = Math.sqrt(sumatoriaCuadradaDeYiMenosPromedioY() / (y.contarCantidadDeNodosExistentes()));
        return desviacionEst;
    }

    /**
     * Este método se encarga de calcular la desviación estandar de la muestra de x.
     * @return Regresa la desviación estandar de la muestra de x.
     */
    public double desviacionEstandarDeMuestraX() {
        double desviacionEst = Math.sqrt(sumatoriaCuadradaDeXiMenosPromedioX() / (x.contarCantidadDeNodosExistentes() - 1));
        return desviacionEst;
    }

    /**
     * Este método se encarga de calcular la desviación estandar de la muestra de y.
     * @return Regresa la desviación estandar de la muestra de y.
     */
    public double desviacionEstandarDeMuestraY() {
        double desviacionEst = Math.sqrt(sumatoriaCuadradaDeYiMenosPromedioY() / (y.contarCantidadDeNodosExistentes() - 1));
        return desviacionEst;
    }

    /**
     * Este método se encarga de calcular la covarianza muestral de x e y.
     * @return Regresa la covarianza muestral de x e y.
     */
    public double covarianzaMuestral() {
        return sumatoriaDeXiMenosPromedioXYiMenosPromedioY() / (x.contarCantidadDeNodosExistentes() - 1);
    }

    /**
     * Este método se encarga de calcular la correlación poblacional de x e y.
     * @return Regresa la correlación poblacional de x e y.
     */
    public double covarianzaPoblacional() {
        return sumatoriaDeXiMenosPromedioXYiMenosPromedioY() / (x.contarCantidadDeNodosExistentes());
    }

    /**
     * Este método se encarga de calcular la sumatoria de (xi - Promedio(x)) * (yi - Promedio(y)).
     * @return Regresa la sumatoria de (xi - Promedio(x)) * (yi - Promedio(y)).
     */
    public double sumatoriaDeXiMenosPromedioXYiMenosPromedioY() {
        double sumatoria = 0;
        for (int i = 0; i < x.contarCantidadDeNodosExistentes(); i++) {
            double valorX = Double.parseDouble(x.obtenerNodo(i).toString());
            double valorY = Double.parseDouble(y.obtenerNodo(i).toString());
            sumatoria += (valorX - promedioDeX()) * (valorY - promedioDeY());
        }
        return sumatoria;
    }

    /**
     * Este método se encarga de calcular la correlación poblacional de x e y.
     * @return Regresa la correlación poblacional de x e y.
     */
    public double correlacionPoblacional() {
        return covarianzaPoblacional() / (desviacionEstandarDePoblacionDeX() * desviacionEstandarDePoblacionDeY());
    }

    /**
     * Este método se encarga de calcular la correlación muestral de x e y.
     * @return Regresa la correlación muestral de x e y.
     */
    public double correlacionMuestral() {
        double multiplicacionDeDesviacionesEst = desviacionEstandarDeMuestraX() * desviacionEstandarDeMuestraY();
        double nMenosUno = (x.contarCantidadDeNodosExistentes() - 1);
        return sumatoriaDeXiMenosPromedioXYiMenosPromedioY() / (nMenosUno * multiplicacionDeDesviacionesEst);
    }

    /**
     * Este método se encarga de calcular beta1 para la ecuación de regresión lineal de x e y.
     * @return Regresa el cálculo de beta1.
     */
    public double beta1() {
        return correlacionMuestral() * (desviacionEstandarDeMuestraY() / desviacionEstandarDeMuestraX());
    }

    /**
     * Este método se encarga de calcular beta0 para la ecuación de regresión lineal de x e y.
     * @return Regresa el cálculo de beta0.
     */
    public double beta0() {
        return promedioDeY() - beta1() * promedioDeX();
    }

    /**
     * Este método se encarga de calcular y mostrar la ecuación de regresión lineal de x e y.
     */
    public void calcularEcuacionDeRegresionLineal() {
        SalidaPorDefecto.consola("*Ecuacion de regresion lineal: " + beta0() + " + " + beta1() + "(x)" + "\n");
    }

    /**
     * Este método estima el valor de y para un valor de x dado, (estimar el precio de una casa basándonos en el número de habitaciones).
     * @param valorX Es el valor de x (número de habitaciones de una casa).
     */
    public void estimarValor(double valorX) {
        SalidaPorDefecto.consola("*Valor estimado para una casa con " + valorX +
                " habitaciones: " + (beta0() + beta1() * valorX) + "\n");
    }

    /**
     * Este método muestra la gráfica de dispersión de los datos x e y.
     */
    public void graficarDatosXY() {
        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries serie = new XYSeries("Datos");
        x.iniciarIteradorDelInicio();
        y.iniciarIteradorDelInicio();
        while (x.hayNodos() == true && y.hayNodos() == true) {
            double valorX = Double.parseDouble(x.obtenerNodoDer().toString());
            double valorY = Double.parseDouble(y.obtenerNodoDer().toString());
            serie.add(valorX, valorY);
        }
        dataset.addSeries(serie);
        JFreeChart chart = ChartFactory.createScatterPlot("Costo de casas de acuerdo al número de habitaciones",
                "X", "Y", dataset, PlotOrientation.VERTICAL, true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(500, 500));
        chartPanel.setVisible(true);
        JFrame frame = new JFrame("Datos de X e Y");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(chartPanel);
        frame.pack();
        frame.setVisible(true);
    }
}