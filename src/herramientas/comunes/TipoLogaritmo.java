package herramientas.comunes;

/**
 * Este enumerado representa los tipos de logaritmos que se pueden aplicar a un número.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public enum TipoLogaritmo {
    NATURAL("NATURAL", 1), BASE10("BASE10", 2), BASE2("BASE2", 3);

    private String nombre;
    private int valor;

    /**
     * Constructor del enumerado TipoLogaritmo que recibe el nombre del tipo de logaritmo y su valor.
     * @param nombre Nombre del tipo de logaritmo.
     * @param valor Valor del tipo de logaritmo.
     */
    private TipoLogaritmo(String nombre, int valor) {
        this.nombre = nombre;
        this.valor = valor;
    }

    /**
     * Método que retorna el nombre del tipo de logaritmo.
     * @return Regresa el nombre del tipo de logaritmo.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Método que retorna el valor del tipo de logaritmo.
     * @return Regresa el valor del tipo de logaritmo.
     */
    public int getValor() {
        return valor;
    }
}
