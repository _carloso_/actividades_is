package actividadesparciales;

/**
 * Esta clase representa una actividad de galeria de arte.
 * @author Cristina Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class ActividadGaleria {

    private TipoActividad tipoActividad;
    private String nombre;
    private String lugar;
    private int aforoPersonas;
    private String descripcion;


    public ActividadGaleria(TipoActividad tipoActividad, String nombre, String lugar, int aforoPersonas, String descripcion) {
        this.tipoActividad = tipoActividad;
        this.nombre = nombre;
        this.lugar = lugar;
        this.aforoPersonas = aforoPersonas;
        this.descripcion = descripcion;
    }

    public ActividadGaleria(TipoActividad tipoActividad, String nombre, String lugar, String descripcion) {
        this(tipoActividad, nombre, lugar, 0, descripcion);
    }

    // Para una actividad de descanso.
    public ActividadGaleria(TipoActividad tipoActividad, String nombre) {
        this(tipoActividad, nombre, "", 0, "");
    }

    public TipoActividad getTipoActividad() {
        return tipoActividad;
    }

    public void setTipoActividad(TipoActividad tipoActividad) {
        this.tipoActividad = tipoActividad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public int getAforoPersonas() {
        return aforoPersonas;
    }

    public void setAforoPersonas(int aforoPersonas) {
        this.aforoPersonas = aforoPersonas;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return tipoActividad + " " + nombre;
    }

    public String obtenerDatos(){
        return "Tipo de actividad: " + tipoActividad + "\n"
                + "Nombre: " + nombre + "\n"
                + "Lugar: " + lugar + "\n"
                + "Aforo: " + aforoPersonas + "\n"
                + "Descripcion: " + descripcion;
    }
}