package registros.arroz;

import estructurasLineales.ListaEstatica;

public class Campesino {
    protected String nombre;
    protected String apellido;
    protected String noEmpleado;
    protected ListaEstatica produccion;

    public Campesino(String nombre, String apellido, String noEmpleado, ListaEstatica produccion){
        this.nombre = nombre;
        this.apellido = apellido;
        this.noEmpleado = noEmpleado;
        this.produccion = produccion;
    }

    public String getNombre(){
        return nombre;
    }

    public String getApellido(){
        return apellido;
    }

    public void setNombre(){
        this.nombre = nombre;
    }

    public void setApellido(){
        this.apellido = apellido;
    }

    public void setProduccion(ListaEstatica produccion) {
        this.produccion = produccion;
    }

    public ListaEstatica getProduccion() {
        return produccion;
    }

    public String getNoEmpleado(){
        return noEmpleado;
    }

    public void setNoEmpleado(){
        this.noEmpleado = noEmpleado;
    }

    public Double obtenerProduccion(){
        double sumaToneladas=0.0;
        double promedioToneladas=0.0;
        if(!produccion.vacia()){ //si hay produccion
            for(int posicion=0; posicion < produccion.numeroElementos(); posicion++){
                sumaToneladas = sumaToneladas+(double)produccion.obtener(posicion);
            }
            promedioToneladas = sumaToneladas / produccion.numeroElementos();
            return promedioToneladas;
        }else{ //no hay produccion
            return null;
        }
    }

    public String toString(){
        return nombre + " " +apellido;
    }
}
