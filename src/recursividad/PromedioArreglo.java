package recursividad;

import entradaSalida.SalidaPorDefecto;

public class PromedioArreglo {

    public static double promedioA(int[] arreglo, int ultimo, int pos){
        double suma = sumarA(arreglo, ultimo, pos);
        SalidaPorDefecto.consola("Suma: " + suma + "\n");
        SalidaPorDefecto.consola("N: " + (ultimo + 1) + "\n");
        return suma / (ultimo + 1);
    }

    public static double sumarA(int[] arreglo, int ultimo, int pos){
        if(pos == ultimo){
            return arreglo[pos];
        } else {
            return arreglo[pos] + sumarA(arreglo, ultimo, pos + 1);
        }
    }

    public static void main(String[] args) {
        int[] arreglo = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        SalidaPorDefecto.consola("Promedio: " + promedioA(arreglo, arreglo.length - 1, 0));
    }
}