package actividadesparciales;

/**
 * Esta clase representa a un pintor de una galería de arte.
 * @author Cristina Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class Pintor {

    private int numPintor;
    private String nombre;
    private String apellido;
    private int edad;
    private Fecha fechaNacimiento;
    private String domicilio;
    private String rfc;
    private String nivelEducativo;
    private String telefono;

    public Pintor(int numPintor, String nombre, String apellido, int edad, Fecha fechaNacimiento, String domicilio, String rfc, String nivelEducativo, String telefono) {
        this.numPintor = numPintor;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.fechaNacimiento = fechaNacimiento;
        this.domicilio = domicilio;
        this.rfc = rfc;
        this.nivelEducativo = nivelEducativo;
        this.telefono = telefono;
    }

    public int getNumPintor() {
        return numPintor;
    }

    public void setNumPintor(int numPintor) {
        this.numPintor = numPintor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Fecha getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Fecha fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNivelEducativo() {
        return nivelEducativo;
    }

    public void setNivelEducativo(String nivelEducativo) {
        this.nivelEducativo = nivelEducativo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "" + numPintor;
    }

    /**
     * Este método permite obtener el nombre completo del pintor.
     * @return Regresa el nombre completo del pintor.
     */
    public String nombreCompleto(){
        return nombre + " " + apellido;
    }

    /**
     * Este método permite obtener los datos del pintor.
     * @return Regresa los datos del pintor.
     */
    public String obtenerDatos() {
        return "No. Pintor: " + numPintor +
                "\nNombre: " + nombre +
                "\nApellido: " + apellido +
                "\nEdad: " + edad +
                "\nFecha de nacimiento: " + fechaNacimiento +
                "\nDomicilio: " + domicilio +
                "\nRFC: " + rfc +
                "\nNivel educativo: " + nivelEducativo +
                "\nTeléfono: " + telefono;
    }
}
