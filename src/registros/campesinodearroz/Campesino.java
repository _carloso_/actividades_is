package registros.campesinodearroz;

import estructurasLineales.ListaEstatica;

/**
 * Esta clase es nuestro registro de campesinos de arroz que realiza una produccion anual.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class Campesino {
    protected String nombre;
    protected String apellido;
    protected Integer numCampesino;
    protected ListaEstatica listaAnios;

    public Campesino(String nombre, String apellido, Integer numCampesino, int canAnios) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.numCampesino = numCampesino;
        this.listaAnios = new ListaEstatica(canAnios);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getNumCampesino() {
        return numCampesino;
    }

    public void setNumCampesino(Integer numCampesino) {
        this.numCampesino = numCampesino;
    }

    public ListaEstatica getListaAnios() {
        return listaAnios;
    }

    public void setListaAnios(ListaEstatica listaAnios) {
        this.listaAnios = listaAnios;
    }

    /**
     * Este metodo agrega un lista de un anio con su produccion de arroz por meses a la lista de anios.
     * @param listaAnio Es la lista de un anio con su produccion de arroz por meses.
     * @return Regresa <b>true</b> si se agrego correctamente, de lo contrario regresa <b>false</b>.
     */
    public boolean agregarAnio(ListaEstatica listaAnio) {
        int valorRetornoAgregado = listaAnios.agregar(listaAnio);
        if (valorRetornoAgregado == -1) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return numCampesino + "";
    }
}
