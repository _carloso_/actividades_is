package herramientas.matematicas;
public class Recursion {
    public int multiplicacion(int x, int y){
            // si x es menor que
            // intercambiar los números
            if (x < y)
                return multiplicacion(y, x);

                // calcular iterativamente
                // y veces la suma de x
            else if (y != 0)
                return (x + multiplicacion(x, y - 1));

                // si cualquiera de los numeros es
                // 0 regresa 0
            else
                return 0;
    }

    public boolean numeroPrimo(int num, int divisor) {
        if (num / 2 < divisor) {
            return true;
        } else {
            if (num % divisor == 0) {
                return false;
            } else {
                return numeroPrimo(num, divisor + 1);
            }
        }
    }

    /*public boolean esBinario(long num){
        String binarioLista = String.valueOf(num);
        int cantidad = binarioLista.length();
        if (cantidad != 0){
            if(binarioLista.charAt(binarioLista.length()-1) == '1' || binarioLista.charAt(binarioLista.length()-1) == '0'){
                return esBinario(num);
            }
            else{
                return false;
            }
        }
        return false;
    }*/

    public String Hexa(int num) {
        String result = "";
        int remainder = num % 16;

        if (num == 0) {
            return "";
        } else {
            switch (remainder) {
                case 10:
                    result = "A";
                    break;
                case 11:
                    result = "B";
                    break;
                case 12:
                    result = "C";
                    break;
                case 13:
                    result = "D";
                    break;
                case 14:
                    result = "E";
                    break;
                case 15:
                    result = "F";
                    break;
                default:
                    result = remainder + result;
                    break;
            }
            return Hexa(Integer.parseInt(Integer.toString(num / 16))) + result;
        }
    }

    public int mcdEuclides(int a, int b){
        if(b==0) {
            return a;
        }
        else {
            return mcdEuclides(b, a % b);
        }
    }

    public long Bin(int n) {
        if (n < 2) {
            return n;
        } else {
            return n % 2 + Bin(n / 2)*10;
        }
    }

    public int A(int m, int n){
        if (m==0){
            return n + 1;
        }
        else if (m>0 && n==0){
            return A(m-1,1);
        }
        else { //m>0 && n>0
            return A(m-1,A(m,n-1));
        }
    }


}
