package registros.prioridad;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaEstatica;

public class SalaDeUrgencias {
     ColaEstatica listaDeEspera;
    public SalaDeUrgencias(int noPacientes){
        listaDeEspera = new ColaEstatica(noPacientes);
    }
    public void agregarPaciente(Object valor){
        listaDeEspera.poner(valor);
    }
    public void ordenarPrioridadUrgencia(){
        listaDeEspera.ordenarPrioridad();
    }
    public void atenderPaciente(){
        listaDeEspera.quitar();
        SalidaPorDefecto.consola("El paciente de prioridad mas alta ha sido atendido exitosamente\n");
    }

    public Object verPrimerPaciente(){
        return listaDeEspera.verPrimero();
    }

    public Object verUltimoPaciente(){
        return listaDeEspera.verUltimo();
    }

    public void imprimirListaPacientes(){
        listaDeEspera.imprimir();
    }

    public String infoPaciente(Paciente paciente){
        String datos = paciente.DatosGenerales();
        return datos;
    }
}
