package pruebas;

import entradaSalida.SalidaPorDefecto;
import registros.campaniavacunacioncovid.ControlVacunacion;
import registros.campaniavacunacioncovid.Persona;
import registros.campaniavacunacioncovid.Vacuna;

public class PruebaVacunacionCovid {
    public static void main(String[] args) {
        ControlVacunacion control = new ControlVacunacion(5, 5);

        // Personas que se registran para ser vacunadas
        Persona per1 = new Persona("Juan", "Alvarado", "JSUYDSTUF2", "25/03/1980", "Masculino", "Zacatecas", 42);
        Persona per2 = new Persona("Maria", "Torres", "MADHU67723", "25/03/1971", "Femenino", "Zacatecas", 51);
        Persona per3 = new Persona("Cristian", "Rodriguez", "CSGDC8743", "02/01/2000", "Masculino", "Zacatecas", 22);
        Persona per4 = new Persona("Ana", "Perez", "ANDSTFY733", "10/09/1950", "Femenino", "Zacatecas", 71);
        Persona per5 = new Persona("Pedro", "García", "PRTERW76276", "05/11/2006", "Masculino", "Zacatecas", 15);

        // vacunas existentes
        Vacuna vac1 = new Vacuna(1, "AstraZeneca");
        Vacuna vac2 = new Vacuna(2, "AstraZeneca");
        Vacuna vac3 = new Vacuna(3, "Sinovac");
        Vacuna vac4 = new Vacuna(4, "Pfizer");
        Vacuna vac5 = new Vacuna(5, "CanSino");

        // agregar personas
        control.agregarPersona(per1);
        control.agregarPersona(per2);
        control.agregarPersona(per3);
        control.agregarPersona(per4);
        control.agregarPersona(per5);

        // agregar vacunas
        control.agregarVacuna(vac1);
        control.agregarVacuna(vac2);
        control.agregarVacuna(vac3);
        control.agregarVacuna(vac4);
        control.agregarVacuna(vac5);

        // vacunar a las personas
        SalidaPorDefecto.consola("---- Vacunar a las personas de mayor a menor edad ----" + "\n");
        control.vacunarPersonas();

    }
}
