package herramientas.matematicas;

import entradaSalida.SalidaPorDefecto;
import entradaSalida.archivos.ArchivoTexto;
import estructurasLineales.ListaDinamicaDoble;
import estructurasLineales.ListaEstatica;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;

/**
 * Operaciones para obtener la Correlacion
 */
public class Correlacion {
    protected ListaDinamicaDoble tabX;
    protected ListaDinamicaDoble tabY;
    protected ListaEstatica tabXEstatica;
    protected ListaEstatica tabYEstatica;
    /**
     * Recibe como parametros la ruta de las tablas
     * @param tablaX archivo .txt de la tabla X
     * @param tablaY archivo .txt de la tabla Y
     */
    public Correlacion(String tablaX, String tablaY){
        tabX = new ListaDinamicaDoble();
        tabY = new ListaDinamicaDoble();
        tabXEstatica = ArchivoTexto.leer(tablaX); //variable tipo ListaEstatica donde se guardara el contenido de la tabla1
        tabYEstatica = ArchivoTexto.leer(tablaY);
        tabX.agregarLista(tabXEstatica);
        tabY.agregarLista(tabYEstatica);
    }
    /**
     * Promediar los valores de la Tabla X
     * @return promedio tabla X
     */
    public double promedioX(){
        double suma = 0;
        for (int cont=0; cont<tabX.contarCantidadNodos();cont++){
            double x = Double.parseDouble(tabX.obtener(cont).toString());
            suma += x;
        }
        return (suma)/tabX.contarCantidadNodos();
    }

    /**
     * Promediar los valores de la Tabla Y
     * @return promedio tabla Y
     */
    public double promedioY(){
        double suma = 0;
        for (int cont=0; cont<tabY.contarCantidadNodos();cont++){
            double y = Double.parseDouble(tabY.obtener(cont).toString());
            suma += y;
        }
        return (suma)/tabY.contarCantidadNodos();
    }

    /**
     * Sumatoria del numerador: ((xi)-promedioX)*(yi-promedioY))
     * @return sumatoria del numerador de la pendiente
     */
    public double sumatoriaNumerador(){
        double promedioX = promedioX();
        double promedioY = promedioY();
        double sumaPendiente = 0;
        for(int cont=0; cont<tabY.contarCantidadNodos(); cont++) {//recorre todas las posiciones para hacer la sumatora
            double x = Double.parseDouble(tabX.obtener(cont).toString()); //obtenemos el valor de x para cada posicion
            double y = Double.parseDouble(tabY.obtener(cont).toString()); //obtenemos el valor de y para cada posicion
            sumaPendiente += (x-promedioX)*(y-promedioY); //aplicamos la formula para la sumatora
        }
        return  sumaPendiente;
    }

    public double sumatoriaX(){
        double promedioX = promedioX();
        double sumaX = 0;
        for(int cont=0; cont<tabY.contarCantidadNodos(); cont++) {//recorre todas las posiciones para hacer la sumatora
            double x = Double.parseDouble(tabX.obtener(cont).toString()); //obtenemos el valor de x para cada posicion
            sumaX += (x-promedioX)*(x-promedioX); //aplicamos la formula para la sumatora
        }
        return  sumaX;
    }

    public double sumatoriaY(){
        double promedioY = promedioY();
        double sumaY = 0;
        for(int cont=0; cont<tabY.contarCantidadNodos(); cont++) {//recorre todas las posiciones para hacer la sumatora
            double y = Double.parseDouble(tabY.obtener(cont).toString()); //obtenemos el valor de y para cada posicion
            sumaY += (y-promedioY)*(y-promedioY); //aplicamos la formula para la sumatora
        }
        return  sumaY;
    }
    /**
     * Desviacion Estandar: (Xi-PromedioX)/n
     * @return desviacion estandar de la tabla X
     */
    public double desviacionEstandarXPoblacional(){
        double promedioX = promedioX();
        double varianzaNumerador = 0;
        double varianza;
        double desviacionE;
        for(int cont=0; cont<tabX.contarCantidadNodos(); cont++) {//recorre todas las posiciones para hacer la sumatora
            double x = Double.parseDouble(tabX.obtener(cont).toString()); //obtenemos el valor de x para cada posicion
            varianzaNumerador += (x-promedioX)*(x-promedioX); //aplicamos la formula para la varianza
        }
        varianza = (varianzaNumerador/tabX.contarCantidadNodos());
        desviacionE = Math.sqrt(varianza);
        return desviacionE;
    }

    /**
     * Desviacion Estandar de la muestra X
     * @return desviacion estandar de la muestra tabla X
     */
    public double desviacionEstandarXMuestral(){
        double sumatoriaCuadrada = sumatoriaX();
        double desviacion =  sumatoriaCuadrada / (tabX.contarCantidadNodos() -1);
        return Math.sqrt(desviacion);
    }

    /**
     * Desviacion Estandar: (Yi-PromedioY)/n
     * @return desviacion estandar de la tabla Y
     */
    public double desviacionEstandarYPoblacional(){
        double promedioY = promedioY();
        double varianzaNumerador= 0;
        double varianza;
        double desviacionE;
        for(int cont=0; cont<tabY.contarCantidadNodos(); cont++) {//recorre todas las posiciones para hacer la sumatora
            double y = Double.parseDouble(tabY.obtener(cont).toString()); //obtenemos el valor de x para cada posicion
            varianzaNumerador += (y-promedioY)*(y-promedioY); //aplicamos la formula para la varianza
        }
        varianza = (varianzaNumerador/tabY.contarCantidadNodos());
        desviacionE = Math.sqrt(varianza);
        return desviacionE;
    }

    /**
     * Desviacion Estandar de la muestra Y
     * @return desviacion estandar de la muestra tabla Y
     */
    public double desviacionEstandarYMuestral(){
        double sumatoriaCuadrada = sumatoriaY();
        double desviacion =  sumatoriaCuadrada / (tabY.contarCantidadNodos() -1);
        return Math.sqrt(desviacion);
    }

    public double covarianza(){
        return sumatoriaNumerador() / (tabX.contarCantidadNodos()-1);
    }

    /**
     * Denominador coorelacion: (n-1)*desviaciónEstándarY*desviaciónEstándarX
     * @return Denominador coorelacion
     */
    public double correlacionDenominadorPoblacional(){
        double denominador = 0;
        int n = tabX.contarCantidadNodos();
        denominador = (n-1)*desviacionEstandarXPoblacional()*desviacionEstandarYPoblacional();
        return  denominador;
    }

    public double correlacionDenominadorMuestral(){
        double denominador = 0;
        denominador = Math.sqrt(sumatoriaX()*sumatoriaY());
        return  denominador;
    }

    /**
     * Coorelacion: ((xi)-promedioX)*(yi-promedioY))/(n-1)*desviaciónEstándarY*desviaciónEstándarX
     * @return Coorelacion
     */
    public double correlacionPoblacional(){
        double  correlacion = 0;
        correlacion = sumatoriaNumerador()/correlacionDenominadorPoblacional();
        return  correlacion;
    }


    public double correlacionMuestral(){
        double  correlacion = 0;
        correlacion = sumatoriaNumerador()/correlacionDenominadorMuestral();
        return  correlacion;
    }


    public void interpretacionVarianza(double valor){
        if(valor == 0){
            SalidaPorDefecto.consola("Grado de Coorelacion: Ninguna correlacion ");
        }
        else if(valor == 1){
            SalidaPorDefecto.consola("Grado de Coorelacion: Correlacion positiva perfecta ");
        }
        else if(0<valor && valor <1){
            SalidaPorDefecto.consola("Grado de Coorelacion: Correlacion Positiva ");
        }
        else if(valor == -1){
            SalidaPorDefecto.consola("Grado de Coorelacion: Correlacion negativa perfecta ");
        }
        else if(-1<valor && valor<0){
            SalidaPorDefecto.consola("Grado de Coorelacion: Correlacion Negativa");
        }
    }

    public void graficarDatos(){
        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries serie = new XYSeries("Datos");
        tabX.iniciarIteradorInicio();
        tabY.iniciarIteradorInicio();
        while(tabX.hayNodos() == true && tabY.hayNodos() == true){
            double valorX = Double.parseDouble(tabX.obtenerNodoDer().toString());
            double valorY = Double.parseDouble(tabY.obtenerNodoDer().toString());
            serie.add(valorX,valorY);
        }
        dataset.addSeries(serie);
        JFreeChart chart = ChartFactory.createScatterPlot("Datos de X y Y", "X","Y",dataset, PlotOrientation.VERTICAL, false, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setVisible(true);
        JFrame frame = new JFrame("Datos de X y Y");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(chartPanel);
        frame.pack();
        frame.setVisible(true);
    }

}
