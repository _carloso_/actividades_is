package registros.procesosistemaoperativo;

/**
 * Clase que representa un proceso en el sistema operativo.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class Proceso {

    private String nombre;
    private TipoProceso tipoProceso;

    public Proceso(String nombre, TipoProceso tipo) {
        this.nombre = nombre;
        this.tipoProceso = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public TipoProceso getTipoProceso() {
        return tipoProceso;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTipoProceso(TipoProceso tipo) {
        this.tipoProceso = tipo;
    }

    public String toString() {
        return nombre + ", " + tipoProceso;
    }
}