package registros.ordenacionpersonas;

/**
 * Esta clase representa a una persona con nombre y edad.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class Persona {

    protected String nombre;
    protected int edad;

    public Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return nombre + " " + edad;
    }
}