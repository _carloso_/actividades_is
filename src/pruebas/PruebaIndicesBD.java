package pruebas;

import entradaSalida.SalidaPorDefecto;
import registros.indicesBD.GeneradorIndicesBD;
import java.io.IOException;

public class PruebaIndicesBD {
    public static void main(String[] args) throws IOException {
        GeneradorIndicesBD generador = new GeneradorIndicesBD("C:\\Users\\Cristian\\Desktop\\customers.txt");

        generador.generarIndices();
        generador.buscarRegistro(5);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Eliminar registro con indice 5: " + generador.eliminarRegistro(5) + "\n");
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("Buscar registro con indice 5: ");
        generador.buscarRegistro(5);

        SalidaPorDefecto.consola("\n");
        generador.imprimirArbol();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Insertar registro: " +
                generador.insertarRegistro("1000\t" +
                                            "Cristian\tAlvarado\toracle.sql.STRUCT@6dc184bc\t" +
                                           "[+1 410 123 4816]\tMX\tAMERICA\t2400\t" +
                                           "Cristian.Alvarado@PIPIT.COM\t145\t" +
                                           "oracle.sql.STRUCT@5e3a9fb6\t" +
                                        "10/05/55\tmarried\tF\t\"H: 180,000 - 190,999\"\t") + "\n");
        generador.leerArchivo();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        generador.buscarRegistro(321);
    }
}
