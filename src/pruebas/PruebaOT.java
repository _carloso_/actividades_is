package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import estructurasnolineales.GrafoEstatico;

public class PruebaOT {
    public static void main(String[] args) {
        GrafoEstatico grafo = new GrafoEstatico(6);

        // Agregar vertices
        grafo.agregarVertice("P1");
        grafo.agregarVertice("P2");
        grafo.agregarVertice("P3");
        grafo.agregarVertice("P4");
        grafo.agregarVertice("P5");
        grafo.agregarVertice("P6");

        // Agregar aristas
        grafo.agregarArista("P1", "P3");
        grafo.agregarArista("P2", "P3");
        grafo.agregarArista("P2", "P4");
        grafo.agregarArista("P3", "P4");
        grafo.agregarArista("P3", "P5");
        grafo.agregarArista("P3", "P6");
        grafo.agregarArista("P4", "P6");
        grafo.agregarArista("P5", "P6");

        grafo.imprimir();

        SalidaPorDefecto.consola("\n");

        ListaDinamica DT = grafo.ordenacionTopologica();
        DT.imprimir();
    }
}
