package pruebas;

import entradaSalida.SalidaPorDefecto;
import registros.ordenacionpersonas.OrdenacionPersonas;
import registros.ordenacionpersonas.Persona;
import registros.ordenacionpersonas.TipoCriterio;

public class PruebaOrdenacionPersonas {
    public static void main(String[] args) {
        OrdenacionPersonas  ordPersonas = new OrdenacionPersonas(6);

        // Crear personas
        Persona per1 = new Persona("Juan", 30);
        Persona per2 = new Persona("Omar", 15);
        Persona per3 = new Persona("Ana", 25);
        Persona per4 = new Persona("Maria", 10);
        Persona per5 = new Persona("Alberto", 20);

        // agregar personas
        ordPersonas.agregarPersona(per1);
        ordPersonas.agregarPersona(per2);
        ordPersonas.agregarPersona(per3);
        ordPersonas.agregarPersona(per4);
        ordPersonas.agregarPersona(per5);

        // mostrar personas sin ordenar
        SalidaPorDefecto.consola("Personas sin ordenar:\n");
        ordPersonas.mostrarPersonas();

        // ordenar por edad
        ordPersonas.ordenar(TipoCriterio.EDAD);

        // mostrar personas ordenadas por edad
        SalidaPorDefecto.consola("\nPersonas ordenadas por edad:\n");
        ordPersonas.mostrarPersonas();

        // ordenar por nombre
        ordPersonas.ordenar(TipoCriterio.NOMBRE);

        // mostrar personas ordenadas por nombre
        SalidaPorDefecto.consola("\nPersonas ordenadas por nombre:\n");
        ordPersonas.mostrarPersonas();
    }
}
