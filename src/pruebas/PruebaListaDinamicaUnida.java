package pruebas;

import estructurasLineales.ListaDinamicaUnida;

public class PruebaListaDinamicaUnida {
    public static void main(String[] args) {
        ListaDinamicaUnida lista = new ListaDinamicaUnida();
        lista.agregar("A");
        lista.agregar("G");
        lista.agregar("B");
        lista.agregar("R");
        lista.agregar("C");
        lista.agregar("Z");

        lista.imprimir();
    }
}
