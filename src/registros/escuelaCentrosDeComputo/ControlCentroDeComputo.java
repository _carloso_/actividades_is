package registros.escuelaCentrosDeComputo;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;

/**
 * Esta calse nos ayuda a controlar los centros de computo que se encuentran en una escuela.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class ControlCentroDeComputo {

    protected Escuela escuela;

    public ControlCentroDeComputo(String nombreEscuela) {
        escuela = new Escuela(nombreEscuela);
    }

    /**
     * Este método que permite agregar un centro de cómputo a la escuela.
     * @param centroDeComputo Es el centro de cómputo que se desea agregar.
     * @return Regresa true si se agrego el centro de computo, false si no se agrego.
     */
    public boolean agregarCentroDeComputo(CentroDeComputo centroDeComputo) {
        return escuela.agregarCentroDeComputo(centroDeComputo);
    }

    /**
     * Este método que permite eliminar un centro de cómputo de la escuela.
     * @param numeroCentroDeComputo Es el número del centro de cómputo que se desea eliminar.
     * @return Regresa true si se elimino el centro de computo, false si no se pudo eliminar.
     */
    public boolean eliminarCentroDeComputo(int numeroCentroDeComputo) {
        return escuela.eliminarCentroDeComputo(numeroCentroDeComputo);
    }

    /**
     * Este método nos permite buscar un centro de cómputo en la escuela.
     * @param numeroCentroDeComputo Es el nombre del centro de cómputo que se desea buscar.
     * @return Regresa el centro de cómputo que se busco, null si no se encontró.
     */
    public CentroDeComputo buscarCentroDeComputoDeLaEscuela(int numeroCentroDeComputo) {
        return escuela.buscarCentroDeComputo(numeroCentroDeComputo);
    }

    /**
     * Este método nos permite agregar una computadora a un centro de cómputo de la escuela.
     * @param numeroCentroDeComputo Es el número del centro de cómputo al que se desea agregar la computadora.
     * @param computadora Es la computadora que se desea agregar al centro de cómputo.
     * @return Regresa true si se agrego la computadora, false si no se agrego.
     */
    public boolean agregarComputadoraAUnCentroDeComputo(int numeroCentroDeComputo, Computadora computadora) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            return centroDeComputo.agregarComputadora(computadora);
        } else {
            return false;
        }
    }

    /**
     * Este método imprime todos los centros de cómputo de la escuela.
     */
    public void imprimirCentrosDeComputo() {
        escuela.mostrarListadoDeCentrosDeComputo();
    }

    /**
     * Este método imprime todas las computadoras de un centro de cómputo de la escuela.
     * @param numeroCentroDeComputo Es el número del centro de cómputo al que se desea imprimir las computadoras.
     */
    public void imprimirComputadorasDeUnCentroDeComputo(int numeroCentroDeComputo) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            centroDeComputo.mostrarListadoDeComputadoras();
        } else {
            SalidaPorDefecto.consola("No se encontro el centro de computo" + "\n");
        }
    }


    /**
     * Este método nos permite buscar una computadora en un centro de cómputo de la escuela.
     * @param numeroCentroDeComputo Es el número del centro de cómputo en el que se desea buscar la computadora.
     * @param numeroComputadora Es el número de la computadora que se desea buscar en el centro de cómputo.
     */
    public void buscarComputadoraDeUnCentroDeComuto(int numeroCentroDeComputo, int numeroComputadora) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            Computadora computadora = centroDeComputo.buscarComputadora(numeroComputadora);
            if (computadora != null) {
                computadora.mostrarDatos();
            } else {
                SalidaPorDefecto.consola("No se encontró la computadora" + "\n");
            }
        } else {
            SalidaPorDefecto.consola("No se encontró el centro de computo" + "\n");
        }
    }

    /**
     * Este método imprime todas las computadoras de un centro de cómputo que tengan la aplicación chrome instalada.
     * @param numeroCentroDeComputo Es el número del centro de cómputo en el que se desea buscar las computadoras con la aplicación chrome instalada.
     */
    public void computadorasDeUnCentroDeComputoConChromeInstalado(int numeroCentroDeComputo) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            SalidaPorDefecto.consola("No. Centro de computo: " + numeroCentroDeComputo + ", Nombre: " + centroDeComputo.getNombre() + "\n");
            ListaDinamica computdorasConChromeInstalado = centroDeComputo.buscarComputadorasConChromeInstalado();
            computdorasConChromeInstalado.iniciarIterador();
            while (computdorasConChromeInstalado.hayNodos() == true) {
                Computadora computadoraIterada = (Computadora) computdorasConChromeInstalado.obtenerNodo();
                computadoraIterada.mostrarDatos();
            }
        }
    }

    /**
     * Este método elimina una computadora de un centro de cómputo de la escuela.
     * @param numeroCentroDeComputo Es el número del centro de cómputo en el que se desea eliminar la computadora.
     * @param numeroComputadora Es el número de la computadora que se desea eliminar del centro de cómputo.
     * @return regresa true si se elimino la computadora, false si no se elimino.
     */
    public boolean eliminarComputadoraDeUnCentroDeComputo(int numeroCentroDeComputo, int numeroComputadora) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            return centroDeComputo.eliminarComputadora(numeroComputadora);
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite añadir una aplicación a una computadora de un centro de cómputo de la escuela.
     * @param numeroCentroDeComputo Es el número del centro de cómputo en el que se desea añadir la aplicación a la computadora.
     * @param numeroComputadora  Es el número de la computadora a la que se le desea añadir la aplicación.
     * @param aplicacion Es la aplicación que se desea añadir a la computadora.
     * @return Regresa true si se añadió la aplicación, false si no se añadió.
     */
    public boolean agregarAplicacionAUnaComputadora(int numeroCentroDeComputo, int numeroComputadora, Aplicacion aplicacion) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            Computadora computadora = centroDeComputo.buscarComputadora(numeroComputadora);
            if (computadora != null) {
                boolean valorRetorno = computadora.agregarAplicacion(aplicacion);
                if (valorRetorno) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite eliminar una aplicación de una computadora de un determinado centro de cómputo de la escuela.
     *
     * @param numeroCentroDeComputo Es el número del centro de cómputo en el que se desea eliminar la aplicación de la computadora.
     * @param numeroComputadora     Es el número de la computadora a la que se le desea eliminar la aplicación.
     * @param aplicacion            Es la aplicación que se desea eliminar de la computadora.
     * @return Regresa true si se eliminó la aplicación, false si no pudo eliminar.
     */
    public boolean eliminarAplicacionDeUnaComputadora(int numeroCentroDeComputo, int numeroComputadora, Aplicacion aplicacion) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            Computadora computadora = centroDeComputo.buscarComputadora(numeroComputadora);
            if (computadora != null) {
                boolean valorRetorno = computadora.eliminarAplicacion(aplicacion);
                if (valorRetorno) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite añadir un usuario a una computadora de un centro de cómputo de la escuela.
     * @param numeroCentroDeComputo Es el número del centro de cómputo en el que se desea añadir el usuario a la computadora.
     * @param numeroComputadora Es el número de la computadora a la que se le desea añadir el usuario.
     * @param usuario Es el usuario que se desea añadir a la computadora.
     * @return Regresa true si se añadió el usuario, false si no se añadió.
     */
    public boolean agregarUnUsuarioAUnaComputadora(int numeroCentroDeComputo, int numeroComputadora, Usuario usuario) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            Computadora computadora = centroDeComputo.buscarComputadora(numeroComputadora);
            if (computadora != null) {
                boolean valorRetorno = computadora.agregarUsuario(usuario);
                if (valorRetorno) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método nos permite eliminar un usuario de una computadora de un centro de cómputo de la escuela.
     * @param numeroCentroDeComputo Es el número del centro de cómputo en el que se desea eliminar el usuario de la computadora.
     * @param numeroComputadora Es el número de la computadora a la que se le desea eliminar el usuario.
     * @param usuario Es el usuario que se desea eliminar de la computadora.
     * @return Regresa true si se eliminó el usuario, false si no pudo eliminar.
     */
    public boolean eliminarUnUsuarioDeUnaComputadora(int numeroCentroDeComputo, int numeroComputadora, Usuario usuario) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            Computadora computadora = centroDeComputo.buscarComputadora(numeroComputadora);
            if (computadora != null) {
                boolean valorRetorno = computadora.eliminarUsuario(usuario);
                if (valorRetorno) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Este método imprime los usuarios que usaron una computadora en un centro de cómputo de la escuela.
     * @param numeroCentroDeComputo Es el número del centro de cómputo en el que se desea imprimir los usuarios de la computadora.
     * @param numeroComputadora Es el número de la computadora a la que se le desea imprimir los usuarios.
     */
    public void usuariosQueUsaronUnaComputadora(int numeroCentroDeComputo, int numeroComputadora) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            Computadora computadora = centroDeComputo.buscarComputadora(numeroComputadora);
            if (computadora != null) {
                ListaDinamica usuariosDeLaComputadora = computadora.getUsuarios();
                usuariosDeLaComputadora.iniciarIterador();
                while (usuariosDeLaComputadora.hayNodos() == true) {
                    Usuario usuarioIterado = (Usuario) usuariosDeLaComputadora.obtenerNodo();
                    if (usuarioIterado.isEstatusDeUso() == true) {
                        SalidaPorDefecto.consola("El usuario " + usuarioIterado.getNombre() + " usó la computadora " + computadora.getNumComputadora() +
                                " del centro de cómputo " + centroDeComputo.getNombre() + " en la fecha " + usuarioIterado.getFechaDeUso() +
                                " hora inicio " + usuarioIterado.getHoraInicio() + " hora fin " + usuarioIterado.getHoraFin() + "\n");
                    }
                }
            }
        }
    }

    /**
     * Este método imprime las aplicaciones usadas por un usuario en un cierta fecha.
     * @param numeroCentroDeComputo Es el número del centro de cómputo en el que se desea imprimir las aplicaciones del usuario.
     * @param numeroComputadora Es el número de la computadora a la que se le desea imprimir las aplicaciones usadas por el usuario.
     * @param numeroUsuario Es el número del usuario que usó las aplicaciones.
     * @param fecha Es la fecha en la que se usaron las aplicaciones.
     */
    public void aplicacionesUsadasPorUnUsuarioEnUnaComputadora(int numeroCentroDeComputo, int numeroComputadora, int numeroUsuario, String fecha) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            Computadora computadora = centroDeComputo.buscarComputadora(numeroComputadora);
            if (computadora != null) {
                Usuario usuarioEncontrado = computadora.buscarUsuario(numeroUsuario);
                if (usuarioEncontrado != null) {
                    if (usuarioEncontrado.getFechaDeUso().equals(fecha)) {
                        ListaDinamica aplicacionesUsadas = usuarioEncontrado.getAplicacionesUsadas();
                        aplicacionesUsadas.iniciarIterador();
                        while (aplicacionesUsadas.hayNodos() == true) {
                            Aplicacion aplicacionIterada = (Aplicacion) aplicacionesUsadas.obtenerNodo();
                            SalidaPorDefecto.consola(aplicacionIterada.obtenerDatos());
                        }
                    }
                }
            } else {
                SalidaPorDefecto.consola("La computadora " + numeroComputadora + " no existe en el centro de cómputo " + numeroCentroDeComputo + "\n");
            }
        } else {
            SalidaPorDefecto.consola("El centro de cómputo " + numeroCentroDeComputo + " no existe.\n");
        }
    }

    /**
     * Este método imprime a los usuarios que no usan el centro de cómputo.
     * @param numeroCentroDeComputo Es el número del centro de cómputo en el que se desea imprimir los usuarios que no usan el centro de cómputo.
     */
    public void usuariosQueNoUsanLaComputadorasDelCentroDeComputo(int numeroCentroDeComputo) {
        CentroDeComputo centroDeComputo = buscarCentroDeComputoDeLaEscuela(numeroCentroDeComputo);
        if (centroDeComputo != null) {
            ListaDinamica computadorasDelCentro = centroDeComputo.getComputadoras();
            computadorasDelCentro.iniciarIterador();
            while (computadorasDelCentro.hayNodos() == true) {
                Computadora computadoraIterada = (Computadora) computadorasDelCentro.obtenerNodo();
                ListaDinamica usuariosDeLaComputadora = computadoraIterada.getUsuarios();
                usuariosDeLaComputadora.iniciarIterador();
                while (usuariosDeLaComputadora.hayNodos() == true) {
                    Usuario usuarioIterado = (Usuario) usuariosDeLaComputadora.obtenerNodo();
                    if (usuarioIterado.isEstatusDeUso() == false) {
                        SalidaPorDefecto.consola("El usuario " + usuarioIterado.getNombre() + " " +usuarioIterado.getApellido() +
                                ", No ha usado el centro de cómputo numero " + centroDeComputo.getNumero() +"\n");
                    }
                }
            }
        }
    }
}