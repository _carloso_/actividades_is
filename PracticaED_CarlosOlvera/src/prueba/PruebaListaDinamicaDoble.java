package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamicaDoble;
import estructurasLineales.ListaEstatica;

public class PruebaListaDinamicaDoble {
    public static void main(String[] args) {
        ListaDinamicaDoble lista = new ListaDinamicaDoble();
        lista.agregar("F");
        lista.agregar("D");
        lista.agregar("A");
        lista.agregar("S");
        lista.agregar("G");
        ListaEstatica listaEstatica = new ListaEstatica(3);
        listaEstatica.agregar("Z");
        listaEstatica.agregar("Z");
        listaEstatica.agregar("Y");
        SalidaPorDefecto.consola("-----IMPRIMIR LISTA-----\n");
        lista.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----IMPRIMIR LISTA INVERSA-----\n");
        lista.imprimirOI();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----AGREGAR AL INICO-----\n");
        lista.agregarInicio("X");
        lista.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----ELIMINAR INICO-----\n");
        lista.eliminarInicio();
        lista.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----ELIMINAR FINAL-----\n");
        lista.eliminar();
        lista.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----VER ULTIMO-----\n");
        SalidaPorDefecto.consola("Ultimo valor de la lista: "+lista.verUltimo());
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----BUSCAR DE FINAL A INICIO-----\n");
        SalidaPorDefecto.consola("Buscar valor: A: "+lista.buscarFinalInicio("A"));
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----BUSCAR DE INICIO A FINAL-----\n");
        SalidaPorDefecto.consola("Buscar valor: S: "+lista.buscar("S"));
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----RECORRER LA LISTA DE INICIO A FINAL-----\n");
        lista.iniciarIteradorInicio();
        while (lista.hayNodos()){
            SalidaPorDefecto.consola(lista.obtenerNodoDer()+"\n");
        }
        SalidaPorDefecto.consola("-----RECORRER LA LISTA DE FINAL A INICIO-----\n");
        lista.iniciarIteradorFinal();
        while (lista.hayNodos()){
            SalidaPorDefecto.consola(lista.obtenerNodoIzq()+"\n");
        }
        SalidaPorDefecto.consola("-----ELIMINAR UN VALOR-----\n");
        SalidaPorDefecto.consola("Eliminar valor A: "+lista.eliminar("A")+"\n");
        lista.imprimir();
        SalidaPorDefecto.consola("\n");

        /////METODOS DE LA INTERFAZ
        SalidaPorDefecto.consola("-----CLONAR UNA LISTA-----\n");
        ListaDinamicaDoble listaClonada = lista.clonar();
        listaClonada.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----RELLENAR UNA LISTA-----\n");
        listaClonada.rellenar("X",3);
        listaClonada.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----INVERTIR UNA LISTA-----\n");
        listaClonada.invertir();
        listaClonada.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----CONTADOR DE VALORES REPETIDOS-----\n");
        SalidaPorDefecto.consola("Las veces que se repite en la lista el valor X son : "+listaClonada.contar("X"));
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----CAMBIAR UN VALOR VIEJO POR UNO NUEVO-----\n");
        SalidaPorDefecto.consola("-Cambiar el valor X por el Z, 2 veces:\n");
        listaClonada.cambiar("X","Z",2);
        listaClonada.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----AGREGAR UN VALOR EN UN INDICE-----\n");
        listaClonada.insertar("Y",2);
        listaClonada.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("-----ES SUBLISTA-----\n");
        SalidaPorDefecto.consola("-Lista Estatica\n");
        listaEstatica.imprimir();
        SalidaPorDefecto.consola("La lista estatica es sublista de la lista dinamica?: " +listaClonada.esSublista(listaEstatica));
    }
}
