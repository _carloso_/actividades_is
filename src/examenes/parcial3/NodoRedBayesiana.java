package examenes.parcial3;

import estructurasnolineales.Matriz2;

/**
 * Esta clase representa un nodo de una red bayesiana.
 * @author Cristian Omar Alvarado Rodriguez y Carlos Eduardo Olvera Mayorga.
 * @version 1.0
 */
public class NodoRedBayesiana {

    protected String nombre; // nombre del nodo (variable)
    protected Matriz2 tablaProbabilidad; // tabla de probabilidades del nodo (Matriz2 de 2x2)

    public NodoRedBayesiana(String nombre, Matriz2 tablaProbabilidad){
        this.nombre = nombre;
        this.tablaProbabilidad = tablaProbabilidad;
    }

    /**
     * Este método regresa el nombre del nodo.
     * @return Regresa el nombre del nodo.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Este método regresa la tabla de probabilidades del nodo.
     * @return Regresa la tabla de probabilidades del nodo.
     */
    public Matriz2 getTablaProbabilidad() {
        return tablaProbabilidad;
    }

    /**
     * Este método asigna un nuevo nombre al nodo.
     * @param nombre Es el nuevo nombre del nodo.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Este método asigna una nueva tabla de probabilidades al nodo.
     * @param tablaProbabilidad Es la nueva tabla de probabilidades del nodo.
     */
    public void setTablaProbabilidad(Matriz2 tablaProbabilidad) {
        this.tablaProbabilidad = tablaProbabilidad;
    }

    @Override
    public String toString() {
        return nombre;
    }
}