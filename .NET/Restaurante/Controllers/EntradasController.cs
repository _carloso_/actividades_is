using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Restaurante.Data;
using Restaurante.Models;

namespace Restaurante.Controllers
{
    public class EntradasController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IWebHostEnvironment _hostEnvironment;

        public EntradasController(ApplicationDbContext context, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            _hostEnvironment = hostEnvironment;
        }

        // GET: Entradas
        public async Task<IActionResult> Index()
        {
              return _context.Entradas != null ? 
                          View(await _context.Entradas.ToListAsync()) :
                          Problem("Entity set 'ApplicationDbContext.Entradas'  is null.");
        }

        // GET: Entradas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Entradas == null)
            {
                return NotFound();
            }

            var entradas = await _context.Entradas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (entradas == null)
            {
                return NotFound();
            }

            return View(entradas);
        }

        // GET: Entradas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Entradas/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,Descripcion,Precio,UrlImagen")] Entradas entradas)
        {
            if (ModelState.IsValid)
            {
                string rutaPrincipal = _hostEnvironment.WebRootPath;
                var archivos = HttpContext.Request.Form.Files;
                if(archivos.Count()>0){
                    string nombreArchivo = Guid.NewGuid().ToString();
                    var subidas = Path.Combine(rutaPrincipal,@"imagenes\entradas\");
                    var extension = Path.GetExtension(archivos[0].FileName);
                    using(var fileStream = new FileStream(Path.Combine(subidas,nombreArchivo + extension),FileMode.Create)){
                        archivos[0].CopyTo(fileStream);
                    }
                    entradas.UrlImagen = @"imagenes\entradas\" + nombreArchivo + extension;
                }
                _context.Add(entradas);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(entradas);
        }

        // GET: Entradas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Entradas == null)
            {
                return NotFound();
            }

            var entradas = await _context.Entradas.FindAsync(id);
            if (entradas == null)
            {
                return NotFound();
            }
            return View(entradas);
        }

        // POST: Entradas/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,Descripcion,Precio,UrlImagen")] Entradas entradas)
        {
            if (id != entradas.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string rutaPrincipal = _hostEnvironment.WebRootPath;
                    var archivos = HttpContext.Request.Form.Files; 
                    if (archivos.Count() > 0)
                    {
                        Entradas? entradasBD = await _context.Entradas.FindAsync(id);
                        if (entradasBD != null)
                        {
                            if (entradasBD.UrlImagen != null)
                            {
                                var rutaImgenActual = Path.Combine(rutaPrincipal, entradasBD.UrlImagen);
                                if (System.IO.File.Exists(rutaImgenActual))
                                {
                                    System.IO.File.Delete(rutaImgenActual);
                                }
                            }
                            _context.Entry(entradasBD).State = EntityState.Detached;
                        }  
                        string nombreArchivo = Guid.NewGuid().ToString();
                        var subidas = Path.Combine(rutaPrincipal, @"imagenes\entradas\");
                        var extension = Path.GetExtension(archivos[0].FileName);
                        using (var fileStream = new FileStream(Path.Combine(subidas, nombreArchivo + extension), FileMode.Create))
                        {
                            archivos[0].CopyTo(fileStream);
                        }
                        entradas.UrlImagen = @"imagenes\entradas\" + nombreArchivo + extension;
                        _context.Entry(entradas).State = EntityState.Modified;
                    }
                    _context.Update(entradas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EntradasExists(entradas.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(entradas);
        }

        // GET: Entradas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Entradas == null)
            {
                return NotFound();
            }

            var entradas = await _context.Entradas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (entradas == null)
            {
                return NotFound();
            }

            return View(entradas);
        }

        // POST: Entradas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Entradas == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Entradas'  is null.");
            }
            var entradas = await _context.Entradas.FindAsync(id);
            if (entradas != null)
            {
                _context.Entradas.Remove(entradas);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EntradasExists(int id)
        {
          return (_context.Entradas?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
