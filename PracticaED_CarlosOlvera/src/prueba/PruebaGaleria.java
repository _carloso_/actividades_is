package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import registros.galeriaDeArte.Actividades;
import registros.galeriaDeArte.ControlDeActividades;
import registros.galeriaDeArte.Pintor;

public class PruebaGaleria {
    public static void main(String[] args) {
        ControlDeActividades controlDeActividades = new ControlDeActividades(5,3);
        //AGREGAR LOS PINTORES Y SUS DATOS
        Pintor pintor1 = new Pintor(1,"Carlos Olvera", 19, "11/04/2002", "Masculino", "OEMC020411UR3", "Licenciatura en Artes Plasticas", "Zacatecas, Mex.");
        controlDeActividades.agregarPintor(pintor1);
        Pintor pintor2 = new Pintor(2,"Leonardo da Vinci", 36,"15/04/1452", "Masculino", "OEMC020411UR6", "Polimatía", "Florencia, Italia.");
        controlDeActividades.agregarPintor(pintor2);
        Pintor pintor3 = new Pintor(3,"Pedro Maromas", 69,"19/09/2002", "Masculino", "MPMC020411UR6", "Preparatoria", "Ecatepec, Mex.");
        controlDeActividades.agregarPintor(pintor3);
        //AGREGAR LAS ACTIVIDADES DE CADA PINTOR
        Actividades actividad1Pintor1 = new Actividades("viajar");
        controlDeActividades.agregarActividad(actividad1Pintor1);
        Actividades actividad1Pintor2 = new Actividades("descansar");
        controlDeActividades.agregarActividad(actividad1Pintor2);
        Actividades actividad1Pintor3 = new Actividades("exponer");
        controlDeActividades.agregarActividad(actividad1Pintor3);
        Actividades actividad2Pintor1 = new Actividades("autografos");
        controlDeActividades.agregarActividad(actividad2Pintor1);
        Actividades actividad3Pintor1 = new Actividades("viajar");
        controlDeActividades.agregarActividad(actividad3Pintor1);
        Actividades actividad2Pintor2 = new Actividades("descansar");
        controlDeActividades.agregarActividad(actividad2Pintor2);
        Actividades actividad3Pintor2 = new Actividades("descansar");
        controlDeActividades.agregarActividad(actividad3Pintor2);
        Actividades actividad2Pintor3 = new Actividades("pintar");
        controlDeActividades.agregarActividad(actividad2Pintor3);
        Actividades actividad3Pintor3 = new Actividades("pintar");
        controlDeActividades.agregarActividad(actividad3Pintor3);
        Actividades actividad4Pintor1 = new Actividades("pintar");
        controlDeActividades.agregarActividad(actividad4Pintor1);
        Actividades actividad5Pintor1 = new Actividades("pintar");
        controlDeActividades.agregarActividad(actividad5Pintor1);
        Actividades actividad6Pintor1 = new Actividades("viajar");
        controlDeActividades.agregarActividad(actividad6Pintor1);
        Actividades actividad7Pintor1 = new Actividades("exponer");
        controlDeActividades.agregarActividad(actividad7Pintor1);
        Actividades actividad4Pintor2 = new Actividades("exponer");
        controlDeActividades.agregarActividad(actividad4Pintor2);
        Actividades actividad5Pintor2 = new Actividades("exponer");
        controlDeActividades.agregarActividad(actividad5Pintor2);
        Actividades actividad4Pintor3 = new Actividades("pintar");
        controlDeActividades.agregarActividad(actividad4Pintor3);


        controlDeActividades.agregarActividad(actividad3Pintor3);
        //AGREGAR LOS AÑOS (SE AGREGAN SOLOS CON EL METODO)
        controlDeActividades.agregarAnio();
        //IMPRIME LAS LISTAS CON LOS DATOS AGREGADOS
        SalidaPorDefecto.consola("-----DATOS DE LA GALERIA-----\n");
        controlDeActividades.imprimirDatosCompetencias();

        SalidaPorDefecto.consola("-----INFORMACION DE LOS PINTORES-----\n");
        SalidaPorDefecto.consola(controlDeActividades.infoPintor(pintor1));
        SalidaPorDefecto.consola(controlDeActividades.infoPintor(pintor2));
        SalidaPorDefecto.consola(controlDeActividades.infoPintor(pintor3));
        //AGREGAR ACTIVIDADES A LA MATRIZ
        controlDeActividades.agregarHistorialActividades(actividad1Pintor1.toString(),1,2021);
        controlDeActividades.agregarHistorialActividades(actividad1Pintor2.toString(),2,2012);
        controlDeActividades.agregarHistorialActividades(actividad1Pintor3.toString(),3,2008);
        controlDeActividades.agregarHistorialActividades(actividad2Pintor1.toString(),1,2019);
        controlDeActividades.agregarHistorialActividades(actividad3Pintor1.toString(),1,2002);
        controlDeActividades.agregarHistorialActividades(actividad2Pintor2.toString(),2,2019);
        controlDeActividades.agregarHistorialActividades(actividad3Pintor2.toString(),2,2002);
        controlDeActividades.agregarHistorialActividades(actividad2Pintor3.toString(),3,2003);
        controlDeActividades.agregarHistorialActividades(actividad3Pintor3.toString(),3,2001);
        controlDeActividades.agregarHistorialActividades(actividad4Pintor1.toString(),1,2007);
        controlDeActividades.agregarHistorialActividades(actividad5Pintor1.toString(),1,2011);
        controlDeActividades.agregarHistorialActividades(actividad6Pintor1.toString(),1,2005);
        controlDeActividades.agregarHistorialActividades(actividad7Pintor1.toString(),1,2008);
        controlDeActividades.agregarHistorialActividades(actividad4Pintor2.toString(),2,2009);
        controlDeActividades.agregarHistorialActividades(actividad5Pintor2.toString(),2,2008);
        controlDeActividades.agregarHistorialActividades(actividad4Pintor3.toString(),3,2010);

        //IMPRIMIR MATRIZ CON LAS ACTIVIDADES
        SalidaPorDefecto.consola("-----HISTORIAL DE ACTIVIDADES-----\n");
        controlDeActividades.imprimirHistorial();
        //IMPRIMIR LAS ACTIVIDADES MAS POPULARES
        SalidaPorDefecto.consola("-----ACTIVIDAD QUE MAS SE REALIZO POR LOS PINTORES-----\n");
        SalidaPorDefecto.consola(controlDeActividades.actividadPopular()+"\n");

        SalidaPorDefecto.consola("-----ACTIVIDAD QUE MAS SE REALIZO POR UN PINTOR EN ESPECIFICO-----\n");
        SalidaPorDefecto.consola("pintor 1: "+controlDeActividades.actividadPopularPintor(pintor1)+"\n");
        SalidaPorDefecto.consola("pintor 2: "+controlDeActividades.actividadPopularPintor(pintor2)+"\n");
        SalidaPorDefecto.consola("pintor 3: "+controlDeActividades.actividadPopularPintor(pintor3)+"\n");

        SalidaPorDefecto.consola("-----PINTOR QUE DESCANSO MAS-----\n");
        int contadorPintor1 = controlDeActividades.contadorDescanso(pintor1);
        int contadorPintor2 = controlDeActividades.contadorDescanso(pintor2);
        int contadorPintor3 = controlDeActividades.contadorDescanso(pintor3);
        if (contadorPintor1>contadorPintor2 && contadorPintor1> contadorPintor3){
            SalidaPorDefecto.consola("Al pintor que mas le gusta descansar es al pintor1\n");
        }
        else if (contadorPintor2>contadorPintor1 && contadorPintor2> contadorPintor3){
            SalidaPorDefecto.consola("Al pintor que mas le gusta descansar es al pintor2\n");
        }
        else if (contadorPintor3>contadorPintor1 && contadorPintor3> contadorPintor2){
            SalidaPorDefecto.consola("Al pintor que mas le gusta descansar es al pintor3\n");
        }

        SalidaPorDefecto.consola("-----INFO DEL PINTOR EN UNA ACTIVIDAD INDICADA-----\n");
        SalidaPorDefecto.consola("Actividad y fecha: viajar en 2021. " +controlDeActividades.infoActividad(pintor1,"viajar", 2021)+"\n");
        SalidaPorDefecto.consola("Actividad y fecha: pintar en 2001. " +controlDeActividades.infoActividad(pintor3,"pintar", 2001)+"\n");

        SalidaPorDefecto.consola("-----INFO DEL EVENTO DE EXPOSICION DE UN PINTOR-----\n");
        SalidaPorDefecto.consola("pintor 3: " +controlDeActividades.fechaXevento(pintor3,actividad1Pintor3)+"\n");
        SalidaPorDefecto.consola("pintor 2: " +controlDeActividades.fechaXevento(pintor2,actividad5Pintor2)+"\n");
        SalidaPorDefecto.consola("pintor 1: " +controlDeActividades.fechaXevento(pintor1,actividad7Pintor1)+"\n");

        SalidaPorDefecto.consola("-----AÑO CON MAS EXPOSICIONES-----\n");
        SalidaPorDefecto.consola(""+controlDeActividades.contadorExposiciones());
    }
}
