package pruebas;

import registros.procesosistemaoperativo.ControlProcesos;
import registros.procesosistemaoperativo.Proceso;
import registros.procesosistemaoperativo.TipoProceso;

public class PruebaProcesoOS {
    public static void main(String[] args) {
        ControlProcesos control = new ControlProcesos(5);

        // Crear procesos
        Proceso pro1 = new Proceso("Imprimir imagen de futbol", TipoProceso.IMPRESION);
        Proceso pro2 = new Proceso("Ejecutar visual studio code", TipoProceso.EJECUCION);
        Proceso pro3 = new Proceso("Descargr pelucula", TipoProceso.DESCARGA);
        Proceso pro4 = new Proceso("Descargar video", TipoProceso.DESCARGA);
        Proceso pro5 = new Proceso("Ejecutar photoshop", TipoProceso.EJECUCION);

        // Agregar procesos
        control.agregarProceso(pro1);
        control.agregarProceso(pro2);
        control.agregarProceso(pro3);
        control.agregarProceso(pro4);
        control.agregarProceso(pro5);

        // realizar procesos
        control.realizarProcesos();
    }
}
