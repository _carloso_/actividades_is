﻿using System;

namespace Ejercicio
{
	public class Concesionaria
	{
		private Object[] automoviles;
		private int LIMITEAUTOS;
		private int numeroActualAutomoviles;

		public Concesionaria(int limiteAutomoviles)
		{
			LIMITEAUTOS = limiteAutomoviles;
			automoviles = new Object[LIMITEAUTOS];
		}
		public Boolean llena()
		{
			if (numeroActualAutomoviles == (LIMITEAUTOS - 1))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public int Agregar(Object valor)
		{
			if (llena() == false)
			{ //hay espacio
				numeroActualAutomoviles = numeroActualAutomoviles + 1;
				automoviles[numeroActualAutomoviles] = valor;
				return numeroActualAutomoviles;
			}
			else
			{ //no hay espacio
				return -1;
			}
		}
		public Object buscarAuto(Object valor)
		{
			int indice = Array.IndexOf(automoviles, valor); //buscar el valor en la lista de automoviles
			if (indice == -1) //no lo encontro
			{
				Console.WriteLine($"El automovil {valor} no existe");
				return null;
			}
            else
            {
				return valor + "\n" + "----------"; //lo encontro
			}
		}

		public void mostrarAutos()
		{
			for (int posicion = numeroActualAutomoviles; posicion >= 0; posicion--)
			{
				Console.WriteLine($"-{automoviles[posicion]}");
			}
			Console.WriteLine($"----------");
		}


		public Object eliminar(Object valor)
		{
			int posicion = Array.IndexOf(automoviles, valor);
			if (posicion != null)
			{ //si lo encontramos
				Object valorEliminado = automoviles[posicion];
				numeroActualAutomoviles--;
				for (int movimiento = posicion; movimiento <= numeroActualAutomoviles; movimiento++)
				{
					automoviles[movimiento] = automoviles[movimiento + 1];
				}
				return valorEliminado;
			}
			else
			{  //no lo encontramos
				return null; 
			}
		}

		public void vaciar()
		{
			for (int posicion = 0; posicion <= LIMITEAUTOS - 1; posicion++)
			{ //se recorre toda la lista de datos
				automoviles[posicion] = null; //todos los valores se sustituyen por null (nada)
			}
		}

	}
}

