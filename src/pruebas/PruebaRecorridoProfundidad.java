package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import estructurasnolineales.GrafoEstatico;

public class PruebaRecorridoProfundidad {
    public static void main(String[] args) {
        GrafoEstatico grafo = new GrafoEstatico(6);

        grafo.agregarVertice("A");
        grafo.agregarVertice("B");
        grafo.agregarVertice("C");
        grafo.agregarVertice("D");
        grafo.agregarVertice("E");
        grafo.agregarVertice("F");

        grafo.agregarArista("A","C");
        grafo.agregarArista("A","E");
        grafo.agregarArista("B","D");
        grafo.agregarArista("C","E");
        grafo.agregarArista("D","E");
        grafo.agregarArista("D","F");
        grafo.agregarArista("E","B");
        grafo.agregarArista("E","F");
        grafo.agregarArista("F","C");

        grafo.imprimir();


        ListaDinamica recorridoProfundidad=grafo.recorridoProfunidad("A");

        SalidaPorDefecto.consola("\n");
        recorridoProfundidad.imprimir();

        SalidaPorDefecto.consola("\n\nComponentes conexas del grafo: " + "\n");
        grafo.mostrarComponentesConexasNoDirigido("A");

    }
}
