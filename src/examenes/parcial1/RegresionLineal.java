package examenes.parcial1;

import entradaSalida.ArchivoTexto;
import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;

/**
 * Esta clase implementa el algoritmo de regresión lineal.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class RegresionLineal {

    /**
     * Este atributo almacena el conjunto de valores de x.
     */
    private ListaEstatica x;
    /**
     * Este atributo almacena el conjunto de valores de y.
     */
    private ListaEstatica y;

    public RegresionLineal(String rutaDeX, String rutaDeY) {
        x = ArchivoTexto.leer(rutaDeX);
        y = ArchivoTexto.leer(rutaDeY);
    }

    /**
     * Este método estima el precio de una casa con un determinado número de habitaciones.
     */
    public void precioEstimado(){
        SalidaPorDefecto.consola("-> Ingrese el número de habitaciones: ");
        double valorX = EntradaPorDefecto.consolaDouble();
        if (valorX > 0) {
            double precioEstimado = calcularOrdenada() + calcularPendiente() * valorX;
            SalidaPorDefecto.consola("--> El precio estimado es: " + precioEstimado);
        } else {
            SalidaPorDefecto.consola("--> El valor ingresado no es válido.");
        }
    }

    /**
     * Este método imprime la ecuación de la regresión lineal.
     */
    public void mostrarEcuacionDeRegresion(){
        SalidaPorDefecto.consola("y = " + calcularOrdenada() + " + " + calcularPendiente() + " (x)\n");
    }

    /**
     * Este método calcula la pendiente de la regresión lineal.
     * @return Regresa el valor de la pendiente de la regresión lineal.
     */
    public double calcularPendiente(){
        return sumatoriaXiMenosPromedioXPorYiMenosPromedioY() / sumatoriaCuadradaDeXiMenosPromedioX();
    }

    /**
     * Este método calcula el valor de la ordenada de la regresión lineal.
     * @return Regresa el valor de la ordenada de la regresión lineal.
     */
    public double calcularOrdenada(){
        // p = promedio(y) - (pendiente * promedio(x))
        return promedioDeY() - calcularPendiente() * promedioDeX();
    }

    /**
     * Este método calcula el promedio del conjunto de valores de x.
     * @return Regresa el promedio del conjunto de valores de x.
     */
    public double promedioDeX(){
        double suma = 0;
        for (int pos = 0; pos < x.numeroElementos(); pos++) {
            double valorX = Double.parseDouble(x.obtener(pos).toString());
            suma += valorX;
        }
        return suma / x.numeroElementos();
    }

    /**
     * Este método calcula el promedio del conjunto de valores de y.
     * @return Regresa el promedio del conjunto de valores de y.
     */
    public double promedioDeY(){
        double suma = 0;
        for (int pos = 0; pos < y.numeroElementos(); pos++) {
            double valorY = Double.parseDouble(y.obtener(pos).toString());
            suma += valorY;
        }
        return suma / y.numeroElementos();
    }

    /**
     * Este método calcula la sumatoria de (xi- promedio(x)) * (yi- promedio(y)).
     * @return Regresa la sumatoria de (xi- promedio(x)) * (yi- promedio(y)).
     */
    public double sumatoriaXiMenosPromedioXPorYiMenosPromedioY(){
        double sumatoria = 0;
        double promedioX = promedioDeX();
        double promedioY = promedioDeY();
        for (int pos = 0; pos < x.numeroElementos(); pos++) {
            double valorX = Double.parseDouble(x.obtener(pos).toString());
            double valorY = Double.parseDouble(y.obtener(pos).toString());
            sumatoria += (valorX - promedioX) * (valorY - promedioY);
        }
        return sumatoria;
    }

    /**
     * Este método calcula la sumatoria de (xi- promedio(x))^2.
     * @return Regresa la sumatoria de (xi- promedio(x))^2.
     */
    public double sumatoriaCuadradaDeXiMenosPromedioX(){
        double sumatoria = 0;
        double promedioX = promedioDeX();
        for (int pos = 0; pos < x.numeroElementos(); pos++) {
            double valorX = Double.parseDouble(x.obtener(pos).toString());
            sumatoria += Math.pow(valorX - promedioX, 2);
        }
        return sumatoria;
    }
}