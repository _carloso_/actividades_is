package pruebas;

import entradaSalida.SalidaPorDefecto;
import registros.ondasDeAudio.AudioFileRecord;

public class PruebaAudio {
    public static void main(String[] args) {
        AudioFileRecord audio = new AudioFileRecord("C:\\Users\\Cristian\\Desktop\\audio.wav");
        audio.leerAudio();
        SalidaPorDefecto.consola("Energia del audio: " + audio.obtenerEnergia() + "\n");
        SalidaPorDefecto.consola("---- Aplicando filtro ----" + "\n");
        audio.preEnfasis();
        SalidaPorDefecto.consola("---- Bajando el volumen ----" + "\n");
        audio.bajarVolumen(3);
        SalidaPorDefecto.consola("Energia del audio: " + audio.obtenerEnergia());
        audio.EscribirAudio();
    }
}
