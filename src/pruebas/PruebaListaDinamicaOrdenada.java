package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamicaOrdenada;
import estructurasLineales.ListaEstatica;
import estructurasnolineales.Matriz2;
import herramientas.comunes.TipoOrden;
import herramientas.comunes.TipoTabla;

public class PruebaListaDinamicaOrdenada {
    public static void main(String[] args) {
        ListaDinamicaOrdenada lista1 = new ListaDinamicaOrdenada(TipoOrden.ASC);
        lista1.agregar("C");
        lista1.agregar("F");
        lista1.agregar("J");
        lista1.agregar("D");

        SalidaPorDefecto.consola("--- Lista Dinámica 1: Ordena de Forma Ascendente ---" + "\n");
        lista1.imprimir();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola( "---- Lista Dinámica 1: Impresa de forma inversa ---" + "\n");
        lista1.imprimirOI();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("*Buscar el valor 'C' en la lista 1: " + lista1.buscar("C"));

        // esta lista se va agregar a la lista dinámica 1
        ListaEstatica listaEstatica1 = new ListaEstatica(5);
        listaEstatica1.agregar("Z");
        listaEstatica1.agregar("B");
        listaEstatica1.agregar("X");
        listaEstatica1.agregar("A");

        SalidaPorDefecto.consola("\n\n--- Lista Estatica ---" + "\n");
        listaEstatica1.imprimir();


        lista1.agregarLista(listaEstatica1);
        SalidaPorDefecto.consola("\n--- Lista Dinamica 1 Con Los Valores De La Lista Estatica ---" + "\n");
        lista1.imprimir();


        ListaDinamicaOrdenada lista2 = new ListaDinamicaOrdenada(TipoOrden.DESC);
        lista2.agregar("Y");
        lista2.agregar("A");
        lista2.agregar("A");
        lista2.agregar("A");
        lista2.agregar("S");
        lista2.agregar("L");

        SalidaPorDefecto.consola("\n\n");
        SalidaPorDefecto.consola("--- Lista Dinámica '2' Ordena de Forma Descendente ---" + "\n");
        lista2.imprimir();


        SalidaPorDefecto.consola("\n\n*Cambiar el valor 'A' por 'Z', 2 Veces: " +
                lista2.cambiar("A", "Z", 2) + "\n");
        lista2.imprimir();
        SalidaPorDefecto.consola("\n");

        Matriz2 tabla = new Matriz2(3, 3);
        tabla.cambiar(0, 0, "R");
        tabla.cambiar(0, 1, "M");
        tabla.cambiar(0, 2, "A");
        tabla.cambiar(1, 0, "P");
        tabla.cambiar(1, 1, "B");
        tabla.cambiar(1, 2, "G");
        tabla.cambiar(2, 0, "C");
        tabla.cambiar(2, 1, "N");
        tabla.cambiar(2, 2, "H");

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Matriz 2D ----" + "\n");
        tabla.imprimirXReglones();

        ListaDinamicaOrdenada lista3 = new ListaDinamicaOrdenada(TipoOrden.ASC);
        lista3.agregar("Z");
        lista3.agregar("Q");

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 3 ---" + "\n");
        lista3.imprimir();

        lista3.agregarMatriz2D(tabla, TipoTabla.RENGLON);

        SalidaPorDefecto.consola("\n\n");
        SalidaPorDefecto.consola("--- Lista dinamica 3 Con los Valores de la matriz 2D ---" + "\n");
        lista3.imprimir();

        SalidaPorDefecto.consola("\n\n");
        SalidaPorDefecto.consola("*Eliminar el ultimo elemnto de la lista: " + lista3.eliminar() + "\n");
        SalidaPorDefecto.consola("*Eliminar el primer elemnto de la lista: " + lista3.eliminarInicio() + "\n");

        SalidaPorDefecto.consola("\n");
        lista3.imprimir();
    }
}
