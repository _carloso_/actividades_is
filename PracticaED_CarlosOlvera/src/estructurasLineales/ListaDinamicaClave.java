package estructurasLineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.auxiliares.Nodo;
import estructurasLineales.auxiliares.NodoClave;
import estructurasNoLineales.Matriz2;

public class ListaDinamicaClave{
    protected NodoClave primero;
    protected NodoClave ultimo;

    public ListaDinamicaClave(){
        primero=null;
        ultimo=null;
    }

    public boolean vacia(){
        if(primero==null){
            return true;
        }else{
            return false;
        }
    }
    public void vaciar(){
        primero=null;
        ultimo=null;
    }
    public boolean claveRepetida(Object clave){
        NodoClave nodoBuscado = primero;
        while (nodoBuscado != null){ //buscamos mientras haya donde buscar
            if (nodoBuscado.getClave().toString().equalsIgnoreCase(clave.toString())){ //si es el mismo valor que el parametro
                return true; //si habia una llave repetida
            }
            nodoBuscado = nodoBuscado.getNodoDer();//avanza hacia el siguiente nodo
        }
        return false;
    }

    public boolean cambiar(Object clave, Object valor){
        NodoClave nodoBuscado = primero;
        while (nodoBuscado != null){ //buscamos mientras haya donde buscar o mientras no lo encontremos
            if (nodoBuscado.getClave().toString().equalsIgnoreCase(clave.toString())){ //no se encontro el valor
                nodoBuscado.setContenido(valor);
                return true; //si habia una llave repetida
            }
            nodoBuscado = nodoBuscado.getNodoDer();//avanza hacia el siguiente nodo
        }
        return false;
    }

    public boolean cambiarClave(Object clave, Object valor){
        NodoClave nodoBuscado = primero;
        while (nodoBuscado != null){ //buscamos mientras haya donde buscar o mientras no lo encontremos
            if (nodoBuscado.getClave().toString().equalsIgnoreCase(clave.toString())){ //no se encontro el valor
                nodoBuscado.setClave(valor);
                return true; //si habia una llave repetida
            }
            nodoBuscado = nodoBuscado.getNodoDer();//avanza hacia el siguiente nodo
        }
        return false;
    }

    public boolean cambiarValor(Object valorViejo, Object valorNuevo){
        NodoClave nodoBuscado = primero;
        while (nodoBuscado != null){ //buscamos mientras haya donde buscar o mientras no lo encontremos
            if (nodoBuscado.getContenido().toString().equalsIgnoreCase(valorViejo.toString())){ //no se encontro el valor
                nodoBuscado.setContenido(valorNuevo);
                return true; //si habia una llave repetida
            }
            nodoBuscado = nodoBuscado.getNodoDer();//avanza hacia el siguiente nodo
        }
        return false;
    }

    public boolean agregar(Object clave, Object valor){
        NodoClave nuevoNodo=new NodoClave(clave, valor); //paso 1
        if(nuevoNodo!=null){ //hay espacio
            if (vacia() == false){
                if (claveRepetida(clave) == false){ //no habia llave repetida
                    ultimo.setNodoDer(nuevoNodo);
                    ultimo = nuevoNodo;
                    return true;
                }
                else { //si habia llave repetida
                    boolean valorReemplazado = cambiar(clave,valor);
                    if (valorReemplazado == true){
                        return true;
                    }
                    else{
                        return false;
                    }
                }
            }
            else { //esta vacia
                primero = nuevoNodo;
                ultimo = nuevoNodo;
                return true;
            }
        }else{ //no hay espacio
            return false;
        }
    }

    public Object eliminarContenido(Object valor) {
        if (vacia() == false) { // hay algo
            NodoClave nodoAnterior = primero;
            NodoClave nodoBuscado = primero;
            while (nodoBuscado != null && !valor.toString().equalsIgnoreCase((String) nodoBuscado.getContenido())) { // buscamos mientras haya nodos y no lo encuentre
                nodoAnterior = nodoBuscado;
                nodoBuscado = nodoBuscado.getNodoDer(); // avanzo al siguiente nodo
            }
            if (nodoBuscado == null) { // no se encontro el valor b)
                return null;
            } else { // si se encontro
                if (primero == ultimo) { // c)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    primero = null;
                    ultimo = null;
                    return contenidoEliminado;
                } else if (nodoBuscado == primero) { // d)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    primero = primero.getNodoDer();
                    return contenidoEliminado;
                } else if (nodoBuscado == ultimo) { // e)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    ultimo = nodoAnterior;
                    ultimo.setNodoDer(null);
                    return contenidoEliminado;
                } else { // f)
                    Object contenidoEliminado = nodoBuscado.getContenido();
                    nodoAnterior.setNodoDer(nodoBuscado.getNodoDer());
                    return contenidoEliminado;
                }
            }
        } else { // no hay nada a)
            return null;
        }
    }

    public Object eliminar(Object clave) {
        if (vacia() == false) { // hay algo
            NodoClave nodoAnterior = primero;
            NodoClave nodoBuscado = primero;
            while (nodoBuscado != null && !clave.toString().equalsIgnoreCase((String) nodoBuscado.getClave())) { // buscamos mientras haya nodos y no lo encuentre
                nodoAnterior = nodoBuscado;
                nodoBuscado = nodoBuscado.getNodoDer(); // avanzo al siguiente nodo
            }
            if (nodoBuscado == null) { // no se encontro el valor b)
                return null;
            } else { // si se encontro
                if (primero == ultimo) { // c)
                    Object contenidoEliminado = nodoBuscado.getClave();
                    primero = null;
                    ultimo = null;
                    return contenidoEliminado;
                } else if (nodoBuscado == primero) { // d)
                    Object contenidoEliminado = nodoBuscado.getClave();
                    primero = primero.getNodoDer();
                    return contenidoEliminado;
                } else if (nodoBuscado == ultimo) { // e)
                    Object contenidoEliminado = nodoBuscado.getClave();
                    ultimo = nodoAnterior;
                    ultimo.setNodoDer(null);
                    return contenidoEliminado;
                } else { // f)
                    Object contenidoEliminado = nodoBuscado.getClave();
                    nodoAnterior.setNodoDer(nodoBuscado.getNodoDer());
                    return contenidoEliminado;
                }
            }
        } else { // no hay nada a)
            return null;
        }
    }

    public void imprimir(){
        NodoClave iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            Object clave=iterador.getClave(); //paso 1
            SalidaPorDefecto.consola("{" + clave + ","+ contenido + "}"+ " -> ");//paso 2
            iterador=iterador.getNodoDer();//paso 3
        }
        SalidaPorDefecto.consola("null");
    }

    public void imprimirClaves(){
        NodoClave iterador=primero; //paso inicial
        while(iterador!=null){
            Object clave=iterador.getClave(); //paso 1
            SalidaPorDefecto.consola(clave +" -> ");//paso 2
            iterador=iterador.getNodoDer();//paso 3
        }
        SalidaPorDefecto.consola("null");
    }

    public void imprimirContenido(){
        NodoClave iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            SalidaPorDefecto.consola(contenido +" -> ");//paso 2
            iterador=iterador.getNodoDer();//paso 3
        }
        SalidaPorDefecto.consola("null");
    }

    public ListaEstatica aListaEstaticas(){
        ListaEstatica listaContenido = new ListaEstatica(contarCantidadNodos()*2);
        ListaEstatica listaClaves = new ListaEstatica(contarCantidadNodos());
        NodoClave iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            Object clave = iterador.getClave();
            listaContenido.agregar(contenido);
            listaClaves.agregar(clave);
            iterador=iterador.getNodoDer();//paso 3
        }
        for(int cont = 0; cont < listaClaves.numeroElementos(); cont++){
            listaContenido.agregar(listaClaves.obtener(cont));
        }
        return listaContenido;
    }

    public ListaDinamica aListaDinamicas(){
        ListaDinamica listaContenido = new ListaDinamica();
        ListaDinamica listaClaves = new ListaDinamica();
        NodoClave iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            Object clave = iterador.getClave();
            listaContenido.agregar(contenido);
            listaClaves.agregar(clave);
            iterador=iterador.getNodoDer();//paso 3
        }
        for(int cont = 0; cont < listaClaves.contarCantidadNodos(); cont++){
            listaContenido.agregar(listaClaves.obtener(cont));
        }
        return listaContenido;
    }

    public Matriz2 aMatriz2D(){
        ListaEstatica listaContenido = new ListaEstatica(contarCantidadNodos());
        ListaEstatica listaClaves = new ListaEstatica(contarCantidadNodos());
        Matriz2 matriz2D = new Matriz2(contarCantidadNodos(), 2,null);
        NodoClave iterador=primero; //paso inicial
        while(iterador!=null){
            Object contenido=iterador.getContenido(); //paso 1
            Object clave = iterador.getClave();
            listaContenido.agregar(contenido);
            listaClaves.agregar(clave);
            iterador=iterador.getNodoDer();//paso 3
        }
        matriz2D.agregarColumna(listaClaves,0);
        matriz2D.agregarColumna(listaContenido,1);
        return matriz2D;
    }

    public int contarCantidadNodos(){
        NodoClave iterador = primero;
        int cantidad = 0;
        while (iterador != null){
            cantidad++;
            iterador = iterador.getNodoDer(); //se recorre el nodo al siguiente
        }
        return cantidad;
    }

    public Object obtener(Object clave){
        NodoClave nodoBuscado = primero;
        while (nodoBuscado != null){ //buscamos mientras haya donde buscar o mientras no lo encontremos
            if (nodoBuscado.getClave().toString().equalsIgnoreCase(clave.toString())){ //no se encontro el valor
                 Object contenido = nodoBuscado.getContenido();
                return contenido; //regresa el contenido de la llave
            }
            nodoBuscado = nodoBuscado.getNodoDer();//avanza hacia el siguiente nodo
        }
        return null;//no encontro la clave
    }

    public boolean agregarLista(ListaDinamicaClave lista2) {
        boolean agregado = false;
        NodoClave iterador = lista2.primero; //iterador de la lista2
        while (iterador != null) { //buscamos mientras haya donde buscar o mientras no lo encontremos
            agregar(iterador.getClave(), iterador.getContenido());
            agregado = true;
            iterador = iterador.getNodoDer();//avanza hacia el siguiente nodo
        }
        return agregado;
    }

    public boolean agregarListasEstaticas(ListaEstatica arregloClaves, ListaEstatica arregloValores){
        if (arregloClaves.numeroElementos() == arregloValores.numeroElementos()){
            for (int cont =0; cont < arregloClaves.numeroElementos();cont++){
                agregar(arregloClaves.obtener(cont),arregloValores.obtener(cont));
            }
            return true;
        }
        else{
            return false;
        }
    }

    public boolean agregarListasDinamicas(ListaDinamica claves, ListaDinamica valores){
        if (claves.contarCantidadNodos() == valores.contarCantidadNodos()){
            Nodo iteradorClave = claves.primero; //iterador de la lista claves
            Nodo iteradorValores = valores.primero; //iterador de la lista valores
            while (iteradorClave != null || iteradorValores != null) { //buscamos mientras haya donde buscar o mientras no lo encontremos
                agregar(iteradorClave.getContenido(), iteradorValores.getContenido());
                iteradorClave = iteradorClave.getNodoDer();//avanza hacia el siguiente nodo
                iteradorValores = iteradorValores.getNodoDer();//avanza hacia el siguiente nodo
            }
            return true;
        }
        else{
            return false;
        }
    }

    public boolean agregarMatriz2(Matriz2 matriz){
        if (matriz.obtenerColumnas() == 2){
            for (int fila = 0; fila < matriz.obtenerRenglones(); fila++){
                    agregar(matriz.obtener(fila,0),matriz.obtener(fila,1));
            }
            return true;
        }
        else{
            return false;
        }
    }
}
