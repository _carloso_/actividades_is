using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restaurante.Models
{
    public class Entradas
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Nombre del Platillo")]
        public string Nombre { get; set; }
        [Required]
        [Display(Name = "Descripción del Platillo")]
        public string Descripcion { get; set; }
        public double? Precio { get; set; }
        [Display(Name = "Imagen del Platillo")]
        public string? UrlImagen { get; set; }
    }
}