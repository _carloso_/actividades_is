package estructurasNoLineales;
import entradaSalida.SalidaPorDefecto;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * clase que modifica una imagen
 */
public class Imagen {
    protected BufferedImage imagen; //almacena la imagen original
    protected BufferedImage imagen2; //almacena la imagen modificada
    protected int w; //almacena el alto de la imagen
    protected int h; //almacena el ancho de la imagen
    protected Matriz2 matriz; //matriz donde se almacenaran los bits

    /**
     * constructor que crea el TDA imagen
     * @param ruta es una cadena de la ruta de la imagen
     */
    public Imagen(String ruta) {
        try {
            imagen = ImageIO.read(new File(ruta)); //Yoda Cholo.jpg
            w = imagen.getWidth();
            h = imagen.getHeight();
            imagen2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
            extraerPixeles();
        } catch (IOException e) {
            SalidaPorDefecto.consola("No se encontro la imagen en la ruta indicada." + e);
        }
    }
    /**
     * Obtiene los pixeles de la imagen y los inserta en la matriz
     */
    public void extraerPixeles() {
        matriz = new Matriz2(w, h);
        for (int fila = 0; fila < w; fila++) { //renglonXrenglon
            for (int col = 0; col < h; col++) { //columnaXcolumna
                int pixel = imagen.getRGB(fila, col);
                matriz.cambiar(fila, col, pixel);
            }
        }
    }
    /**
     * separa los canales de cada color de la imagen
     * @return regresa una matriz que contiene los canales separados
     */
    public Matriz2 separarColores() {
        Matriz2 matrizColores = new Matriz2(w, h);
        for (int fila = 0; fila < w; fila++) { //renglonXrenglon
            for (int col = 0; col < h; col++) { //columnaXcolumna
                int pixel = (int) matriz.obtener(fila, col);
                int blue = pixel & 0xff;
                int green = (pixel >> 8) & 0xff;
                int red = (pixel >> 16) & 0xff;
                int alpha = (pixel >> 24) & 0xff;
                Pixel nuevoPixel = new Pixel(blue, green, red, alpha);
                matrizColores.cambiar(fila, col, nuevoPixel);
            }
        }
        return matrizColores;
    }
    /**
     * Promedia los colores de la imagen para sobreescribirlos en imagen2, asi volviendlols grises
     */
    public void imagenGris() {
        Matriz2 matrizColores = separarColores();
        for (int fila = 0; fila < w; fila++) { //renglonXrenglon
            for (int col = 0; col < h; col++) { //columnaXcolumna
                Pixel color = (Pixel) matrizColores.obtener(fila, col);
                int alpha = color.getAlpha();
                int red = color.getRed();
                int green = color.getGreen();
                int blue = color.getBlue();
                int promedio = (red + green + blue) / 3;
                int pixelTuneado = (alpha << 24) | (promedio << 16) | (promedio << 8) | (promedio);
                imagen2.setRGB(fila, col, pixelTuneado); //se le modifican los colores a la imagen2 con los datos de la imagen1
            }
        }
        try {
            ImageIO.write(imagen2, "JPG", new File("src/Yoda Gangsta.jpg")); //se crea la imagen (formato y nombre)
        } catch (IOException e) {
            SalidaPorDefecto.consola("Imagen en escala de grises no exitosa" + e);
        }
    }
    /**
     * Asigna los valores de la imagen en un sentido inverso para que se vuelva horizontal
     */
    public void vueltaHorizontal() {
        imagen2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        int fila2 = w - 1;
        for (int fila = 0; fila < w; fila++) { //renglonXrenglon
            for (int col = 0; col < h; col++) { //columnaXcolumna
                int pixel = imagen.getRGB(fila, col);
                imagen2.setRGB(fila2 - fila, col, pixel);
            }
        }
        try {
            ImageIO.write(imagen2, "JPG", new File("src/Yoda Cholo Horizontal.jpg")); //se crea la imagen (formato y nombre)
        } catch (IOException e) {
            SalidaPorDefecto.consola("Imagen Horizontal no exitosa" + e);
        }
    }

    /**
     * Asigna los valores de la imagen en un sentido inverso para que se vuelva vertical
     */
    public void vueltaVertical() {
        imagen2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        int col2 = h - 1;
        for (int fila = 0; fila < w; fila++) { //renglonXrenglon
            for (int col = 0; col < h; col++) { //columnaXcolumna
                int pixel = imagen.getRGB(fila, col);
                imagen2.setRGB(fila, col2 - col, pixel);
            }
        }
        try {
            ImageIO.write(imagen2, "JPG", new File("src/Yoda Cholo Vertical.jpg")); //se crea la imagen (formato y nombre)
        } catch (IOException e) {
            SalidaPorDefecto.consola("Imagen Vertical no exitosa" + e);
        }
    }

    /**
     * este metodo modifica el brillo de la imagen.
     * @param intensidad es la variable que modifica el brillo de la imagen
     */
    public void modificarBrillo(int intensidad) {
        Matriz2 matrizColores = separarColores();
        for (int fila = 0; fila < w; fila++) { //renglonXrenglon
            for (int col = 0; col < h; col++) { //columnaXcolumna
                Pixel color = (Pixel) matrizColores.obtener(fila, col);
                int alpha = color.getAlpha() + intensidad;
                int red = color.getRed() + intensidad;
                int green = color.getGreen() + intensidad;
                int blue = color.getBlue() + intensidad;
                int promedio = (red + green + blue) / 3;
                if (promedio > 255) {
                    promedio = 255; //para que no se pase de los limites, le asignamos el valor maximo
                }
                int pixelTuneado = (alpha << 24) | (promedio << 16) | (promedio << 8) | (promedio);
                imagen2.setRGB(fila, col, pixelTuneado); //se le modifican los colores a la imagen2 con los datos de la imagen1
            }
        }
        try {
            ImageIO.write(imagen2, "JPG", new File("src/Yoda Brilloso.jpg")); //se crea la imagen (formato y nombre)
        } catch (IOException e) {
            SalidaPorDefecto.consola("Subir brillo a imagen no exitosa" + e);
        }
    }
}







