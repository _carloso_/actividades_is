package estructurasLineales;

public interface VectorLista extends ListaAlmacenamiento{
    public boolean llena();

    public int maximo();

    public Object obtener(int indice);

    /**
     * modifica el elemento de la posición indicada por indice. valor es el nuevo elemento a colocar.
     * @return Regresa <b>true</b> si se pudo, <b>false</b> en caso contrario.
     */
    public boolean cambiar(int indice, Object valor);


    /**
     * modifica el elemento del arreglo actual en las posiciones de indicesBusqueda por el contenido de valoresNuevos.
     * @return Regresa <b>true</b> si se pudo, <b>false</b> debido a las dimensiones o índices inválidos, por ejemplo..
     */
    public boolean cambiarListaEstatica(ListaEstatica indicesBusqueda, ListaEstatica valoresNuevos);

    /**
     * elimina un elemento del arreglo en una posición específica.
     * @return regresa el elemento eliminado.
     */
    public Object eliminar(int indice);

    /**
     * redimensiona el tamaño del arreglo al nuevo tamaño indicado por máximo.
     * @return Si el tamaño es menor, los elementos sobrantes deben ser eliminados. Si el tamaño es mayor,
     * los datos anteriores deben conservarse y los nuevos espacios deben estar vacíos.
     */
    public Object redimensionar(int maximo);

    public int numeroElementos();

    /**
     * reacomoda la lista de forma INC.
     */

}
