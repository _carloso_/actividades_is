package estructurasLineales;

import entradaSalida.SalidaPorDefecto;

/**
 * Esta clase representa a una lista de almacenamiento estatica.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class ListaEstatica implements VectorLista {

    protected int MAXIMO;
    protected int ultimo;
    protected Object[] datos;

    public ListaEstatica(int maximo) {
        MAXIMO = maximo;
        datos = new Object[MAXIMO];
        ultimo = -1;
    }

    @Override
    public boolean vacia() {
        if(ultimo == -1){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean llena() {
        if (ultimo == MAXIMO - 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int maximo() {
        return MAXIMO;
    }

    @Override
    public int numeroElementos() {
        return ultimo + 1;
    }

    /**
     * Este método permite saber si un índice está dentro de los límites de la lista.
     * @param indice Es el índice a evaluar.
     * @return Regresa <b>true</b> si el índice está dentro de los límites de la lista y <b>false</b> en caso contrario.
     */
    protected boolean enRango(int indice){
        if(indice >=0 && indice <= ultimo){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public Object obtener(int indice){
        if(enRango(indice)){
            return datos[indice];
        }else{
            return null;
        }
    }

    @Override
    public boolean cambiar(int indice, Object valor) {
        if (enRango(indice)) {
            datos[indice] = valor;
            return true;
        } else {
            return false;
        }
    }

    // TODO: Checar si esta bien.
    @Override
    public boolean cambiarListaEstatica(ListaEstatica indicesBusqueda, ListaEstatica valoresNuevos) {
        boolean cambio = false;
        if (indicesBusqueda.numeroElementos() == valoresNuevos.numeroElementos()) {
            for (int pos = 0; pos < indicesBusqueda.numeroElementos(); pos++) {
                if (indicesBusqueda.obtener(pos) != null) {
                    if (!cambiar((Integer) indicesBusqueda.obtener(pos), valoresNuevos.obtener(pos))) {
                        cambio = false;
                    }
                } else {
                    cambio = false;
                }
            }
            return cambio;
        } else {
            return cambio;
        }
    }

    @Override
    public Object eliminar(int indice) {
       if (obtener(indice) != null) { // si lo encontramos
           Object valorEliminado = datos[indice];
           ultimo--;
           for (int movimiento = indice; movimiento <= ultimo; movimiento++) {
               datos[movimiento] = datos[movimiento + 1];
           }
           return valorEliminado;
       } else { // no lo encontramos
           return null;
       }
    }

    // TODO: Metodo No Terminado al 100%
    @Override
    public Object redimensionar(int maximo) {
        if (maximo < 0) {
            return null;
        } else {
            //  Si el tamaño es menor, los elementos sobrantes deben ser eliminados de la lista.
            if (maximo < numeroElementos()) {
                for (int pos = maximo; pos < numeroElementos(); pos++) {
                    eliminar(pos);
                }
            } else {
                //Si el tamaño es mayor, los datos anteriores deben
                // conservarse y los nuevos espacios deben estar vacíos.
                Object[] datosNuevos = new Object[maximo];
                for (int pos = 0; pos < numeroElementos(); pos++) {
                    datosNuevos[pos] = datos[pos];
                }
                datos = datosNuevos;
            }
            return datos;
        }
    }

    @Override
    public int agregar(Object valor) {
        if (llena() == false) { // hay espacio.
            ultimo = ultimo + 1;
            datos[ultimo] = valor;
            return ultimo;
        } else { // no hay espacio.
            return -1;
        }
    }

    @Override
    public void imprimir() {
        for (int posicion = ultimo; posicion >= 0; posicion--) {
            SalidaPorDefecto.consola(datos[posicion] + "\n");
        }
    }

    @Override
    public void imprimirOI() {
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            SalidaPorDefecto.consola(datos[posicion] + "\n");
        }
    }

    @Override
    public Object buscar(Object valor) {
        int posicion = 0;
        // bucamos mintras haya donde buscar y mientras no lo encontremos
        while (posicion <= ultimo && !valor.toString().equalsIgnoreCase(datos[posicion].toString())) {
            posicion = posicion + 1;
        }
        // no lo encontramos
        if (posicion > ultimo) {
            return null;
        // si lo encontramos
        } else {
            return posicion;
        }
    }

    @Override
    public Object eliminar(Object valor) {
        Integer posicion = (Integer) buscar(valor);
        if (posicion != null) { // si lo encontramos
            Object valorEliminado = datos[posicion];
            ultimo--;
            for (int movimiento = posicion; movimiento <= ultimo; movimiento++) {
                datos[movimiento] = datos[movimiento + 1];
            }
            return valorEliminado;
        } else { // no lo encontramos
            return null;
        }
    }

    @Override
    public boolean esIgual(ListaAlmacenamiento lista2) {
        if (lista2 instanceof ListaEstatica) {
            ListaEstatica lista = (ListaEstatica) lista2;
            if (lista.numeroElementos() == numeroElementos()) {
                for (int posicion = 0; posicion <= ultimo; posicion++) {
                    if (!datos[posicion].equals(lista.obtener(posicion))) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // TODO: Terminado pero chequear.
    @Override
    public boolean cambiar(Object valorViejo, Object valorNuevo, int numVeces) {
        int veces;
        boolean cambio = false;
        if (numVeces <= 0) {
            cambio = false;
        } else {
            veces = contar(valorViejo);
            if (veces == 0) {
                cambio = false;
            } else {
                if (veces < numVeces) {
                    cambio = false;
                } else {
                    for (int posicion = 0; posicion <= ultimo; posicion++) {
                        if (datos[posicion].toString().equalsIgnoreCase(valorViejo.toString())) {
                            datos[posicion] = valorNuevo;
                            numVeces--;
                            if (numVeces == 0) {
                                cambio = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return cambio;
    }

    @Override
    public ListaEstatica buscarValores(Object valor) {
        // Obtener cuntas veces aparece el valor en la lista estatica
        int veces = contar(valor);

        // Creo una lista estatica con las posiciones en donde aparece el valor
        ListaEstatica listaPosiciones = new ListaEstatica(veces);
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            if (valor.toString().equalsIgnoreCase(datos[posicion].toString())) {
                listaPosiciones.agregar(posicion);
            }
        }
        return listaPosiciones;
    }

    @Override
    public Object eliminar() {
        if(!vacia()){
            Object valorEliminado = datos[ultimo];
            ultimo--;
            return valorEliminado;
        } else {
            return null;
        }
    }

    @Override
    public boolean agregarLista(ListaAlmacenamiento lista2) {
        if (lista2 instanceof ListaEstatica) {
            ListaEstatica listaTem = (ListaEstatica) lista2;
            for (int posicion = 0; posicion <= listaTem.ultimo; posicion++) {
                agregar(listaTem.obtener(posicion));
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void invertir() {
        // Invertir el orden de la lista
        for (int posicion = 0; posicion <= ultimo / 2; posicion++) {
            Object aux = datos[posicion];
            datos[posicion] = datos[ultimo - posicion];
            datos[ultimo - posicion] = aux;
        }
    }

    @Override
    public int contar(Object valor) {
        int numVeces = 0;
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            if (valor.toString().equalsIgnoreCase(datos[posicion].toString())) {
                numVeces++;
            }
        }
        return numVeces;
    }

    @Override
    public boolean eliminarLista(ListaAlmacenamiento lista2) {
        // Validar que lista2 sea una lista estatica
        if (lista2 instanceof ListaEstatica) {
            ListaEstatica listaTem = (ListaEstatica) lista2;
            // Eliminar todos los elementos que contiene la lista2 de la lista actual
            for (int posicion = 0; posicion <= listaTem.ultimo; posicion++) {
                eliminar(listaTem.obtener(posicion));
            }
            return true;
        } else {
            return false;
        }
    }

    // TODO: Terminado pero chequear.
    @Override
    public void rellenar(Object valor, int cantidad){
        int maximo=(cantidad>MAXIMO ? MAXIMO : cantidad );
        for(int pos=0;pos<maximo;pos++){
            agregar(valor);
        }
    }

    @Override
    public Object clonar() {
        ListaEstatica listaTmp = new ListaEstatica(numeroElementos());
        for (int posicion = 0; posicion <= ultimo; posicion++) {
            listaTmp.agregar(datos[posicion]);
        }
        return listaTmp;
    }

    @Override
    public Object subLista(int indiceInicial, int indiceFinal) {
        // Validar que los índices sean correctos
        if (enRango(indiceInicial) && enRango(indiceFinal)) {
            // Crear una lista estatica con los elementos de la lista actual
            ListaEstatica listaTmp = new ListaEstatica(indiceFinal - indiceInicial + 1);
            for (int posicion = indiceInicial; posicion <= indiceFinal; posicion++) {
                listaTmp.agregar(datos[posicion]);
            }
            return listaTmp;
        } else {
            return null;
        }
    }

    @Override
    public boolean esSublista(ListaAlmacenamiento lista2) {
        return false;
    }

    @Override
    public boolean insertar(Object valor, int indice) {
        if (enRango(indice)){
            datos[indice] = valor;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void rellenar(Object valor) {

    }

    @Override
    public Object verUltimo() {
        if (!vacia()){
            return datos[ultimo];
        } else {
            return null;
        }
    }

    @Override
    public Object verPrimero() {
        if (!vacia()){
            return datos[0];
        } else {
            return null;
        }
    }

    /**
     * Este método nos permite obtener un sub arreglo de la lista actual.
     * @param listaIndices Es el arreglo que contiene los indices de los elementos que se desean obtener.
     * @return Regrsa un arreglo con los elementos de la lista actual segun los indices que contenga el arreglo listaIndices.
     */
    public ListaEstatica subLista(ListaEstaticaNumerica listaIndices) {
        ListaEstatica subListaEelmentos = new ListaEstatica(listaIndices.numeroElementos());
        if (!vacia() && !listaIndices.vacia() && listaIndices.numeroElementos() > 0) {
            for (int posicion = 0; posicion < listaIndices.ultimo; posicion++) {
                int indice = listaIndices.datos[posicion].intValue();
                if (enRango(indice)) {
                    subListaEelmentos.agregar(obtener(indice));
                }
            }
        }
        return subListaEelmentos;
    }

    /**
     * Este método agrega el contenido de una lista a la lista actual.
     * @param buffer Es la lista que contiene los elementos que se desean agregar a la lista actual.
     * @return Regresa true si la lista fue agregada correctamente, false en caso contrario.
     */
    public boolean agregarBuffer(double[] buffer) {
        boolean agregado = false;
        if (vacia()) {
            for (int posicion = 0; posicion < buffer.length; posicion++) {
                agregar(buffer[posicion]);
            }
            agregado = true;
        }
        return agregado;
    }

    /**
     * Este nos permite obtener una copia de la lista actual.
     * @return Regresa una copia de la lista actual.
     */
    public Object leerArreglo() {
        // regresar una copia del arrglo actual
        return clonar();
    }

    public int obtenerPosicion(Object valor) {
        int posicion = -1;
        for (int indice = 0; indice <= ultimo; indice++) {
            if (datos[indice].equals(valor)) {
                posicion = indice;
                break;
            }
        }
        return posicion;
    }

    /**
     * Este método nos permite intercambiar dos elementos de la lista actual dados sus indices.
     * @param indice1 Es el indice del primer elemento que se desea intercambiar.
     * @param indice2 Es el indice del segundo elemento que se desea intercambiar.
     * @return Regresa <b>true</b> si los elementos fueron intercambiados correctamente, <b>false</b> en caso contrario.
     */
    public boolean intercambiar(int indice1, int indice2) {
        Object temp = null;
        if (enRango(indice1) && enRango(indice2)) { // Validar que los índices sean correctos
            temp = datos[indice1];
            datos[indice1] = datos[indice2];
            datos[indice2] = temp;
            return true;
        } else {
            return false;
        }
    }
}