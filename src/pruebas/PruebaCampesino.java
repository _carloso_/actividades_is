package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;
import pruebas.menus.MenuCampesinos;
import registros.campesinodearroz.Campesino;

public class PruebaCampesino {
    public static void main(String[] args) {
        
        int opcion = 0;
        ListaEstatica listaCampesinos = new ListaEstatica(2);
        
        ListaEstatica anio1_Campesino1 = new ListaEstatica(12);
        anio1_Campesino1.agregar(100);
        anio1_Campesino1.agregar(240);
        anio1_Campesino1.agregar(80);
        anio1_Campesino1.agregar(10);
        anio1_Campesino1.agregar(38);
        anio1_Campesino1.agregar(50);
        anio1_Campesino1.agregar(20);
        anio1_Campesino1.agregar(30);
        // (568 / 8) = 71.00

        ListaEstatica anio2_Campesino1 = new ListaEstatica(12);
        anio2_Campesino1.agregar(300);
        anio2_Campesino1.agregar(40);
        anio2_Campesino1.agregar(60);
        anio2_Campesino1.agregar(70);
        anio2_Campesino1.agregar(20);
        anio2_Campesino1.agregar(10);
        anio2_Campesino1.agregar(80);
        anio2_Campesino1.agregar(5);
        // (585 / 8) = 73.125

        ListaEstatica anio1_Campesino2 = new ListaEstatica(12);
        anio1_Campesino2.agregar(50);
        anio1_Campesino2.agregar(20);
        anio1_Campesino2.agregar(30);
        anio1_Campesino2.agregar(40);
        anio1_Campesino2.agregar(72);
        anio1_Campesino2.agregar(10);
        anio1_Campesino2.agregar(80);
        // (302 / 7) = 43.1428...

        ListaEstatica anio2_Campesino2 = new ListaEstatica(12);
        anio2_Campesino2.agregar(110);
        anio2_Campesino2.agregar(120);
        anio2_Campesino2.agregar(10);
        anio2_Campesino2.agregar(20);
        anio2_Campesino2.agregar(9);
        // (269 / 5) = 53.8

        Campesino campesino1 = new Campesino("Juan", "Perez", 1, 4);
        listaCampesinos.agregar(campesino1);
        campesino1.agregarAnio(anio1_Campesino1);
        campesino1.agregarAnio(anio2_Campesino1);
        SalidaPorDefecto.consola("\n");

        Campesino campesino2 = new Campesino("Pedro", "Gomez", 2, 4);
        listaCampesinos.agregar(campesino2);
        campesino2.agregarAnio(anio1_Campesino2);
        campesino2.agregarAnio(anio2_Campesino2);

        // Menu de opciones para el usuario del programa
        do {
            try {
                MenuCampesinos.mostrarMenu();
                opcion = MenuCampesinos.leerOpcion();
                MenuCampesinos.menuCampesinos(opcion, listaCampesinos);
            } catch (NumberFormatException e) {
                SalidaPorDefecto.consola("---Opcion incorrecta---" + "\n");
            } 
        } while (opcion != 9);
    }
}