package estructurasLineales;

/**
 * Esta interfaz gestiona la funcionalidad de una lista de almacenamiento.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public interface ListaAlmacenamiento {
    /**
     * Determine si una lista de almacenamiento esta vacía.
     * @return Regresa <b>true</b> si la lista está vacía, <b>false</b> en caso contrario.
     */
    public boolean vacia();

    /**
     * Este método inserta al un nuevo elemento en la lista de almacenamiento.
     * @param valor Es el dato que se va a agreagar a la lista.
     * @return Regresa la posicion
     */
    public int agregar(Object valor);

    /**
     * Este metodo imprime la lista de almacenamiento de forma decendente.
     */
    public void imprimir();

    /**
     * Este metodo imprime la lista de almacenamiento de forma aceendente (orden inverso).
     */
    public void imprimirOI();

    /**
     * Este metodo busca un elemento en la lista de almacenamiento.
     * @param valor Es el dato que se va a buscar en la lista.
     * @return Regresa la posicion del elemento si lo encuentra, si no regresa un valor null.
     */
    public Object buscar(Object valor);

    /**
     * Este metodo elimina un elemento de la lista de almacenamiento.
     * @param valor Es el dato que se va a eliminar de la lista.
     * @return Regresa el valor eliminado si lo encuentra, si no regresa un valor null.
     */
    public Object eliminar(Object valor);

    /**
     * Este método nos indica si la lista actual es igual a la lista que se pasa como parametro.
     * @param lista2 Es la lista que se va a comparar con la lista actual.
     * @return Regresa <b>true</b> si las listas son iguales, <b>false</b> en caso contrario.
     */
    public boolean esIgual(ListaAlmacenamiento lista2);

    /**
     * Este método modifica el elemento de la lista por un nuevo elemento.
     * @param valorViejo Es el valor que se va a modificar.
     * @param valorNuevo Es el nuevo valor que se va a colocar en la lista.
     * @param numVeces Es el numero de ocurrencias que se van a modificar el valor viejo por el nuevo valor.
     * @return Regresa <b>true</b> si se realizo alguna modificacion, <b>false</b> en caso contrario.
     */
    public boolean cambiar(Object valorViejo, Object valorNuevo, int numVeces);

    /**
     * Este método busca dentro de una lista de almacenamiento los elementos indicados por algún valor.
     * @param valor Es el valor que se va a buscar dentro del arreglo.
     * @return Regresa un arreglo con las posiciones en donde se encuentra el valor.
     */
    public ListaEstatica buscarValores(Object valor);

    /**
     * Este metodo nos ayuda a obtener el ultimo elemento de la lista.
     * @return Regresa el ultimo elemento de la lista o <b>null</b> si la lista esta vacia.
     */
    public Object eliminar();

    /**
     * Este metodo agrega al final de la lista actual el contenido de la lista que se pasa como parametro.
     * @param lista2 Es la lista que contiene los elementos que se van a agregar al final de la lista actual.
     * @return Regresa <b>true</b> si se agregaron los elementos de la lista2, <b>false</b> en caso contrario.
     */
    public boolean agregarLista(ListaAlmacenamiento lista2);

    /**
     * Este metodo invierte el orden de los elementos de la lista.
     */
    public void invertir();

    /**
     * Este metodo nos ayuda a contar cuantos elementos tienen el mismo valor;
     * @param valor Es el valor especificado para contar cuantos elementos tienen el mismo valor.
     * @return Regresa el numero de veces que se encuentra el valor en la lista.
     */
    public int contar(Object valor);

    /**
     * Este método elimina cada elemento de la lista2 de la lista actual.
     * @param lista2 Es la lista que contiene los elementos que se van a eliminar de la lista actual.
     * @return Regresa <b>true</b> si se eliminaron los elementos de la lista actual, <b>false</b> en caso contrario.
     */
    public boolean eliminarLista(ListaAlmacenamiento lista2);

    /**
     * Este método nos ayuda a rellenar la lista de almacenamiento con un valor especificado un número de veces.
     * @param valor Es el valor con el que se va a rellenar la lista.
     * @param cantidad Es la cantidad de veces que se va a rellenar la lista.
     */
    public void rellenar(Object valor, int cantidad);

    /**
     * Este metodo nos ayuda a obtener una copia de la lista actual.
     * @return Regresa una copia de la lista actual.
     */
    public Object clonar();

    /**
     * Este metodo nos ayuda a obtener una sublista de la lista actual en un rango especificado.
     * @param indiceInicial Es la posicion inicial de la sublista.
     * @param indiceFinal Es la posicion final de la sublista.
     * @return Regresa una sublista de la lista actual en el rango especificado.
     */
    public Object subLista(int indiceInicial, int indiceFinal);

    /**
     * Este metodo nos indica si una lista es sublista de la lista actual.
     * @param lista2 Es la lista que se va a comparar con la lista actual.
     * @return Regresa <b>true</b> si la lista2 es sublista de la lista actual, <b>false</b> en caso contrario.
     */
    public boolean esSublista(ListaAlmacenamiento lista2);


    /**
     * Este metodo inserta un valor en una determinada posicion de la lista.
     * @param valor Es el valor que se va a insertar en la lista.
     * @param indice Es la posicion en la que se va a insertar el valor.
     * @return Regresa <b>true</b> si se inserto el valor, <b>false</b> en caso contrario.
     */
    public boolean insertar(Object valor, int indice);


    /**
     * Rellena la lista con valores incrementales o decrementales según el parametro y si es
     * letra realiza el mismo proceso.
     * @param valor Es el valor con el que se va a rellenar la lista.
     */
    public void rellenar(Object valor);

    /**
     * Este método nos ayuda a obtener el último elemento de la lista.
     * @return Regresa el último elemento de la lista o <b>null</b> si la lista esta vacia.
     */
    public Object verUltimo();

    /**
     * Este método nos permite obtener el primer elemento de la lista dinámica.
     * @return Regresa el primer elemento de la lista dinámica.
     */
    public Object verPrimero();
}