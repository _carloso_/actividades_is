package herramientas.matematicas;

import estructurasLineales.PilaEstatica;

public class ExpresionesAritmeticas {

    public static String infijaAPostfija(String infija){
        return "";
    }

    public static String infijaAPrefija(String infija){
        //1) Tokenizar la expresión infija.
        //2) Analizar el token de uno por uno.
        //2.1) Si el token es Operando, concantenarlo en la EPRE
        //2.2) Si el token es Operador, lo metemos en la pila
        //(si en la cima de la pila existen operadores [ciclo] de
        //mayor prioridad, se sacan esos operadores
        //y se concatenan en la EPRE, después ya se mete el
        //token actual en la pila).
        //2.3) Si es paréntesis de apertura ), se mete en la pila.
        //2.4) Si es paréntesis de cierre (, se sacan [se mandan a EPRE]
        //todos los operadores [ciclo] de la pila hasta llegar al
        //paréntesis de apertura. Los paréntesis no van a ir en la EPRE.
        //3) Cuando ya se procesaron todos los tokens, los operadores
        //[ciclo] que quedaron en la pila se concatenan en la EPRE.
        return "";

    }

    public static Double evaluarPostfija(String postfija){
        // Pila que almacena los operandos y operadores
        PilaEstatica pila = new PilaEstatica(postfija.length());
        // 1) Tokenizar la expresión postfija
        // a b c d - * e f ^ / +
        // 0 1 2 3 4 5 6 7 8 9 10, indiceToken
        for (int indiceToken = 0; indiceToken < postfija.length(); indiceToken++) {
            // 2) Analizar de uno por uno los tokens.
            char token = postfija.charAt(indiceToken);
            if (esOperando(token) == true) {
                // 2.1) Si el token es un Operando, se mete en la pila
                if (pila.poner("" + token) == false) { // No hay espacio en la pila
                    return null;
                }
            } else { // es operador
                // 2.2) Si el token es un Operador, se saca dos operandos de la pila.
                // (el primer operando que se saque equivale a OP2).
                // se aplica la operación del token y el resultado se mete en la pila.
                String operando2 = (String) pila.quitar();
                String operando1 = (String) pila.quitar();
                if (operando1 == null || operando2 == null) {
                    return null;
                } else {
                    Double operacionParcial = aplicarOperacionA(token, Double.parseDouble(operando1), Double.parseDouble(operando2));
                    if (operacionParcial == null) { // El operador es invalido
                        return null;
                    } else {
                        if (pila.poner("" + operacionParcial) == false) { // No hay espacio en la pila
                            return null;
                        }
                    }
                }
            }
        }
        // 3) Es resultado de la operación queda en la pila.
        String resultadoEvaluacion = (String) pila.quitar();

        if (resultadoEvaluacion == null) {
            return null;
        } else {
            return Double.parseDouble(resultadoEvaluacion);
        }
    }

    private static Double aplicarOperacionA(char operador, Double operando1, Double operando2) {
        switch (operador) {
            case '+':
                return operando1 + operando2;
            case '-':
                return operando1 - operando2;
            case '*':
                return operando1 * operando2;
            case '/':
                if (operando2 == 0) {
                    return null; // Si el denominador es 0, no se puede dividir
                } else {
                    return operando1 / operando2;
                }
            case '^':
                return Math.pow(operando1, operando2);
            case '%':
                return operando1 % operando2;
            default:
                return null; // No es un operador válido
        }
    }

    public static Boolean esOperando(char token){
        if (token == '+' || token == '-' || token == '*' || token == '/' || token == '^' || token == '(' || token == ')'
            || token == '%'){
            return false; // No es operando
        } else { // Como no es operador y otro simbolo, es operando
            return true;
        }
    }

    public static Double evaluarPrefija(String prefija){
        // Pila que almacena los operandos y operadores
        PilaEstatica pila = new PilaEstatica(prefija.length());
        // 1) Tokenizar la expresión postfija
        return null;
    }

}
