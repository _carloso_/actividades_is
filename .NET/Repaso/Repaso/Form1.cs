namespace Repaso
{
    public partial class Form1 : Form
    {
        private Button btnVentana;
        public Form1()
        {
            btnVentana = new Button();
            InitializeComponent();
            InicializarComponentes();
        }

        private void InicializarComponentes()
        {
            btnVentana.Text = "Abrir Nueva Ventana";
            btnVentana.AutoSize = true; //se acomoda el boton dependiendo el tama�o del texto
            btnVentana.Location = new Point(10, 10);
            this.Controls.Add(btnVentana);

        }
    }
}