package prueba;
import entradaSalida.SalidaPorDefecto;
import estructurasLineales.PilaEstatica;
import herramientas.matematicas.Recursion;

public class PruebaRecursion {
    public static void main(String[] args) {
        Recursion recursion = new Recursion();
        SalidaPorDefecto.consola("MULTIPLICACION\n");
        SalidaPorDefecto.consola("2x10= "+recursion.multiplicacion(2,10)+"\n");
        SalidaPorDefecto.consola("3x4= "+recursion.multiplicacion(3,4)+"\n");

        SalidaPorDefecto.consola("NUMERO PRIMO\n");
        SalidaPorDefecto.consola("3 numero primo: "+recursion.numeroPrimo(3,2)+"\n");
        SalidaPorDefecto.consola("23 numero primo: "+recursion.numeroPrimo(23,2)+"\n");
        SalidaPorDefecto.consola("6 numero primo: "+recursion.numeroPrimo(6,2)+"\n");

        SalidaPorDefecto.consola("DECIMAL A HEXADECIMAL\n");
        SalidaPorDefecto.consola("10 a Hexadecimal: "+recursion.Hexa(10)+"\n");
        SalidaPorDefecto.consola("15 a Hexadecimal: "+recursion.Hexa(15)+"\n");
        SalidaPorDefecto.consola("25 a Hexadecimal: "+recursion.Hexa(25)+"\n");

        SalidaPorDefecto.consola("MCD TEOREMA EUCLIDES\n");
        SalidaPorDefecto.consola("MCD 12,16: "+recursion.mcdEuclides(12,16)+"\n");
        SalidaPorDefecto.consola("MCD 225,300: "+recursion.mcdEuclides(225,300)+"\n");

        SalidaPorDefecto.consola("DECIMAL A BINARIO\n");
        SalidaPorDefecto.consola("16 a Binario: "+recursion.Bin(16)+"\n");
        SalidaPorDefecto.consola("2 a Binario: "+recursion.Bin(2)+"\n");
        SalidaPorDefecto.consola("EJERCICIO 1\n");
        SalidaPorDefecto.consola("12: "+a(12)+"\n");
        SalidaPorDefecto.consola("10: "+a(10));
    }

    public static int a(int x){
        if (x>11){
            return x;
        }
        else{
            return a(a(x+2)+a(x+2));
        }
    }
}
