package pruebas;

import entradaSalida.SalidaPorDefecto;
import herramientas.matematicas.ExpresionesAritmeticas;

public class PruebaExpresionesAritmeticas {
    public static void main(String[] args) {
        SalidaPorDefecto.consola("La prueba de evaluación de la expresión postfija a b c d - * e f ^ / + es "+
                ExpresionesAritmeticas.evaluarPostfija("2231-*22^/+"));
    }
}
