package prueba;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import estructurasNoLineales.GrafoEstatico;

public class PruebaGrafoNoDirigido {
    public static void main(String[] args) {
        GrafoEstatico grafoNoDirigidoConexo = new GrafoEstatico(5);
        SalidaPorDefecto.consola("-----GRAFO NO DIRIGIDO ejemplo: Si es conexo-----\n");
        grafoNoDirigidoConexo.agregarVertice("A");
        grafoNoDirigidoConexo.agregarVertice("B");
        grafoNoDirigidoConexo.agregarVertice("C");

        grafoNoDirigidoConexo.agregarArista("A","B");
        grafoNoDirigidoConexo.agregarArista("A","C");
        grafoNoDirigidoConexo.agregarArista("B","A");
        grafoNoDirigidoConexo.agregarArista("B","C");
        grafoNoDirigidoConexo.agregarArista("C","A");
        grafoNoDirigidoConexo.agregarArista("C","B");

        grafoNoDirigidoConexo.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----CONEXO-----\n");
        grafoNoDirigidoConexo.grafoNoDirigido();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("------------------------------\n");
        GrafoEstatico grafoNoDirigidoNOConexo = new GrafoEstatico(5);
        SalidaPorDefecto.consola("-----GRAFO NO DIRIGIDO ejemplo: No es conexo-----\n");
        grafoNoDirigidoNOConexo.agregarVertice("A");
        grafoNoDirigidoNOConexo.agregarVertice("B");
        grafoNoDirigidoNOConexo.agregarVertice("C");

        grafoNoDirigidoNOConexo.agregarArista("A","B");
        grafoNoDirigidoNOConexo.agregarArista("B","A");

        grafoNoDirigidoNOConexo.imprimir();
        SalidaPorDefecto.consola("\n");

        SalidaPorDefecto.consola("-----CONEXO-----\n");
        grafoNoDirigidoNOConexo.grafoNoDirigido();
        SalidaPorDefecto.consola("\n");

       // ListaDinamica recorridoAnchura = grafoNoDirigido.recorridoProfunidad("A");
        //recorridoAnchura.imprimir();
    }
}
