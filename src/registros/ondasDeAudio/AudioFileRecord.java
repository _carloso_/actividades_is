package registros.ondasDeAudio;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstaticaNumerica;

import java.io.File;

public class AudioFileRecord {
    private long numFrames;  //numero de tramas, número de muestras totales del archivo por canal
    private long sampleRate; //numero de muestras por segundo a la que se discretiza la señal
    private int numChannels; //número de canales
    private double[] buffer; //audio original
    private double[] buffer2; //audio modificado
    private WavFile wavFileR; //apuntador de archivo leido
    private WavFile wavFileW;  //apuntador de archivo a escribir
    private String nomArchivo; //nombre archivo (uno.wav)
    private String nomRuta;    //ruta donde esta guardado el archivo (/home)
    private int validBits;     //bits usados para la discretización
    private ListaEstaticaNumerica BufferTemporal; //buffer temporal para la modificación de la señal

    public AudioFileRecord(String archivo) {
        //nombre de archivo dado por el usuario
        // Abre el archivo wav y asigna parámetros a las variables
        try {
            File file = new File(archivo);
            wavFileR = WavFile.openWavFile(file);
            nomArchivo = file.getName();
            nomRuta = file.getParent();
        } catch (Exception e) {

        }
        numChannels = wavFileR.getNumChannels();
        numFrames = wavFileR.getNumFrames();
        sampleRate = wavFileR.getSampleRate();
        validBits=wavFileR.getValidBits();
    }

    /**
     * Este metodo se encarga de leer el archivo de audio y guardarlo en un buffer temporal.
     */
    public void leerAudio() {
        try {
            // Muestra datos del archivo
            wavFileR.display();

            // Crea buffer de origen y de temporal
            buffer = new double[(int) numFrames * numChannels];
            BufferTemporal = new ListaEstaticaNumerica(buffer.length);
            buffer2 = new double[buffer.length];


            //tramas leidas
            int framesRead;

            // Lee tramas totales
            framesRead = wavFileR.readFrames(buffer, (int) numFrames);

            // Recorrer todas las tramas del archivo y guardarlas en el arreglo.
            for (int pos = 0; pos < framesRead * numChannels; pos++) {
                buffer2[pos] = buffer[pos];
            }
            BufferTemporal.agregarBuffer(buffer2);

            // Cierra archivo
            wavFileR.close();
        } catch (Exception e) {
            SalidaPorDefecto.consola(e.toString());
        }
    }

    /**
     * Este metodo se encarga de escribir el archivo de audio modificado.
     */
    public void EscribirAudio() {
        try {

            //Crear el apuntador al archivo para guardar datos del temporal
            File file = new File(nomRuta + "/2_" + nomArchivo);

            // Creamos un nuevo archivo de audio con los mismos datos que el original
            wavFileW = WavFile.newWavFile(file, numChannels, numFrames, validBits, sampleRate);

            // Escribimos los frames o muestras totales del temporal
            wavFileW.writeFrames(BufferTemporal.leerArreglo(), (int) numFrames);

            // Cerramos el archivo
            wavFileW.close();
        } catch (Exception e) {
            SalidaPorDefecto.consola(e.toString());
        }
    }

    /**
     * Este metodo se encarga de aplicar un filtro FIR de primer orden a la señal de audio.
     */
    public void preEnfasis(){
        // aplicar un filtro FIR de primer orden en
        //las altas frecuencias, dado por y[n] = x[n] + ax[n − 1], donde a es un valor
        //configurable, habitualmente con 0.95 o 0.97.
        double alfa = 0.97;
        double[] x = BufferTemporal.leerArreglo();
        for (int pos = 1; pos < x.length; pos++) {
            BufferTemporal.remplazar(pos - 1, x[pos] + alfa * x[pos - 1]);
        }
    }

    /**
     * Este metdo sube el volumen de la señal de audio.
     * @param intensidad Es la cantidad de volumen a aumentar.
     */
    public void subirVolumen(int intensidad){
        BufferTemporal.porEscalar(intensidad);
    }

    /**
     * Este método baja el volumen de la señal de audio.
     * @param intensidad Es la cantidad de volumen a disminuir.
     */
    public void bajarVolumen(int intensidad){
        double[] x = BufferTemporal.leerArreglo();
        for (int pos = 0; pos < x.length; pos++) {
            BufferTemporal.remplazar(pos, x[pos]/intensidad);
        }
    }

    /**
     * Este método elimina el silencio de la señal de audio.
     */
    public void eliminarSilencio(){
        // si el audio tine frecuencias de sonido de 0.1 Hz o menos, se elimina el audio.
        double[] x = BufferTemporal.leerArreglo();
        for (int pos = 0; pos < x.length; pos++) {
            if (x[pos] < 0.1) {
                BufferTemporal.remplazar(pos, 0);
            }
        }
    }

    /**
     * Este método nos permite obtener la energía de la señal de audio.
     * @return Regresa la energía de la señal de audio.
     */
    public double obtenerEnergia(){
        //una señal de audio se obtiene elevando al cuadrado cada una de las muestras
        // individuales, posteriormente se suman todas las muestras y se obtiene la energía.
        double[] x = BufferTemporal.leerArreglo();
        double energia = 0;
        for (int pos = 0; pos < x.length; pos++) {
            energia += Math.pow(x[pos], 2);
        }
        return energia;
    }
}