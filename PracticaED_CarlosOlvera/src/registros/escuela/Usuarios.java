package registros.escuela;

import estructurasLineales.ListaDinamica;

public class Usuarios {
    protected String usuario;
    protected String horaInicio;
    protected String horaFin;
    protected String aplicaciones;
    protected String dia;
    public Usuarios(String usuario, String dia, String horaInicio, String horaFin, ListaDinamica aplicaciones){
        this.usuario = usuario;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
        this.aplicaciones = aplicaciones.toString();
        this.dia = dia;
    }

    public String getHoraFin() {
        return horaFin;
    }
    public String getHoraInicio() {
        return horaInicio;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAplicaciones() {
        return aplicaciones;
    }

    public String getDia() {
        return dia;
    }

    public void setAplicaciones(ListaDinamica aplicaciones) {
        this.aplicaciones = String.valueOf(aplicaciones);
    }

    @Override
    public String toString() {
        return "Usuario: "+ usuario + ", Hora: " + horaInicio + "-" + horaFin + ", Dia: " + dia + ", Aplicaciones utilizadas por el usuario: " + aplicaciones;
    }
}
