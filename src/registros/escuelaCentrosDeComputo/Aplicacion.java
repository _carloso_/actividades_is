package registros.escuelaCentrosDeComputo;

/**
 * Esta clase representa una aplicación de una computadora en un centro de cómputo de la escuela.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0.
 */
public class Aplicacion {

    protected int numAplicacion;
    protected String nombre;
    protected String autor;
    protected String version;
    protected String ramMinimaParaCorrer;

    public Aplicacion(int numAplicacion, String nombre, String autor, String version, String ramMinimaParaCorrer) {
        this.numAplicacion = numAplicacion;
        this.nombre = nombre;
        this.autor = autor;
        this.version = version;
        this.ramMinimaParaCorrer = ramMinimaParaCorrer;
    }

    public int getNumAplicacion() {
        return numAplicacion;
    }

    public void setNumAplicacion(int numAplicacion) {
        this.numAplicacion = numAplicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getRamMinimaParaCorrer() {
        return ramMinimaParaCorrer;
    }

    public void setRamMinimaParaCorrer(String ramMinimaParaCorrer) {
        this.ramMinimaParaCorrer = ramMinimaParaCorrer;
    }

    /**
     * Este método nos permite obtener los datos de una aplicación.
     * @return Regresa una cadena con los datos de la aplicación.
     */
    public String obtenerDatos(){
        return  "No. de aplicacion: " + numAplicacion + "\n" +
                "Nombre: " + nombre + "\n" +
                "Autor: " + autor + "\n" +
                "Version: " + version + "\n" +
                "RAM minima para correr: " + ramMinimaParaCorrer + "\n";
    }

    @Override
    public String toString() {
        return numAplicacion + "";
    }
}
