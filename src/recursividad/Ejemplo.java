package recursividad;

import entradaSalida.SalidaPorDefecto;

public class Ejemplo {
    public static void main(String[] args) {
        SalidaPorDefecto.consola("f(x)= " + f(10));
    }

    // Si x > 11, entonces f(x) = x, en caso contrario f(x) = f(f(x+2) + f(x+2))
    public static int f(int x) {
        if (x > 11) {
            return x;
        } else {
            return f(f(x + 2) + f(x + 2));
        }
    }
}
