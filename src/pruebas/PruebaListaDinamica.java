package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaAlmacenamiento;
import estructurasLineales.ListaDinamica;
import estructurasLineales.ListaEstatica;
import estructurasnolineales.Matriz2;
import herramientas.comunes.TipoTabla;


public class PruebaListaDinamica {
    public static void main(String[] args) {
        ListaDinamica listaDinamica1 = new ListaDinamica();
        listaDinamica1.agregar(1);
        listaDinamica1.agregar(2);
        listaDinamica1.agregar(10);
        listaDinamica1.agregar(7);
        listaDinamica1.agregarInicio(20);
        listaDinamica1.agregar(3);

        SalidaPorDefecto.consola("--- Imprimir lista dinamica 1 de forma normal ---" + "\n");
        listaDinamica1.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Imprimir lista dinamica 1 de forma inversa ---" + "\n");
        listaDinamica1.imprimirOI();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Imprimir lista dinamica 1 de forma normal ---" + "\n");
        listaDinamica1.imprimir();

        SalidaPorDefecto.consola("\n\n");
        SalidaPorDefecto.consola("Nodo eliminado del final: " + listaDinamica1.eliminar() + "\n");

        listaDinamica1.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Nodo eliminado del inicio: " + listaDinamica1.eliminarInicio() + "\n");
        listaDinamica1.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Buscar Nodo con valor 2: " + listaDinamica1.buscar(2) + "\n");

        ListaEstatica listaEstatica = listaDinamica1.aListaEstatica();
        listaEstatica.imprimir();

        ListaEstatica elementosADescartar = new ListaEstatica(3);
        elementosADescartar.agregar(3);
        elementosADescartar.agregar(10);
        elementosADescartar.agregar(7);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista de elementos a descartar ---" + "\n");
        elementosADescartar.imprimir();
        SalidaPorDefecto.consola("\n");

        ListaEstatica listaDescartada = listaDinamica1.aListaEstatica(elementosADescartar);
        SalidaPorDefecto.consola("--- Lista de elementos no descartados ---" + "\n");
        listaDescartada.imprimir();

        ListaDinamica listaDinamica2 = new ListaDinamica();
        listaDinamica2.agregar(20);
        listaDinamica2.agregar(30);
        listaDinamica2.agregar(40);
        listaDinamica2.agregar(50);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista Dinamica 2 ---" + "\n");
        listaDinamica2.imprimir();

        ListaAlmacenamiento listaAlmacenaminto = new ListaEstatica(3);
        listaAlmacenaminto.agregar(100);
        listaAlmacenaminto.agregar(200);
        listaAlmacenaminto.agregar(300);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista de almacenamiento ---" + "\n");
        listaAlmacenaminto.imprimir();

        listaDinamica2.agregarLista(listaAlmacenaminto);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica con los elementos de la lista de almacenamiento ---" + "\n");
        listaDinamica2.imprimir();

        ListaDinamica listaDinamicaCopia = (ListaDinamica) listaDinamica2.clonar();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica copia de lista dinamica 2 ---" + "\n");
        listaDinamicaCopia.imprimir();

        listaDinamicaCopia.vaciar();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica copia vaciada ---" + "\n");
        listaDinamicaCopia.imprimir();


        Matriz2 matriz2 = listaDinamica2.aMatriz2D(3, 3);
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Matriz 2D de lista dinamica 2 ---" + "\n");
        matriz2.imprimirXReglones();

        ListaDinamica listaDinamica3 = new ListaDinamica();
        listaDinamica3.agregar(10);
        listaDinamica3.agregar(15);
        listaDinamica3.agregar(5);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 3 ---" + "\n");
        listaDinamica3.imprimir();

        Matriz2 tabla = new Matriz2(3, 3);
        tabla.cambiar(0, 0, 70);
        tabla.cambiar(0, 1, 20);
        tabla.cambiar(0, 2, 80);
        tabla.cambiar(1, 0, 30);
        tabla.cambiar(1, 1, 90);
        tabla.cambiar(1, 2, 50);
        tabla.cambiar(2, 0, 17);
        tabla.cambiar(2, 1, 25);
        tabla.cambiar(2, 2, 40);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Matriz 2D ---" + "\n");
        tabla.imprimirXReglones();

        listaDinamica3.agregarMatriz2D(tabla, TipoTabla.RENGLON);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 3 con los elementos de la matriz 2D ---" + "\n");
        listaDinamica3.imprimir();

        ListaDinamica listaDinamica4 = new ListaDinamica();
        listaDinamica4.agregar(5);
        listaDinamica4.agregar(9);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 4 ---" + "\n");
        listaDinamica4.imprimir();

        listaDinamica4.rellenar(10, 5);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 4 rellenada con el valor 10, 5 veces ---" + "\n");
        listaDinamica4.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Contar cuntas veces se repite el elemento 10 en la lista 4: " +
                listaDinamica4.contar(10) + "\n");

        listaDinamica4.cambiar(10, 7, 3);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 4 con el elemento 10 cambiado a 7, 3 veces ---" + "\n");
        listaDinamica4.imprimir();

        SalidaPorDefecto.consola("\n");
        ListaDinamica listaDinamica5 = new ListaDinamica();
        listaDinamica5.agregar(1);
        listaDinamica5.agregar(2);
        listaDinamica5.agregar(3);
        listaDinamica5.agregar(4);
        listaDinamica5.agregar(5);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 5 ---" + "\n");
        listaDinamica5.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 5 invertida ---" + "\n");
        listaDinamica5.invertir();
        listaDinamica5.imprimir();

        ListaDinamica listaDinamica6 = new ListaDinamica();
        listaDinamica6.agregar(5);
        listaDinamica6.agregar(2);
        listaDinamica6.agregar(7);
        listaDinamica6.insertar(8, 1);
        listaDinamica6.insertar(4, 5);

        SalidaPorDefecto.consola("\n\n");
        SalidaPorDefecto.consola("--- Lista dinamica 6 ---" + "\n");
        listaDinamica6.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Cambiar el elemento de la posicion 3 a 10: " +
                listaDinamica6.cambiar(3, 10) + "\n");

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 6 con el elemento de la posicion 3 cambiado a 10 ---" + "\n");
        listaDinamica6.imprimir();

        ListaDinamica listaDinamica7 = new ListaDinamica();
        listaDinamica7.agregar(3);
        listaDinamica7.agregar(2);
        listaDinamica7.agregar(5);
        listaDinamica7.agregar(1);

        SalidaPorDefecto.consola("\n\n");
        SalidaPorDefecto.consola("--- Lista dinamica 7 ---" + "\n");
        listaDinamica7.imprimir();

        ListaDinamica listaDinamica8 = new ListaDinamica();
        listaDinamica8.agregar(3);
        listaDinamica8.agregar(2);
        listaDinamica8.agregar(7);
        listaDinamica8.agregar(1);

        SalidaPorDefecto.consola("\n\n");
        SalidaPorDefecto.consola("--- Lista dinamica 8 ---" + "\n");
        listaDinamica8.imprimir();


        SalidaPorDefecto.consola("\n\n");
        SalidaPorDefecto.consola("Lista dinamica 7 y Lista dinamica 8 son iguales: " +
                listaDinamica7.esIgual(listaDinamica8) + "\n");


        ListaDinamica listaDinamica9 = new ListaDinamica();
        listaDinamica9.agregar(4);
        listaDinamica9.agregar(2);
        listaDinamica9.agregar(7);
        listaDinamica9.agregar(1);
        listaDinamica9.agregar(3);
        listaDinamica9.agregar(5);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 9 ---" + "\n");
        listaDinamica9.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Redimensionar la lista dinamica 9 a 4 elementos: " +
                listaDinamica9.redimensionar(4) + "\n");

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 9 redimensionada a 4 elementos ---" + "\n");
        listaDinamica9.imprimir();


        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Redimensionar la lista dinamica 9 a 10 elementos: " +
                listaDinamica9.redimensionar(10) + "\n");

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista dinamica 9 redimensionada a 10 elementos ---" + "\n");
        listaDinamica9.imprimir();

        ListaDinamica listaDinamica10 = new ListaDinamica();
        listaDinamica10.agregar(3);
        listaDinamica10.agregar(9);
        listaDinamica10.agregar(6);
        listaDinamica10.agregar(5);
        listaDinamica10.agregar(2);

        SalidaPorDefecto.consola("\n \n");
        SalidaPorDefecto.consola("--- Lista dinamica 10 ---" + "\n");
        listaDinamica10.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Eliminara el elemento de la posicion 2: " +
                listaDinamica10.eliminarPos(2) + "\n");
        listaDinamica10.imprimir();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Eliminar el elemento '9': " +
                listaDinamica10.eliminar(9) + "\n");
        listaDinamica10.imprimir();
    }
}