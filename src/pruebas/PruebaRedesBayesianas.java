package pruebas;

import estructurasnolineales.Matriz2;
import examenes.parcial3.NodoRedBayesiana;
import examenes.parcial3.RedBayesiana;

public class PruebaRedesBayesianas {
    public static void main(String[] args) {
        RedBayesiana redBayesiana = new RedBayesiana(7);

        // matriz de probabilidades de exposición al virus AH1N1
        Matriz2 tablaProbabilidadVirus = new Matriz2(1, 2);
        tablaProbabilidadVirus.cambiar(0, 0, 0.4);
        tablaProbabilidadVirus.cambiar(0, 1, 0.6);
        //SalidaPorDefecto.consola("Tabla de probabilidad de virus: \n");
        //tablaProbabilidadVirus.imprimirXReglones();

        // matriz de probabilidades de fiebre
        Matriz2 tablaProbabilidadFiebre= new Matriz2(2, 2);
        tablaProbabilidadFiebre.cambiar(0, 0, 0.8);
        tablaProbabilidadFiebre.cambiar(0, 1, 0.1);
        tablaProbabilidadFiebre.cambiar(1, 0, 0.2);
        tablaProbabilidadFiebre.cambiar(1, 1, 0.9);
        //SalidaPorDefecto.consola("\nTabla de probabilidades de fiebre\n");
        //tablaProbabilidadFiebre.imprimirXReglones();

        // matriz de probabilidades de estornudos
        Matriz2 tablaProbabilidadEstornudos = new Matriz2(2, 2);
        tablaProbabilidadEstornudos.cambiar(0, 0, 0.7);
        tablaProbabilidadEstornudos.cambiar(0, 1, 0.2);
        tablaProbabilidadEstornudos.cambiar(1, 0, 0.3);
        tablaProbabilidadEstornudos.cambiar(1, 1, 0.8);
        //SalidaPorDefecto.consola("\nTabla de probabilidades de estornudos\n");
        //tablaProbabilidadEstornudos.imprimirXReglones();

        // matriz de probabilidades de Resfriado
        Matriz2 tablaProbabilidadResfriado = new Matriz2(2, 4);
        tablaProbabilidadResfriado.cambiar(0, 0, 0.8);
        tablaProbabilidadResfriado.cambiar(0, 1, 0.5);
        tablaProbabilidadResfriado.cambiar(0, 2, 0.7);
        tablaProbabilidadResfriado.cambiar(0, 3, 0.1);
        tablaProbabilidadResfriado.cambiar(1, 0, 0.2);
        tablaProbabilidadResfriado.cambiar(1, 1, 0.5);
        tablaProbabilidadResfriado.cambiar(1, 2, 0.3);
        tablaProbabilidadResfriado.cambiar(1, 3, 0.9);
        //SalidaPorDefecto.consola("\nTabla de probabilidades de Resfriado\n");
        //tablaProbabilidadResfriado.imprimirXReglones();

        // matriz de probabilidades de influenza
        Matriz2 tablaProbabilidadInfluenza = new Matriz2(2, 4);
        tablaProbabilidadInfluenza.cambiar(0, 0, 0.8);
        tablaProbabilidadInfluenza.cambiar(0, 1, 0.7);
        tablaProbabilidadInfluenza.cambiar(0, 2, 0.2);
        tablaProbabilidadInfluenza.cambiar(0, 3, 0.1);
        tablaProbabilidadInfluenza.cambiar(1, 0, 0.2);
        tablaProbabilidadInfluenza.cambiar(1, 1, 0.3);
        tablaProbabilidadInfluenza.cambiar(1, 2, 0.8);
        tablaProbabilidadInfluenza.cambiar(1, 3, 0.9);
        //SalidaPorDefecto.consola("\nTabla de probabilidades de influenza\n");
        //tablaProbabilidadInfluenza.imprimirXReglones();

        // matriz de probabilidades de Tos
        Matriz2 tablaProbabilidadTos = new Matriz2(2, 2);
        tablaProbabilidadTos.cambiar(0, 0, 0.8);
        tablaProbabilidadTos.cambiar(0, 1, 0.1);
        tablaProbabilidadTos.cambiar(1, 0, 0.2);
        tablaProbabilidadTos.cambiar(1, 1, 0.9);
        //SalidaPorDefecto.consola("\nTabla de probabilidades de Tos\n");
        //tablaProbabilidadTos.imprimirXReglones();

        // matriz de probabilidades de Dolor muscular
        Matriz2 tablaProbabilidadDolorMuscular = new Matriz2(2, 2);
        tablaProbabilidadDolorMuscular.cambiar(0, 0, 0.8);
        tablaProbabilidadDolorMuscular.cambiar(0, 1, 0.1);
        tablaProbabilidadDolorMuscular.cambiar(1, 0, 0.2);
        tablaProbabilidadDolorMuscular.cambiar(1, 1, 0.9);
        //SalidaPorDefecto.consola("\nTabla de probabilidades de Dolor muscular\n");
        //tablaProbabilidadDolorMuscular.imprimirXReglones();

        // crear los nodos de la red bayesiana
        NodoRedBayesiana nodoVirus = new NodoRedBayesiana("virus", tablaProbabilidadVirus);
        NodoRedBayesiana nodoFiebre = new NodoRedBayesiana("fiebre", tablaProbabilidadFiebre);
        NodoRedBayesiana nodoEstornudos = new NodoRedBayesiana("estornudos", tablaProbabilidadEstornudos);
        NodoRedBayesiana nodoResfriado = new NodoRedBayesiana("resfriado", tablaProbabilidadResfriado);
        NodoRedBayesiana nodoInfluenza = new NodoRedBayesiana("influenza", tablaProbabilidadInfluenza);
        NodoRedBayesiana nodoTos = new NodoRedBayesiana("tos", tablaProbabilidadTos);
        NodoRedBayesiana nodoDolorMuscular = new NodoRedBayesiana("dolor muscular", tablaProbabilidadDolorMuscular);

        // agregar los nodos a la red
        redBayesiana.agregarNodo(nodoVirus);
        redBayesiana.agregarNodo(nodoFiebre);
        redBayesiana.agregarNodo(nodoEstornudos);
        redBayesiana.agregarNodo(nodoResfriado);
        redBayesiana.agregarNodo(nodoInfluenza);
        redBayesiana.agregarNodo(nodoTos);
        redBayesiana.agregarNodo(nodoDolorMuscular);

        // agregar las aristas a la red
        redBayesiana.agregarArista(nodoVirus, nodoFiebre);
        redBayesiana.agregarArista(nodoVirus, nodoEstornudos);
        redBayesiana.agregarArista(nodoFiebre, nodoResfriado);
        redBayesiana.agregarArista(nodoEstornudos, nodoResfriado);
        redBayesiana.agregarArista(nodoFiebre, nodoInfluenza);
        redBayesiana.agregarArista(nodoEstornudos, nodoInfluenza);
        redBayesiana.agregarArista(nodoResfriado, nodoTos);
        redBayesiana.agregarArista(nodoInfluenza, nodoDolorMuscular);

        // imprimir la red
        //redBayesiana.imprimirRedBayesiana();

        redBayesiana.menuOpciones();
    }
}
