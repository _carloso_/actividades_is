package estructurasLineales;

import entradaSalida.SalidaPorDefecto;

/**
 * Esta clase Representa un TDA ColaEstatica.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class ColaEstatica implements LoteAlmacenamiento {

    protected Object datos[];
    protected int primero;
    protected int ultimo;
    protected int MAXIMO;

    public ColaEstatica(int maximo){
        MAXIMO = maximo;
        datos = new Object[MAXIMO];
        primero = -1;
        ultimo = -1;
    }

    @Override
    public boolean vacio(){
        if(primero == -1){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean lleno(){
        if((primero == 0 && ultimo == (MAXIMO - 1)) || primero == (ultimo + 1)){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean poner(Object valor){
        if (!lleno()){ // Hay espacio
            if(vacio()){ // Cola vacia a)
                primero = 0;
                ultimo = 0;
                datos[ultimo] = valor;
            } else if (ultimo == (MAXIMO - 1)){ // si ultimo esta hasta la derecha d)
                ultimo = 0;
                datos[ultimo] = valor;
            } else { // c) y e)
                ultimo++;
                datos[ultimo] = valor;
            }
            return true;
        } else { // No hay espacio
            return false;
        }
    }

    @Override
    public Object quitar(){
        if(!vacio()){ // hay datos a)
            Object valorEliminado;
            if(primero == ultimo){ // c)
                valorEliminado = datos[primero];
                primero = -1;
                ultimo = -1;
            } else if(primero == (MAXIMO - 1)){ // d)
                valorEliminado = datos[primero];
                primero = 0;
            } else { // b) y e)
                valorEliminado = datos[primero];
                primero++;
            }
            return valorEliminado;
        } else { // no hay datos
            return null;
        }
    }

    public Object buscar(Object valor){
        // buscar en la cola de datos el valor y devolver el indice de la posicion
        // si no esta devolver -1
        int posicion = 0;
        // bucamos mintras haya donde buscar y mientras no lo encontremos
        while(posicion <= ultimo && primero <= ultimo && !valor.toString().equalsIgnoreCase(datos[posicion].toString())){
            posicion++;
        }
        if(posicion > ultimo || primero > ultimo){
            return null;
        } else {
            return posicion;
        }
    }

    @Override
    public void imprimir(){
        if(vacio() == false){ //hay algo, a)
            if(primero <= ultimo){ //b, comportamiento lineal
                for(int indice=primero;indice<=ultimo;indice++){
                    SalidaPorDefecto.consola(datos[indice]+ "\n");
                }
            }else{ //c, comportamiento circular
                for(int indice=primero;indice<=(MAXIMO - 1);indice++){
                    SalidaPorDefecto.consola(datos[indice] + "\n");
                }
                for(int indice=0;indice<=ultimo;indice++){
                    SalidaPorDefecto.consola(datos[indice] + "\n");
                }
            }
        }
    }

    @Override
    public Object verUltimo(){
        if(vacio()==false){
            return datos[ultimo];
        }else{
            return null;
        }
    }

    public Object verPrimero(){
        if(vacio()==false){
            return datos[primero];
        }else{
            return null;
        }
    }
}