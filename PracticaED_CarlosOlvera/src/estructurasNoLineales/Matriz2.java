package estructurasNoLineales;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstatica;

import static estructurasNoLineales.TipoColumna.*;
import static estructurasNoLineales.TipoRenglon.*;

public class Matriz2 {
    protected int renglones;
    protected int columnas;
    protected Object datos[][];

    public Matriz2(int renglones, int columnas){
        this.renglones=renglones;
        this.columnas=columnas;
        datos=new Object[renglones][columnas];
    }

    public Matriz2(int renglones, int columnas, Object valor){
        this.renglones=renglones;
        this.columnas=columnas;
        datos=new Object[renglones][columnas];
        rellenar(valor);
    }

    public void rellenar(Object valor){
        for(int fila=0; fila<renglones; fila++){ //recorre fila por fila
            //podemos asumir que en nuestro escenario hay solo un renglón
            for(int columna=0;columna<columnas; columna++){ //recorremos una por una las columnas de un solo renglòn
                datos[fila][columna]=valor;
            }
        }
    }

    private boolean enRango(int indice, int limiteDimension){
        if(indice>=0 && indice<limiteDimension){ //verficiar el limite de las dimensiones
            return true;
        }else{
            return false;
        }
    }

    public Object obtener(int fila, int col){
        if(enRango(fila,renglones)==true && enRango(col,columnas)==true){
            return datos[fila][col];
        }else{ //índices fuera de rango
            return null;
        }
    }

    public boolean cambiar(int fila, int col, Object valor){
        if(enRango(fila,renglones)==true && enRango(col,columnas)==true ){
            datos[fila][col]=valor;
            return true;
        }else{ //índices fuera de rango
            return false;
        }
    }

    public int obtenerRenglones(){
        return renglones;
    }

    public int obtenerColumnas(){
        return columnas;
    }

    public void imprimirXColumnas(){
        for(int col=0;col<columnas;col++){  //una columna
            for(int fila=0;fila<renglones;fila++){ //extrae renglón por renglón
                SalidaPorDefecto.consola(datos[fila][col]+" ");
            }
            //Cuando termina cada renglon, hacer salto de línea
            SalidaPorDefecto.consola("\n");
        }
    }

    public void imprimirXRenglones(){
        for(int fila=0;fila<renglones;fila++){   //reng x reng
            for(int col=0;col<columnas;col++){ //extrae col x col
                SalidaPorDefecto.consola(datos[fila][col]+" ");
            }
            //Cuando termina cada columna, hacer salto de línea
            SalidaPorDefecto.consola("\n");
        }
    }

    public void transpuesta(){
        Object [][] matrizT = new Object[columnas][renglones];
        for(int fila=0;fila<renglones;fila++){   //reng x reng
                for(int col=0;col<columnas;col++){ //extrae col x col
                matrizT[col][fila]=datos[fila][col];
            }
        }
        datos = matrizT;
    }

    public Object clonar() {
        Matriz2 matrizCopia = new Matriz2(renglones, columnas);
        for (int fila = 0; fila < renglones; fila++) {   //reng x reng
            for (int col = 0; col < columnas; col++) { //extrae col x col
                matrizCopia.cambiar(fila, col, datos[fila][col]);
            }
        }
        return matrizCopia;
    }

    public boolean esIgual(Matriz2 matriz2) {
        boolean verificacion = false;
        if (renglones == matriz2.obtenerRenglones() && columnas == matriz2.obtenerColumnas()) {
            verificacion = true;
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    if(datos[fila][col] != matriz2.obtener(fila,col)){
                        verificacion = false;
                        break;
                }
            }
        }
      }
        return verificacion;
    }

    public Object vectorColumna(int filas, Object valor){
        Matriz2 vectorC = new Matriz2(filas, 1);
        for (int fila = 0; fila < filas; fila++){
            vectorC.cambiar(fila,0, valor);
        }
        return vectorC;
    }

    public Object vectorRenglon(int columnas, Object valor) {
        Matriz2 vectorR = new Matriz2(1, columnas);
        for (int col = 0; col < columnas; col++) {
            vectorR.cambiar(0, col, valor);
        }
        return vectorR;
    }

    public boolean redefinir(Matriz2 matriz2){
        int numRenglonesMatriz2 = matriz2.obtenerRenglones();
        int numColumnasMatriz2 = matriz2.obtenerColumnas();
        datos = new Object[numRenglonesMatriz2][numColumnasMatriz2];
        renglones = numRenglonesMatriz2;
        columnas = numColumnasMatriz2;
        for (int fila = 0; fila < renglones; fila++) {   //reng x reng
            for (int col = 0; col < columnas; col++) { //extrae col x col
                datos[fila][col] = matriz2.obtener(fila, col);
            }
        }
        return true;
    }

    public boolean agregarRenglon(ListaEstatica arreglo, int numFila){
        boolean agregar = false;
        int numElelementosArreglo = arreglo.numeroElementos();
        if (numElelementosArreglo == columnas){
            if (numFila < renglones){
                for (int col = 0; col < columnas; col++){
                    datos[numFila][col] = arreglo.obtener(col);
                }
                agregar = true;
            }
        }
        return agregar;
    }

    public boolean agregarColumna(ListaEstatica arreglo, int numCol){
        boolean agregar = false;
        int numElelementosArreglo = arreglo.numeroElementos();
        if (numElelementosArreglo == renglones){
            if (numCol < renglones){
                for (int fila = 0; fila < renglones; fila++){
                    datos[fila][numCol] = arreglo.obtener(fila);
                }
            }
        }
        return agregar;
    }
    public int numElementos(){
        return renglones*columnas;
    }

    public Matriz2 matrizVectorColumna(){
        Matriz2 matriz2 = new Matriz2(numElementos(),1);
        int contador = 0;
        for (int col = 0; col < columnas; col++){
            for (int fila = 0; fila < renglones; fila++){
                matriz2.datos[contador][0] =datos[fila][col];
                contador++;
            }
        }
        return matriz2;
    }

    public Matriz2 matrizVectorRenglon(){
        Matriz2 matriz2 = new Matriz2(1,numElementos());
        int contador = 0;
            for (int fila = 0; fila < renglones; fila++){
                for (int col = 0; col < columnas; col++){
                matriz2.datos[0][contador] =datos[fila][col];
                contador++;
            }
        }
        return matriz2;
    }

    public boolean quitarColumna(TipoColumna tipoCol){
        boolean quitar = false;
        if (tipoCol == IZQ){
            if (columnas > 0){
                for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                    for (int col = 0; col < columnas -1; col++) { //extrae col x col
                        datos[fila][col] = datos[fila][col + 1];
                    }
                }
                columnas = columnas-1;
                quitar = true;
            }
        }
        if (tipoCol == DER){
            if (columnas > 0){
                columnas = columnas-1;
                quitar = true;
            }
        }
        return quitar;
    }

    public boolean quitarRenglon(TipoRenglon tipoRen){
        boolean quitar = false;
        if (tipoRen == SUP){
            if (renglones > 0){
                for (int fila = 0; fila < renglones - 1; fila++) {   //reng x reng
                    for (int col = 0; col < columnas; col++) { //extrae col x col
                        datos[fila][col] = datos[fila + 1][col];
                    }
                }
                renglones = renglones-1;
                quitar = true;
            }
        }
        if (tipoRen == INF){
            renglones = renglones-1;
            quitar = true;
        }
        return quitar;
    }

    public boolean eliminarColumna(int columna){
        boolean eliminado = false;
        if (enRango(columna,columnas)){
            for (int fila = 0; fila < renglones; fila++) {   //reng x reng
                for (int col = 0; col < columnas - 1; col++) { //extrae col x col
                    datos[fila][col] = datos[fila][col + 1];
                }
            }
            columnas = columnas-1;
            eliminado = true;
        }
        return eliminado;
    }

    public boolean eliminarRenglon(int renglon){
        boolean eliminado = false;
        if (enRango(renglon,renglones)){
            for (int fila = 0; fila < renglones -1; fila++) {   //reng x reng
                for (int col = 0; col < columnas; col++) { //extrae col x col
                    datos[fila][col] = datos[fila + 1][col];
                }
            }
            renglones= renglones-1;
            eliminado = true;
        }
        return eliminado;
    }

    public void matrizDiagonal(Object elemento){
        if(renglones==columnas){
            for(int diag=0;diag<renglones;diag++){
                datos[diag][diag]=elemento;
            }
        }else{ //hacer lo que corresponda,

        }
    }

    public Object obtenerValor(int fila, int col) {
        if (enRango(fila,renglones) && enRango(col,columnas)){
            return datos[fila][col];
        }
        else{
            return null;
        }
    }

    public static Matriz2 multiplicacion (Matriz2 A, Matriz2 B){
        // columnas de la matriz A
        int n= A.columnas;
        // filas de la matriz A
        int m= A.renglones;
        // filas de la matriz B
        int n2= B.renglones;
        // columnas de la matriz B
        int o= B.columnas;
        // nueva matriz
        Matriz2 C = new Matriz2(m,o);
        // se comprueba si las matrices se pueden multiplicar
        if (n==n2){
            for (int i=0; i<n;i++){

                for (int j=0; j<n2;j++){
                    //aqui se multiplica la matriz
                    double a=0;
                    for(int k=0;k<o;k++){
                        a=a+(double)A.obtener(i,k)*(double)B.obtener(k,j);
                    }
                    C.cambiar(i,j,a);
                }

            }
        }
        /**
         *  si no se cumple la condición se retorna una matriz vacía
         */
        return C;
    }
}

