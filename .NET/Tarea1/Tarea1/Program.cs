﻿using Ejercicio;

Concesionaria tesla = new Concesionaria(5);
Automovil auto1 = new Automovil(12345, "Tesla", "Model 3", 261, 600000);
Automovil auto2 = new Automovil(6789, "Tesla", "Model S", 322, 800000);
Automovil auto3 = new Automovil(6789, "Tesla", "Model T", 531, 850000);
AutomovilDeLujo auto4 = new AutomovilDeLujo(6789, "Tesla", "Model X", 536, 990000);
auto4.agregarAireAcondicionado();
auto4.agregarVentanaElectrica();
Console.WriteLine("LISTA DE AUTOS : \n");
tesla.Agregar(auto1);
tesla.Agregar(auto2);
tesla.Agregar(auto3);
tesla.Agregar(auto4);
tesla.mostrarAutos();
Console.WriteLine("MOSTRAR AUTO : \n");
Console.WriteLine("El auto que estas buscando es : "+tesla.buscarAuto(auto4));
Console.WriteLine("LISTA DE AUTOS CON ELIMINACION : \n");
tesla.eliminar(auto4);
tesla.mostrarAutos();
Console.WriteLine("LISTA VACIA DE AUTOS : \n");
tesla.vaciar();
tesla.mostrarAutos();

//AutomovilDeLujo automovilDeLujo = new AutomovilDeLujo(123, "Nissan", "GTR", 0, 100000000);
//automovilDeLujo.agregarAireAcondicionado();
//automovilDeLujo.agregarVentanaElectrica();
//Console.WriteLine("Caracteristicas del auto de lujo: " + automovilDeLujo.ToString());

