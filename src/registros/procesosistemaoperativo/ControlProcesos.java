package registros.procesosistemaoperativo;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ColaEstaticaDePrioridad;
import herramientas.comunes.TipoPrioridad;

/**
 * Clase que representa el control de procesos del sistema operativo.
 * @author Cristian Omar Alvarado Rodriguez.
 * @version 1.0
 */
public class ControlProcesos {

    private ColaEstaticaDePrioridad colaProcesos;

    public ControlProcesos(int numeroProcesos) {
        colaProcesos = new ColaEstaticaDePrioridad(numeroProcesos, TipoPrioridad.MENOR_A_MAYOR);
    }

    /**
     * Este método agrega un proceso a la cola de procesos.
     * @param proceso Es el proceso que se agregará a la cola.
     * @return Retorna true si el proceso se agregó correctamente, false si no se pudo agregar.
     */
    public boolean agregarProceso(Proceso proceso) {
        boolean agregado = false;
        int prioridad = proceso.getTipoProceso().getId();
        if (colaProcesos.insertar(proceso, prioridad)) {
            agregado = true;
        }
        return agregado;
    }

    /**
     * Este método simula la realización de los procesos de un sistema operativo.
     */
    public void realizarProcesos() {
       while (!colaProcesos.vacio()) {
           // dormir al hilo
           try {
               Thread.sleep(1000);
               SalidaPorDefecto.consola("Procesando...");
               Proceso procesoARealizar = (Proceso) colaProcesos.quitar();
               SalidaPorDefecto.consola("Proceso " + procesoARealizar.getNombre() + " realizado" + "\n");
           } catch (InterruptedException ex) {
               SalidaPorDefecto.consola("Error al realizar proceso " + ex.getMessage());
           }
       }
       if (colaProcesos.vacio()) {
            SalidaPorDefecto.consola("*** No hay más procesos en la cola ***");
       }
    }
}