package herramientas.matematicas;

import entradaSalida.SalidaPorDefecto;
import entradaSalida.archivos.ArchivoTexto;
import estructurasLineales.ListaDinamicaDoble;
import estructurasLineales.ListaEstatica;

/**
 * Operaciones para obtener la regresion Lineal
 */
public class RegresionLineal {
    protected ListaDinamicaDoble tabX;
    protected ListaDinamicaDoble tabY;
    protected ListaEstatica tabXEstatica;
    protected ListaEstatica tabYEstatica;


    /**
     * Recibe como parametros la ruta de las tablas
     * @param tablaX archivo .txt de la tabla X
     * @param tablaY archivo .txt de la tabla Y
     */
    public RegresionLineal(String tablaX, String tablaY){

        tabX = new ListaDinamicaDoble();
        tabY = new ListaDinamicaDoble();
        tabXEstatica = ArchivoTexto.leer(tablaX); //variable tipo ListaEstatica donde se guardara el contenido de la tabla1
        tabYEstatica = ArchivoTexto.leer(tablaY);
        tabX.agregarLista(tabXEstatica);
        tabY.agregarLista(tabYEstatica);

    }
    /**
     * Promediar los valores de la Tabla X
     * @return promedio tabla X
     */
    public double promedioX(){
        double suma = 0;
        for (int cont=0; cont<tabX.contarCantidadNodos();cont++){
            double x = Double.parseDouble(tabX.obtener(cont).toString());
            suma += x;
        }
        return (suma)/tabX.contarCantidadNodos();
    }

    /**
     * Promediar los valores de la Tabla Y
     * @return promedio tabla Y
     */
    public double promedioY(){
        double suma = 0;
        for (int cont=0; cont<tabY.contarCantidadNodos();cont++){
            double y = Double.parseDouble(tabY.obtener(cont).toString());
            suma += y;
        }
        return (suma)/tabY.contarCantidadNodos();
    }

    /**
     * Sumatoria del numerador: ((xi)-promedioX)*(yi-promedioY))
     * @return sumatoria del numerador de la pendiente
     */
    public double sumatoriaNumerador(){
        double promedioX = promedioX();
        double promedioY = promedioY();
        double sumaPendiente = 0;
        for(int cont=0; cont<tabY.contarCantidadNodos(); cont++) {//recorre todas las posiciones para hacer la sumatora
            double x = Double.parseDouble(tabX.obtener(cont).toString()); //obtenemos el valor de x para cada posicion
            double y = Double.parseDouble(tabY.obtener(cont).toString()); //obtenemos el valor de y para cada posicion
            sumaPendiente += (x-promedioX)*(y-promedioY); //aplicamos la formula para la sumatora
        }
        return  sumaPendiente;
    }

    /**
     * Sumatoria del denominador: (xi-promedioX)^2
     * @return sumatoria del denominador de la pendiente
     */
    public double sumatoriaDenominador(){
        double promedioX = promedioX();
        double sumatoriaAlCuadrado = 0;
        for(int cont=0; cont<tabY.contarCantidadNodos(); cont++) {//recorre todas las posiciones para hacer la sumatora
            double x = Double.parseDouble(tabX.obtener(cont).toString()); //obtenemos el valor de x para cada posicion
            sumatoriaAlCuadrado += (x-promedioX)*(x-promedioX); //aplicamos la formula para la sumatora
        }
        return sumatoriaAlCuadrado;
    }

    /**
     * Pendiente: ((xi)-promedioX)*(yi-promedioY))/(xi-promedioX)^2
     * @return reusltado de la pendiente
     */
    public double pendiente(){
        double pendiente = 0;
        pendiente = sumatoriaNumerador()/sumatoriaDenominador(); //se aplico la formula de la pendiente
        return pendiente;
    }

    /**
     * Ordenada al origen = promedioY-pendiente-promedioX
     * @return Ordenada al origen
     */
    public double ordenada(){
        double ordenada = 0;
        ordenada = promedioY() - pendiente() *  promedioX();
        return ordenada;
    }

    public void ecuacion(){
        SalidaPorDefecto.consola("y = "+ordenada()+ " + "+pendiente()+" * x");
    }

    /**
     * Metodo para sacar el aproximado
     * @return aproximacion
     */
    public double aproximacion(double valor){
        double aproximacion= 0;
        aproximacion = ordenada()+pendiente()*valor;
        return aproximacion;
    }
}
