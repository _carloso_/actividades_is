package prueba;

import entradaSalida.EntradaPorDefecto;
import entradaSalida.SalidaPorDefecto;
import herramientas.matematicas.ExpresionesAritmeticas;
public class PruebaCalculadora {
    public static void main(String[] args) {
        SalidaPorDefecto.consola("La operacion infija es: a+b*(c-d)/e^f \n");
        SalidaPorDefecto.consola("Ingrese un valor para la variable 'a': \n");
        String aCadena = EntradaPorDefecto.consolaCadenas();
        double a = ExpresionesAritmeticas.agregarValor(aCadena);
        SalidaPorDefecto.consola("Ingrese un valor para la variable 'b': \n");
        String bCadena = EntradaPorDefecto.consolaCadenas();
        double b = ExpresionesAritmeticas.agregarValor(bCadena);
        SalidaPorDefecto.consola("Ingrese un valor para la variable 'c': \n");
        String cCadena = EntradaPorDefecto.consolaCadenas();
        double c = ExpresionesAritmeticas.agregarValor(cCadena);
        SalidaPorDefecto.consola("Ingrese un valor para la variable 'd': \n");
        String dCadena = EntradaPorDefecto.consolaCadenas();
        double d = ExpresionesAritmeticas.agregarValor(dCadena);
        SalidaPorDefecto.consola("Ingrese un valor para la variable 'e': \n");
        String eCadena = EntradaPorDefecto.consolaCadenas();
        double e = ExpresionesAritmeticas.agregarValor(eCadena);
        SalidaPorDefecto.consola("Ingrese un valor para la variable 'f': \n");
        String fCadena = EntradaPorDefecto.consolaCadenas();
        double f = ExpresionesAritmeticas.agregarValor(fCadena);
        String operacion = ExpresionesAritmeticas.infijaAPostfija("a+b*(c-d)/e^f");
        SalidaPorDefecto.consola("El resultado de infija a postfija es: "+ operacion +"\n");
        SalidaPorDefecto.consola("El resultado de evaluación de la expresión postfija es: "+
               ExpresionesAritmeticas.evaluarPostfija("1234-*56^/+"));
    }
}
