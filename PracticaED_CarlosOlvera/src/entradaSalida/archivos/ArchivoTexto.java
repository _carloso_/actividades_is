package entradaSalida.archivos;

import estructurasLineales.*;
import java.io.*;

public class ArchivoTexto {

    public static ListaEstatica leer(String archivo){
        FileReader input=null;
        int registro=0;
        ListaEstatica datos=null;
        BufferedReader buffer = null;

        try {
            String cadena=null;
            input = new FileReader(archivo);
            buffer = new BufferedReader(input);
            datos=new ListaEstatica((int)buffer.lines().count());
            buffer.close();
            input.close();
            input = new FileReader(archivo);
            buffer = new BufferedReader(input);
            while((cadena = buffer.readLine())!=null) {
                datos.agregar(cadena);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try{
                input.close();
                buffer.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return datos;
    }
    //METODO DE LEER RECURSIVO PARA EL EXAMEN 2
    public static void leerRecursivo(BufferedReader buffer, ListaEstatica datos){
        String cadena = "";
        try{
            if ((cadena = buffer.readLine()) != null){ //caso base
                datos.agregar(cadena);
                leerRecursivo(buffer,datos);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static ListaEstatica leerRecursivo(String archivo){
        FileReader input=null;
        ListaEstatica datos=null;
        BufferedReader buffer = null;
        try {
            input = new FileReader(archivo);
            buffer = new BufferedReader(input);
            datos=new ListaEstatica((int)buffer.lines().count());
            buffer.close();
            input.close();
            input = new FileReader(archivo);
            buffer = new BufferedReader(input);
            leerRecursivo(buffer,datos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try{
                input.close();
                buffer.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return datos;
    }
    public static void escribir(ListaEstatica arreglo, String archivo){
        FileWriter output=null;
        try {
            output = new FileWriter(archivo);
            for(int posicion=0;posicion<arreglo.numeroElementos() -1 ;posicion++) {
                output.write((String)arreglo.obtener(posicion)+ "\n");
            }
            output.write((String)arreglo.obtener(arreglo.numeroElementos() - 1));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try{
                output.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}
