package prueba;

import entradaSalida.SalidaPorDefecto;
import registros.mensajes.Encriptar;

public class PruebaEncriptar {
    public static void main(String[] args) {
        //Encriptar mensaje = new Encriptar("src/carlosOlvera.txt");
        SalidaPorDefecto.consola("Mensaje Codificado\n");
        Encriptar.codificar("src/carlosOlvera.txt");
        SalidaPorDefecto.consola(Encriptar.imprimirMensajeCodificado()+"\n");
        SalidaPorDefecto.consola("Mensaje Decodificado\n");
        Encriptar.imprimirMensajeDecodificado("src/carlosOlvera.txt");
        Encriptar.hipervinculos("src/pagina1.txt","src/pagina2.txt","src/pagina3.txt","src/pagina4.txt","src/pagina5.txt","src/pagina6.txt","src/pagina7.txt","src/pagina8.txt","src/pagina9.txt","src/pagina10.txt","src/pagina11.txt","src/pagina12.txt",
                "src/pagina13.txt","src/pagina14.txt","src/pagina15.txt","src/pagina16.txt","src/pagina17.txt","src/pagina18.txt","src/pagina19.txt","src/pagina20.txt");
        SalidaPorDefecto.consola("-----HISTORIAL DE BUSQUEDA-----\n");
        Encriptar.historial();
    }
}
