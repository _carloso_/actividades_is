package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaEstaticaNumerica;
import estructurasnolineales.Matriz2Numerica;
import herramientas.comunes.TipoLogaritmo;

public class PruebaMatriz2Numerica {
    public static void main(String[] args) {
        Matriz2Numerica matriz2Numerica1 = new Matriz2Numerica(2, 2);
        matriz2Numerica1.cambiar(0, 0, 5);
        matriz2Numerica1.cambiar(0, 1, 2);
        matriz2Numerica1.cambiar(1, 0, 3);
        matriz2Numerica1.cambiar(1, 1, 1);

        SalidaPorDefecto.consola("---- Matriz numerica 1 ----" + "\n");
        matriz2Numerica1.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->Matriz numerica 1 por escalar 3: " + matriz2Numerica1.porEscalar(3)+ "\n");
        matriz2Numerica1.imprimirXReglones();

        ListaEstaticaNumerica escalares = new ListaEstaticaNumerica(4);
        escalares.agregar(2);
        escalares.agregar(1);
        escalares.agregar(3);
        escalares.agregar(5);
        SalidaPorDefecto.consola("---- Lista de escalares ----" + "\n");
        escalares.imprimir();

        SalidaPorDefecto.consola("->Multiplicar Matriz numerica 1 por la lista de escalares: "
                + matriz2Numerica1.porEscalares(escalares)+ "\n");
        matriz2Numerica1.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->Sumar escalar 2 a la matriz 1: "
                + matriz2Numerica1.sumarEscalar(2)+ "\n");
        matriz2Numerica1.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("---- Matriz numerica 2 ----" + "\n");
        Matriz2Numerica matriz2Numerica2 = new Matriz2Numerica(2, 2);
        matriz2Numerica2.cambiar(0, 0, 3);
        matriz2Numerica2.cambiar(0, 1, 5);
        matriz2Numerica2.cambiar(1, 0, 4);
        matriz2Numerica2.cambiar(1, 1, 6);
        matriz2Numerica2.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->Suma de Matriz numerica 2 con la lista de escalares: " +
                matriz2Numerica2.sumarEscalares(escalares)+ "\n");
        matriz2Numerica2.imprimirXReglones();

        Matriz2Numerica matriz2Numerica3 = new Matriz2Numerica(2, 2);
        matriz2Numerica3.cambiar(0, 0, 4);
        matriz2Numerica3.cambiar(0, 1, 3);
        matriz2Numerica3.cambiar(1, 0, 2);
        matriz2Numerica3.cambiar(1, 1, 5);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("---- Matriz numerica 3 ----" + "\n");
        matriz2Numerica3.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        Matriz2Numerica matriz2Numerica4 = new Matriz2Numerica(2, 2);
        matriz2Numerica4.cambiar(0, 0, 2);
        matriz2Numerica4.cambiar(0, 1, 5);
        matriz2Numerica4.cambiar(1, 0, 3);
        matriz2Numerica4.cambiar(1, 1, 1);

        SalidaPorDefecto.consola("---- Matriz numerica 4 ----" + "\n");
        matriz2Numerica4.imprimirXReglones();


        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->Multiplicar la Matriz 3 por la matriz 4: " +
                        matriz2Numerica3.multiplicar(matriz2Numerica4)+ "\n");
        matriz2Numerica3.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->Sumar la Matriz 4 más la Matriz 3: " +
                        matriz2Numerica4.sumar(matriz2Numerica3)+ "\n");
        matriz2Numerica4.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->Aplicar potencia (2) a la Matriz 4: " +
                matriz2Numerica4.aplicarPotencia(2)+ "\n");
        matriz2Numerica4.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        Matriz2Numerica matriz2Numerica5 = new Matriz2Numerica(2, 2);
        matriz2Numerica5.cambiar(0, 0, 2);
        matriz2Numerica5.cambiar(0, 1, 5);
        matriz2Numerica5.cambiar(1, 0, 6);
        matriz2Numerica5.cambiar(1, 1, 3);

        SalidaPorDefecto.consola("---- Matriz numerica 5 ----" + "\n");
        matriz2Numerica5.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->Aplicar logaritmo Natural a la Matriz 5: " +
                matriz2Numerica5.aplicarLogaritmo(TipoLogaritmo.NATURAL) + "\n");
        matriz2Numerica5.imprimirXReglones();

        Matriz2Numerica matriz2Numerica6 = new Matriz2Numerica(2, 2);
        matriz2Numerica6.cambiar(0, 0, 3);
        matriz2Numerica6.cambiar(0, 1, 2);
        matriz2Numerica6.cambiar(1, 0, 4);
        matriz2Numerica6.cambiar(1, 1, 7);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("---- Matriz numerica 6 ----" + "\n");
        matriz2Numerica6.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->Aplicar logaritmo Base 10 a la Matriz 6: " +
                matriz2Numerica6.aplicarLogaritmo(TipoLogaritmo.BASE10) + "\n");
        matriz2Numerica6.imprimirXReglones();

        Matriz2Numerica matriz2Numerica7 = new Matriz2Numerica(2, 2);
        matriz2Numerica7.cambiar(0, 0, 9);
        matriz2Numerica7.cambiar(0, 1, 7);
        matriz2Numerica7.cambiar(1, 0, 5);
        matriz2Numerica7.cambiar(1, 1, 3);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("---- Matriz numerica 7 ----" + "\n");
        matriz2Numerica7.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->Aplicar logaritmo Base 2 a la Matriz 7: " +
                matriz2Numerica7.aplicarLogaritmo(TipoLogaritmo.BASE2) + "\n");
        matriz2Numerica7.imprimirXReglones();

        Matriz2Numerica matrizDiagonal = new Matriz2Numerica(3, 3);
        matrizDiagonal.matrizDiagonal(7);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("---- Matriz diagonal con el valor 7 ----" + "\n");
        matrizDiagonal.imprimirXReglones();

        Matriz2Numerica matriz2Numerica8 = new Matriz2Numerica(3, 3);
        matriz2Numerica8.cambiar(0, 0, 1);
        matriz2Numerica8.cambiar(0, 1, 2);
        matriz2Numerica8.cambiar(0, 2, 3);
        matriz2Numerica8.cambiar(1, 0, 0);
        matriz2Numerica8.cambiar(1, 1, 1);
        matriz2Numerica8.cambiar(1, 2, 3);
        matriz2Numerica8.cambiar(2, 0, 0);
        matriz2Numerica8.cambiar(2, 1, 0);
        matriz2Numerica8.cambiar(2, 2, 1);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("---- Matriz numerica 8 ----" + "\n");
        matriz2Numerica8.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->¿La matriz numerica 8 es triangular superior? " +
                matriz2Numerica8.esDiagonalSuperior() + "\n");

        SalidaPorDefecto.consola("->¿La matriz numerica 8 es triangular inferior? " +
                matriz2Numerica8.esDiagonalInferior() + "\n");

        Matriz2Numerica matriz2Numerica9 = new Matriz2Numerica(3, 3);
        matriz2Numerica9.cambiar(0, 0, 8);
        matriz2Numerica9.cambiar(0, 1, 0);
        matriz2Numerica9.cambiar(0, 2, 0);
        matriz2Numerica9.cambiar(1, 0, 4);
        matriz2Numerica9.cambiar(1, 1, 2);
        matriz2Numerica9.cambiar(1, 2, 0);
        matriz2Numerica9.cambiar(2, 0, 6);
        matriz2Numerica9.cambiar(2, 1, 5);
        matriz2Numerica9.cambiar(2, 2, 7);

        SalidaPorDefecto.consola("\n---- Matriz numerica 9 ----" + "\n");
        matriz2Numerica9.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->¿La matriz numerica 9 es triangular superior? " +
                matriz2Numerica9.esDiagonalSuperior() + "\n");

        SalidaPorDefecto.consola("->¿La matriz numerica 9 es triangular inferior? " +
                matriz2Numerica9.esDiagonalInferior() + "\n");

        Matriz2Numerica matriz2Numerica10 = new Matriz2Numerica(2, 2);
        matriz2Numerica10.cambiar(0, 0, 2);
        matriz2Numerica10.cambiar(0, 1, 3);
        matriz2Numerica10.cambiar(1, 0, -1);
        matriz2Numerica10.cambiar(1, 1, 0);

        SalidaPorDefecto.consola("\n---- Matriz numerica 10 ----" + "\n");
        matriz2Numerica10.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("->Elevar al 5 la matriz 10: " +
                matriz2Numerica10.potencia(5) + "\n");
        matriz2Numerica10.imprimirXReglones();

        Matriz2Numerica matriz2Numerica11 = new Matriz2Numerica(4, 4);
        matriz2Numerica11.cambiar(0, 0, 4);
        matriz2Numerica11.cambiar(0, 1, 6);
        matriz2Numerica11.cambiar(0, 2, 4);
        matriz2Numerica11.cambiar(0, 3, 3);
        matriz2Numerica11.cambiar(1, 0, 1);
        matriz2Numerica11.cambiar(1, 1, 2);
        matriz2Numerica11.cambiar(1, 2, 2);
        matriz2Numerica11.cambiar(1, 3, 4);
        matriz2Numerica11.cambiar(2, 0, 5);
        matriz2Numerica11.cambiar(2, 1, 3);
        matriz2Numerica11.cambiar(2, 2, 2);
        matriz2Numerica11.cambiar(2, 3, 1);
        matriz2Numerica11.cambiar(3, 0, 0);
        matriz2Numerica11.cambiar(3, 1, 8);
        matriz2Numerica11.cambiar(3, 2, 7);
        matriz2Numerica11.cambiar(3, 3, 3);

        SalidaPorDefecto.consola("\n---- Matriz numerica 11 ----" + "\n");
        matriz2Numerica11.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("---- Matriz numerica 11 doblada por renglones ----" + "\n");
        matriz2Numerica11.doblarRenglones();
        matriz2Numerica11.imprimirXReglones();

        Matriz2Numerica matriz2Numerica12 = new Matriz2Numerica(4, 4);
        matriz2Numerica12.cambiar(0, 0, 4);
        matriz2Numerica12.cambiar(0, 1, 6);
        matriz2Numerica12.cambiar(0, 2, 4);
        matriz2Numerica12.cambiar(0, 3, 3);
        matriz2Numerica12.cambiar(1, 0, 1);
        matriz2Numerica12.cambiar(1, 1, 2);
        matriz2Numerica12.cambiar(1, 2, 2);
        matriz2Numerica12.cambiar(1, 3, 4);
        matriz2Numerica12.cambiar(2, 0, 5);
        matriz2Numerica12.cambiar(2, 1, 3);
        matriz2Numerica12.cambiar(2, 2, 2);
        matriz2Numerica12.cambiar(2, 3, 1);
        matriz2Numerica12.cambiar(3, 0, 0);
        matriz2Numerica12.cambiar(3, 1, 8);
        matriz2Numerica12.cambiar(3, 2, 7);
        matriz2Numerica12.cambiar(3, 3, 3);

        SalidaPorDefecto.consola("\n---- Matriz numerica 12 ----" + "\n");
        matriz2Numerica12.imprimirXReglones();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("---- Matriz numerica 12 doblada por Columnas ----" + "\n");
        matriz2Numerica12.doblarColumnas();
        matriz2Numerica12.imprimirXReglones();
    }
}
