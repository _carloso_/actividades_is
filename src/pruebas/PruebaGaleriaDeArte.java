package pruebas;

import actividadesparciales.*;
import entradaSalida.SalidaPorDefecto;

public class PruebaGaleriaDeArte {
    public static void main(String[] args) {
        ControlGaleriaDeArte galeria = new ControlGaleriaDeArte(8,5, 10);

        // Fechas de Naciminto de los pintores
        Fecha fechaNacimientoPintor1 = new Fecha(5, 2, 1988);
        Fecha fechaNacimientoPintor2 = new Fecha(20, 8, 1990);
        Fecha fechaNacimientoPintor3 = new Fecha(12, 10, 1994);
        Fecha fechaNacimientoPintor4 = new Fecha(2, 9, 2000);

        // Fechas de las actividades de la galeria.
        Fecha fecha1 = new Fecha(18, 4, 2019);
        Fecha fecha2 = new Fecha(7, 1, 2021);
        Fecha fecha3 = new Fecha(10, 5, 2022);
        Fecha fecha4 = new Fecha(1, 1, 2022);
        Fecha fecha5 = new Fecha(4, 5, 2022);
        Fecha fecha6 = new Fecha(18, 4, 2019);

        // Pintores
        Pintor pintor1 = new Pintor(1, "Juan", "Pérez", 34, fechaNacimientoPintor1,
                "Zacatecas, zac", "HGHFTYSD6876S", "Licenciatura", "4925758745");

        Pintor pintor2 = new Pintor(2, "Luis", "López", 31, fechaNacimientoPintor2,
                "CDMX", "LUIDYS878323", "Licenciatura", "8674567346");

        Pintor pintor3 = new Pintor(3, "Ana", "Torres", 27, fechaNacimientoPintor3,
                "Monterrey", "ANTODFOIUF78", "Licenciatura", "5483929654");

        Pintor pintor4 = new Pintor(4, "María", "Alvarado", 22, fechaNacimientoPintor4,
                        "Guadalajara", "MARIASD876S", "Licenciatura", "8764567346");

        // Actividades
        ActividadGaleria actividad1 = new ActividadGaleria(TipoActividad.PINTAR, "Obra Clasica", "Sala 1",
                "Pintar una obra clasica");

        ActividadGaleria actividad2 = new ActividadGaleria(TipoActividad.EXPONER, "Yves Klein", "CDMX",
                120, "Exposicion de arte clasica");

        ActividadGaleria actividad3 = new ActividadGaleria(TipoActividad.VIAJAR, "A Italia", "Italia",
                "Viajar a Italia");

        ActividadGaleria actividad4 = new ActividadGaleria(TipoActividad.FIRMA_AUTOGRAFOS, "unidos por el arte",
                "Guadalajara", 200, "Firmar de autografos para el arte");

        ActividadGaleria actividad5 = new ActividadGaleria(TipoActividad.DESCANSAR, "Tiempo Libre");

        ActividadGaleria actividad6 = new ActividadGaleria(TipoActividad.EXPONER, "Arte Moderno", "Oaxaca",
                        600, "Exposicion de arte moderno");

        ActividadGaleria actividad7 = new ActividadGaleria(TipoActividad.DESCANSAR, "vacaciones");
        ActividadGaleria actividad8 = new ActividadGaleria(TipoActividad.DESCANSAR, "por covid-19");

        // Agregar actividades a la galeria
        galeria.agregarActividad(actividad1);
        galeria.agregarActividad(actividad2);
        galeria.agregarActividad(actividad3);
        galeria.agregarActividad(actividad4);
        galeria.agregarActividad(actividad5);
        galeria.agregarActividad(actividad6);
        galeria.agregarActividad(actividad7);
        galeria.agregarActividad(actividad8);

        // Agregar pintores a la galeria
        galeria.agregarPintor(pintor1);
        galeria.agregarPintor(pintor2);
        galeria.agregarPintor(pintor3);
        galeria.agregarPintor(pintor4);

        // agregar fechas a la galeria
        galeria.agregarFecha(fecha1);
        galeria.agregarFecha(fecha2);
        galeria.agregarFecha(fecha3);
        galeria.agregarFecha(fecha4);
        galeria.agregarFecha(fecha5);
        galeria.agregarFecha(fecha6);

        // Imprimir datos de la galeria
        galeria.imprimirDatosDeGaleria();

        // asignar actividades a los pintores
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Asignar actividades a los pintores ---" + "\n");
        galeria.asignarActividadAPintor(1, fecha1);
        galeria.asignarActividadAPintor(3, fecha2);
        galeria.asignarActividadAPintor(1, fecha3);
        galeria.asignarActividadAPintor(2, fecha3);
        galeria.asignarActividadAPintor(1, fecha4);
        galeria.asignarActividadAPintor(4, fecha5);
        galeria.asignarActividadAPintor(2, fecha6);
        galeria.asignarActividadAPintor(2, fecha4);

        // Imprimir datos de la galeria
        SalidaPorDefecto.consola("\n");
        galeria.imprimirActividadesDesarrolladas();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Pintor que desarrollo mas actividades ---" + "\n");
        galeria.imprimirPintorQueDesarrolloMasActividades();

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista de actividades del pintor 1 ---" + "\n");
        galeria.listaActividadesDesarrolladasPorUnPintor(pintor1);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Nombre y Edad que Desarrollo la Exposicion de arte moderno ---" + "\n");
        galeria.pintorQueDesarrolloUnaActividad(actividad6, fecha5);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Exposiciones que desarrollo el pintor 3 ---" + "\n");
        galeria.cantidadDeExpociocionesDeUnPintor(pintor3);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Cantidad de Aforo de una exposicion ---" + "\n");
        galeria.cantidadDeAforoDeUnaExpocicion(actividad6, fecha5);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista de actividades del año 2022 ---" + "\n");
        galeria.imprimirActividaesDesarrolldasEnUnAnio(2022);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Lista de actividades Desarrolladas por el pintor1 en el año 2022 ---" + "\n");
        galeria.imprimirActividaesDesarrolldasEnUnAnioPorPintor(2022, pintor1);

        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("--- Pintor que mas descanso ---" + "\n");
        galeria.pintorQueMasDescansa();

    }
}
