package entradaSalida.archivos;

import estructurasLineales.ListaEstatica;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class AccederArchivo {
    static ListaEstatica direccion;
    static ListaEstatica datos;
    public static void leerArchivo(String rutaArchivo) throws IOException {
        boolean finArchivo = false;
        RandomAccessFile archivo = null;

        try {
            archivo = new RandomAccessFile(rutaArchivo, "r");
            direccion = new ListaEstatica((int) archivo.length());
            datos = new ListaEstatica((int) archivo.length());
            System.out.println("El tamaño es: " + archivo.length());
            String cad="";
            while(true) {
                System.out.print("Pos: "+archivo.getFilePointer());
                direccion.agregar(archivo.getFilePointer());
                cad = archivo.readLine();
                if (cad==null)
                    break;
                System.out.println(" - " +cad);
                datos.agregar(cad);
            }
        } catch (FileNotFoundException fe) {
            System.out.println("No se encontro el archivo");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("\n");
        archivo.seek(9071);
        System.out.println(archivo.readLine());
        System.out.println("\n");
        archivo.close();
    }
    public static ListaEstatica getDatos(){
        return datos;
    }
    public static ListaEstatica getDireccion(){
        return direccion;
    }
}
