package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasnolineales.ArbolExpAritm;

public class PruebaAEA {
    public static void main(String[] args) {
        ArbolExpAritm arbol = new ArbolExpAritm();
        /*
        arbol.generarArbolPostfija("x4+568/*-");

        SalidaPorDefecto.consola("Imprimiendo el árbol: \n");
        SalidaPorDefecto.consola("Inorden: ");
        arbol.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: ");
        arbol.postOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: ");
        arbol.preOrden();
        SalidaPorDefecto.consola("\n");

         */

        arbol.generarArbol();

        SalidaPorDefecto.consola("\nImprimiendo el árbol: \n");
        SalidaPorDefecto.consola("Inorden: ");
        arbol.inOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Postorden: ");
        arbol.postOrden();
        SalidaPorDefecto.consola("\n");
        SalidaPorDefecto.consola("Preorden: ");
        arbol.preOrden();

        SalidaPorDefecto.consola("\n");
        arbol.mostrarListasDinamicas();

    }
}
