package registros.escuela;

public class Aplicaciones {
    protected String nombre;
    protected String version;
    protected int ramMinima;
    protected String peso;
    protected String autor;
    public Aplicaciones(String nombre, String version, String autor, int ramMinima, String peso){
        this.nombre = nombre;
        this.version = version;
        this. ramMinima = ramMinima;
        this.peso = peso;
        this.autor = autor;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPeso() {
        return peso;
    }

    public int getRamMinima() {
        return ramMinima;
    }

    public String getVersion() {
        return version;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public void setRamMinima(int ramMinima) {
        this.ramMinima = ramMinima;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String datos() {
        return "Nombre: " + nombre + ", Version: " + version + ", Autor: "+ autor + ", RAM minima: " + ramMinima + "GB" +", Peso: " + peso;
    }
    @Override
    public String toString() {
        return nombre;
    }
}
