package pruebas;

import entradaSalida.SalidaPorDefecto;
import estructurasLineales.ListaDinamica;
import estructurasnolineales.GrafoDinamico;

public class PruebaGrafoDinamico {
    public static void main(String[] args) {
        GrafoDinamico grafo = new GrafoDinamico(4);

        // agregar vertices
        grafo.agregarVertice("A");
        grafo.agregarVertice("B");
        grafo.agregarVertice("C");
        grafo.agregarVertice("D");

        // agregar aristas
        grafo.agregarArista("A", "B",2);
        grafo.agregarArista("B", "D",1);
        grafo.agregarArista("C", "A",5);
        grafo.agregarArista("C", "B",2);
        grafo.agregarArista("D", "A",3);
        grafo.agregarArista("D", "C",1);

        // imprimir
        SalidaPorDefecto.consola("Grafo: \n");
        grafo.imprimir();

        // imprimir matriz de adyacencia
        SalidaPorDefecto.consola("\nMatriz de adyacencia (Pesos): \n");
        grafo.imprimirMatrizAdyacencia();

        // recorrido en profundidad
        SalidaPorDefecto.consola("\nRecorrido en profundidad a partir de A: \n");
        ListaDinamica recorridoP = grafo.recorridoProfundidad("A");
        recorridoP.imprimir();

        // costo arbol recubridor minimo Algoritmo de Kruskal
        SalidaPorDefecto.consola("\n\nCosto arbol recubridor minimo: " + grafo.algoritmoKruskal());
    }
}