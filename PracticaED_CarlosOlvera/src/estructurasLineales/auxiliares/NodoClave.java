package estructurasLineales.auxiliares;

public class NodoClave {
    protected Object contenido;
    protected NodoClave nodoDer;
    protected Object clave;

    public NodoClave(Object clave,Object valor){
        this.contenido=valor;
        nodoDer=null;
        this.clave=clave;
    }

    public Object getContenido() {
        return contenido;
    }

    public NodoClave getNodoDer() {
        return nodoDer;
    }

    public Object getClave() {
        return clave;
    }


    public void setContenido(Object contenido) {
        this.contenido = contenido;
    }

    public void setNodoDer(NodoClave nodoDer) {
        this.nodoDer = nodoDer;
    }

    public void setClave(Object clave) {
        this.clave= clave;
    }

    @Override
    public String toString(){
        return clave.toString() + ", " + contenido.toString();
    }
}
