package registros.arroz;

import estructurasLineales.ListaEstatica;

public class Granja {
    protected String nombre;
    protected ListaEstatica campesinos;

    public Granja(String nombre, int cantidadCampesinos) {
        this.nombre = nombre;
        campesinos=new ListaEstatica(cantidadCampesinos);
    }

    public boolean agregarCampesino(Campesino campesino){
        int valorRetornoAgregado=campesinos.agregar(campesino);
        if(valorRetornoAgregado==-1){ //no se pudo agregar el campesino
            return false;
        }else{ //si se pudo agregar
            return true;
        }
    }

    public void imprimirListadoCampesinos(){
        campesinos.imprimirOI();
    }

    public String getNombre() {
        return nombre;
    }

    public Double calcularPromedioAnualCampesino(String noEmpleado){
        //Buscar al campesino con ese numero de empleado
        Integer posicionBusqueda=(Integer)campesinos.buscar(noEmpleado);

        if(posicionBusqueda!=null){ //si esta el campesino
            Campesino campesinoBusqueda=(Campesino)campesinos.obtener(posicionBusqueda);
            return campesinoBusqueda.obtenerProduccion(); //regreso el promedio o un nulo
        }
        else { //no esta
            return null;
        }
    }
}
